#include "wordexp.h"

char* tilde_exp(struct shell_env* cur_env, const char* words);
char* parameter_exp(struct shell_env* cur_env, const char* words);
char* command_substitution_exp(struct shell_env* cur_env, const char* words);
char* arithmetic_exp(struct shell_env* cur_env, const char* words);
bool char_is_in_IFS(char c, const char* IFS);
struct dyn_arr_str* field_split(struct shell_env* cur_env, const char* words);
void pathname_exp(struct dyn_arr_str* fields);
void quote_removal(struct dyn_arr_str* fields);
char* quote_removal_str(const char* in);


union wordexp_ret
expand(struct shell_env* cur_env, const char* restrict in, uint8_t flags) {
   if (flags == 0) {
      return (union wordexp_ret) { NULL };
   }

   char* exp = psh_strdup(in);
   if (flags & EXP_TILDE) {
      char* tmp = tilde_exp(cur_env, exp);
      free(exp);
      exp = tmp;
   }
   if (flags & EXP_PARAM) {
      char* tmp = parameter_exp(cur_env, exp);
      free(exp);
      exp = tmp;
   }
   if (flags & EXP_CMD_SUB) {
      char* tmp = command_substitution_exp(cur_env, exp);
      free(exp);
      exp = tmp;
   }
   if (flags & EXP_ARITH) {
      char* tmp = arithmetic_exp(cur_env, exp);
      free(exp);
      exp = tmp;
   }

   if (! (flags & (EXP_FIELD_SPLIT | EXP_PATH | EXP_QUOTE)) ) {
      return (union wordexp_ret) { .charp = exp };
   }

   if ((flags & EXP_QUOTE) && !(flags & (EXP_FIELD_SPLIT | EXP_PATH))) {
      union wordexp_ret ret = (union wordexp_ret) { .charp = quote_removal_str(exp) };
      free(exp);
      return ret;
   }

   struct dyn_arr_str* exp_fields = field_split(cur_env, exp);
   free(exp);

   if (flags & EXP_PATH) {
      pathname_exp(exp_fields);
   }

   if (flags & EXP_QUOTE) {
      quote_removal(exp_fields);
   }

   return (union wordexp_ret) { .arrstr = exp_fields };
}

int
wordexp(struct shell_env* cur_env, const char* words, struct wordexp_t* /*restrict*/ pwordexp, int flags) {
   PSH_LOG(LOG_INFO, "wordexp: words: '%s'\n", words);
   assert(words);
   assert(pwordexp);

   const char* tilde_str = tilde_exp(cur_env, words);
   const char* param_str = parameter_exp(cur_env, tilde_str);
   const char* command_sub_str = command_substitution_exp(cur_env, param_str);
   const char* arithmetic_str = arithmetic_exp(cur_env, command_sub_str);

   struct dyn_arr_str* fields = field_split(cur_env, arithmetic_str);

   if (cur_env->flag_noglob)
      pathname_exp(fields);

   quote_removal(fields);

   //append to the given, allocated pwordexp

   size_t nb_prev_elems = pwordexp->we_wordc;
   size_t nb_new_elems = fields->size;
   size_t nb_total_elems = nb_prev_elems + nb_new_elems;

   while (nb_total_elems > pwordexp->we_offs) {
      size_t new_offs = pwordexp->we_offs * 2 + 1;
      pwordexp->we_wordv = realloc(pwordexp->we_wordv, sizeof(char*) * new_offs);
      assert(pwordexp->we_wordv);
      pwordexp->we_offs = new_offs;
   }

   pwordexp->we_wordc = nb_total_elems;

   for (size_t i = 0; i < nb_new_elems; i++) {
      pwordexp->we_wordv[i + nb_prev_elems] = psh_strdup(fields->strs[i]);
      PSH_LOG(LOG_INFO, "wordexp: out: '%lu': %s\n", i, fields->strs[i]);
   }


   free((char*)tilde_str);
   free((char*)param_str);
   free((char*)command_sub_str);
   free((char*)arithmetic_str);
   dyn_arr_str_free(fields, true);
   free(fields);

   return 0;
}

//TODO Not fully implemented, needs to work after assignments and after colons (for 'PATH=~:~pierre:...')
char*
tilde_exp(struct shell_env* cur_env, const char* words) {
   if (words[0] != '~') {
      return psh_strdup(words);
   }
   words++;

   struct psh_string* ret_str = psh_string_create(NULL);
   struct psh_string* username = psh_string_create(NULL);

   while (*words && *words != '/') {
      pstr_append(username, *words);
      words++;
   }

   if (username->str_len == 0) {
      struct sym_var_entry* HOME_entry = sym_get_var_entry(cur_env->var_table, "HOME");
      if (HOME_entry) {
         //quoted so that it's not altered by anything
         pstr_append(ret_str, '\'');
         psh_string_append_str(ret_str, (HOME_entry->value) ? HOME_entry->value : "");
         pstr_append(ret_str, '\'');
      } else {
         //if unset, unspecified behaviour, I chopose not to print anything
      }
   } else {
      struct passwd* pwnam = getpwnam(username->str);
      if (pwnam) {
         pstr_append(ret_str, '\'');
         pstr_append(ret_str, pwnam->pw_dir);
         pstr_append(ret_str, '\'');
      } else {
         //unspecified bahaviour, so do nothing
      }
   }

   while (*words) {
      pstr_append(ret_str, *words);
      words++;
   }

   return psh_string_to_cstr(username, true);
}


static const char* str_ignore_single_quote(const char* cur_c, struct psh_string* pstr);
static const char* str_ignore_double_quote(const char* cur_c, struct psh_string* pstr);

//Unfortunately can't reuse code from psh_tokenise_expansion
//If it has a pstr, it will append the characters to it
static const
char*
str_ignore_expansion(const char* cur_c, struct psh_string* pstr) {
   if (*cur_c == '$') {
      if (pstr) pstr_append(pstr, '$');
      cur_c++;
      if (*cur_c == '{') {
         //TODO ask for more?
         if (pstr) pstr_append(pstr, '{');
         do {
            switch (*++cur_c) {
               case '\0': return NULL;
               case '}': {
                  if (pstr) pstr_append(pstr, '}');
                  return ++cur_c;
               };
               case '\'': { if (!(cur_c = str_ignore_single_quote(cur_c, pstr))) return NULL; } break;
               case '"':  { if (!(cur_c = str_ignore_double_quote(cur_c, pstr))) return NULL; } break;
               case '`': case '$': { if (!(cur_c = str_ignore_expansion(cur_c, pstr))) return NULL; } break;
            }
            if (pstr) pstr_append(pstr, *cur_c);

         } while(1);

      } else if (*cur_c == '(' && *(cur_c + 1) == '(') {
         //TODO parse also $((
         assert(0);
      } else if (*cur_c == '(') {
         if (pstr) pstr_append(pstr, '(');
         uint64_t nb_parens = 1;
         do {
            switch (*++cur_c) {
               case '\0': return NULL;
               case '(': { nb_parens++; } break;
               case ')': {
                  if (--nb_parens == 0) {
                     if (pstr) pstr_append(pstr, ')');
                     return ++cur_c;
                  }
               } break;
               case '\'': { if (!(cur_c = str_ignore_single_quote(cur_c, pstr))) return NULL; } break;
               case '\"': { if (!(cur_c = str_ignore_double_quote(cur_c, pstr))) return NULL; } break;
               case '$': case '`': { if (!(cur_c = str_ignore_expansion(cur_c, pstr))) return NULL; } break;
               case '\\': {
                  //TODO
                  assert(0);
               }
            }
            if (pstr) pstr_append(pstr, *cur_c);

         } while(1);
      } else {
         // It's a normal $, tokenise the rest of the word outside the function
         return cur_c;
      }
   } else if (*cur_c == '`') {
      while (1) {
         cur_c++;
         if (*cur_c == '\0') {
            return NULL;
         } else if (*cur_c == '\'') {
            cur_c = str_ignore_single_quote(cur_c, pstr);
            if (!cur_c)
               return NULL;
            continue;
         } else if (*cur_c == '\"') {
            cur_c = str_ignore_double_quote(cur_c, pstr);
            if (!cur_c)
               return NULL;
            continue;
         } else if (*cur_c == '\\') {
            //TODO
            assert(0);
         } else if (*cur_c == '$') {
            cur_c = str_ignore_expansion(cur_c, pstr);
            if (!cur_c)
               return NULL;
            continue;
         } else if (*cur_c == '`') {
            return ++cur_c;
         }
      }
   }
   else
      return NULL;

   //TODO If it's a '$((', weird shit happens. Not sure what to do, probably count the parens
}


//returns NULL on error
static const
char*
str_ignore_single_quote(const char* cur_c, struct psh_string* pstr) {
   DEBUG_ASSERT(*cur_c == '\'');
      
   if (pstr) pstr_append(pstr, '\'');

   do {
      cur_c++;
      if (pstr) pstr_append(pstr, *cur_c);
   } while(*cur_c && *cur_c != '\'');

   if (*cur_c == '\'') {
      if (pstr) pstr_append(pstr, '\'');
      return ++cur_c;
   }
   else return NULL;
}

//returns NULL on error
static const
char*
str_ignore_double_quote(const char* cur_c, struct psh_string* pstr) {
   DEBUG_ASSERT(*cur_c == '"');
   if (pstr) pstr_append(pstr, '"');

   do {
      cur_c++; //ignore first d-quote
      switch (*cur_c) {
         case '\0': return NULL;
         case '"': { 
            if (pstr) pstr_append(pstr, '"');
            return ++cur_c;
         }
         case '$': case '`': { if (!(cur_c = str_ignore_expansion(cur_c, pstr))) return NULL; } break;
         case '\\': {
            cur_c++;
            if (*cur_c == '\0') return NULL;
            else if (*cur_c == '\n') continue; //TODO not sure about this one
            else if (*cur_c == '$' || *cur_c == '`' || *cur_c == '"' || *cur_c == '\\') continue;
            else cur_c++;
         } break;
      }

      if (pstr) pstr_append(pstr, *cur_c);
   } while(1);
}

enum param_exp_type {
   EXP_NORMAL = 0,
   EXP_COLON_MINUS,
   EXP_MINUS,
   EXP_COLON_EQUAL,
   EXP_EQUAL,
   EXP_COLON_QUESTIONMARK,
   EXP_QUESTIONMARK,
   EXP_COLON_PLUS,
   EXP_PLUS,
   EXP_LENGTH,
   EXP_PERCENT,
   EXP_PERCENT_PERCENT,
   EXP_POUND,
   EXP_POUND_POUND,
};

/*
   * this is for $name, $* or ${name}
*/
char*
parameter_exp(struct shell_env* cur_env, const char* words) {
   struct psh_string* ret_str = psh_string_create(NULL);
   const char *cur_c = words;
   struct psh_string* param_str = NULL;
   struct psh_string* param_val = NULL;
   struct psh_string* word_str = NULL;

   /* fprintf(stderr, "input to param_exp: '%s'\n", words); */

   while (1) {
      if (!*cur_c) break;

      enum param_exp_type exp_type = EXP_NORMAL;
      param_str = NULL;
      word_str = NULL;
      //identify places that have unquoted $<name> or $<special> or ${}
      if (*cur_c == '$' && *(cur_c + 1) == '{') {
         cur_c += 2;
         param_str = psh_string_create(NULL);

         //parse longest word
         if (*cur_c == '@' || (*cur_c == '#' && *(cur_c+1) == '}') ||  *cur_c == '*' || *cur_c == '?' || *cur_c == '-' || *cur_c == '$' || *cur_c == '!') {
            pstr_append(param_str, *cur_c);
            cur_c++;
         } else if (*cur_c == '#') {
            //don't do anything
         } else if (isdigit(*cur_c)) {
            do pstr_append(param_str, *cur_c++);
            while (isdigit(*cur_c));
         } else {
            while (isalnum(*cur_c) || *cur_c == '_' || *cur_c == '\'' || *cur_c == '"' || *cur_c == '\\') {
               if (*cur_c == '\'') {
                  cur_c = str_ignore_single_quote(cur_c, NULL);
                  if (!cur_c) {
                     //TODO ask for more
                     assert(0);
                  }
                  continue;
               } else if (*cur_c == '"') {
                  cur_c = str_ignore_double_quote(cur_c, NULL);
                  if (!cur_c) {
                     //TODO ask for more
                     assert(0);
                  }
                  continue;
               } else if (*cur_c == '\\') {
                  //TODO not sure if it ignores a newline?
                  cur_c++;
                  continue;
               }
               pstr_append(param_str, *cur_c);
               cur_c++;
            }
         }
         //delimited param_str (unless EXP_LENGTH)
         switch (*cur_c) {
            case '}': { cur_c++; } goto delimited_params;
            case ':': {
               switch (*(cur_c+1)) {
                  case '-': exp_type = EXP_COLON_MINUS; break;
                  case '=': exp_type = EXP_COLON_EQUAL; break;
                  case '?': exp_type = EXP_COLON_QUESTIONMARK; break;
                  case '+': exp_type = EXP_COLON_PLUS; break;
                  case EOF: default: {
                     //TODO add better line number
                     expansion_error(cur_env, 1000, "bad substitution");
                  }
               }
               cur_c += 2;
            } break;
            case '-': exp_type = EXP_MINUS;        cur_c++; break;
            case '=': exp_type = EXP_EQUAL;        cur_c++; break;
            case '?': exp_type = EXP_QUESTIONMARK; cur_c++; break;
            case '+': exp_type = EXP_PLUS;         cur_c++; break;
            case '#': {
               if (param_str->str_len == 0) {
                  exp_type = EXP_LENGTH;
                  cur_c++;
                  while (isalnum(*cur_c) || *cur_c == '_') {
                     pstr_append(param_str, *cur_c);
                     cur_c++;
                  }
                  if (*cur_c != '}') {
                     //TODO syntax error
                     assert(0);
                  }
                  cur_c++;
                  goto delimited_params;
               } else if (*(cur_c+1) == '#') { exp_type = EXP_POUND_POUND; cur_c += 2; }
               else { exp_type = EXP_POUND; cur_c++; }
            } break;
            case '%': {
               if (*(cur_c+1) == '%') { exp_type = EXP_PERCENT_PERCENT; cur_c += 2; }
               else                   { exp_type = EXP_PERCENT;         cur_c++;    }
            } break;
            default: {
               //TODO get line no somehow and add substitution to msg
               expansion_error(cur_env, 1000, "bad substitution");
            } break;
         }

         //parse word
         /* fprintf(stderr, "parsed param: '%s'\n", param_str->str); */

         word_str = psh_string_create(NULL);
         while (*cur_c && *cur_c != '}') {
            switch (*cur_c) {
               case '\'': { if (!(cur_c = str_ignore_single_quote(cur_c, word_str))) assert(0); } break; //TODO ask for more
               case '"': { if (!(cur_c = str_ignore_double_quote(cur_c, word_str))) assert(0); } break; //TODO ask for more
               case '\\': { cur_c++; } break; /*TODO not sure if it ignores a newline*/
               case '`': case '$': { if (!(cur_c = str_ignore_expansion(cur_c, word_str))) assert(0); } break; //TODO ask for more
               default: { pstr_append(word_str, *cur_c++); } break;
            }
         }
         /* fprintf(stderr, "word_str: '%s'\n", word_str->str); */
         if (*cur_c == '}') {
            cur_c++;
            goto delimited_params;
         } else {
            //ask for more?
            assert(0);
         }
      } else if (*cur_c == '$') {
         cur_c++;
         param_str = psh_string_create(NULL);
         //Check if it's followed by a special character
         switch (*cur_c) {
            case '*': case '@': case '#': case '?': case '-': case '$': case '!':
            case '0': case '1': case '2': case '3': case '4':
            case '5': case '6': case '7': case '8': case '9': {
               pstr_append(param_str, *cur_c++);
            } goto delimited_params;
         }

         //parse name
         while (isalnum(*cur_c) || *cur_c == '_') {
            pstr_append(param_str, *cur_c);
            cur_c++;
         }
         goto delimited_params;
      } else { //Normal character
         pstr_append(ret_str, *cur_c++);
         goto continue_outer_while;
      }

delimited_params:
      if (!param_str) {
         pstr_append(ret_str, '$');
         continue;
      } else if (param_str->str_len == 0) {
         psh_string_free(param_str);
         param_str = NULL;
         pstr_append(ret_str, '$');
         continue;
      }

      param_val = psh_string_create(NULL);
      if (param_str->str_len == 1 && exp_type != EXP_LENGTH) {
         switch (param_str->str[0]) {
            case '*': {
               for (uint64_t i = 0; i < cur_env->args->size; i++) {
                  psh_string_append_str(param_val, cur_env->args->strs[i]);
                  if (i != cur_env->args->size)
                     pstr_append(param_val, ' ');
               }
            } break;
            case '@': {
               //TODO implement that
               assert(0);
            } break;
            case '#': {
               size_t nb_args = cur_env->args->size;
               struct psh_string* nb_str = psh_string_from_nb(nb_args);
               psh_string_append_str(param_val, nb_str->str);
               psh_string_free(nb_str);
            } break;
            case '?': {
               struct psh_string* nb_str = psh_string_from_nb(cur_env->last_pipeline_status);
               pstr_append(param_val, nb_str);
               psh_string_free(nb_str);
            } break;
            case '-': {
               //TODO implement that
               assert(0);
               struct psh_string* ret = psh_string_create("-");
            } break;
            case '$': {
               struct psh_string* nb_str = psh_string_from_nb(cur_env->pid_invoked_shell);
               psh_string_append_str(param_val, nb_str->str);
               psh_string_free(nb_str);
            } break;
            case '!': {
               struct psh_string* nb_str = psh_string_from_nb(cur_env->pid_last_backgroud_process);
               psh_string_append_str(param_val, nb_str->str);
               psh_string_free(nb_str);
            } break;
            case '0': {
               psh_string_append_str(param_val, cur_env->arg0);
            } break;
         }
      }

      if (param_val->str_len == 0) {
         //check if it's a number
         bool is_number = true;
         for (uint i = 0; i < param_str->str_len; i++) {
            if (param_str->str[i] < '0' || param_str->str[i] > '9') {
               is_number = false;
               break;
            }
         }
         if (is_number) {
            errno = 0;
            uint64_t index = strtoul(param_str->str, NULL, 10);
            DEBUG_ASSERT(errno == 0);
            DEBUG_ASSERT(index > 0);
            psh_string_append_str(param_val, (cur_env->args->size > index - 1) ? cur_env->args->strs[index-1] : "");
         } else {
            psh_string_free(param_val);
            param_val = NULL;
         }
      }


      //get value from hashmap
      struct sym_var_entry* param_entry = NULL;
      if (!param_val) {
         param_entry = sym_get_var_entry(cur_env->var_table, param_str->str);
      }
      //value of the parameter is either in param_val->str if special case, or in param_entry else

      switch (exp_type) {
         case EXP_NORMAL: {
            if (param_val) pstr_append(ret_str, param_val);
            else if (param_entry)
               pstr_append(ret_str, (param_entry->value == NULL) ? "" : param_entry->value);
         } break;
         case EXP_COLON_MINUS: {
            if (param_val) psh_string_append_str(ret_str, param_val->str);
            else if (param_entry && param_entry->value != NULL && strcmp(param_entry->value, "")) psh_string_append_str(ret_str, param_entry->value);
            else {
               const char* word_str_exp = expand(cur_env, word_str->str, EXP_TILDE | EXP_PARAM | EXP_CMD_SUB | EXP_ARITH).charp;
               psh_string_append_str(ret_str, word_str->str);
               free((char*)word_str_exp);
            }
         } break;
         case EXP_MINUS: {
            if (param_val) psh_string_append_str(ret_str, param_val->str);
            else if (param_entry) psh_string_append_str(ret_str, (param_entry->value == NULL) ? "" : param_entry->value);
            else {
               const char* word_str_exp = expand(cur_env, word_str->str, EXP_TILDE | EXP_PARAM | EXP_CMD_SUB | EXP_ARITH).charp;
               psh_string_append_str(ret_str, word_str->str);
               free((char*)word_str_exp);
            }
         } break;
         case EXP_COLON_EQUAL: {
            if (param_val) psh_string_append_str(ret_str, param_val->str);
            else if (param_entry && param_entry->value != NULL && strcmp(param_entry->value, "")) psh_string_append_str(ret_str, param_entry->value);
            else if (param_entry && (param_entry->value == NULL || !strcmp(param_entry->value, ""))) {
               const char* word_str_exp = expand(cur_env, word_str->str, EXP_TILDE | EXP_PARAM | EXP_CMD_SUB | EXP_ARITH).charp;
               psh_string_append_str(ret_str, word_str_exp);
               free(param_entry->value);
               param_entry->value = (char*)word_str_exp;
            } else {
               const char* word_str_exp = expand(cur_env, word_str->str, EXP_TILDE | EXP_PARAM | EXP_CMD_SUB | EXP_ARITH).charp;
               psh_string_append_str(ret_str, word_str_exp);
               sym_var_insert(cur_env->var_table, param_str->str, word_str_exp, false);
               free((char*)word_str_exp);
            }
         } break;
         case EXP_EQUAL: {
            if (param_val) psh_string_append_str(ret_str, param_val->str);
            else if (param_entry && param_entry->value != NULL) psh_string_append_str(ret_str, param_entry->value);
            else if (!param_entry) {
               const char* word_str_exp = expand(cur_env, word_str->str, EXP_TILDE | EXP_PARAM | EXP_CMD_SUB | EXP_ARITH).charp;
               psh_string_append_str(ret_str, word_str_exp);
               sym_var_insert(cur_env->var_table, param_str->str, word_str_exp, false);
               free((char*)word_str_exp);
            }
         } break;
         case EXP_COLON_QUESTIONMARK: {
            if (param_val) psh_string_append_str(ret_str, param_val->str);
            else if (param_entry && param_entry->value != NULL) psh_string_append_str(ret_str, param_entry->value);
            else {
               struct psh_string* msg = psh_string_create(param_str->str);
               const char* word_str_exp = (word_str != NULL) ? expand(cur_env, word_str->str, EXP_TILDE | EXP_PARAM | EXP_CMD_SUB | EXP_ARITH).charp : NULL;
               if (!word_str_exp || strlen(word_str_exp) == 0) {
                  pstr_append(msg, ": parameter null or not set");
               } else {
                  pstr_append(msg, ": ");
                  pstr_append(msg, (char*)word_str_exp);
               }
               free((char*)word_str_exp);
               expansion_error(cur_env, 1000, psh_string_to_cstr(msg, true));
            }
         } break;
         case EXP_QUESTIONMARK: {
            if (param_val) psh_string_append_str(ret_str, param_val->str);
            else if (param_entry && param_entry->value != NULL && strcmp(param_entry->value, "")) psh_string_append_str(ret_str, param_entry->value);
            else {
               struct psh_string* msg = psh_string_create(param_str->str);
               const char* word_str_exp = (word_str != NULL) ? expand(cur_env, word_str->str, EXP_TILDE | EXP_PARAM | EXP_CMD_SUB | EXP_ARITH).charp : NULL;
               if (!word_str_exp || strlen(word_str_exp) == 0) {
                  pstr_append(msg, "parameter not set");
               } else {
                  pstr_append(msg, ": ");
                  pstr_append(msg, word_str);
               }
               free((char*)word_str_exp);
               expansion_error(cur_env, 1000, psh_string_to_cstr(msg, true));
            }
         } break;
         case EXP_COLON_PLUS: {
            if (param_val || (param_entry && param_entry->value != NULL && strcmp(param_entry->value, ""))) {
               const char* word_str_exp = expand(cur_env, word_str->str, EXP_TILDE | EXP_PARAM | EXP_CMD_SUB | EXP_ARITH).charp;
               psh_string_append_str(ret_str, word_str_exp);
               free((char*)word_str_exp);
            }
            //else "substitute null"
         } break;
         case EXP_PLUS: {
            if (param_val || param_entry) {
               //TODO perform expansions on word
               const char* word_str_exp = expand(cur_env, word_str->str, EXP_TILDE | EXP_PARAM | EXP_CMD_SUB | EXP_ARITH).charp;
               psh_string_append_str(ret_str, word_str_exp);
               free((char*)word_str_exp);
            }
            //else "substitute null"
         } break;
         case EXP_LENGTH: {
            if (param_entry) {
               uint64_t len = (param_entry->value == NULL) ? 0 : strlen(param_entry->value);
               struct psh_string* nb_str = psh_string_from_nb(len);
               psh_string_append_str(ret_str, nb_str->str);
               psh_string_free(nb_str);
            /* } else if () { */

            }
         } break;
         case EXP_PERCENT: {
            const char* word_str_exp = expand(cur_env, word_str->str, EXP_TILDE | EXP_PARAM | EXP_CMD_SUB | EXP_ARITH).charp;
            if (param_val) {
               struct psh_string* processed_pstr = remove_min_suffix(param_val->str, word_str_exp);
               pstr_append(ret_str, processed_pstr);
               psh_string_free(processed_pstr);
            } else if (param_entry) {
               struct psh_string* processed_pstr = remove_min_suffix(param_entry->value, word_str_exp);
               pstr_append(ret_str, processed_pstr);
               psh_string_free(processed_pstr);
            }
            free((char*)word_str_exp);
         } break;
         case EXP_PERCENT_PERCENT: {
            const char* word_str_exp = expand(cur_env, word_str->str, EXP_TILDE | EXP_PARAM | EXP_CMD_SUB | EXP_ARITH).charp;
            if (param_val) {
               struct psh_string* processed_pstr = remove_max_suffix(param_val->str, word_str_exp);
               pstr_append(ret_str, processed_pstr);
               psh_string_free(processed_pstr);
            } else if (param_entry) {
               struct psh_string* processed_pstr = remove_max_suffix(param_entry->value, word_str_exp);
               pstr_append(ret_str, processed_pstr);
               psh_string_free(processed_pstr);
            }
            free((char*)word_str_exp);
			} break;
         case EXP_POUND: {
            const char* word_str_exp = expand(cur_env, word_str->str, EXP_TILDE | EXP_PARAM | EXP_CMD_SUB | EXP_ARITH).charp;
            if (param_val) {
               struct psh_string* processed_pstr = remove_min_prefix(param_val->str, word_str_exp);
               pstr_append(ret_str, processed_pstr);
               psh_string_free(processed_pstr);
            } else if (param_entry) {
               struct psh_string* processed_pstr = remove_min_prefix(param_entry->value, word_str_exp);
               pstr_append(ret_str, processed_pstr);
               psh_string_free(processed_pstr);
            }
            free((char*)word_str_exp);
			} break;
         case EXP_POUND_POUND: {
            const char* word_str_exp = expand(cur_env, word_str->str, EXP_TILDE | EXP_PARAM | EXP_CMD_SUB | EXP_ARITH).charp;
            if (param_val) {
               struct psh_string* processed_pstr = remove_max_prefix(param_val->str, word_str_exp);
               pstr_append(ret_str, processed_pstr);
               psh_string_free(processed_pstr);
            } else if (param_entry) {
               struct psh_string* processed_pstr = remove_max_prefix(param_entry->value, word_str_exp);
               pstr_append(ret_str, processed_pstr);
               psh_string_free(processed_pstr);
            }
            free((char*)word_str_exp);
			} break;
      }
continue_outer_while:;
      if (param_str) psh_string_free(param_str);
      if (param_val) psh_string_free(param_val);
      if (word_str)  psh_string_free(word_str);
      param_str = NULL;
      param_val = NULL;
      word_str = NULL;
   }

   /* fprintf(stderr, "ret_str: '%s'\n", ret_str->str); */
   return psh_string_to_cstr(ret_str, true);
}

struct psh_result* eval_command_substitution(struct shell_env* cur_env, struct psh_string* cmd_str);

//TODO finish implementing it
char*
command_substitution_exp(struct shell_env* cur_env, const char* words) {
   struct psh_string* ret_str = psh_string_create(NULL);
   const char *cur_c = words;

   while (1) {
      if (!*cur_c)
         break;

      if (*cur_c == '$') {
         cur_c++;
         if (*cur_c == '(') {
            cur_c++;
            if (*cur_c == '(') {
               pstr_append(ret_str, "$((");
               cur_c++;
               continue;
            }
            uint64_t nb_parens = 1;
            struct psh_string* cmd_str = psh_string_create(NULL);
            //Search till unquoted )
            while (nb_parens > 0) {
               switch (*cur_c) {
                  case '\0': {
                     //TODO get more input
                     assert(0);
                  } break;
                  case '(': {
                     nb_parens++;
                     pstr_append(cmd_str, *cur_c++);
                  } break;
                  case ')': {
                     nb_parens--;
                     if (nb_parens > 0)
                        pstr_append(cmd_str, *cur_c);
                     cur_c++;
                  } break;
                  case '\'': {
                     do pstr_append(cmd_str, *cur_c++);
                     while (*cur_c && *cur_c != '\'');
                     if (!*cur_c) {
                        //TODO ask for more
                        assert(0);
                     } else pstr_append(cmd_str, *cur_c++);
                  } break;
                  case '"': {
                     cur_c = str_ignore_double_quote(cur_c, cmd_str);
                  } break;
                  case '\\': {
                     cur_c++;
                     if (!*cur_c) { /*TODO ask for more */ assert(0); }
                     else pstr_append(cmd_str, *cur_c++);
                  } break;
                  default: { pstr_append(cmd_str, *cur_c++); } break;
               }
            }

            //Delimited cmd_str
            struct psh_result* cur_result = eval_command_substitution(cur_env, cmd_str);
            pstr_append(ret_str, cur_result->substitution_output);
            /* PSH_LOG(LOG_INFO, "cmd_sub: running: '%s', output: '%s'\n", cmd_str->str, cur_result->substitution_output); */
            free(cur_result->substitution_output);
            free(cur_result);
            continue;
         } else {
            //$ but not (
            pstr_append(ret_str, '$');
            continue;
         }
      } else if (*cur_c == '`') {
         cur_c++;
         struct psh_string* cmd_str = psh_string_create(NULL);
         //Search till unquoted `
         bool found_backquote = false;
         while (!found_backquote) {
            switch (*cur_c) {
               case '\0': {
                  //TODO get more input
                  assert(0);
               } break;
               case '`': {
                  found_backquote = true;
                  cur_c++;
               } break;
               case '\'': {
                  do pstr_append(cmd_str, *cur_c++);
                  while (*cur_c && *cur_c != '\'');
                  if (!*cur_c) {
                     //TODO ask for more
                     assert(0);
                  } else pstr_append(cmd_str, *cur_c++);
               } break;
               case '"': {
                  //TODO implement
                  assert(0);
               } break;
               case '\\': {
                  cur_c++;
                  if (*cur_c == '$' || *cur_c == '`' || *cur_c == '\\') {
                     pstr_append(cmd_str, *cur_c);
                     cur_c++;
                  } else {
                     pstr_append(cmd_str, '\\');
                  }
               } break;
               default: { pstr_append(cmd_str, *cur_c++); } break;
            }
         }

         //Delimited cmd_str
         struct psh_result* cur_result = eval_command_substitution(cur_env, cmd_str);
         psh_string_append_str(ret_str, cur_result->substitution_output);
         /* PSH_LOG(LOG_INFO, "cmd_sub: running: '%s', output: '%s'\n", cmd_str->str, cur_result->substitution_output); */
         free(cur_result->substitution_output);
         free(cur_result);
         continue;
      } else {
         pstr_append(ret_str, *cur_c);
         cur_c++;
      }
   }

   return psh_string_to_cstr(ret_str, true);
}

//TODO implement it
char*
arithmetic_exp(struct shell_env* cur_env, const char* words) {
   return psh_strdup(words);

   struct psh_string* ret_str = psh_string_create(NULL);

   const char* cur_c = words;

   while (1) {
      if (!*cur_c) {
         break;
      } else if (*cur_c == '$' && *(cur_c+1) == '(' && *(cur_c+2) == '(') {
         cur_c += 3;
         uint64_t nb_parens = 2;
         struct psh_string* cmd_str = psh_string_create(NULL);
         assert(cmd_str);
         while (nb_parens > 0) {
            if (!*cur_c) {
               //TODO get more input
               assert(0);
            } else if (*cur_c == '(') {
               nb_parens++;
               pstr_append(cmd_str, *cur_c);
               cur_c++;
               continue;
            } else if (*cur_c == ')') {
               nb_parens--;
               if (nb_parens > 0)
                  pstr_append(cmd_str, *cur_c);
               cur_c++;
               continue;
            } else if (*cur_c == '\'') {
               do pstr_append(cmd_str, *cur_c++);
               while (*cur_c && *cur_c != '\'');
               if (!*cur_c) {
                  //TODO ask for more
                  assert(0);
               } else { //is '
                  pstr_append(cmd_str, *cur_c);
                  cur_c++;
               }
               continue;
            } else if (*cur_c == '"') {
               //TODO implement
               assert(0);
            } else if (*cur_c == '\\') {
               cur_c++;
               if (!*cur_c) {
                  //TODO ask for more
                  assert(0);
               } else {
                  pstr_append(cmd_str, *cur_c);
                  cur_c++;
                  continue;
               }
            } else {
               pstr_append(cmd_str, *cur_c);
               cur_c++;
               continue;
            }
         }
         //delimited cmd_str
         //remove single trailing )
         psh_string_delete_char(cmd_str, cmd_str->str_len-1);
         
         //TODO handle the actual arithmetic substitution
         assert(0);
         //Firstly: do parameter expansion, command substitution and quote removal
         const union wordexp_ret cmd_str_exp_ret = expand(cur_env, cmd_str->str, EXP_PARAM | EXP_CMD_SUB | EXP_QUOTE);
         const char* cmd_str_exp = cmd_str_exp_ret.charp;
         psh_string_free(cmd_str);
      } else {
         pstr_append(ret_str, *cur_c);
         continue;
      }
   }
}

bool
is_in_IFS(char c, const char* IFS) {
   for (; *IFS && *IFS++ != c;);
   return *IFS != '\0';
}

struct dyn_arr_str*
field_split(struct shell_env* cur_env, const char* words) {
   const char* IFS = sym_get_value(cur_env->var_table, "IFS");
   if (!IFS) {
      IFS = " \n\t";
   }

   struct dyn_arr_str* str_arr = dyn_arr_str_create();

   size_t nb_words = 0;
   const char* p_words = words;
   while (1) {
      while (*p_words && is_in_IFS(*p_words, IFS)) {
         p_words++;
      }
      if (*p_words == '\0') {
         break;
      }

      struct psh_string* cur_word = psh_string_create(NULL);
      //found a word
      for (; *p_words && !is_in_IFS(*p_words, IFS); p_words++) {
         if (*p_words == '\'') {
            pstr_append(cur_word, *p_words);
            p_words++;
            while (*p_words && *p_words != '\'') {
               pstr_append(cur_word, *p_words);
               p_words++;
            }
            if (!*p_words) assert(0);
         }
         //TODO implement the more complex behaviour
         else if (*p_words == '\"') {
            pstr_append(cur_word, *p_words);
            p_words++;
            while (*p_words && *p_words != '\"') {
               pstr_append(cur_word, *p_words);
               p_words++;
            }
            if (!*p_words)
               assert(0);
         }
         else if (*p_words == '\\') {
            p_words++;
            if (!*p_words)
               assert(0);
            pstr_append(cur_word, *p_words);
         }
         pstr_append(cur_word, *p_words);
      }
      nb_words++;

      
      dyn_arr_str_push(str_arr, psh_string_to_cstr(cur_word, true), false);
   }

   return str_arr;
}


bool
is_regexp(const char* in) {
   char c;
   for (; (c = *in); in++) {
      if (c == '\\') in++;
      else if (c == '[' || c == '?' || c == '*') return true;
   }

   return false;
}

bool
is_path(const char* in) {
   char c;
   for (; (c = *in); in++) {
      if (c == '/') return true;
   }
   return false;
}

//Matches stuff like '*.c', 'test.?', etc...
//Returns null on no matches
struct dyn_arr_str*
get_matches_from_path(const char* path, const char* reg) {
   struct dyn_arr_str* ret = dyn_arr_str_create();
   DIR* dir = opendir(path);

   if (!dir) {
      dyn_arr_str_free(ret, false);
      return NULL;
   }

   struct dirent* cur_ent;
   while ((cur_ent = readdir(dir)) != NULL) {
      if (is_pattern_match(reg, cur_ent->d_name)) {
         dyn_arr_str_push(ret, cur_ent->d_name, true);
      }
   }

   closedir(dir);
   if (ret->size == 0) {
      dyn_arr_str_free(ret, false);
      return NULL;
   }

   return ret;
}

//TODO could really do a refactor
//TODO optimise (currently always looks through all entries of a directory even if the entry is not a regex)
//Reg is some thing like '/home/*/Documents/*' or '../*.c'
struct dyn_arr_str*
get_matches_from_path_recursive(const char* reg) {
   char* cur_path = psh_strdup(reg);
   bool is_absolute = (reg[0] == '/');

   size_t nb_entries = 1;
   if (is_absolute)
      nb_entries--;

   for (const char* c = cur_path; *c; c++) {
      if (*c == '/') {
         nb_entries++;
         while (*c == '/') c++;
         c--;
         continue;
      }
   }

   //if ends with '/', path ends with no entry
   bool ends_in_slash = (reg[strlen(reg)-1] == '/');
   if (ends_in_slash) {
      nb_entries--;
   }

   assert(nb_entries > 0);
   
   DIR* dir = NULL;
   if (is_absolute) {
      dir = opendir("/");
   } else {
      dir = opendir(".");
   }
   if (!dir) {
      free(cur_path);
      return NULL;
   }
   while (*cur_path == '/')
      cur_path++;

   struct dyn_arr_str** lists = psh_calloc(sizeof(struct dyn_arr_str*) * nb_entries);

   struct dyn_arr_str* first_list = dyn_arr_str_create();

   struct psh_string* cur_word = next_word_in_path(cur_path);
   //if regex, look at all of the matching paths
   struct dirent* cur_ent;
   while ((cur_ent = readdir(dir)) != NULL) {
      if (is_pattern_match(cur_word->str, cur_ent->d_name)) {
         //Add a '/' at begin if it's absolute
         if (is_absolute) {
            //if it's the last entry and the pattern ends in '/', only get directories (plus append '/' at the ends of the paths)
            if (nb_entries <= 1 && ends_in_slash) {
               size_t entry_len = strlen(cur_ent->d_name);
               char* actual_path = psh_malloc(1 + entry_len + 2);
               actual_path[0] = '/';
               strcpy(actual_path+1, cur_ent->d_name);
               actual_path[1+entry_len] = '/';
               actual_path[1+entry_len+1] = 0;
               struct stat path_stat;
               if (stat(actual_path, &path_stat) == 0) {
                  if (S_ISDIR(path_stat.st_mode))
                     dyn_arr_str_push(first_list, actual_path, false);
                  else
                     free(actual_path);
               }
            } else {
               size_t entry_len = strlen(cur_ent->d_name);
               char* actual_path = psh_malloc(1 + entry_len + 1);
               actual_path[0] = '/';
               strcpy(actual_path+1, cur_ent->d_name);
               actual_path[1+entry_len] = 0;
               dyn_arr_str_push(first_list, actual_path, false);
            }
         } else {
            //if it's the last entry and the pattern ends in '/', only get directories (plus append '/' at the ends of the paths)
            if (nb_entries <= 1 && ends_in_slash) {
               size_t entry_len = strlen(cur_ent->d_name);
               char* actual_path = psh_malloc(entry_len + 2);
               strcpy(actual_path, cur_ent->d_name);
               actual_path[entry_len] = '/';
               actual_path[entry_len+1] = 0;
               struct stat path_stat;
               if (stat(actual_path, &path_stat) == 0) {
                  if (S_ISDIR(path_stat.st_mode))
                     dyn_arr_str_push(first_list, actual_path, false);
                  else
                     free(actual_path);
               }
            } else {
               dyn_arr_str_push(first_list, cur_ent->d_name, true);
            }
         }
      }
   }
   //TODO else just see if the file exists

   closedir(dir);
   lists[0] = first_list;

   cur_path += cur_word->str_len;
   while (*cur_path == '/')
      cur_path++;

   //Add all matches to list
   for (size_t i = 1; i < nb_entries; i++) {
      struct dyn_arr_str* prev_list = lists[i-1];
      struct dyn_arr_str* cur_list = dyn_arr_str_create();

      struct psh_string* cur_word = next_word_in_path(cur_path);
      assert(cur_word);

      //for every element of the previous list, look through those directories and see if something matches the cur_word
      for (size_t j = 0; j < prev_list->size; j++) {
         DIR* dir = opendir(prev_list->strs[j]);
         if (!dir) {
            continue;
         }
         struct dirent* cur_ent;
         //TODO check if it's not a regex, you can simply try to open the dir/file in that case
         while ((cur_ent = readdir(dir)) != NULL) {
            if (is_pattern_match(cur_word->str, cur_ent->d_name)) {
               size_t base_len = strlen(prev_list->strs[j]);
               size_t entry_len = strlen(cur_ent->d_name);
               //if it's the last entry and the pattern ends in '/', only get directories (plus append '/' at the ends of the paths)
               if (i == nb_entries-1 && ends_in_slash) {
                  char* actual_path = psh_malloc(base_len +  1 + entry_len + 2);
                  strcpy(actual_path, prev_list->strs[j]);
                  strcpy(actual_path + base_len, "/");
                  strcpy(actual_path + base_len + 1, cur_ent->d_name);
                  actual_path[base_len + 1 + entry_len] = '/';
                  actual_path[base_len + 1 + entry_len + 1] = 0;
                  struct stat path_stat;
                  if (stat(actual_path, &path_stat) == 0) {
                     if (S_ISDIR(path_stat.st_mode)) {
                        dyn_arr_str_push(cur_list, actual_path, false);
                     } else {
                        free(actual_path);
                     }
                  }
               } else {
                  char* actual_path = psh_malloc(base_len +  1 + entry_len + 1);
                  strcpy(actual_path, prev_list->strs[j]);
                  strcpy(actual_path + base_len, "/");
                  strcpy(actual_path + base_len + 1, cur_ent->d_name);
                  actual_path[base_len + 1 + entry_len] = 0;
                  dyn_arr_str_push(cur_list, actual_path, false);
               }
            }
         }

         closedir(dir);
      }

      cur_path += cur_word->str_len;
      while (*cur_path == '/')
         cur_path++;

      lists[i] = cur_list;
      psh_string_free(cur_word);
   }


   for (size_t i = 0; i < nb_entries-1; i++) {
      dyn_arr_str_free(lists[i], true);
   }
   struct dyn_arr_str* ret = lists[nb_entries-1];
   free(lists);

   if (ret->size == 0) {
      dyn_arr_str_free(ret, true);
      return NULL;
   }

   return ret;
}

//Returns input
void
pathname_exp(struct dyn_arr_str* fields) {
   for (size_t i = 0; i < fields->size; i++) {
      const char* cur_field = fields->strs[i];
      bool is_reg = is_regexp(cur_field);
      if (!is_reg)
         continue;

      bool is_pat = is_path(cur_field);

      struct dyn_arr_str* matches = NULL;
      if (!is_pat) {
         //only look at files in current path
         matches = get_matches_from_path(".", cur_field);
      } else {
         //it's a path, need to check recursively 
         matches = get_matches_from_path_recursive(cur_field);
      }

      if (matches) {
         //add all matches to the table remove regex from fields
         dyn_arr_str_delete(fields, i, 1, true);
         dyn_arr_str_insert_arr_at(fields, matches, i);
         i += matches->size;
         dyn_arr_str_free(matches, true);
      }
   }
}

void
quote_removal(struct dyn_arr_str* fields) {
   for (size_t i = 0; i < fields->size; i++) {
      //psh_string of the string with quote expanded
      struct psh_string* cur_str = psh_string_create("");
      for (const char* cur_c = fields->strs[i]; *cur_c; cur_c++) {
         if (*cur_c == '\'') {
            cur_c++;
            while (*cur_c && *cur_c != '\'') {
               pstr_append(cur_str, *cur_c++);
            }
            if (!*cur_c) {
               //TODO ask for more?
               assert(0);
            }
            continue;
         } else if (*cur_c == '\"') {
            //TODO do more complex expansions and stuff
            cur_c++;
            while (*cur_c && *cur_c != '\"') {
               pstr_append(cur_str, *cur_c);
               cur_c++;
            }
            if (!*cur_c) {
               //TODO ask for more?
               assert(0);
            }
            continue;
         } else if (*cur_c == '\\') {
            cur_c++;
            if (*cur_c && *cur_c != '\n') {
               pstr_append(cur_str, *cur_c);
            }
            if (!*cur_c) {
               //TODO ask for more?
               assert(0);
            }
            continue;
         } else {
            pstr_append(cur_str, *cur_c);
         }
      }

      free(fields->strs[i]);
      fields->strs[i] = psh_string_to_cstr(cur_str, true);
   }
}

char*
quote_removal_str(const char* in) {
   //psh_string of the string with quote expanded
   struct psh_string* cur_str = psh_string_create(NULL);
   while (*in) {
      switch (*in) {
         case '\'': {
            in++;
            while (*in && *in != '\'') {
               pstr_append(cur_str, *in++);
            }
            if (!*in) {
               //TODO ask for more?
               assert(0);
            }
         } break;
         case '\"': {
            //TODO do more complex expansions and stuff
            in++;
            while (*in && *in != '\"') {
               pstr_append(cur_str, *in++);
            }
            if (!*in) {
               //TODO ask for more?
               assert(0);
            }
         } break;
         case '\\': {
            in++;
            if (*in && *in != '\n') {
               pstr_append(cur_str, *in);
            }
            if (!*in) {
               //TODO ask for more?
               assert(0);
            }
         } break;
         default: {
            pstr_append(cur_str, *in);
         } break;
      }

      in++;
   }

   return psh_string_to_cstr(cur_str, true);
}

void
wordfree(struct wordexp_t* wordexp) {
   if (!wordexp)
      return;
   for (uint64_t i = 0; i < wordexp->we_wordc; i++)
      free(wordexp->we_wordv[i]);
   free(wordexp->we_wordv);
   free(wordexp);
}
