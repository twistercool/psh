#include "log.h"

//Global log level
enum log_level __log_level = LOG_FATAL+1;
bool __log_is_init = false;

//Reads log level from environment (PSH_LOG can be either)
void
psh_log_init(void) {
   if (__log_is_init)
      return;

   //read from environment
   const char* const log_lvl_env = getenv("PSH_LOG");
   if (!log_lvl_env)
      return;

   if (strcasestr(log_lvl_env, "info")) { __log_level = LOG_INFO; }
   else if (strcasestr(log_lvl_env, "trace")) { __log_level = LOG_TRACE; }
   else if (strcasestr(log_lvl_env, "debug")) { __log_level = LOG_DEBUG; }
   else if (strcasestr(log_lvl_env, "warn")) { __log_level = LOG_WARNING; }
   else if (strcasestr(log_lvl_env, "err")) { __log_level = LOG_ERROR; }
   else if (strcasestr(log_lvl_env, "fatal")) { __log_level = LOG_FATAL; }
}
