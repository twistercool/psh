#include "parser.h"

enum token_type get_operator_token_type(const char *in);

/*
   * This function implements rule 1 of grammar parsing
*/
enum token_type
get_reserved_word(const struct token* in_tok) {
   if (!in_tok)
      return TOKTYPE_EOF;

   if (in_tok->type != TOKTYPE_TOKEN)
      return in_tok->type;

   const char* const tok_str = in_tok->psh_str->str;

   switch (in_tok->psh_str->str_len) {
      case 1:
         switch(tok_str[0]) {
            case '!': return TOKTYPE_Bang;
            case '{': return TOKTYPE_Lbrace;
            case '}': return TOKTYPE_Rbrace;
            default: return TOKTYPE_WORD;
         }
      case 2:
         if (!strcmp(tok_str, "do")) return TOKTYPE_Do;
         else if (!strcmp(tok_str, "fi")) return TOKTYPE_Fi;
         else if (!strcmp(tok_str, "if")) return TOKTYPE_If;
         else if (!strcmp(tok_str, "in")) return TOKTYPE_In;
         else return TOKTYPE_WORD;
      case 3:
         if (!strcmp(tok_str, "for")) return TOKTYPE_For;
         else return TOKTYPE_WORD;
      case 4:
         if (!strcmp(tok_str, "case")) return TOKTYPE_Case;
         else if (!strcmp(tok_str, "done")) return TOKTYPE_Done;
         else if (!strcmp(tok_str, "elif")) return TOKTYPE_Elif;
         else if (!strcmp(tok_str, "else")) return TOKTYPE_Else;
         else if (!strcmp(tok_str, "esac")) return TOKTYPE_Esac;
         else if (!strcmp(tok_str, "then")) return TOKTYPE_Then;
         else return TOKTYPE_WORD;
      case 5:
         if (!strcmp(tok_str, "until")) return TOKTYPE_Until;
         else if (!strcmp(tok_str, "while")) return TOKTYPE_While;
         else return TOKTYPE_WORD;
      default: return TOKTYPE_WORD;
   }
}
   
bool
is_posix_name(const char* in) {
   if (!*in)
      return false;

   while ((isalnum(*in) || *in == '_'))
      in++;

   return *in == '\0';
}

/*
   Removes tokens from the cur_env to be reparsed later (for interactive errors)
   NOTE: only nodes that can be "ungotten" actually record the index, so before ungetting a node, ensure that it records its position first
*/
void
unget_node(struct shell_env* cur_env, struct psh_node* in_node) {
   struct dyn_arr_tok* tok_arr = cur_env->tok_arr;
   //TODO I don't think I have correct handling when there are EOF tokens
   tok_arr->nb_EOF_toks = 0;
   cur_input(cur_env)->is_last_EOF = false;
   cur_input(cur_env)->cur_pos = in_node->begin_index;
   struct token* cur_tok_in_list = dyn_arr_tok_point_last_no_pushback(tok_arr);
   while (cur_tok_in_list) {
      if (in_node->begin_index > cur_tok_in_list->index_start)
         break;

      //Remove the token from the list
      cur_input(cur_env)->cur_pos = cur_tok_in_list->index_start;
      //Remove the last token from the list
      dyn_arr_tok_delete_last_no_pushback(tok_arr);
      //Rewind the psh_input (not sure if works well)
      cur_tok_in_list = dyn_arr_tok_point_last_no_pushback(tok_arr);
   }
   if (!cur_tok_in_list && tok_arr->size != 0) {
      assert(0);
   }

   psh_node_free(in_node, true);
}


/*
   Almost all of the below parse functions return:
      - NULL if unparsed
      - NODE_ERROR on parse error
      - their respective node if parsing succeeds,
*/

//Can't return NULL, but might return an empty node
struct psh_node*
parse_program(struct shell_env* cur_env) {
/*
program : linebreak complete_commands linebreak
        | linebreak
*/
   _Nullable struct psh_node* node_complete_commands = NULL;

   uint64_t nb_newlines_0 = parse_linebreak(cur_env);

   node_complete_commands = parse_complete_commands(cur_env);
   if (IS_PARSE_ERR(node_complete_commands)) goto bubble_error;
   else parse_linebreak(cur_env);

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));
   ret_node->node_type = PSH_NODE_PROGRAM;
   ret_node->node_program.node_complete_commands = node_complete_commands;

   return ret_node;

bubble_error:
   return NODE_ERROR;
}

struct psh_node*
parse_complete_commands(struct shell_env* cur_env) {
/*
complete_commands: complete_commands newline_list complete_command
                 | complete_command
*/
   struct dyn_arr_node* nodes_complete_command = NULL;

   struct psh_node* node_complete_command = parse_complete_command(cur_env);
   if (!node_complete_command) return NULL;
   else if (IS_PARSE_ERR(node_complete_command)) goto bubble_error;

   nodes_complete_command = dyn_arr_node_create();

   do {
      if (IS_PARSE_ERR(node_complete_command)) goto bubble_error;
      dyn_arr_node_push(nodes_complete_command, node_complete_command);
      uint64_t nb_newlines = parse_newline_list(cur_env);
   }
   while ((node_complete_command = parse_complete_command(cur_env)));

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));
   ret_node->node_type = PSH_NODE_COMPLETE_COMMANDS;
   ret_node->node_complete_commands.nodes_complete_command = nodes_complete_command;

   return ret_node;

bubble_error:
   dyn_arr_node_free(nodes_complete_command, true);
   return NODE_ERROR;
}

struct psh_node*
parse_complete_command(struct shell_env* cur_env) {
/*
complete_command : list separator_op
                 | list
*/
   struct psh_node* node_list = NULL;
   struct psh_node* node_separator_op = NULL;

   node_list = parse_list(cur_env);
   if (!node_list) return NULL;
   else if (IS_PARSE_ERR(node_list)) goto bubble_error;


   const size_t nb_separators_op = (node_list->node_list.nodes_separator_op == NULL) ? 0 : node_list->node_list.nodes_separator_op->size;
   const size_t nb_and_ors_list = node_list->node_list.nodes_and_or->size;

   if (nb_separators_op >= nb_and_ors_list) {
      //Bubbles up the separator_op found in the list node
      node_separator_op = dyn_arr_node_pop(node_list->node_list.nodes_separator_op);
      DEBUG_ASSERT(node_separator_op);
   } else {
      node_separator_op = parse_separator(cur_env);
   }

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));

   ret_node->node_type = PSH_NODE_COMPLETE_COMMAND;
   ret_node->node_complete_command.node_list = node_list;
   ret_node->node_complete_command.node_separator_op = node_separator_op;

   return ret_node;

bubble_error:
   psh_node_free(node_list, true);
   return NODE_ERROR;
}

struct psh_node*
parse_list(struct shell_env* cur_env) {
/*
list : list separator_op and_or
     |                   and_or
*/
   struct dyn_arr_node* nodes_separator_op = NULL;
   struct dyn_arr_node* nodes_and_or = NULL;

   struct psh_node* node_and_or = parse_and_or(cur_env);
   if (!node_and_or) return NULL;
   else if (IS_PARSE_ERR(node_and_or)) goto bubble_error;


   nodes_separator_op = dyn_arr_node_create();
   nodes_and_or = dyn_arr_node_create();
   dyn_arr_node_push(nodes_and_or, node_and_or);

   struct psh_node* node_separator_op;
   while ((node_separator_op = parse_separator_op(cur_env))) {
      dyn_arr_node_push(nodes_separator_op, node_separator_op);
      node_and_or = parse_and_or(cur_env);

      //if there is no and_or, the separator_op node will be bubbled up to the complete_command
      //TODO: actually, undo it, I don't think it should be that
      if (!node_and_or) break;
      else if (IS_PARSE_ERR(node_and_or)) goto bubble_error;
      else dyn_arr_node_push(nodes_and_or, node_and_or);
   }

   if (nodes_separator_op->size == 0) {
      dyn_arr_node_free(nodes_separator_op, true);
      free(nodes_separator_op);
      nodes_separator_op = NULL;
   }

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));

   ret_node->node_type = PSH_NODE_LIST;
   ret_node->node_list.nodes_separator_op = nodes_separator_op;
   ret_node->node_list.nodes_and_or = nodes_and_or;

   return ret_node;

bubble_error:
   dyn_arr_node_free(nodes_and_or, true);
   dyn_arr_node_free(nodes_separator_op, true);
   return NODE_ERROR;
}


enum psh_error psh_secondary_prompt(struct shell_env*);

struct psh_node*
parse_and_or(struct shell_env* cur_env) {
/*
and_or : pipeline
       | and_or AND_IF linebreak pipeline
       | and_or OR_IF  linebreak pipeline
*/
   struct dyn_arr_uint8* dyn_is_AND_IF = NULL;
   struct dyn_arr_node* nodes_pipeline = NULL;

   enum token_type tok_type;
   struct token *cur_tok;
   struct psh_node* node_pipeline = parse_pipeline(cur_env);
   if (!node_pipeline) return NULL;
   else if (IS_PARSE_ERR(node_pipeline)) goto bubble_error;

   dyn_is_AND_IF = dyn_arr_uint8_create();
   nodes_pipeline = dyn_arr_node_create();
   dyn_arr_node_push(nodes_pipeline, node_pipeline);

   while (1) {
      // parse a "AND_IF/OR_IF linebreak pipeline"
      tok_type = psh_get_token(cur_env);
      if (tok_type == TOKTYPE_AND_IF) {
         dyn_arr_uint8_push(dyn_is_AND_IF, 1);
      } else if (tok_type == TOKTYPE_OR_IF) {
         dyn_arr_uint8_push(dyn_is_AND_IF, 0);
      } else {
         psh_unget_token(cur_env);
         break;
      }

      uint64_t nb_newlines = parse_linebreak(cur_env);

      if (cur_env->is_interactive) {
         while ((node_pipeline = parse_pipeline(cur_env)) == NULL) {
            if (IS_PARSE_ERR(node_pipeline)) goto bubble_error;
            psh_secondary_prompt(cur_env);
         }
      } else {
         node_pipeline = parse_pipeline(cur_env);
         if (!node_pipeline) {
            shell_language_syntax_error(cur_env); goto bubble_error;
         } else if (IS_PARSE_ERR(node_pipeline)) goto bubble_error;
      }

      dyn_arr_node_push(nodes_pipeline, node_pipeline);
   }

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));
   ret_node->node_type = PSH_NODE_AND_OR;
   ret_node->node_and_or.nodes_pipeline = nodes_pipeline;

   if (dyn_is_AND_IF->size == 0) {
      dyn_arr_uint8_free(dyn_is_AND_IF);
      free(dyn_is_AND_IF);
   } else {
      ret_node->node_and_or.dyn_is_AND_IF = dyn_is_AND_IF;
   }

   return ret_node;

bubble_error:
   dyn_arr_node_free(nodes_pipeline, true);
   dyn_arr_uint8_free(dyn_is_AND_IF);
   return NODE_ERROR;
}

struct psh_node*
parse_pipeline(struct shell_env* cur_env) {
/*
pipeline : pipe_sequence
         | Bang pipe_sequence
pipe_sequence : command
              | pipe_sequence '|' linebreak command
*/
   struct dyn_arr_node* nodes_command = NULL;

   enum token_type type = psh_get_token(cur_env);
   struct token *cur_tok = dyn_arr_tok_point_last(cur_env->tok_arr);
   bool has_bang = false;

   type = get_reserved_word(cur_tok);
   if (type == TOKTYPE_Bang) {
      has_bang = true;
   } else {
      psh_unget_token(cur_env);
   }

   struct psh_node* node_command = parse_command(cur_env);
   if (!node_command) {
      if (has_bang) //Maybe an error?
         psh_unget_token(cur_env);
      return NULL;
   } else if (IS_PARSE_ERR(node_command)) goto bubble_error;
   
   nodes_command = dyn_arr_node_create();
   dyn_arr_node_push(nodes_command, node_command);

   while (1) {
      enum token_type tok_type = psh_get_token(cur_env);
      if (tok_type != TOKTYPE_PIPE) {
         psh_unget_token(cur_env);
         break;
      }

      uint64_t nb_newlines = parse_linebreak(cur_env);
      
      if (cur_env->is_interactive) {
         while ((node_command = parse_command(cur_env)) == NULL) {
            if (IS_PARSE_ERR(node_command)) goto bubble_error;
            //Is this asking more input or is this a pipe error?
            psh_secondary_prompt(cur_env);
         }
      } else {
         node_command = parse_command(cur_env);
         if (!node_command) {
            shell_language_syntax_error(cur_env); goto bubble_error;
         } else if (IS_PARSE_ERR(node_command)) goto bubble_error;
      }

      dyn_arr_node_push(nodes_command, node_command);
   }

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));

   ret_node->node_type = PSH_NODE_PIPELINE;
   ret_node->node_pipeline.nodes_command = nodes_command;
   ret_node->node_pipeline.is_banged = has_bang;

   return ret_node;

bubble_error:
   dyn_arr_node_free(nodes_command, true);
   return NODE_ERROR;
}

struct psh_node*
parse_command(struct shell_env* cur_env) {
/*
command : simple_command
        | compound_command
        | compound_command redirect_list
        | function_definition
*/

   struct psh_node* node_function_definition = parse_function(cur_env);
   if (node_function_definition) {
      if (IS_PARSE_ERR(node_function_definition)) goto bubble_error;
      struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));
      ret_node->node_type = PSH_NODE_COMMAND;
      ret_node->node_command = (struct psh_node_command) {
         .node_function = node_function_definition,
         .node_type = PSH_NODE_FUNCTION
      };
      return ret_node;
   }

   struct psh_node* node_simple_command = parse_simple_command(cur_env);
   if (node_simple_command) {
      if (IS_PARSE_ERR(node_simple_command)) goto bubble_error;
      struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));
      ret_node->node_type = PSH_NODE_COMMAND;
      ret_node->node_command = (struct psh_node_command) {
         .node_simple_command = node_simple_command,
         .node_type = PSH_NODE_SIMPLE_COMMAND
      };
      return ret_node;
   }

   struct psh_node* node_compound_command = parse_compound_command(cur_env);
   if (node_compound_command) {
      if (IS_PARSE_ERR(node_compound_command)) goto bubble_error;
      struct dyn_arr_node* node_redirect_list = parse_redirect_list(cur_env);
      if (IS_PARSE_ERR(node_redirect_list)) {
         psh_node_free(node_compound_command, true); goto bubble_error;
      }
      struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));
      ret_node->node_type = PSH_NODE_COMMAND;
      ret_node->node_command = (struct psh_node_command) {
         .node_compound_command = node_compound_command,
         .nodes_io_redirect = node_redirect_list,
         .node_type = PSH_NODE_COMPOUND_COMMAND
      };
      return ret_node;
   }

   return NULL;

bubble_error:
   return NODE_ERROR;
}

struct psh_node*
parse_compound_command(struct shell_env* cur_env) {
/*
compound_command : brace_group
                 | subshell
                 | for_clause
                 | case_clause
                 | if_clause
                 | while_clause
                 | until_clause
*/
   struct dyn_arr_tok* tok_arr = cur_env->tok_arr;

   struct psh_node* node_brace_group = parse_brace_group(cur_env);
   if (node_brace_group) {
      if (IS_PARSE_ERR(node_brace_group)) goto bubble_error;
      struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));
      ret_node->node_type = PSH_NODE_COMPOUND_COMMAND;
      ret_node->node_compound_command = (struct psh_node_compound_command) {
         .node_brace_group = node_brace_group,
         .node_type = PSH_NODE_BRACE_GROUP
      };
      return ret_node;
   }

   struct psh_node* node_subshell = parse_subshell(cur_env);
   if (node_subshell) {
      if (IS_PARSE_ERR(node_subshell)) goto bubble_error;
      struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));
      ret_node->node_type = PSH_NODE_COMPOUND_COMMAND;
      ret_node->node_compound_command = (struct psh_node_compound_command) {
         .node_subshell = node_subshell,
         .node_type = PSH_NODE_SUBSHELL
      };
      return ret_node;
   }

   struct psh_node* node_for_clause = parse_for_clause(cur_env);
   if (node_for_clause) {
      if (IS_PARSE_ERR(node_for_clause)) goto bubble_error;
      struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));
      ret_node->node_type = PSH_NODE_COMPOUND_COMMAND;
      ret_node->node_compound_command = (struct psh_node_compound_command) {
         .node_for_clause = node_for_clause,
         .node_type = PSH_NODE_FOR_CLAUSE
      };
      return ret_node;
   }

   struct psh_node* node_case_clause = parse_case_clause(cur_env);
   if (node_case_clause) {
      if (IS_PARSE_ERR(node_case_clause)) goto bubble_error;
      struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));
      ret_node->node_type = PSH_NODE_COMPOUND_COMMAND;
      ret_node->node_compound_command = (struct psh_node_compound_command) {
         .node_case_clause = node_case_clause,
         .node_type = PSH_NODE_CASE_CLAUSE
      };
      return ret_node;
   }

   struct psh_node* node_if_clause = parse_if_clause(cur_env);
   if (node_if_clause) {
      if (IS_PARSE_ERR(node_if_clause)) goto bubble_error;
      struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));
      ret_node->node_type = PSH_NODE_COMPOUND_COMMAND;
      ret_node->node_compound_command = (struct psh_node_compound_command) {
         .node_if_clause = node_if_clause,
         .node_type = PSH_NODE_IF_CLAUSE
      };
      return ret_node;
   }

   struct psh_node* node_loop = parse_loop(cur_env);
   if (node_loop) {
      if (IS_PARSE_ERR(node_loop)) goto bubble_error;
      struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));
      ret_node->node_type = PSH_NODE_COMPOUND_COMMAND;
      ret_node->node_compound_command = (struct psh_node_compound_command) {
         .node_loop = node_loop,
         .node_type = PSH_NODE_LOOP
      };
      return ret_node;
   }

   return NULL;

bubble_error:
   return NODE_ERROR;
}

struct psh_node*
parse_subshell(struct shell_env* cur_env) {
/*
subshell         : '(' compound_list ')'
*/
   enum token_type tok_type = psh_get_token(cur_env);
   if (tok_type != TOKTYPE_LPAREN) {
      psh_unget_token(cur_env);
      return NULL;
   }

   struct psh_node* node_compound_list = NULL;
   if (cur_env->is_interactive) {
      while (1) {
         while (!(node_compound_list = parse_compound_list(cur_env))) {
            if (IS_PARSE_ERR(node_compound_list)) goto bubble_error;
            psh_secondary_prompt(cur_env);
         }
         tok_type = psh_get_token(cur_env);
         if (tok_type != TOKTYPE_RPAREN) {
            psh_unget_token(cur_env);
            unget_node(cur_env, node_compound_list);
            node_compound_list = NULL;
            psh_secondary_prompt(cur_env);
            continue;
         }
         break;
      }
   } else {
      node_compound_list = parse_compound_list(cur_env);
      if (!node_compound_list) {
         shell_language_syntax_error(cur_env); goto bubble_error;
      } else if (IS_PARSE_ERR(node_compound_list)) goto bubble_error;
      tok_type = psh_get_token(cur_env);
      if (tok_type != TOKTYPE_RPAREN) {
         psh_unget_token(cur_env);
         psh_node_free(node_compound_list, true);
         shell_language_syntax_error(cur_env); goto bubble_error;
      }
   }

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));

   ret_node->node_type = PSH_NODE_SUBSHELL;
   ret_node->node_subshell = (struct psh_node_subshell) {
      .node_compound_list = node_compound_list
   };

   return ret_node;

bubble_error:
   return NODE_ERROR;
}

struct psh_node*
parse_compound_list(struct shell_env* cur_env) {
/*
compound_list    : linebreak term
                 | linebreak term separator
*/
   size_t index = (cur_input(cur_env)->cur_pos == 0) ? 0 : cur_input(cur_env)->cur_pos - 1;

   uint64_t nb_newlines = parse_linebreak(cur_env);

   struct psh_node* node_term = parse_term(cur_env);
   if (!node_term) {
      //TODO push back the linebreak
      return NULL;
   } else if (IS_PARSE_ERR(node_term)) goto bubble_error;

   struct psh_node* node_separator = NULL;

   size_t nb_separators = (node_term->node_term.nodes_separator == NULL) ? 0 : node_term->node_term.nodes_separator->size;
   size_t nb_and_ors_term = node_term->node_term.nodes_and_or->size;
   //bubble up the extra separator node parsed by the term (simpler that way)
   if (nb_separators >= nb_and_ors_term) {
      node_separator = dyn_arr_node_pop(node_term->node_term.nodes_separator);
      DEBUG_ASSERT(node_separator);
   } else {
      node_separator = parse_separator(cur_env);
   }

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));

   ret_node->node_type = PSH_NODE_COMPOUND_LIST;
   ret_node->begin_index = index;
   ret_node->node_compound_list.node_term = node_term;
   ret_node->node_compound_list.node_separator = node_separator;

   return ret_node;

bubble_error:
   return NODE_ERROR;
}

struct psh_node*
parse_term(struct shell_env* cur_env) {
/*
term             : term separator and_or
                 |                and_or
*/
   struct dyn_arr_node* nodes_separator = NULL;
   struct dyn_arr_node* nodes_and_or = NULL;

   struct psh_node* node_and_or = parse_and_or(cur_env);
   if (!node_and_or) return NULL;
   else if (IS_PARSE_ERR(node_and_or)) goto bubble_error;

   nodes_separator = dyn_arr_node_create();
   nodes_and_or = dyn_arr_node_create();
   dyn_arr_node_push(nodes_and_or, node_and_or);

   struct psh_node* node_separator;
   while ((node_separator = parse_separator(cur_env))) {
      dyn_arr_node_push(nodes_separator, node_separator);

      //if there is no and_or, the separator node will be bubbled up to the compound_list
      if ((node_and_or = parse_and_or(cur_env))) {
         if (IS_PARSE_ERR(node_and_or)) goto bubble_error;
         dyn_arr_node_push(nodes_and_or, node_and_or);
      } else break;
   }

   if (nodes_separator->size == 0) {
      dyn_arr_node_free(nodes_separator, true);
      free(nodes_separator);
      nodes_separator = NULL;
   }

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));
   ret_node->node_type = PSH_NODE_TERM;
   ret_node->node_term.nodes_separator = nodes_separator;
   ret_node->node_term.nodes_and_or = nodes_and_or;
   return ret_node;

bubble_error:
   dyn_arr_node_free(nodes_and_or, true);
   dyn_arr_node_free(nodes_separator, true);
   return NODE_ERROR;
}

struct psh_node*
parse_for_clause(struct shell_env* cur_env) {
/*
for_clause       : For name                                      do_group
                 | For name                       sequential_sep do_group
                 | For name linebreak in          sequential_sep do_group
                 | For name linebreak in wordlist sequential_sep do_group
*/
   struct dyn_arr_tok* tok_arr = cur_env->tok_arr;

   enum token_type tok_type = psh_get_token(cur_env);
   struct token *cur_tok = dyn_arr_tok_point_last(tok_arr);
   tok_type = get_reserved_word(cur_tok);

   if (tok_type != TOKTYPE_For) {
      psh_unget_token(cur_env);
      return NULL;
   }

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));
   ret_node->node_type = PSH_NODE_FOR_CLAUSE;

   char* name_str = parse_name(cur_env);
   if (!name_str) {
      //TODO ask for more if EOF, else syntax error
      if (cur_env->is_interactive) {

      } else {
         
      }
   }

   assert(name_str);
   ret_node->node_for_clause.name = name_str;

   struct psh_node* node_do_group = parse_do_group(cur_env);
   if (node_do_group) {
      ret_node->node_for_clause.node_do_group = node_do_group;

      return ret_node;
   }

   struct psh_node* node_sequential_sep = parse_sequential_sep(cur_env);
   if (node_sequential_sep) {
      if (cur_env->is_interactive) {
         while ( !(node_do_group = parse_do_group(cur_env)) ) {
            psh_secondary_prompt(cur_env);
         }
      } else {
         node_do_group = parse_do_group(cur_env);
         if (!node_do_group) {
            shell_language_syntax_error(cur_env); goto bubble_error;
         }
      }
      psh_node_free(node_sequential_sep, true);
      ret_node->node_for_clause.node_do_group = node_do_group;
      return ret_node;
   }

   uint64_t nb_newlines = parse_linebreak(cur_env);

   enum token_type tok_type_in;

   if (cur_env->is_interactive) {
      while ((tok_type_in = psh_get_token(cur_env)) == TOKTYPE_EOF) {
         psh_secondary_prompt(cur_env);
      }
   } else {
      tok_type_in = psh_get_token(cur_env);
   }

   cur_tok = dyn_arr_tok_point_last(tok_arr);
   tok_type_in = get_reserved_word(cur_tok);

   if (tok_type_in != TOKTYPE_In) {
      psh_unget_token(cur_env);
      shell_language_syntax_error(cur_env); goto bubble_error;
   }

   struct dyn_arr_str* dyn_wordlist = parse_wordlist(cur_env);

   if (cur_env->is_interactive) {
      while (1) {
         node_sequential_sep = parse_sequential_sep(cur_env);
         if (!node_sequential_sep) {
            tok_type = psh_get_token(cur_env);
            if (tok_type == TOKTYPE_EOF) {
               psh_secondary_prompt(cur_env);
               continue;
            }
            //TODO Syntax error
            assert(0);
         }

         node_do_group = parse_do_group(cur_env);
         if (node_do_group)
            break;
         
         tok_type = psh_get_token(cur_env);
         if (tok_type == TOKTYPE_EOF) {
            psh_unget_token(cur_env);
            unget_node(cur_env, node_sequential_sep);
            node_sequential_sep = NULL;
            psh_secondary_prompt(cur_env);
            continue;
         } else {
            //TODO Syntax error
            assert(0);
         }   
      }
   } else {
      node_sequential_sep = parse_sequential_sep(cur_env);
      if (!node_sequential_sep) {
         shell_language_syntax_error(cur_env); goto bubble_error;
      }

      node_do_group = parse_do_group(cur_env);
      if (!node_do_group) {
         shell_language_syntax_error(cur_env); goto bubble_error;
      }
   }

   ret_node->node_for_clause.dyn_wordlist = dyn_wordlist;
   ret_node->node_for_clause.node_do_group = node_do_group;
   return ret_node;

bubble_error:
   //TODO
   return NODE_ERROR;
}

char*
parse_name(struct shell_env* cur_env) {
/*
name             : NAME                     // Apply rule 5
*/
   struct dyn_arr_tok* tok_arr = cur_env->tok_arr;

   enum token_type tok_type = psh_get_token(cur_env);
   if (tok_type == TOKTYPE_EOF) {
      psh_unget_token(cur_env);
      return NULL;
   }

   struct token *cur_tok = dyn_arr_tok_point_last(tok_arr);

   if (!is_posix_name(cur_tok->psh_str->str)) {
      psh_unget_token(cur_env);
      return NULL;
   }
   char* ret_str = psh_string_to_cstr(cur_tok->psh_str, false);

   return ret_str;
}

char*
parse_wordlist_word(struct shell_env* cur_env) {
   struct dyn_arr_tok* tok_arr = cur_env->tok_arr;

   enum token_type tok_type = psh_get_token(cur_env);
   if (tok_type == TOKTYPE_EOF) {
      psh_unget_token(cur_env);
      return NULL;
   }

   struct token *cur_tok = dyn_arr_tok_point_last(tok_arr);
   tok_type = get_reserved_word(cur_tok);
   if (tok_type != TOKTYPE_WORD) {
      psh_unget_token(cur_env);
      return NULL;
   }

   char* ret_str = psh_string_to_cstr(cur_tok->psh_str, false);

   return ret_str;
}

struct dyn_arr_str*
parse_wordlist(struct shell_env* cur_env) {
/*
wordlist         : wordlist WORD
                 |          WORD
*/
   char* cur_word = parse_wordlist_word(cur_env);
   if (!cur_word) {
      psh_unget_token(cur_env);
      return NULL;
   }

   struct dyn_arr_str* dyn_wordlist = dyn_arr_str_create();

   do dyn_arr_str_push(dyn_wordlist, cur_word, false);
   while ((cur_word = parse_wordlist_word(cur_env)));

   return dyn_wordlist;
}

struct psh_node*
parse_case_clause(struct shell_env* cur_env) {
/*
case_clause      : Case WORD linebreak in linebreak case_list    Esac
                 | Case WORD linebreak in linebreak case_list_ns Esac
                 | Case WORD linebreak in linebreak              Esac
*/
   struct dyn_arr_tok* tok_arr = cur_env->tok_arr;

   enum token_type tok_type = psh_get_token(cur_env);
   struct token *cur_tok = dyn_arr_tok_point_last(tok_arr);
   tok_type = get_reserved_word(cur_tok);
   if (tok_type != TOKTYPE_Case) {
      psh_unget_token(cur_env);
      return NULL;
   }

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));
   ret_node->node_type = PSH_NODE_CASE_CLAUSE;

   //TODO check if it's the same kind of word
   char* word_str = parse_wordlist_word(cur_env);
   //TODO ask for more
   assert(word_str);
   ret_node->node_case_clause.word_str = word_str;

   uint64_t nb_newlines = parse_linebreak(cur_env);

   tok_type = psh_get_token(cur_env);
   cur_tok = dyn_arr_tok_point_last(tok_arr);
   tok_type = get_reserved_word(cur_tok);
   if (tok_type != TOKTYPE_In) {
      //TODO ask for input
      assert(0);
   }

   nb_newlines = parse_linebreak(cur_env);

   tok_type = psh_get_token(cur_env);
   cur_tok = dyn_arr_tok_point_last(tok_arr);
   tok_type = get_reserved_word(cur_tok);
   if (tok_type == TOKTYPE_Esac) {
      return ret_node;
   }
   psh_unget_token(cur_env);

   struct dyn_arr_node* nodes_case_item = parse_case_list(cur_env);

   if (!nodes_case_item) {
      if (cur_env->is_interactive) {
         //TODO try to recover from parse error
         assert(0);
      } else {
         shell_language_syntax_error(cur_env); goto bubble_error;
      }
   }

   ret_node->node_case_clause.nodes_case_item = nodes_case_item;

   tok_type = psh_get_token(cur_env);
   cur_tok = dyn_arr_tok_point_last(tok_arr);
   tok_type = get_reserved_word(cur_tok);
   if (tok_type != TOKTYPE_Esac) {
      //TODO ask for more
      assert(0);
   }
   
   return ret_node;

bubble_error:
   //TODO
   return NODE_ERROR;
}

struct dyn_arr_node*
parse_case_list(struct shell_env* cur_env) {
/*
case_list        : case_list case_item
                 |           case_item
*/
   struct psh_node* cur_node_case_item = parse_case_item(cur_env);
   if (!cur_node_case_item)
      return NULL;

   struct dyn_arr_node* ret_arr = dyn_arr_node_create();
   
   do {
      dyn_arr_node_push(ret_arr, cur_node_case_item);
      if (cur_node_case_item->node_case_item.is_ns) break;
   } while ((cur_node_case_item = parse_case_item(cur_env)));

   return ret_arr;
}

struct psh_node*
parse_case_item(struct shell_env* cur_env) {
/*
case_item        :     pattern ')' linebreak     DSEMI linebreak
                 |     pattern ')' compound_list DSEMI linebreak
                 | '(' pattern ')' linebreak     DSEMI linebreak
                 | '(' pattern ')' compound_list DSEMI linebreak
                 ;
*/
   enum token_type tok_type = psh_get_token(cur_env);
   bool parsed_lparen = true;
   if (tok_type != TOKTYPE_LPAREN) {
      psh_unget_token(cur_env);
      parsed_lparen = false;
   }

   struct dyn_arr_str* pattern_strs = parse_pattern(cur_env);
   if (!pattern_strs) {
      if (parsed_lparen) {
         //TODO not sure
         psh_unget_token(cur_env);
         assert(0);
      }
      return NULL;
   }

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));
   ret_node->node_type = PSH_NODE_CASE_ITEM;
   ret_node->node_case_item.dyn_patterns = pattern_strs;

   tok_type = psh_get_token(cur_env);
   if (tok_type != TOKTYPE_RPAREN) {
      if (cur_env->is_interactive) {
         //TODO try to recover from parse error
         assert(0);
      } else {
         psh_unget_token(cur_env);
         shell_language_syntax_error(cur_env); goto bubble_error;
      }
   }

   struct psh_node* node_compound_list = parse_compound_list(cur_env);
   if (!node_compound_list) {
      parse_linebreak(cur_env);
   }
   ret_node->node_case_item.node_compound_list = node_compound_list;

   tok_type = psh_get_token(cur_env);
   if (tok_type != TOKTYPE_DSEMI) {
      struct token* cur_tok = dyn_arr_tok_point_last(cur_env->tok_arr);
      tok_type = get_reserved_word(cur_tok);
      //TODO maybe unnecessary check?
      if (tok_type == TOKTYPE_Esac) {
         psh_unget_token(cur_env);
         ret_node->node_case_item.is_ns = true;
         //set it to PSH_NODE_CASE_ITEM_NS?
         ret_node->node_type = PSH_NODE_CASE_ITEM;
         return ret_node;
      } else {
         //TODO deal with it
         assert(0);
      }
   }

   parse_linebreak(cur_env);

   return ret_node;

bubble_error:
   //TODO
   return NODE_ERROR;
}

char*
parse_pattern_word(struct shell_env* cur_env) {
   //TODO check if that's the correct parsing function
   return parse_word_7b(cur_env);
}

struct dyn_arr_str*
parse_pattern(struct shell_env* cur_env) {
/*
pattern          :             WORD         // Apply rule 4
                 | pattern '|' WORD         // Do not apply rule 4
*/
   //if no '|' and pattern is esac, the pattern matching fails
   char* cur_word = parse_pattern_word(cur_env);
   if (!cur_word)
      return NULL;
   if (!strcmp(cur_word, "esac")) {
      psh_unget_token(cur_env);
      free(cur_word);
      return NULL;
   }

   struct dyn_arr_str* dyn_strs = dyn_arr_str_create();
   dyn_arr_str_push(dyn_strs, cur_word, false);
      
   enum token_type tok_type;
   while (1) {
      tok_type = psh_get_token(cur_env);
      if (tok_type != TOKTYPE_PIPE) {
         psh_unget_token(cur_env);
         break;
      }
      cur_word = parse_pattern_word(cur_env);
      if (!cur_word) {
         //TODO ask for more
         assert(0);
      }
      dyn_arr_str_push(dyn_strs, cur_word, false);
   }

   return dyn_strs;
}

struct psh_node*
parse_if_clause(struct shell_env* cur_env) {
/*
if_clause        : If compound_list Then compound_list else_part Fi
                 | If compound_list Then compound_list           Fi
*/
   struct psh_node* node_compound_list_if = NULL;
   struct psh_node* node_compound_list_then = NULL;
   struct psh_node* node_else_part = NULL;

   struct dyn_arr_tok* tok_arr = cur_env->tok_arr;

   enum token_type tok_type = psh_get_token(cur_env);
   struct token *cur_tok = dyn_arr_tok_point_last(tok_arr);

   tok_type = get_reserved_word(cur_tok);
   if (tok_type != TOKTYPE_If) {
      psh_unget_token(cur_env);
      return NULL;
   }


   if (cur_env->is_interactive) {
      while (1) {
         while (!(node_compound_list_if = parse_compound_list(cur_env))) {
            psh_secondary_prompt(cur_env);
         }
         if (IS_PARSE_ERR(node_compound_list_if)) goto bubble_error;
         tok_type = psh_get_token(cur_env);
         cur_tok = dyn_arr_tok_point_last(tok_arr);
         tok_type = get_reserved_word(cur_tok);
         if (tok_type != TOKTYPE_Then) {
            psh_unget_token(cur_env);
            unget_node(cur_env, node_compound_list_if);
            node_compound_list_if = NULL;
            psh_secondary_prompt(cur_env);
            continue;
         }
         break;
      }
   } else {
      node_compound_list_if = parse_compound_list(cur_env);
      if (!node_compound_list_if) {
         shell_language_syntax_error(cur_env); goto bubble_error;
      } else if (IS_PARSE_ERR(node_compound_list_if)) goto bubble_error;
      tok_type = psh_get_token(cur_env);
      cur_tok = dyn_arr_tok_point_last(tok_arr);
      tok_type = get_reserved_word(cur_tok);
      if (tok_type != TOKTYPE_Then) {
         psh_unget_token(cur_env);
         shell_language_syntax_error(cur_env); goto bubble_error;
      }
   }

   if (cur_env->is_interactive) {
      while (1) {
         while (!(node_compound_list_then = parse_compound_list(cur_env))) {
            psh_secondary_prompt(cur_env);
         }
         if (IS_PARSE_ERR(node_compound_list_then)) goto bubble_error;
         tok_type = psh_get_token(cur_env);
         cur_tok = dyn_arr_tok_point_last(tok_arr);
         tok_type = get_reserved_word(cur_tok);
         if (tok_type == TOKTYPE_Fi) {
            break;
         }
         psh_unget_token(cur_env);
         
         if (tok_type != TOKTYPE_Elif && tok_type != TOKTYPE_Else) {
            unget_node(cur_env, node_compound_list_then);
            node_compound_list_then = NULL;
            psh_secondary_prompt(cur_env);
            continue;
         }

         //TODO ensure that it works fine
else_part_parsing:
         while (!(node_else_part = parse_else_part(cur_env))) {
            psh_secondary_prompt(cur_env);
         }
         if (IS_PARSE_ERR(node_else_part)) goto bubble_error;
         tok_type = psh_get_token(cur_env);
         cur_tok = dyn_arr_tok_point_last(tok_arr);
         tok_type = get_reserved_word(cur_tok);
         if (tok_type != TOKTYPE_Fi) {
            unget_node(cur_env, node_else_part);
            node_else_part = NULL;
            psh_secondary_prompt(cur_env);
            goto else_part_parsing;
         }
         
         break;
      }
   } else {
      node_compound_list_then = parse_compound_list(cur_env);
      if (!node_compound_list_then) {
         shell_language_syntax_error(cur_env); goto bubble_error;
      } else if (IS_PARSE_ERR(node_compound_list_then)) goto bubble_error;
      node_else_part = parse_else_part(cur_env);
      if (IS_PARSE_ERR(node_else_part)) goto bubble_error; 
      tok_type = psh_get_token(cur_env);
      cur_tok = dyn_arr_tok_point_last(tok_arr);
      tok_type = get_reserved_word(cur_tok);
      if (tok_type != TOKTYPE_Fi) {
         psh_unget_token(cur_env);
         shell_language_syntax_error(cur_env); goto bubble_error;
      }
   }

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));
   ret_node->node_type = PSH_NODE_IF_CLAUSE;
   ret_node->node_if_clause = (struct psh_node_if_clause) {
      .node_compound_list_if = node_compound_list_if,
      .node_compound_list_then = node_compound_list_then,
      .node_else_part = node_else_part
   };

   return ret_node;

bubble_error:
   psh_node_free(node_compound_list_if, true);
   psh_node_free(node_compound_list_then, true);
   psh_node_free(node_else_part, true);

   return NODE_ERROR;
}

struct psh_node*
parse_else_part(struct shell_env* cur_env) {
/*
else_part        : Elif compound_list Then compound_list
                 | Elif compound_list Then compound_list else_part
                 | Else compound_list
ajouter else fi for paul lol
*/
   struct dyn_arr_tok* tok_arr = cur_env->tok_arr;

   enum token_type tok_type = psh_get_token(cur_env);
   struct token *cur_tok = dyn_arr_tok_point_last(tok_arr);
   tok_type = get_reserved_word(cur_tok);
   psh_unget_token(cur_env);
   if (tok_type != TOKTYPE_Elif && tok_type != TOKTYPE_Else) {
      return NULL;
   }

   size_t index = cur_input(cur_env)->cur_pos;

   struct dyn_arr_node* nodes_compound_list_elif = dyn_arr_node_create();
   struct dyn_arr_node* nodes_compound_list_body = dyn_arr_node_create();

   while (1) {
      enum token_type tok_type_else_elif = psh_get_token(cur_env);
      cur_tok = dyn_arr_tok_point_last(tok_arr);
      tok_type_else_elif = get_reserved_word(cur_tok);
      if (tok_type_else_elif != TOKTYPE_Else && tok_type_else_elif != TOKTYPE_Elif) {
         psh_unget_token(cur_env);
         break;
      }

      struct psh_node* node_compound_list_elif = NULL;
      struct psh_node* node_compound_list_body = NULL;
      if (tok_type_else_elif == TOKTYPE_Elif) {
         if (cur_env->is_interactive) {
            while (1) {
               while (!(node_compound_list_elif = parse_compound_list(cur_env))) {
                  psh_secondary_prompt(cur_env);
               }
               tok_type = psh_get_token(cur_env);
               cur_tok = dyn_arr_tok_point_last(tok_arr);
               tok_type = get_reserved_word(cur_tok);
               if (tok_type != TOKTYPE_Then) {
                  psh_unget_token(cur_env);
                  unget_node(cur_env, node_compound_list_elif);
                  node_compound_list_elif = NULL;
                  psh_secondary_prompt(cur_env);
                  continue;
               }
               break;
            }
         } else {
            //TODO Shell language syntax error: shall exit with diagnostic
            node_compound_list_elif = parse_compound_list(cur_env);
            if (!node_compound_list_elif) {
               assert(0);
            }
            tok_type = psh_get_token(cur_env);
            cur_tok = dyn_arr_tok_point_last(tok_arr);
            tok_type = get_reserved_word(cur_tok);
            if (tok_type != TOKTYPE_Then) {
               psh_unget_token(cur_env);
               shell_language_syntax_error(cur_env); goto bubble_error;
            }
         }

         if (cur_env->is_interactive) {
            while (!(node_compound_list_body = parse_compound_list(cur_env))) {
               psh_secondary_prompt(cur_env);
            }
         } else {
            node_compound_list_body = parse_compound_list(cur_env);
            if (!node_compound_list_body) {
               shell_language_syntax_error(cur_env); goto bubble_error;
            }
         }

         dyn_arr_node_push(nodes_compound_list_elif, node_compound_list_elif);
         dyn_arr_node_push(nodes_compound_list_body, node_compound_list_body);
      } else {
         //parse else
         while (!(node_compound_list_body = parse_compound_list(cur_env))) {
            psh_secondary_prompt(cur_env);
         }
         dyn_arr_node_push(nodes_compound_list_body, node_compound_list_body);
         break;
      }
   }

   assert(nodes_compound_list_body->size != 0);

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));
   ret_node->node_type = PSH_NODE_ELSE_PART;
   ret_node->begin_index = index;
   ret_node->node_else_part.nodes_compound_list_elif = nodes_compound_list_elif;
   ret_node->node_else_part.nodes_compound_list_body = nodes_compound_list_body;


   return ret_node;

bubble_error:
   /* psh_node_free(node_compound_list, true); */

   return NODE_ERROR;
}
   
struct psh_node*
parse_loop(struct shell_env* cur_env) {
/*
while_clause     : While compound_list do_group
until_clause     : Until compound_list do_group
*/
   struct dyn_arr_tok* tok_arr = cur_env->tok_arr;
   struct psh_node* node_compound_list = NULL;
   struct psh_node* node_do_group = NULL;

   enum token_type tok_type = psh_get_token(cur_env);
   struct token *cur_tok = dyn_arr_tok_point_last(cur_env->tok_arr);
   tok_type = get_reserved_word(cur_tok);
   if (tok_type != TOKTYPE_While && tok_type != TOKTYPE_Until) {
      psh_unget_token(cur_env);
      return NULL;
   }

   enum token_type first_tok_type = tok_type;

   if (cur_env->is_interactive) {
      while (1) {
         while (!(node_compound_list = parse_compound_list(cur_env))) {
            psh_secondary_prompt(cur_env);
         }
         if (IS_PARSE_ERR(node_compound_list)) goto bubble_error;
         tok_type = psh_get_token(cur_env);
         cur_tok = dyn_arr_tok_point_last(tok_arr);
         tok_type = get_reserved_word(cur_tok);
         if (tok_type != TOKTYPE_Do) {
            psh_unget_token(cur_env);
            unget_node(cur_env, node_compound_list);
            node_compound_list = NULL;
            psh_secondary_prompt(cur_env);
            continue;
         }
         break;
      }
   } else {
      node_compound_list = parse_compound_list(cur_env);
      if (!node_compound_list) {
         shell_language_syntax_error(cur_env); goto bubble_error;
      } else if (IS_PARSE_ERR(node_compound_list)) goto bubble_error;
      tok_type = psh_get_token(cur_env);
      cur_tok = dyn_arr_tok_point_last(tok_arr);
      tok_type = get_reserved_word(cur_tok);
      if (tok_type != TOKTYPE_Do) {
         psh_unget_token(cur_env);
         shell_language_syntax_error(cur_env); goto bubble_error;
      }
   }
   psh_unget_token(cur_env);


   if (cur_env->is_interactive) {
      //TODO: actually need to do more complex error checking
      while (!(node_do_group = parse_do_group(cur_env))) {
         psh_secondary_prompt(cur_env);
      }
      if (IS_PARSE_ERR(node_do_group)) goto bubble_error;
   } else {
      node_do_group = parse_do_group(cur_env);
      if (!node_do_group) {
         shell_language_syntax_error(cur_env); goto bubble_error;
      }
   }

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));

   ret_node->node_type = PSH_NODE_LOOP;
   ret_node->node_loop = (struct psh_node_loop) {
      .node_compound_list = node_compound_list,
      .node_do_group = node_do_group,
      .is_while_clause = (first_tok_type == TOKTYPE_While)
   };

   return ret_node;

bubble_error:
   psh_node_free(node_compound_list, true);
   psh_node_free(node_do_group, true);

   return NODE_ERROR;
}

struct psh_node*
parse_function(struct shell_env* cur_env) {
/*
function_definition : fname '(' ')' linebreak function_body
function_body    : compound_command                // Apply rule 9
                 | compound_command redirect_list  // Apply rule 9
*/
   struct psh_node* node_compound_command = NULL;
   struct dyn_arr_node* nodes_io_redirect = NULL;

   char* fname_str = parse_fname(cur_env);
   if (!fname_str)
      return NULL;

   enum token_type tok_type = psh_get_token(cur_env);
   if (tok_type != TOKTYPE_LPAREN) {
      psh_unget_token(cur_env);
      psh_unget_token(cur_env);
      free(fname_str);
      return NULL;
   }

   tok_type = psh_get_token(cur_env);
   if (tok_type != TOKTYPE_RPAREN) {
      if (cur_env->is_interactive) {
         //TODO Syntax error, the RPAREN HAS to be there
         assert(0);
      } else {
         psh_unget_token(cur_env);
         shell_language_syntax_error(cur_env); goto bubble_error;
      }
   }

   uint64_t nb_newlines = parse_linebreak(cur_env);

   //TODO handle better
   while (!(node_compound_command = parse_compound_command(cur_env))) {
      psh_secondary_prompt(cur_env);
   }
   if (IS_PARSE_ERR(node_compound_command)) goto bubble_error;
   nodes_io_redirect = parse_redirect_list(cur_env);
   if (nodes_io_redirect == NODE_ERROR) goto bubble_error;
   
   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));
   ret_node->node_type = PSH_NODE_FUNCTION;
   ret_node->node_function = (struct psh_node_function) {
      .function_name = fname_str,
      .node_compound_command = node_compound_command,
      .nodes_io_redirect = nodes_io_redirect
   };
   return ret_node;

bubble_error:
   free(fname_str);
   psh_node_free(node_compound_command, true);
   dyn_arr_node_free(nodes_io_redirect, true);

   return NODE_ERROR;
}

char*
parse_fname(struct shell_env* cur_env) {
/*
fname            : NAME   // Apply rule 8
*/
   enum token_type tok_type = psh_get_token(cur_env);
   if (tok_type != TOKTYPE_TOKEN) {
      psh_unget_token(cur_env);
      return NULL;
   }

   struct token *cur_tok = dyn_arr_tok_point_last(cur_env->tok_arr);
   char* tok_str = cur_tok->psh_str->str;

   if (!is_posix_name(tok_str)) {
      psh_unget_token(cur_env);
      return NULL;
   }

   return psh_string_to_cstr(cur_tok->psh_str, false);
}

char*
parse_word_7a(struct shell_env* cur_env) {
/*
word            : WORD  // Apply rule 7a
*/
   enum token_type tok_type = psh_get_token(cur_env);
   if (tok_type != TOKTYPE_TOKEN) {
      psh_unget_token(cur_env);
      return NULL;
   }
   struct token *cur_tok = dyn_arr_tok_point_last(cur_env->tok_arr);

   if (strchr(cur_tok->psh_str->str, '=') != NULL) {
      psh_unget_token(cur_env);
      return parse_word_7b(cur_env);
   } else {
      //Rule 1
      cur_tok->type = get_reserved_word(cur_tok);
      return psh_string_to_cstr(cur_tok->psh_str, false);
   }
}


char*
parse_word_7b(struct shell_env* cur_env) {
/*
word            : WORD  // Apply rule 7b
*/
   enum token_type tok_type = psh_get_token(cur_env);
   if (tok_type != TOKTYPE_TOKEN) {
      psh_unget_token(cur_env);
      return NULL;
   }
   struct token *cur_tok = dyn_arr_tok_point_last(cur_env->tok_arr);
   if (cur_tok->psh_str->str[0] == '=') {
      cur_tok->type = get_reserved_word(cur_tok);
      return psh_string_to_cstr(cur_tok->psh_str, false);
   }

   const char* equal_sign = strchr(cur_tok->psh_str->str, '=');
   if (equal_sign != NULL) {
      struct psh_string* name_pre_equals = psh_string_create(NULL);
      for (const char* c = cur_tok->psh_str->str; c < equal_sign; c++) {
         pstr_append(name_pre_equals, *c);
      }
      if (is_posix_name(name_pre_equals->str)) {
         cur_tok->type = TOKTYPE_ASSIGNMENT_WORD;
         psh_string_free(name_pre_equals);
         return psh_string_to_cstr(cur_tok->psh_str, false);
      } else {
         //Posix standard says either ASSIGNMENT_WORD or apply rule 1
         //I choose rule 1
         psh_string_free(name_pre_equals);
         cur_tok->type = get_reserved_word(cur_tok);
         return psh_string_to_cstr(cur_tok->psh_str, false);
      }
   } else {
      return psh_string_to_cstr(cur_tok->psh_str, false);
   }
}

char*
parse_assignment_word(struct shell_env* cur_env) {
   char* word = parse_word_7b(cur_env);
   if (!word) {
      return NULL;
   }

   struct token *cur_tok = dyn_arr_tok_point_last(cur_env->tok_arr);
   if (cur_tok->type != TOKTYPE_ASSIGNMENT_WORD) {
      psh_unget_token(cur_env);
      free(word);
      return NULL;
   }

   return word;
}

struct psh_node*
parse_brace_group(struct shell_env* cur_env) {
/*
brace_group      : Lbrace compound_list Rbrace
*/
   struct dyn_arr_tok* tok_arr = cur_env->tok_arr;

   enum token_type tok_type = psh_get_token(cur_env);
   struct token *cur_tok = dyn_arr_tok_point_last(tok_arr);
   tok_type = get_reserved_word(cur_tok);
   if (tok_type != TOKTYPE_Lbrace) {
      psh_unget_token(cur_env);
      return NULL;
   }

   struct psh_node* node_compound_list = NULL;
   if (cur_env->is_interactive) {
      while (1) {
         while (!(node_compound_list = parse_compound_list(cur_env))) {
            psh_secondary_prompt(cur_env);
         }
         tok_type = psh_get_token(cur_env);
         cur_tok = dyn_arr_tok_point_last(tok_arr);
         tok_type = get_reserved_word(cur_tok);
         if (tok_type != TOKTYPE_Rbrace) {
            psh_unget_token(cur_env);
            unget_node(cur_env, node_compound_list);
            node_compound_list = NULL;
            psh_secondary_prompt(cur_env);
            continue;
         }
         break;
      }
   } else {
      node_compound_list = parse_compound_list(cur_env);
      if (!node_compound_list) {
         shell_language_syntax_error(cur_env);
      } else if (IS_PARSE_ERR(node_compound_list)) goto bubble_error;
      tok_type = psh_get_token(cur_env);
      cur_tok = dyn_arr_tok_point_last(tok_arr);
      tok_type = get_reserved_word(cur_tok);
      if (tok_type != TOKTYPE_Rbrace) {
         psh_unget_token(cur_env);
         shell_language_syntax_error(cur_env);
      }
   }
   

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));
   
   ret_node->node_type = PSH_NODE_BRACE_GROUP;
   ret_node->node_brace_group = (struct psh_node_brace_group) {
      .node_compound_list = node_compound_list
   };

   return ret_node;

bubble_error:
   psh_node_free(node_compound_list, true);
   return NODE_ERROR;
}

struct psh_node*
parse_do_group(struct shell_env* cur_env) {
/*
do_group         : Do compound_list Done           // Apply rule 6
*/
   struct dyn_arr_tok* tok_arr = cur_env->tok_arr;

   enum token_type tok_type = psh_get_token(cur_env);
   struct token *cur_tok = dyn_arr_tok_point_last(tok_arr);
   tok_type = get_reserved_word(cur_tok);
   if (tok_type != TOKTYPE_Do) {
      psh_unget_token(cur_env);
      return NULL;
   }

   struct psh_node* node_compound_list = NULL;
   if (cur_env->is_interactive) {
      while (1) {
         while (!(node_compound_list = parse_compound_list(cur_env))) {
            psh_secondary_prompt(cur_env);
         }
         tok_type = psh_get_token(cur_env);
         cur_tok = dyn_arr_tok_point_last(tok_arr);
         tok_type = get_reserved_word(cur_tok);
         if (tok_type != TOKTYPE_Done) {
            psh_unget_token(cur_env);
            unget_node(cur_env, node_compound_list);
            node_compound_list = NULL;
            psh_secondary_prompt(cur_env);
            continue;
         }
         break;
      }
   } else {
      node_compound_list = parse_compound_list(cur_env);
      if (!node_compound_list) {
         shell_language_syntax_error(cur_env); goto bubble_error;
      } else if (IS_PARSE_ERR(node_compound_list)) goto bubble_error;
      tok_type = psh_get_token(cur_env);
      cur_tok = dyn_arr_tok_point_last(tok_arr);
      tok_type = get_reserved_word(cur_tok);
      if (tok_type != TOKTYPE_Done) {
         psh_unget_token(cur_env);
         shell_language_syntax_error(cur_env); goto bubble_error;
      }
   }

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));

   ret_node->node_type = PSH_NODE_DO_GROUP;
   ret_node->node_do_group.node_compound_list = node_compound_list;
   return ret_node;

bubble_error:
   psh_node_free(node_compound_list, true);
   return NODE_ERROR;
}

// TODO: only a control operator will end a simple command
// perform all kinds of expansions
struct psh_node*
parse_simple_command(struct shell_env* cur_env) {
/*
simple_command   : cmd_prefix cmd_word cmd_suffix
                 | cmd_prefix cmd_word
                 | cmd_prefix
                 | cmd_name cmd_suffix
                 | cmd_name
*/
   struct psh_node* node_cmd_prefix = parse_cmd_prefix(cur_env);
   struct psh_node* node_cmd_word = NULL;
   struct psh_node* node_cmd_name = NULL;
   struct psh_node* node_cmd_suffix = NULL;

   struct psh_node* ret_node = NULL;


   if (IS_PARSE_ERR(node_cmd_prefix)) goto bubble_error;
   else if (node_cmd_prefix) {
      node_cmd_word = parse_cmd_word(cur_env);
      if (IS_PARSE_ERR(node_cmd_word)) goto bubble_error;
      else if (node_cmd_word) 
         if ((IS_PARSE_ERR((node_cmd_suffix = parse_cmd_suffix(cur_env))))) goto bubble_error;
   } else {
      node_cmd_name = parse_cmd_name(cur_env);
      if (IS_PARSE_ERR(node_cmd_name)) goto bubble_error;
      else if (!node_cmd_name)
         return NULL;

      node_cmd_suffix = parse_cmd_suffix(cur_env);
      if (IS_PARSE_ERR(node_cmd_suffix)) goto bubble_error;
   }

   ret_node = psh_calloc(sizeof(struct psh_node));
   ret_node->node_type = PSH_NODE_SIMPLE_COMMAND;

   if (node_cmd_word) {
      ret_node->node_simple_command = (struct psh_node_simple_command) {
         .node_cmd_prefix = node_cmd_prefix,
         .node_cmd_word = node_cmd_word,
         .node_cmd_suffix = node_cmd_suffix,
         .is_cmd_word = true
      };
   } else {
      ret_node->node_simple_command = (struct psh_node_simple_command) {
         .node_cmd_prefix = node_cmd_prefix,
         .node_cmd_name = node_cmd_name,
         .node_cmd_suffix = node_cmd_suffix,
         .is_cmd_word = false
      };
   }

   if (!node_cmd_name && !node_cmd_word)
      return ret_node;

   //Unfortunately, since we have separate parsing and evaluation phases, we need to run 'alias' at parse time.
   //We can't change/reparse a node that's already been parsed and returned (not without major refactors at least), and we can only know an alias exists after running the alias command with arguments
   //We also still need to write on stdout/err during eval time.
   //Therefore, we have two implementations of eval, one for parse time which only updates the alias assignments
   enum psh_error sym_alias_insert(struct sym_table* in_table, const char* name, const char* cmd);
   const char* command_name = (node_cmd_word) ? node_cmd_word->node_cmd_word.word_str : node_cmd_name->node_cmd_name.command_str /*or assignment_str*/;
   if (!strcmp(command_name, "alias")) {
      const struct dyn_arr_str* words_suffix = (node_cmd_suffix) ? node_cmd_suffix->node_cmd_suffix.words : NULL;
      size_t words_len = (words_suffix) ? words_suffix->size : 0;
      for (size_t i = 0; i < words_len; i++) {
         const char* cur_arg = words_suffix->strs[i];
         struct wordexp_t* wordexp_cmd = psh_calloc(sizeof(struct wordexp_t));
         int ret = wordexp(cur_env, (char*)cur_arg, wordexp_cmd, 0);
         const char* exp_var = psh_strdup((wordexp_cmd->we_wordc == 0) ? cur_arg : wordexp_cmd->we_wordv[0]);
         wordfree(wordexp_cmd);

         //check if it's an assignment
         const char* end_word = strchr(exp_var, '=');
         if (end_word) {
            // assign alias
            char* name_str = psh_calloc(end_word - exp_var + 1);
            memcpy(name_str, exp_var, end_word - exp_var);
            sym_alias_insert(cur_env->alias_table, name_str, (char*)end_word + 1);
            free(name_str);
         }

         free((void*)exp_var);
      }
   } else if (!strcmp(command_name, "unalias")) {
      const struct dyn_arr_str* words_suffix = (node_cmd_suffix) ? node_cmd_suffix->node_cmd_suffix.words : NULL;
      size_t words_len = (words_suffix) ? words_suffix->size : 0;

      if (words_len == 1 && !strcmp(words_suffix->strs[0], "-a")) {
         sym_table_empty(cur_env->alias_table);
      } else {
         for (size_t i = 0; i < words_len; i++) {
            const char* cur_arg = words_suffix->strs[i];
            struct wordexp_t* wordexp_cmd = psh_calloc(sizeof(struct wordexp_t));
            int ret = wordexp(cur_env, (char*)cur_arg, wordexp_cmd, 0);
            const char* exp_var = psh_strdup((wordexp_cmd->we_wordc == 0) ? cur_arg : wordexp_cmd->we_wordv[0]);
            wordfree(wordexp_cmd);

            char* alias_str = sym_get_alias_value(cur_env->alias_table, cur_arg);
            if (!alias_str) {
               //TODO work out how to deal at eval time?
            } else {
               sym_alias_delete(cur_env->alias_table, cur_arg);
            }

            free((void*)exp_var);
         }
      }
   }

   return ret_node;

bubble_error:
   psh_node_free(node_cmd_prefix, true);
   psh_node_free(node_cmd_word, true);
   psh_node_free(node_cmd_name, true);
   psh_node_free(node_cmd_suffix, true);
   psh_node_free(ret_node, true);

   return NODE_ERROR;
}

// NOTE: Tokens found in this context are checked for aliases
struct psh_node*
parse_cmd_name(struct shell_env* cur_env) {
/*
cmd_name         : WORD                   // Apply rule 7a
*/
   struct psh_node* ret_node = NULL;
   cur_env->_is_parsing_cmd_name = true;

   enum token_type tok_type = psh_get_token(cur_env);
   if (tok_type == TOKTYPE_EOF) {
      psh_unget_token(cur_env);
      goto exit_func;
   }

   struct token *cur_tok = dyn_arr_tok_point_last(cur_env->tok_arr);
   tok_type = get_reserved_word(cur_tok);
   if (tok_type != TOKTYPE_WORD) {
      psh_unget_token(cur_env);
      goto exit_func;
   }

   ret_node = psh_calloc(sizeof(struct psh_node));

   ret_node->node_type = PSH_NODE_CMD_NAME;

   char* tok_str = psh_strdup(cur_tok->psh_str->str);

   if (strchr(cur_tok->psh_str->str, '=')) {
      // TODO Apply rule 7b
      tok_type = TOKTYPE_ASSIGNMENT_WORD;
      cur_tok->type = tok_type;
      ret_node->node_cmd_name = (struct psh_node_cmd_name) {
         .assignment_str = tok_str,
         .is_assignment = true
      };
   } else {
      // Apply rule 1
      tok_type = get_reserved_word(cur_tok);
      cur_tok->type = tok_type;
      ret_node->node_cmd_name = (struct psh_node_cmd_name) {
         .command_str = tok_str,
         .is_assignment = false
      };
   }

exit_func:
   cur_env->_is_parsing_cmd_name = false;
   /* if (ret_node) */
      /* fprintf(stderr, "cmd_name: '%s'\n", ret_node->node_cmd_name.assignment_str); */
   return ret_node;

/* bubble_error: */
}

struct psh_node*
parse_cmd_word(struct shell_env* cur_env) {
/*
cmd_word         : WORD                   // Apply rule 7b
*/
   enum token_type type = psh_get_token(cur_env);

   if (type == TOKTYPE_EOF) {
      psh_unget_token(cur_env);
      return NULL;
   }

   struct token *cur_tok = dyn_arr_tok_point_last(cur_env->tok_arr);
   if (type != TOKTYPE_TOKEN) {
      psh_unget_token(cur_env);
      return NULL;
   }

   /*TODO this doesn't take quoting and substitution into account, implement that asap*/
   if (!strchr(cur_tok->psh_str->str, '=')) {
      psh_unget_token(cur_env);
      return NULL;
   }

   char* word_str = psh_calloc(cur_tok->psh_str->str_len + 1);
   strcpy(word_str, cur_tok->psh_str->str);

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));

   ret_node->node_type = PSH_NODE_CMD_WORD;
   ret_node->node_cmd_word = (struct psh_node_cmd_word) {
      .word_str = word_str
   };

   return ret_node;
}

struct psh_node*
parse_cmd_prefix(struct shell_env* cur_env) {
/*
cmd_prefix       :            io_redirect
                 | cmd_prefix io_redirect
                 |            ASSIGNMENT_WORD
                 | cmd_prefix ASSIGNMENT_WORD
*/
   //parse any amount of io_redirects or ASSIGNMENT_WORDS
   struct dyn_arr_node* nodes_io_redirect = NULL;
   struct dyn_arr_str* assignment_words = NULL;
   struct psh_node* node_io_redirect = parse_io_redirect(cur_env);
   char* assignment_word = NULL;
   if (!node_io_redirect) {
      assignment_word = parse_assignment_word(cur_env);
      if (!assignment_word) return NULL;
      else if (assignment_word == NODE_ERROR) goto bubble_error;
   } else if (IS_PARSE_ERR(node_io_redirect)) goto bubble_error;

   nodes_io_redirect = dyn_arr_node_create();
   assignment_words = dyn_arr_str_create();

   if (node_io_redirect) {
      dyn_arr_node_push(nodes_io_redirect, node_io_redirect);
   } else {
      dyn_arr_str_push(assignment_words, assignment_word, false);
   }

   while (1) {
      assignment_word = parse_assignment_word(cur_env);
      if (assignment_word == NODE_ERROR) goto bubble_error;
      else if (assignment_word) {
         dyn_arr_str_push(assignment_words, assignment_word, false);
         continue;
      }

      node_io_redirect = parse_io_redirect(cur_env);
      if (IS_PARSE_ERR(node_io_redirect)) goto bubble_error;
      else if (node_io_redirect) {
         dyn_arr_node_push(nodes_io_redirect, node_io_redirect);
         continue;
      }

      break;
   }

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));

   ret_node->node_type = PSH_NODE_CMD_PREFIX;
   ret_node->node_cmd_prefix.nodes_io_redirect = nodes_io_redirect;
   ret_node->node_cmd_prefix.assignment_words = assignment_words;

   return ret_node;

bubble_error:
   dyn_arr_node_free(nodes_io_redirect, true);
   dyn_arr_str_free(assignment_words, true);

   return NODE_ERROR;
}

struct psh_node*
parse_cmd_suffix(struct shell_env* cur_env) {
/*
cmd_suffix       :            io_redirect
                 | cmd_suffix io_redirect
                 |            WORD
                 | cmd_suffix WORD
*/
   struct dyn_arr_node* nodes_io_redirect = NULL;
   struct dyn_arr_str* words = NULL;
   struct psh_node* node_io_redirect = parse_io_redirect(cur_env);
   char* word = NULL;
   if (!node_io_redirect) {
      word = parse_word_7b(cur_env);
      if (!word) {
         return NULL;
      }
   } else if (IS_PARSE_ERR(node_io_redirect)) goto bubble_error;

   nodes_io_redirect = dyn_arr_node_create();
   words = dyn_arr_str_create();

   if (node_io_redirect) {
      dyn_arr_node_push(nodes_io_redirect, node_io_redirect);
   } else {
      dyn_arr_str_push(words, word, false);
   }

   while (1) {
      node_io_redirect = parse_io_redirect(cur_env);
      if (IS_PARSE_ERR(node_io_redirect)) goto bubble_error;
      if (node_io_redirect) {
         dyn_arr_node_push(nodes_io_redirect, node_io_redirect);
         continue;
      }

      word = parse_word_7b(cur_env);
      if (word) {
         dyn_arr_str_push(words, word, false);
         continue;
      }
      break;
   }

   if (nodes_io_redirect->size == 0) {
      dyn_arr_node_free(nodes_io_redirect, true);
      free(nodes_io_redirect);
      nodes_io_redirect = NULL;
   }

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));

   ret_node->node_type = PSH_NODE_CMD_SUFFIX;
   ret_node->node_cmd_suffix.nodes_io_redirect = nodes_io_redirect;
   ret_node->node_cmd_suffix.words = words;

   return ret_node;

bubble_error:
   dyn_arr_node_free(nodes_io_redirect, true);
   dyn_arr_str_free(words, true);

   return NODE_ERROR;
}

struct dyn_arr_node*
parse_redirect_list(struct shell_env* cur_env) {
/*
redirect_list    :               io_redirect
                 | redirect_list io_redirect
*/
   struct psh_node* node_io_redirect = parse_io_redirect(cur_env);
   if (!node_io_redirect)
      return NULL;
   
   struct dyn_arr_node* nodes_redirect_list = dyn_arr_node_create();

   do {
      if (IS_PARSE_ERR(node_io_redirect)) goto bubble_error;
      dyn_arr_node_push(nodes_redirect_list, node_io_redirect);
   } while ((node_io_redirect = parse_io_redirect(cur_env)));

   return nodes_redirect_list;

bubble_error:
   dyn_arr_node_free(nodes_redirect_list, true);
   return NODE_ERROR;
}

struct psh_node*
parse_io_redirect(struct shell_env* cur_env) {
/*
io_redirect      :           io_file
                 | IO_NUMBER io_file
                 |           io_here
                 | IO_NUMBER io_here
*/
   enum token_type tok_type = psh_get_token(cur_env);
   bool has_io_number = false;
   int64_t io_number = -1;

   if (tok_type != TOKTYPE_IO_NUMBER) {
      psh_unget_token(cur_env);
   } else {
      has_io_number = true;
      struct token *cur_tok = dyn_arr_tok_point_last(cur_env->tok_arr);
      io_number = strtoll(cur_tok->psh_str->str, NULL, 10);
   }

   struct psh_node* node_io_file = parse_io_file(cur_env);
   struct psh_node* node_io_here = NULL;
   struct psh_node* ret_node = NULL;

   if (node_io_file) {
      if (IS_PARSE_ERR(node_io_file)) goto bubble_error;
      ret_node = psh_calloc(sizeof(struct psh_node));
      ret_node->node_type = PSH_NODE_IO_REDIRECT;
      ret_node->node_io_redirect = (struct psh_node_io_redirect) {
         .node_io_file = node_io_file,
         .io_number = io_number,
         .is_heredoc = false
      };
   } else if ( (node_io_here = parse_io_here(cur_env)) ) {
      if (IS_PARSE_ERR(node_io_here)) goto bubble_error;
      ret_node = psh_calloc(sizeof(struct psh_node));
      ret_node->node_type = PSH_NODE_IO_REDIRECT;
      ret_node->node_io_redirect = (struct psh_node_io_redirect) {
         .node_io_here = node_io_here,
         .io_number = io_number,
         .is_heredoc = true,
      };
   } else {
      if (has_io_number)
         psh_unget_token(cur_env);
   }

   return ret_node;

bubble_error:

   return NODE_ERROR;
}

struct psh_node*
parse_io_file(struct shell_env* cur_env) {
/*
io_file          : '<'       filename
                 | LESSAND   filename
                 | '>'       filename
                 | GREATAND  filename
                 | DGREAT    filename
                 | LESSGREAT filename
                 | CLOBBER   filename
*/
   enum token_type type = psh_get_token(cur_env);

   if (type != TOKTYPE_LESSTHAN && type != TOKTYPE_LESSAND && type != TOKTYPE_GREATTHAN && type != TOKTYPE_GREATAND && type != TOKTYPE_DGREAT && type != TOKTYPE_LESSGREAT && type != TOKTYPE_CLOBBER) {
      psh_unget_token(cur_env);
      return NULL;
   }

   char* filename_str = parse_filename(cur_env);
   assert(filename_str);
   //TODO ask for filename

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));

   ret_node->node_type = PSH_NODE_IO_FILE;
   ret_node->node_io_file.redirect_op = type;
   ret_node->node_io_file.filename = filename_str;

   return ret_node;
}

char*
parse_filename(struct shell_env* cur_env) {
/*
filename         : WORD                      // Apply rule 2
*/
   enum token_type type = psh_get_token(cur_env);

   if (type != TOKTYPE_TOKEN) {
      psh_unget_token(cur_env);
      return NULL;
   }

   struct token *cur_tok = dyn_arr_tok_point_last(cur_env->tok_arr);

   return psh_strdup(psh_string_to_cstr(cur_tok->psh_str, false));
}

struct psh_node*
parse_io_here(struct shell_env* cur_env) {
/*
io_here          : DLESS     here_end
                 | DLESSDASH here_end
*/
   char* here_end = NULL;
   union wordexp_ret here_end_expanded_u = (union wordexp_ret) {NULL};

   enum token_type type = psh_get_token(cur_env);
   if (type != TOKTYPE_DLESS && type != TOKTYPE_DLESSDASH) {
      psh_unget_token(cur_env);
      return NULL;
   }
   bool is_dlessdash = (type == TOKTYPE_DLESSDASH);

   //parse the here_end
   if (cur_env->is_interactive) {
      while (1) {
         here_end = parse_wordlist_word(cur_env);
         if (here_end)
            break;
         enum token_type tok_type = psh_get_token(cur_env);
         if (psh_get_token(cur_env) == TOKTYPE_EOF) {
            psh_unget_token(cur_env);
            psh_secondary_prompt(cur_env);
         } else {
            shell_language_syntax_error(cur_env); goto bubble_error;
         }
      }
   } else {
      here_end = parse_wordlist_word(cur_env);
      if (!here_end) {
         shell_language_syntax_error(cur_env); goto bubble_error;
      }
   }

   here_end_expanded_u = expand(cur_env, here_end, EXP_QUOTE);
   char* here_end_expanded = here_end_expanded_u.charp;
   
   struct token* heredoc_token = NULL;
   if (cur_env->is_interactive) {
      bool retried = false;
      while (1) {
         heredoc_token = psh_tokenise_heredoc(cur_env, here_end_expanded, is_dlessdash);
         if (heredoc_token)
            break;
         if (!retried) {
            pstr_append(cur_input(cur_env)->psh_str, '\n');
            retried = true;
         }
         psh_secondary_prompt(cur_env);
         pstr_append(cur_input(cur_env)->psh_str, '\n');
      }
   } else {
      heredoc_token = psh_tokenise_heredoc(cur_env, here_end_expanded, is_dlessdash);
      if (!heredoc_token) {
         shell_language_syntax_error(cur_env); goto bubble_error;
      }
   }

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));
   ret_node->node_type = PSH_NODE_IO_HERE;
   ret_node->node_io_here = (struct psh_node_io_here) {
      .here_end = psh_strdup(here_end_expanded),
      .heredoc = psh_strdup(heredoc_token->psh_str->str),
      .is_dlessdash = is_dlessdash,
   };

   return ret_node;

bubble_error:
   free(here_end);
   free(here_end_expanded_u.charp);

   return NODE_ERROR;
}

///Returns the amount of newlines parsed
///Failed means it returns 0
uint64_t
parse_newline_list(struct shell_env* cur_env) {
/*
newline_list     :              NEWLINE
                 | newline_list NEWLINE
*/
   enum token_type next_tok_type;

   size_t nb_newlines = 0;
   while ((next_tok_type = psh_get_token(cur_env)) == TOKTYPE_NEWLINE)
      nb_newlines++;

   psh_unget_token(cur_env);

   return nb_newlines;
}

///Returns the number of newlines parsed (can be 0)
///Can't fail
uint64_t
parse_linebreak(struct shell_env* cur_env) {
/*
linebreak        : newline_list
                 | // empty
*/
   uint64_t ret = parse_newline_list(cur_env);

   return ret;
}

struct psh_node*
parse_separator_op(struct shell_env* cur_env) {
/*
separator_op     : '&'
                 | ';'
*/
   enum token_type next_tok_type = psh_get_token(cur_env);
   if (next_tok_type != TOKTYPE_AND && next_tok_type != TOKTYPE_SEMICOLON) {
      psh_unget_token(cur_env);
      return NULL;
   }

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));

   ret_node->node_type = PSH_NODE_SEPARATOR_OP;
   ret_node->node_separator_op.type = next_tok_type;

   return ret_node;
}

struct psh_node*
parse_separator(struct shell_env* cur_env) {
/*
separator        : separator_op linebreak
                 | newline_list
*/
   uint64_t nb_newlines;
   struct psh_node* ret_node;
   struct psh_node* node_sep = parse_separator_op(cur_env);
   if (node_sep) {
      //parsed a linebreak and got the separator op
      uint64_t nb_newlines = parse_linebreak(cur_env);
      ret_node = psh_calloc(sizeof(struct psh_node));
      ret_node->node_type = PSH_NODE_SEPARATOR;
      ret_node->node_separator.type = node_sep->node_separator_op.type;
      psh_node_free(node_sep, true);
   } else if ((nb_newlines = parse_newline_list(cur_env) > 0)) {
      //parsed a newline_list
      ret_node = psh_calloc(sizeof(struct psh_node));
      ret_node->node_type = PSH_NODE_SEPARATOR;
      ret_node->node_separator.type = TOKTYPE_NEWLINE;
   } else {
      return NULL;
   }

   return ret_node;
}

struct psh_node*
parse_sequential_sep(struct shell_env* cur_env) {
/*
sequential_sep   : ';' linebreak
                 | newline_list
*/
   size_t index = cur_input(cur_env)->cur_pos;
   enum token_type tok_type = psh_get_token(cur_env);
   struct psh_node* ret_node;

   if (tok_type == TOKTYPE_SEMICOLON) {
      uint64_t nb_newlines = parse_linebreak(cur_env);
      ret_node = psh_calloc(sizeof(struct psh_node));
      ret_node->node_type = PSH_NODE_SEQUENTIAL_SEP;
      ret_node->node_sequential_sep = (struct psh_node_sequential_sep) {
         .nb_newlines = nb_newlines,
      };
      return ret_node;
   } else {
      psh_unget_token(cur_env);
   }

   uint64_t nb_newlines = parse_newline_list(cur_env);
   if (nb_newlines > 0) {
      ret_node = psh_calloc(sizeof(struct psh_node));
      ret_node->begin_index = index;
      ret_node->node_type = PSH_NODE_SEQUENTIAL_SEP;
      ret_node->node_sequential_sep.nb_newlines = nb_newlines;
      return ret_node;
   } else {
      return NULL;
   }
}

void
psh_node_free(struct psh_node* in_node, bool free_itself) {
   if (!in_node || in_node == NODE_ERROR)
      return;

   switch (in_node->node_type) {
      case PSH_NODE_UNKNOWN:
         fprintf(stderr, "Freeing a node of type PSH_UNKNOWN");
         DEBUG_ASSERT(0);
         return;
      case PSH_NODE_PROGRAM:
         DEBUG_ASSERT(in_node->node_program.node_complete_commands);
         psh_node_free(in_node->node_program.node_complete_commands, true);
         break;
      case PSH_NODE_COMPLETE_COMMANDS:
         DEBUG_ASSERT(in_node->node_complete_commands.nodes_complete_command);
         dyn_arr_node_free(in_node->node_complete_commands.nodes_complete_command, true);
         free(in_node->node_complete_commands.nodes_complete_command);
         break;
      case PSH_NODE_COMPLETE_COMMAND:
         DEBUG_ASSERT(in_node->node_complete_command.node_list);
         psh_node_free(in_node->node_complete_command.node_list, true);
         psh_node_free(in_node->node_complete_command.node_separator_op, true);
         break;
      case PSH_NODE_LIST:
         DEBUG_ASSERT(in_node->node_list.nodes_and_or);
         dyn_arr_node_free(in_node->node_list.nodes_and_or, true);
         free(in_node->node_list.nodes_and_or);
         dyn_arr_node_free(in_node->node_list.nodes_separator_op, true);
         free(in_node->node_list.nodes_separator_op);
         break;
      case PSH_NODE_AND_OR:
         DEBUG_ASSERT(in_node->node_and_or.nodes_pipeline);
         dyn_arr_node_free(in_node->node_and_or.nodes_pipeline, true);
         free(in_node->node_and_or.nodes_pipeline);
         if (in_node->node_and_or.dyn_is_AND_IF) {
            dyn_arr_uint8_free(in_node->node_and_or.dyn_is_AND_IF);
            free(in_node->node_and_or.dyn_is_AND_IF);
         }
         break;
      case PSH_NODE_PIPELINE:
         DEBUG_ASSERT(in_node->node_pipeline.nodes_command);
         dyn_arr_node_free(in_node->node_pipeline.nodes_command, true);
         free(in_node->node_pipeline.nodes_command);
         break;
      case PSH_NODE_COMMAND: {
         switch (in_node->node_command.node_type) {
            case PSH_NODE_SIMPLE_COMMAND:
               psh_node_free(in_node->node_command.node_simple_command, true);
               break;
            case PSH_NODE_COMPOUND_COMMAND:
               psh_node_free(in_node->node_command.node_compound_command, true);
               dyn_arr_node_free(in_node->node_command.nodes_io_redirect, true);
               free(in_node->node_command.nodes_io_redirect);
               break;
            case PSH_NODE_FUNCTION:
               psh_node_free(in_node->node_command.node_function, true);
               break;
            default:
               DEBUG_ASSERT(0);
               break;
         }
         break;
      }
      case PSH_NODE_COMPOUND_COMMAND:
         //node_brace_group refers to all of them as it's a union on pointers, so it can be any compound_command
         psh_node_free(in_node->node_compound_command.node_brace_group, true);
         break;
      case PSH_NODE_SUBSHELL:
         psh_node_free(in_node->node_subshell.node_compound_list, true);
         break;
      case PSH_NODE_COMPOUND_LIST:
         psh_node_free(in_node->node_compound_list.node_term, true);
         psh_node_free(in_node->node_compound_list.node_separator, true);
         break;
      case PSH_NODE_TERM:
         dyn_arr_node_free(in_node->node_term.nodes_and_or, true);
         free(in_node->node_term.nodes_and_or);
         dyn_arr_node_free(in_node->node_term.nodes_separator, true);
         free(in_node->node_term.nodes_separator);
         break;
      case PSH_NODE_FOR_CLAUSE:
         free(in_node->node_for_clause.name);
         dyn_arr_str_free(in_node->node_for_clause.dyn_wordlist, true);
         free(in_node->node_for_clause.dyn_wordlist);
         psh_node_free(in_node->node_for_clause.node_do_group, true);
         break;
      case PSH_NODE_CASE_CLAUSE:
         //TODO
         break;
      /* case PSH_NODE_CASE_LIST_NS: */
      /*    printf("Node of type "RED"PSH_NODE_CASE_LIST_NS"RESET", number %d:\n", in_node->node_type); */
      /*    break; */
      /* case PSH_NODE_CASE_LIST: */
      /*    printf("Node of type "RED"PSH_NODE_CASE_LIST"RESET", number %d:\n", in_node->node_type); */
      /*    break; */
      /* case PSH_NODE_CASE_ITEM_NS: */
      /*    printf("Node of type "RED"PSH_NODE_CASE_ITEM_NS"RESET", number %d:\n", in_node->node_type); */
      /*    break; */
      /* case PSH_NODE_CASE_ITEM: */
      /*    printf("Node of type "RED"PSH_NODE_CASE_ITEM"RESET", number %d:\n", in_node->node_type); */
      /*    break; */
      case PSH_NODE_PATTERN:
         //TODO
         break;
      case PSH_NODE_IF_CLAUSE:
         psh_node_free(in_node->node_if_clause.node_compound_list_if, true);
         psh_node_free(in_node->node_if_clause.node_compound_list_then, true);
         psh_node_free(in_node->node_if_clause.node_else_part, true);
         break;
      case PSH_NODE_ELSE_PART:
         dyn_arr_node_free(in_node->node_else_part.nodes_compound_list_elif, true);
         free(in_node->node_else_part.nodes_compound_list_elif);
         dyn_arr_node_free(in_node->node_else_part.nodes_compound_list_body, true);
         free(in_node->node_else_part.nodes_compound_list_body);
         break;
      case PSH_NODE_LOOP:
         psh_node_free(in_node->node_loop.node_compound_list, true);
         psh_node_free(in_node->node_loop.node_do_group, true);
         break;
      case PSH_NODE_FUNCTION:
         free(in_node->node_function.function_name);
         psh_node_free(in_node->node_function.node_compound_command, true);
         dyn_arr_node_free(in_node->node_function.nodes_io_redirect, true);
         free(in_node->node_function.nodes_io_redirect);
         break;
      case PSH_NODE_BRACE_GROUP:
         psh_node_free(in_node->node_brace_group.node_compound_list, true);
         break;
      case PSH_NODE_DO_GROUP:
         psh_node_free(in_node->node_do_group.node_compound_list, true);
         break;
      case PSH_NODE_SIMPLE_COMMAND:
         psh_node_free(in_node->node_simple_command.node_cmd_prefix, true);
         psh_node_free(in_node->node_simple_command.node_cmd_word, true); //can be name but doesn't matter
         psh_node_free(in_node->node_simple_command.node_cmd_suffix, true);
         break;
      case PSH_NODE_CMD_NAME:
         free(in_node->node_cmd_name.command_str); //can be assignment_str, doesn't matter
         break;
      case PSH_NODE_CMD_WORD:
         free(in_node->node_cmd_word.word_str); //can be assignment_str, doesn't matter
         break;
      case PSH_NODE_CMD_PREFIX:
         dyn_arr_node_free(in_node->node_cmd_prefix.nodes_io_redirect, true);
         free(in_node->node_cmd_prefix.nodes_io_redirect);
         dyn_arr_str_free(in_node->node_cmd_prefix.assignment_words, true);
         free(in_node->node_cmd_prefix.assignment_words);
         break;
      case PSH_NODE_CMD_SUFFIX:
         dyn_arr_node_free(in_node->node_cmd_suffix.nodes_io_redirect, true);
         free(in_node->node_cmd_suffix.nodes_io_redirect);
         dyn_arr_str_free(in_node->node_cmd_suffix.words, true);
         free(in_node->node_cmd_suffix.words);
         break;
      case PSH_NODE_IO_REDIRECT:
         if (in_node->node_io_redirect.is_heredoc)
            psh_node_free(in_node->node_io_redirect.node_io_here, true);
         else
            psh_node_free(in_node->node_io_redirect.node_io_file, true);
         break;
      case PSH_NODE_IO_FILE:
         free(in_node->node_io_file.filename);
         break;
      case PSH_NODE_IO_HERE:
         free(in_node->node_io_here.here_end);
         free(in_node->node_io_here.heredoc);
         break;
      case PSH_NODE_SEPARATOR_OP:
      case PSH_NODE_SEPARATOR:
      case PSH_NODE_SEQUENTIAL_SEP:
         break;
      default:
         break;
   }

   if (free_itself)
      free(in_node);
   else
      memset(in_node, 0, sizeof(struct psh_node));
}

struct dyn_arr_str*
dyn_arr_str_copy(const struct dyn_arr_str* in_arr) {
   if (!in_arr)
      return NULL;
   struct dyn_arr_str* ret_arr = dyn_arr_str_create();
   free(ret_arr->strs);
   ret_arr->strs = NULL;
   
   ret_arr->strs = psh_calloc(in_arr->capacity * sizeof(char*));
   for (size_t i = 0; i < in_arr->size; i++) {
      ret_arr->strs[i] = psh_strdup(in_arr->strs[i]);
   }

   ret_arr->size = in_arr->size;
   ret_arr->capacity = in_arr->capacity;

   return ret_arr;
}

struct dyn_arr_uint8*
dyn_arr_uint8_copy(const struct dyn_arr_uint8* in_arr) {
   if (!in_arr)
      return NULL;
   struct dyn_arr_uint8* ret_arr = dyn_arr_uint8_create();
   free(ret_arr->uint8s);
   ret_arr->uint8s = NULL;
   
   ret_arr->uint8s = psh_calloc(in_arr->capacity * sizeof(uint8_t));
   memcpy(ret_arr->uint8s, in_arr->uint8s, sizeof(uint8_t) * in_arr->size);

   ret_arr->size = in_arr->size;
   ret_arr->capacity = in_arr->capacity;

   return ret_arr;
}

struct dyn_arr_node*
dyn_arr_node_copy(const struct dyn_arr_node* in_arr) {
   if (!in_arr)
      return NULL;
   struct dyn_arr_node* ret_arr = dyn_arr_node_create();
   free(ret_arr->nodes);
   ret_arr->nodes = NULL;
   
   ret_arr->nodes = psh_calloc(in_arr->capacity * sizeof(struct dyn_arr_node*));
   for (size_t i = 0; i < in_arr->size; i++) {
      ret_arr->nodes[i] = psh_node_copy(in_arr->nodes[i]);
      assert(ret_arr->nodes[i]);
   }

   ret_arr->size = in_arr->size;
   ret_arr->capacity = in_arr->capacity;

   return ret_arr;
}

///TODO put more debug runtime checks
/// Performs a deep copy of a node
struct psh_node*
psh_node_copy(const struct psh_node* in_node) {
   if (!in_node)
      return NULL;

   struct psh_node* ret_node = psh_calloc(sizeof(struct psh_node));
   ret_node->node_type = in_node->node_type;

   switch (in_node->node_type) {
      case PSH_NODE_UNKNOWN:
         DEBUG_ASSERT(0);
         break;
      case PSH_NODE_PROGRAM:
         DEBUG_ASSERT(in_node->node_program.node_complete_commands);
         ret_node->node_program.node_complete_commands = psh_node_copy(in_node->node_program.node_complete_commands);
         break;
      case PSH_NODE_COMPLETE_COMMANDS:
         DEBUG_ASSERT(in_node->node_complete_commands.nodes_complete_command);
         ret_node->node_complete_commands.nodes_complete_command = dyn_arr_node_copy(in_node->node_complete_commands.nodes_complete_command);
         assert(ret_node->node_complete_commands.nodes_complete_command);
         break;
      case PSH_NODE_COMPLETE_COMMAND:
         DEBUG_ASSERT(in_node->node_complete_command.node_list);
         ret_node->node_complete_command.node_list = psh_node_copy(in_node->node_complete_command.node_list);
         ret_node->node_complete_command.node_separator_op = psh_node_copy(in_node->node_complete_command.node_separator_op);
         break;
      case PSH_NODE_LIST:
         DEBUG_ASSERT(in_node->node_list.nodes_and_or);
         ret_node->node_list.nodes_separator_op = dyn_arr_node_copy(in_node->node_list.nodes_and_or);
         ret_node->node_list.nodes_separator_op = dyn_arr_node_copy(in_node->node_list.nodes_and_or);
         break;
      case PSH_NODE_AND_OR:
         DEBUG_ASSERT(in_node->node_and_or.nodes_pipeline);
         ret_node->node_and_or.nodes_pipeline = dyn_arr_node_copy(in_node->node_and_or.nodes_pipeline);
         ret_node->node_and_or.dyn_is_AND_IF = dyn_arr_uint8_copy(in_node->node_and_or.dyn_is_AND_IF);
         break;
      case PSH_NODE_PIPELINE:
         DEBUG_ASSERT(in_node->node_pipeline.nodes_command);
         ret_node->node_pipeline.nodes_command = dyn_arr_node_copy(in_node->node_pipeline.nodes_command);
         ret_node->node_pipeline.is_banged = in_node->node_pipeline.is_banged;
         break;
      case PSH_NODE_COMMAND: {
         ret_node->node_command.node_type = in_node->node_command.node_type;
         switch (in_node->node_command.node_type) {
            case PSH_NODE_SIMPLE_COMMAND:
               ret_node->node_command.node_simple_command = psh_node_copy(in_node->node_command.node_simple_command);
               break;
            case PSH_NODE_COMPOUND_COMMAND:
               ret_node->node_command.node_compound_command = psh_node_copy(in_node->node_command.node_compound_command);
               ret_node->node_command.nodes_io_redirect = dyn_arr_node_copy(in_node->node_command.nodes_io_redirect);
               break;
            case PSH_NODE_FUNCTION:
               ret_node->node_command.node_function = psh_node_copy(in_node->node_command.node_function);
               break;
            default:
               DEBUG_ASSERT(0);
               break;
         }
         break;
      }
      case PSH_NODE_COMPOUND_COMMAND:
         //node_brace_group refers to all of them as it's a union on pointers, so it can be any compound_command
         ret_node->node_compound_command.node_type = in_node->node_compound_command.node_type;
         ret_node->node_compound_command.node_brace_group = psh_node_copy(in_node->node_compound_command.node_brace_group);
         break;
      case PSH_NODE_SUBSHELL:
         ret_node->node_subshell.node_compound_list = psh_node_copy(in_node->node_subshell.node_compound_list);
         break;
      case PSH_NODE_COMPOUND_LIST:
         ret_node->node_compound_list.node_term = psh_node_copy(in_node->node_compound_list.node_term);
         ret_node->node_compound_list.node_separator = psh_node_copy(in_node->node_compound_list.node_separator);
         break;
      case PSH_NODE_TERM:
         ret_node->node_term.nodes_and_or = dyn_arr_node_copy(in_node->node_term.nodes_and_or);
         ret_node->node_term.nodes_separator = dyn_arr_node_copy(in_node->node_term.nodes_separator);
         break;
      case PSH_NODE_FOR_CLAUSE:
         ret_node->node_for_clause.name = psh_strdup(in_node->node_for_clause.name);
         ret_node->node_for_clause.dyn_wordlist = dyn_arr_str_copy(in_node->node_for_clause.dyn_wordlist);
         ret_node->node_for_clause.node_do_group = psh_node_copy(in_node->node_for_clause.node_do_group);
         break;
      case PSH_NODE_CASE_CLAUSE:
         //TODO
         break;
      /* case PSH_NODE_CASE_LIST_NS: */
      /*    printf("Node of type "RED"PSH_NODE_CASE_LIST_NS"RESET", number %d:\n", in_node->node_type); */
      /*    break; */
      /* case PSH_NODE_CASE_LIST: */
      /*    printf("Node of type "RED"PSH_NODE_CASE_LIST"RESET", number %d:\n", in_node->node_type); */
      /*    break; */
      /* case PSH_NODE_CASE_ITEM_NS: */
      /*    printf("Node of type "RED"PSH_NODE_CASE_ITEM_NS"RESET", number %d:\n", in_node->node_type); */
      /*    break; */
      /* case PSH_NODE_CASE_ITEM: */
      /*    printf("Node of type "RED"PSH_NODE_CASE_ITEM"RESET", number %d:\n", in_node->node_type); */
      /*    break; */
      case PSH_NODE_PATTERN:
         //TODO
         break;
      case PSH_NODE_IF_CLAUSE:
         ret_node->node_if_clause.node_compound_list_if = psh_node_copy(in_node->node_if_clause.node_compound_list_if);
         ret_node->node_if_clause.node_compound_list_then = psh_node_copy(in_node->node_if_clause.node_compound_list_then);
         ret_node->node_if_clause.node_else_part = psh_node_copy(ret_node->node_if_clause.node_else_part);
         break;
      case PSH_NODE_ELSE_PART:
         ret_node->node_else_part.nodes_compound_list_elif = dyn_arr_node_copy(in_node->node_else_part.nodes_compound_list_elif);
         ret_node->node_else_part.nodes_compound_list_body = dyn_arr_node_copy(in_node->node_else_part.nodes_compound_list_body);
         break;
      case PSH_NODE_LOOP:
         ret_node->node_loop.node_compound_list = psh_node_copy(in_node->node_loop.node_compound_list);
         ret_node->node_loop.node_do_group = psh_node_copy(in_node->node_loop.node_do_group);
         ret_node->node_loop.is_while_clause = in_node->node_loop.is_while_clause;
         break;
      case PSH_NODE_FUNCTION:
         ret_node->node_function.function_name = psh_strdup(in_node->node_function.function_name);
         ret_node->node_function.node_compound_command = psh_node_copy(in_node->node_function.node_compound_command);
         ret_node->node_function.nodes_io_redirect = dyn_arr_node_copy(in_node->node_function.nodes_io_redirect);
         break;
      case PSH_NODE_BRACE_GROUP:
         ret_node->node_brace_group.node_compound_list = psh_node_copy(in_node->node_brace_group.node_compound_list);
         break;
      case PSH_NODE_DO_GROUP:
         ret_node->node_do_group.node_compound_list = psh_node_copy(in_node->node_do_group.node_compound_list);
         break;
      case PSH_NODE_SIMPLE_COMMAND:
         ret_node->node_simple_command.node_cmd_prefix = psh_node_copy(in_node->node_simple_command.node_cmd_prefix);
         ret_node->node_simple_command.node_cmd_word = psh_node_copy(in_node->node_simple_command.node_cmd_word); //either this or name
         ret_node->node_simple_command.node_cmd_suffix = psh_node_copy(in_node->node_simple_command.node_cmd_suffix);
         ret_node->node_simple_command.is_cmd_word = in_node->node_simple_command.is_cmd_word;
         break;
      case PSH_NODE_CMD_NAME:
         ret_node->node_cmd_name.assignment_str = psh_strdup(in_node->node_cmd_name.assignment_str); //either this or assignment
         ret_node->node_cmd_name.is_assignment = in_node->node_cmd_name.is_assignment;
         break;
      case PSH_NODE_CMD_WORD:
         ret_node->node_cmd_word.word_str = psh_strdup(in_node->node_cmd_word.word_str);
         break;
      case PSH_NODE_CMD_PREFIX:
         ret_node->node_cmd_prefix.assignment_words = dyn_arr_str_copy(in_node->node_cmd_prefix.assignment_words);
         ret_node->node_cmd_prefix.nodes_io_redirect = dyn_arr_node_copy(in_node->node_cmd_prefix.nodes_io_redirect);
         break;
      case PSH_NODE_CMD_SUFFIX:
         ret_node->node_cmd_suffix.words = dyn_arr_str_copy(in_node->node_cmd_suffix.words);
         ret_node->node_cmd_suffix.nodes_io_redirect = dyn_arr_node_copy(in_node->node_cmd_suffix.nodes_io_redirect);
         break;
      case PSH_NODE_IO_REDIRECT:
         ret_node->node_io_redirect.node_io_file = psh_node_copy(in_node->node_io_redirect.node_io_file); //either this or 'io_here'
         ret_node->node_io_redirect.io_number = in_node->node_io_redirect.io_number;
         ret_node->node_io_redirect.is_heredoc = in_node->node_io_redirect.is_heredoc;
         break;
      case PSH_NODE_IO_FILE:
         ret_node->node_io_file.filename = psh_strdup(in_node->node_io_file.filename);
         ret_node->node_io_file.redirect_op = in_node->node_io_file.redirect_op;
         break;
      case PSH_NODE_IO_HERE:
         ret_node->node_io_here.here_end = psh_strdup(in_node->node_io_here.here_end);
         ret_node->node_io_here.heredoc = psh_strdup(in_node->node_io_here.heredoc);
         ret_node->node_io_here.is_dlessdash = in_node->node_io_here.is_dlessdash;
         break;
      case PSH_NODE_SEPARATOR_OP:
         ret_node->node_separator_op.type = in_node->node_separator_op.type;
         break;
      case PSH_NODE_SEPARATOR:
         ret_node->node_separator.type = in_node->node_separator.type;
         break;
      case PSH_NODE_SEQUENTIAL_SEP:
         ret_node->node_sequential_sep.nb_newlines = in_node->node_sequential_sep.nb_newlines;
         break;
      default:
         DEBUG_ASSERT(0);
         break;
   }

   return ret_node;
}



#define RED ESC_S "[31m"
#define RESET ESC_S "[39m"

void
print_depth(int depth) {
   for (int i = 0; i < depth; i++)
      printf("   ");
}

void
debug_node_print_recursive(struct psh_node* input_node, int depth) {
   print_depth(depth);
   switch (input_node->node_type) {
      case PSH_NODE_UNKNOWN:
         printf("Node of type "RED"PSH_NODE_UNKNOWN"RESET", number %d:\n", input_node->node_type);
         break;

      case PSH_NODE_PROGRAM:
         printf("Node of type "RED"PSH_NODE_PROGRAM"RESET", number %d:\n", input_node->node_type);
         print_depth(depth);
         if (input_node->node_program.node_complete_commands) {
            printf("- node_complete_commands: %p:\n", (void*)input_node->node_program.node_complete_commands);
            debug_node_print_recursive(input_node->node_program.node_complete_commands, depth + 1);
         }
         else {
            printf("- node_complete_commands: NULL:\n");
         }
         break;

      case PSH_NODE_COMPLETE_COMMANDS:
      //TODO rewrite with new dynamic array
         printf("Node of type "RED"PSH_NODE_COMPLETE_COMMANDS"RESET", number %d:\n", input_node->node_type);
         /* print_depth(depth); */
         /* if (input_node->node_complete_commands.node_complete_command) { */
            /* printf("- node_complete_command: %p:\n", input_node->node_complete_commands.node_complete_command); */
            /* debug_node_print_recursive(input_node->node_complete_commands.node_complete_command, depth + 1); */
         /* } */
         /* else { */
            /* printf("- node_complete_commands: NULL:\n"); */
         /* } */

         /* print_depth(depth); */
         /* if (input_node->node_complete_commands.next_node_complete_commands) { */
            /* printf("# next_node_complete_commands: %p:\n", input_node->node_complete_commands.next_node_complete_commands); */
            /* debug_node_print_recursive(input_node->node_complete_commands.next_node_complete_commands, depth); */
         /* } */
         /* else { */
            /* printf("# next_node_complete_commands: NULL:\n"); */
         /* } */
         break;

      case PSH_NODE_COMPLETE_COMMAND:
         printf("Node of type "RED"PSH_NODE_COMPLETE_COMMAND"RESET", number %d:\n", input_node->node_type);
         print_depth(depth);
         printf("- node_list: %p:\n", (void*)input_node->node_complete_command.node_list);
         debug_node_print_recursive(input_node->node_complete_command.node_list, depth + 1);
         print_depth(depth);
         if (input_node->node_complete_command.node_separator_op) {
            printf("- node_separator_op: %p:\n", (void*)input_node->node_complete_command.node_separator_op);
         }
         else {
            printf("- node_separator_op: NULL:\n");
         }
         break;

      case PSH_NODE_LIST:
         printf("Node of type "RED"PSH_NODE_LIST"RESET", number %d:\n", input_node->node_type);
         /* print_depth(depth); */
         /* printf("- node_list: %p:\n", input_node->node_list.node_and_or); */
         /* debug_node_print_recursive(input_node->node_list.node_and_or, depth + 1); */
         /* print_depth(depth); */
         /* if (input_node->node_list.node_separator_op) { */
         /*    printf("- node_separator_op: %p:\n", input_node->node_list.node_separator_op); */
         /* } */
         /* else { */
         /*    printf("- node_separator_op: NULL:\n"); */
         /* } */
         /* print_depth(depth); */
         /* if (input_node->node_list.next_node_list) { */
         /*    printf("# next_node_list: %p:\n", input_node->node_list.next_node_list); */
         /*    debug_node_print_recursive(input_node->node_list.next_node_list, depth); */
         /* } */
         /* else { */
         /*    printf("# next_node_list: NULL:\n"); */
         /* } */
         break;

      case PSH_NODE_AND_OR:
         printf("Node of type "RED"PSH_NODE_AND_OR"RESET", number %d:\n", input_node->node_type);
         /* print_depth(depth); */
         /* printf("- node_pipeline: %p:\n", input_node->node_and_or.node_pipeline); */
         /* debug_node_print_recursive(input_node->node_and_or.node_pipeline, depth + 1); */
         
         /* print_depth(depth); */
         /* if (input_node->node_and_or.cur_operator == TOKTYPE_UNKNOWN) { */
         /*    printf("- cur_operator: TOKTYPE_UNKNOWN\n"); */
         /* } */
         /* else if (input_node->node_and_or.cur_operator == TOKTYPE_AND_IF) { */
         /*    printf("- cur_operator: TOKTYPE_AND_IF\n"); */
         /* } */
         /* else if (input_node->node_and_or.cur_operator == TOKTYPE_OR_IF) { */
         /*    printf("- cur_operator: TOKTYPE_OR_IF\n"); */
         /* } */
         /* else { */
         /*    printf("- cur_operator: Invalid value\n"); */
         /* } */

         /* print_depth(depth); */
         /* if (input_node->node_and_or.next_node_and_or) { */
         /*    printf("# next_node_and_or: %p:\n", input_node->node_and_or.next_node_and_or); */
         /*    debug_node_print_recursive(input_node->node_and_or.next_node_and_or, depth + 1); */
         /* } */
         /* else { */
         /*    printf("# next_node_and_or: NULL\n"); */
         /* } */
         break;

      case PSH_NODE_PIPELINE:
         printf("Node of type "RED"PSH_NODE_PIPELINE"RESET", number %d:\n", input_node->node_type);
         /* print_depth(depth); */
         /* printf("- node_pipe_sequence: %p:\n", input_node->node_pipeline.node_pipe_sequence); */
         /* debug_node_print_recursive(input_node->node_pipeline.node_pipe_sequence, depth + 1); */
         /* print_depth(depth); */
         /* printf("- is_banged: %s\n", (input_node->node_pipeline.is_banged) ? "true" : "false"); */
         break;

      case PSH_NODE_COMMAND:
         printf("Node of type "RED"PSH_NODE_COMMAND"RESET", number %d:\n", input_node->node_type);
         print_depth(depth);
         if (input_node->node_command.node_type == PSH_NODE_SIMPLE_COMMAND) {
            printf("- node_type: PSH_NODE_SIMPLE_COMMAND\n");
            print_depth(depth);
            printf("- node_simple_command: %p:\n", (void*)input_node->node_command.node_simple_command);
            debug_node_print_recursive(input_node->node_command.node_simple_command, depth + 1);
         }
         else if (input_node->node_command.node_type == PSH_NODE_COMPOUND_COMMAND) {
            printf("- node_type: PSH_NODE_COMPOUND_COMMAND\n");
            print_depth(depth);
            printf("- node_compound_command: %p:\n", (void*)input_node->node_command.node_compound_command);
            debug_node_print_recursive(input_node->node_command.node_compound_command, depth + 1);
            print_depth(depth);
            /* if (input_node->node_command.node_redirect_list) { */
            /*    printf("- node_redirect_list: %p:\n", input_node->node_command.node_redirect_list); */
            /*    debug_node_print_recursive(input_node->node_command.node_redirect_list, depth + 1); */
            /* } */
            /* else { */
            /*    printf("- node_redirect_list: NULL\n"); */
            /* } */
         }
         else if (input_node->node_command.node_type == PSH_NODE_FUNCTION) {
            printf("- node_type: PSH_NODE_FUNCTION_DEFINITION\n");
            print_depth(depth);
            printf("- node_function_definition: %p:\n", (void*)input_node->node_command.node_function);
            debug_node_print_recursive(input_node->node_command.node_function, depth + 1);
         }
         else {
            printf("- node_type: ERROR\n");
         }
         break;

      case PSH_NODE_COMPOUND_COMMAND:
         printf("Node of type "RED"PSH_NODE_COMPOUND_COMMAND"RESET", number %d:\n", input_node->node_type);

         print_depth(depth);
         enum node_type node_type = input_node->node_compound_command.node_type;
         if (node_type == PSH_NODE_BRACE_GROUP) {
            printf("- node_type: PSH_NODE_BRACE_GROUP\n");
            debug_node_print_recursive(input_node->node_compound_command.node_brace_group, depth + 1);
         }
         else if (node_type == PSH_NODE_SUBSHELL) {
            printf("- node_type: PSH_NODE_SUBSHELL\n");
            debug_node_print_recursive(input_node->node_compound_command.node_subshell, depth + 1);
         }
         else if (node_type == PSH_NODE_FOR_CLAUSE) {
            printf("- node_type: PSH_NODE_FOR_CLAUSE\n");
            debug_node_print_recursive(input_node->node_compound_command.node_for_clause, depth + 1);
         }
         else if (node_type == PSH_NODE_CASE_CLAUSE) {
            printf("- node_type: PSH_NODE_CASE_CLAUSE\n");
            debug_node_print_recursive(input_node->node_compound_command.node_case_clause, depth + 1);
         }
         else if (node_type == PSH_NODE_IF_CLAUSE) {
            printf("- node_type: PSH_NODE_IF_CLAUSE\n");
            debug_node_print_recursive(input_node->node_compound_command.node_if_clause, depth + 1);
         }
         else if (node_type == PSH_NODE_LOOP) {
            printf("- node_type: PSH_NODE_LOOP\n");
            debug_node_print_recursive(input_node->node_compound_command.node_loop, depth + 1);
         }
         else {
            printf("- node_type: "RED"ERROR"RESET"\n");
         }
         break;

      case PSH_NODE_SUBSHELL:
         printf("Node of type "RED"PSH_NODE_SUBSHELL"RESET", number %d:\n", input_node->node_type);
         break;

      case PSH_NODE_COMPOUND_LIST:
         printf("Node of type "RED"PSH_NODE_COMPOUND_LIST"RESET", number %d:\n", input_node->node_type);
         print_depth(depth);
         if (input_node->node_compound_list.node_term) {
            printf("- node_term: %p:\n", (void*)input_node->node_compound_list.node_term);
            debug_node_print_recursive(input_node->node_compound_list.node_term, depth + 1);
         }
         else {
            printf("- node_term: "RED"NULL"RESET"\n");
         }
         print_depth(depth);
         if (input_node->node_compound_list.node_separator) {
            printf("- node_separator: %p:\n", (void*)input_node->node_compound_list.node_separator);
            debug_node_print_recursive(input_node->node_compound_list.node_separator, depth + 1);
         }
         else {
            printf("- node_separator: NULL\n");
         }
         break;

      case PSH_NODE_TERM:
         printf("Node of type "RED"PSH_NODE_TERM"RESET", number %d:\n", input_node->node_type);
         /* print_depth(depth); */
         /* if (input_node->node_term.node_and_or) { */
         /*    printf("- node_and_or: %p:\n", input_node->node_term.node_and_or); */
         /*    debug_node_print_recursive(input_node->node_term.node_and_or, depth + 1); */
         /* } */
         /* else { */
         /*    printf("- node_and_or: "RED"NULL"RESET"\n"); */
         /* } */
         /* print_depth(depth); */
         /* if (input_node->node_term.node_separator) { */
         /*    printf("- node_separator: %p:\n", input_node->node_term.node_separator); */
         /*    debug_node_print_recursive(input_node->node_term.node_separator, depth + 1); */
         /* } */
         /* else { */
         /*    printf("- node_separator: NULL\n"); */
         /* } */
         /* print_depth(depth); */
         /* if (input_node->node_term.next_node_term) { */
         /*    printf("# next_node_term: %p:\n", input_node->node_term.next_node_term); */
         /*    debug_node_print_recursive(input_node->node_term.next_node_term, depth + 1); */
         /* } */
         /* else { */
         /*    printf("# next_node_term: NULL\n"); */
         /* } */
         break;

      case PSH_NODE_FOR_CLAUSE:
         printf("Node of type "RED"PSH_NODE_FOR_CLAUSE"RESET", number %d:\n", input_node->node_type);
         break;
      case PSH_NODE_CASE_CLAUSE:
         printf("Node of type "RED"PSH_NODE_CASE_CLAUSE"RESET", number %d:\n", input_node->node_type);
         break;
      /* case PSH_NODE_CASE_LIST_NS: */
      /*    printf("Node of type "RED"PSH_NODE_CASE_LIST_NS"RESET", number %d:\n", input_node->node_type); */
      /*    break; */
      /* case PSH_NODE_CASE_LIST: */
      /*    printf("Node of type "RED"PSH_NODE_CASE_LIST"RESET", number %d:\n", input_node->node_type); */
      /*    break; */
      /* case PSH_NODE_CASE_ITEM_NS: */
      /*    printf("Node of type "RED"PSH_NODE_CASE_ITEM_NS"RESET", number %d:\n", input_node->node_type); */
      /*    break; */
      /* case PSH_NODE_CASE_ITEM: */
      /*    printf("Node of type "RED"PSH_NODE_CASE_ITEM"RESET", number %d:\n", input_node->node_type); */
      /*    break; */
      case PSH_NODE_PATTERN:
         printf("Node of type "RED"PSH_NODE_PATTERN"RESET", number %d:\n", input_node->node_type);
         break;
      case PSH_NODE_IF_CLAUSE:
         printf("Node of type "RED"PSH_NODE_IF_CLAUSE"RESET", number %d:\n", input_node->node_type);
         break;
      case PSH_NODE_ELSE_PART:
         printf("Node of type "RED"PSH_NODE_ELSE_PART"RESET", number %d:\n", input_node->node_type);
         break;

      case PSH_NODE_LOOP:
         printf("Node of type "RED"PSH_NODE_WHILE_CLAUSE"RESET", number %d:\n", input_node->node_type);
         print_depth(depth);
         if (input_node->node_loop.node_compound_list) {
            printf("- node_compound_list: %p:\n", (void*)input_node->node_loop.node_compound_list);
            debug_node_print_recursive(input_node->node_loop.node_compound_list, depth + 1);
         }
         else {
            printf("- node_compound_list: "RED"NULL"RESET"\n");
         }
         print_depth(depth);
         if (input_node->node_loop.node_do_group) {
            printf("- node_do_group: %p:\n", (void*)input_node->node_loop.node_do_group);
            debug_node_print_recursive(input_node->node_loop.node_do_group, depth + 1);
         }
         else {
            printf("- node_do_group: "RED"NULL"RESET"\n");
         }
         break;

      case PSH_NODE_FUNCTION:
         printf("Node of type "RED"PSH_NODE_FUNCTION"RESET", number %d:\n", input_node->node_type);
         break;
      case PSH_NODE_BRACE_GROUP:
         printf("Node of type "RED"PSH_NODE_BRACE_GROUP"RESET", number %d:\n", input_node->node_type);
         break;

      case PSH_NODE_DO_GROUP:
         printf("Node of type "RED"PSH_NODE_DO_GROUP"RESET", number %d:\n", input_node->node_type);
         print_depth(depth);
         if (input_node->node_do_group.node_compound_list) {
            printf("- node_compound_list: %p:\n", (void*)input_node->node_do_group.node_compound_list);
            debug_node_print_recursive(input_node->node_do_group.node_compound_list, depth + 1);
         }
         else {
            printf("- node_compound_list: "RED"NULL"RESET"\n");
         }
         break;

      case PSH_NODE_SIMPLE_COMMAND:
         printf("Node of type "RED"PSH_NODE_SIMPLE_COMMAND"RESET", number %d:\n", input_node->node_type);
         print_depth(depth);
         if (input_node->node_simple_command.node_cmd_prefix) {
            printf("- node_cmd_prefix: %p:\n", (void*)input_node->node_simple_command.node_cmd_prefix);
            debug_node_print_recursive(input_node->node_simple_command.node_cmd_prefix, depth + 1);
         }
         else {
            printf("- node_cmd_prefix: NULL:\n");
         }
         print_depth(depth);
         if (input_node->node_simple_command.node_cmd_word) {
            printf("- node_cmd_word: %p:\n", (void*)input_node->node_simple_command.node_cmd_word);
            debug_node_print_recursive(input_node->node_simple_command.node_cmd_word, depth + 1);
         }
         else {
            printf("- node_cmd_word: NULL:\n");
         }
         print_depth(depth);
         if (input_node->node_simple_command.node_cmd_name) {
            printf("- node_cmd_name: %p:\n", (void*)input_node->node_simple_command.node_cmd_name);
            debug_node_print_recursive(input_node->node_simple_command.node_cmd_name, depth + 1);
         }
         else {
            printf("- node_cmd_name: NULL:\n");
         }
         print_depth(depth);
         if (input_node->node_simple_command.node_cmd_suffix) {
            printf("- node_cmd_suffix: %p:\n", (void*)input_node->node_simple_command.node_cmd_suffix);
            debug_node_print_recursive(input_node->node_simple_command.node_cmd_suffix, depth + 1);
         }
         else {
            printf("- node_cmd_suffix: NULL:\n");
         }
         break;

      case PSH_NODE_CMD_NAME:
         printf("Node of type "RED"PSH_NODE_CMD_NAME"RESET", number %d:\n", input_node->node_type);

         print_depth(depth);
         if (input_node->node_cmd_name.is_assignment) {
            printf("- assignment_str: '%s'\n", input_node->node_cmd_name.assignment_str);
            print_depth(depth);
            printf("- is_assignment: true\n");
         }
         else {
            printf("- command_str: '%s'\n", input_node->node_cmd_name.command_str);
            print_depth(depth);
            printf("- is_assignment: false\n");
         }
         break;

      case PSH_NODE_CMD_WORD:
         printf("Node of type "RED"PSH_NODE_CMD_WORD"RESET", number %d:\n", input_node->node_type);
         break;
      case PSH_NODE_CMD_PREFIX:
         printf("Node of type "RED"PSH_NODE_CMD_PREFIX"RESET", number %d:\n", input_node->node_type);
         break;

      case PSH_NODE_CMD_SUFFIX:
         printf("Node of type "RED"PSH_NODE_CMD_SUFFIX"RESET", number %d:\n", input_node->node_type);
         /* print_depth(depth); */
         /* if (input_node->node_cmd_suffix.is_io_redirect) { */
         /*    printf("- node_io_redirect, %p:\n", input_node->node_cmd_suffix.node_io_redirect); */
         /*    debug_node_print_recursive(input_node->node_cmd_suffix.node_io_redirect, depth + 1); */

         /*    print_depth(depth); */
         /*    printf("- is_io_redirect: true\n"); */
         /* } */
         /* else { */
         /*    printf("- word: '%s'\n", input_node->node_cmd_suffix.word); */
         /*    print_depth(depth); */
         /*    printf("- is_io_redirect: true\n"); */
         /* } */
         /* print_depth(depth); */
         /* if (input_node->node_cmd_suffix.next_node_cmd_suffix) { */
         /*    printf("# next_node_cmd_suffix, %p:\n", input_node->node_cmd_suffix.next_node_cmd_suffix); */
         /*    debug_node_print_recursive(input_node->node_cmd_suffix.next_node_cmd_suffix, depth + 1); */
         /* } */
         /* else { */
         /*    printf("# next_node_cmd_suffix: NULL\n"); */
         /* } */
         break;

      case PSH_NODE_IO_REDIRECT:
         printf("Node of type "RED"PSH_NODE_IO_REDIRECT"RESET", number %d:\n", input_node->node_type);
         print_depth(depth);
         if (input_node->node_io_redirect.is_heredoc) {
            printf("- node_io_here, %p:\n", (void*)input_node->node_io_redirect.node_io_here);
            debug_node_print_recursive(input_node->node_io_redirect.node_io_here, depth + 1);

            print_depth(depth);
            printf("- is_heredoc: true\n");
         }
         else {
            printf("- node_io_file, %p:\n", (void*)input_node->node_io_redirect.node_io_file);
            debug_node_print_recursive(input_node->node_io_redirect.node_io_file, depth + 1);
            print_depth(depth);
            printf("- is_heredoc: false\n");
         }

         print_depth(depth);
         printf("- io_number: %ld\n", input_node->node_io_redirect.io_number);
      break;

      case PSH_NODE_IO_FILE:
         printf("Node of type "RED"PSH_NODE_IO_FILE"RESET", number %d:\n", input_node->node_type);
         /* print_depth(); */
         break;

      case PSH_NODE_IO_HERE:
         printf("Node of type "RED"PSH_NODE_IO_HERE"RESET", number %d:\n", input_node->node_type);
         break;
      case PSH_NODE_SEPARATOR_OP:
         printf("Node of type "RED"PSH_NODE_SEPARATOR_OP"RESET", number %d:\n", input_node->node_type);
         break;
      case PSH_NODE_SEPARATOR:
         printf("Node of type "RED"PSH_NODE_SEPARATOR"RESET", number %d:\n", input_node->node_type);
         break;
      case PSH_NODE_SEQUENTIAL_SEP:
         printf("Node of type "RED"PSH_NODE_SEQUENTIAL_SEP"RESET", number %d:\n", input_node->node_type);
         break;
      default:
         printf("Node of type Unknown\n");
         break;
   }
}
