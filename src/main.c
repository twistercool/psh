#include "psh.h"


int
main(int argc, char** argv, char** env) {
   assert(argv); //for fanalyzer
   int ret_value = 0;

   struct shell_env cur_env = {0};
   init_shell_env(&cur_env, argc, argv, env);

   //parse args:
   // -c <arg>: interpret <arg>
   int c;
   bool is_cmd_str = false;

   while ((c = getopt(argc, argv, "abc:Cefimno:uvx")) != -1) {
      switch (c) {
         case 'a': cur_env.flag_allexport = true; break;
         case 'b': cur_env.flag_notify = true;    break;
         case 'c':
            is_cmd_str = true;
            cur_env.is_interactive = false;
            pstr_append(cur_input(&cur_env)->psh_str, optarg);
            break;
         case 'C': cur_env.flag_noclobber = true; break;
         case 'e': cur_env.flag_errexit = true;   break;
         case 'f': cur_env.flag_noglob = true;    break;
         case 'i': cur_env.is_interactive = true; break;
         case 'm': cur_env.flag_monitor = true;   break;
         case 'n': cur_env.flag_noexec = true;    break;
         case 'o': assert(0 && "TODO implement this"); break;
         case 'u': cur_env.flag_nounset = true;   break;
         case 'v': cur_env.flag_verbose = true;   break;
         case 'x': cur_env.flag_xtrace = true;    break;
         default:
            fprintf(stderr, "psh: unknown option: '%c'\n", c);
            /* perror("psh"); */
            break;
      }
   }

   //argv after the flags and '-' arguments
   if (optind < argc) {
      if (is_cmd_str) {
         //set the args
         for (int i = optind; i < argc; i++) {
            dyn_arr_str_push(cur_env.args, argv[i], true);
         }
         struct psh_node* node_program = parse_program(&cur_env);
         if (IS_PARSE_ERR(node_program)) {
            ret_value = 1;
            goto ret_free_all;
         }
         struct psh_result* ret_result = eval_program(&cur_env, node_program);
         psh_node_free(node_program, true);
         if (!ret_result) {
            ret_value = 1;
            goto ret_free_all;
         }

         ret_value = ret_result->exit_status;
         free(ret_result);
         goto ret_free_all;
      } else {
         //path to the scripts
         struct psh_result* ret_result = NULL;
         for (int i = optind; i < argc; i++) {
            if (ret_result) {
               //TODO free ret_result
            }
            ret_result = NULL;
            struct psh_input* cur_file_in = read_file_to_psh_input(argv[i]);
            if (!cur_file_in) {
               fprintf(stderr, "Failed to read file '%s'\n", argv[i]);
               exit(127);
            }

            dyn_arr_psh_input_free(cur_env.psh_inputs, true);
            cur_env.psh_inputs = NULL;


            free(cur_env.arg0);
            cur_env.psh_inputs = dyn_arr_psh_input_create_with(cur_file_in);
            cur_env.arg0 = psh_strdup(argv[i]);
      
            struct psh_node* node_program = parse_program(&cur_env);
            if (IS_PARSE_ERR(node_program)) {
               fprintf(stderr, "piersh: failed to parse program from file '%s'\n", argv[i]);
               ret_value = 1;
               goto ret_free_all;
            }
            struct psh_result* ret_result = eval_program(&cur_env, node_program);
            free(cur_env.arg0);
            dyn_arr_psh_input_free(cur_env.psh_inputs, true);
            cur_env.psh_inputs = NULL;

            cur_env.arg0 = NULL;
            if (ret_result == NULL) {
               fprintf(stderr, "piersh: failed to execute program '%s'\n", argv[i]);
            }
         }

         ret_value = (ret_result == NULL) ? 1 : ret_result->exit_status;
         free(ret_result);
         goto ret_free_all;
      }
   } else if (optind > argc) {
      assert(0);
   }

   if (is_cmd_str) {
      //no args to set
      struct psh_node* node_program = parse_program(&cur_env);
      if (IS_PARSE_ERR(node_program)) {
         ret_value = 1;
         goto ret_free_all;
      }
      struct psh_result* ret_result = eval_program(&cur_env, node_program);
      psh_node_free(node_program, true);

      ret_value = ret_result->exit_status;
      free(ret_result);
      
      goto ret_free_all;
   } else {
      cur_env.psh_inputs = dyn_arr_psh_input_create();
      cur_env.is_interactive = true;

      enum psh_error p_err = source_profile_files(&cur_env);
      if (p_err != PSH_SUCCESS) {
         perror("source_profile_files");
      }

      enum psh_error interactive_ret = psh_start_interactive_shell(&cur_env);
      ret_value = PSH_SUCCESS;
      goto ret_free_all;
   }


ret_free_all:
   free_shell_env(&cur_env, false);

   return ret_value;
}
