#include "tmp_alloc.h"


struct tmp_allocator*
tmp_alloc_create(void) {
   struct tmp_allocator* ret = psh_malloc(sizeof(*ret));
   *ret = (struct tmp_allocator) {
      .dyn_mem = dyn_arr_ptr_create(),
   };
   return ret;
}

void
tmp_alloc_free(struct tmp_allocator* tmp_alloc) {
   tmp_reset(tmp_alloc);
   dyn_arr_ptr_free(tmp_alloc->dyn_mem, true);
   free(tmp_alloc);
}

void*
tmp_alloc(struct tmp_allocator* tmp_alloc, size_t len) {
   void* ret = psh_calloc(len);
   dyn_arr_ptr_push(tmp_alloc->dyn_mem, ret);
   return ret;
}

void* tmp_realloc(struct tmp_allocator*, void*, size_t);

void
tmp_reset(struct tmp_allocator* tmp_alloc) {
   dyn_arr_ptr_free_elems(tmp_alloc->dyn_mem);
}
