#include "token.h"

enum token_type
psh_get_token(struct shell_env* cur_env) {
   struct dyn_arr_tok* tok_arr = cur_env->tok_arr;
   struct psh_input* psh_input = cur_input(cur_env);

   enum psh_error err;
   int c;
   bool is_alias;
   struct token cur_token = {0};

   bool is_tokenising_here_document = false;
   /* bool is_double_quoted = false, is_single_quoted = false; */

   // TODO when there's pushback and an alias is found
   // Previously ungot a token which has already been tokenised tokenised
   if (tok_arr->pushback >= 1) {
      tok_arr->pushback--;

      if (tok_arr->nb_EOF_toks >= 1) {
         tok_arr->nb_EOF_toks--;
         return TOKTYPE_EOF;
      } else {
         struct token* last_tok = dyn_arr_tok_point_last(tok_arr);
         if (!(cur_env->_is_parsing_cmd_name && token_alias_expand(cur_env, last_tok))) {
            return last_tok->type;
         }
      }
   }

   psh_skip_whitespace(cur_env);
   while (1) {
      c = psh_getc(cur_env);

      /*rule 1: if the end of input is recognised, delimit current token (if there is one)*/
      if (c == EOF) {
         if (!cur_token.psh_str) {
            tok_arr->nb_EOF_toks++;
            return TOKTYPE_EOF;
         }

         if (cur_env->_is_parsing_cmd_name && token_alias_expand(cur_env, &cur_token)) {
            continue;
      }

         /*delimit current token if there is one*/
         err = token_preparse(&cur_token, false);
         if (err)
            assert(0);

         dyn_arr_tok_push(tok_arr, cur_token);

         return cur_token.type;
      }

      /* if (is_tokenising_here_document) { */
         /*process the here document*/
         /* continue; */
      /* } */

      /*rule 2: if the previous character was part of an operator, and the current one is unquoted and can be used to form another operator with the previous characters, use it in that operator token*/
      /*rule 3: if the previous character was used as part of an operator and the current character cannot be used to make another operator, delimit the previous operator token*/
      if (cur_token.psh_str && cur_token.psh_str->str_len > 0 && cur_token.type == TOKTYPE_OPERATOR) {
         /*cur char is unquoted and we can add the current character to the token to make a new operator*/
         if (can_make_new_operator(&cur_token, c)) {
            psh_string_append_char(cur_token.psh_str, c);
            continue;
         } else {
            if (cur_env->_is_parsing_cmd_name && token_alias_expand(cur_env, &cur_token))
               continue;

            err = token_preparse(&cur_token, false);
            if (err)
               assert(0);
            dyn_arr_tok_push(tok_arr, cur_token);

            psh_ungetc(cur_env);

            return cur_token.type;
         }
      }

      /*rule 4: if the current character is a backslash or a quote type, affect the quoting of subsequent characters (there are many rules)*/
      if (c == '\"' || c == '\'' || c == '\\') {
         /*set the quoting parameters*/
         if (!cur_token.psh_str) {
            memset(&cur_token, 0, sizeof(struct token));
            cur_token.index_start = (psh_input->cur_pos == 0) ? 0 : psh_input->cur_pos - 1;
            cur_token.psh_str = psh_string_create("");
         }
         switch (c) {
            case '\"': { 
               psh_string_append_char(cur_token.psh_str, c);
               psh_tokenise_double_quote(&cur_token, cur_env);
            } break;
            case '\'': {
               psh_string_append_char(cur_token.psh_str, c);
               psh_tokenise_single_quote(&cur_token, cur_env);
            } break;
            case '\\': {
               /*process next char*/
               c = psh_getc(cur_env);
               if (c == EOF) psh_ungetc(cur_env);
               else if (c != '\n') psh_string_append_char(cur_token.psh_str, c);
            } break;
         }
         continue;
      }

      /*rule 5: if the current char is unquoted and is a '$' or a '`', identify the candidates for shell expansion and recursively parse them*/
      if (c == '$' || c == '`') {
         // Note: Do not delimit token here!
         /*identify starts of parameter expansion, command substitution or arithmetic substitution and parse that*/
         if (!cur_token.psh_str) {
            memset(&cur_token, 0, sizeof(struct token));
            cur_token.index_start = (psh_input->cur_pos == 0) ? 0 : psh_input->cur_pos - 1;
            cur_token.psh_str = psh_string_create(NULL);
         }
         psh_tokenise_expansion(&cur_token, cur_env, c);

         continue;
      }

      /*rule 6: if the current char is unquoted and can be used as the first character of a new operator, the current token shall be delimited if it exists, and the current character should start a new operator token*/
      if (can_make_new_operator(NULL, c)) {
         /* delimit previous token if exists */
         if (cur_token.psh_str && cur_token.psh_str->str_len > 0) {
            if (cur_env->_is_parsing_cmd_name && token_alias_expand(cur_env, &cur_token))
               continue;

            // TODO: if the previous number is quoted, it should NOT be set to TOKTYPE_IO_NUMBER
            bool is_char_less_or_great = (c == '<' || c == '>');
            err = token_preparse(&cur_token, is_char_less_or_great);
            if (err)
               assert(0);
            dyn_arr_tok_push(tok_arr, cur_token);
            psh_ungetc(cur_env);

            return cur_token.type;
         }

         /* start a new operator token */
         memset(&cur_token, 0, sizeof(struct token));
         cur_token.psh_str = psh_string_create("");
         cur_token.index_start = (psh_input->cur_pos == 0) ? 0 : psh_input->cur_pos - 1;
         psh_string_append_char(cur_token.psh_str, c);
         cur_token.type = TOKTYPE_OPERATOR;
         continue;
      }

      /*rule 7: if the current character is unquoted and is a blank, delimit the previous token and discard the current char*/
      if (c == ' ' || c == '\t' || c == '\n' || c == '\r') {
         /*delimit previous token if exists and discard the current one*/
         if (cur_token.psh_str && cur_token.psh_str->str_len > 0) {
            if (cur_env->_is_parsing_cmd_name && token_alias_expand(cur_env, &cur_token))
               continue;
            err = token_preparse(&cur_token, false);
            if (err)
               assert(0);
            dyn_arr_tok_push(tok_arr, cur_token);
            psh_ungetc(cur_env);

            return cur_token.type;
         } else if (c != '\n') {
            // TODO what to do when it's a NEWLINE? Make a token just for it or no? (probably yes)
            continue;
         }
      }

      /*rule 8: is the previous character was part of a word, append the current char to the word*/
      if (cur_token.psh_str && cur_token.psh_str->str_len > 0) {
         /*append the current char to that word*/
         psh_string_append_char(cur_token.psh_str, c);
         continue;
      }

      /*rule 9: if the current char is unquoted and is '#', discard all future characters until the newline*/
      if (c == '#') {
         /*discard till newline or the end*/
         while (c != EOF && c != '\n') {
            c = psh_getc(cur_env);
         }
         psh_ungetc(cur_env);
         continue;
      }

      /*rule 10: use the character to start a new word*/
      /* => start a new word*/
      memset(&cur_token, 0, sizeof(struct token));
      cur_token = (struct token){
         .index_start = (psh_input->cur_pos == 0) ? 0 : psh_input->cur_pos - 1,
         .psh_str = psh_string_create(""),
      };
      psh_string_append_char(cur_token.psh_str, c);

      //A single newline is a full token (I think)
      if (c == '\n') {
         cur_token.type = TOKTYPE_NEWLINE;
         dyn_arr_tok_push(tok_arr, cur_token);
         return TOKTYPE_NEWLINE;
      }
   }
}

void
psh_unget_token(struct shell_env* cur_env) {
   struct dyn_arr_tok* tok_arr = cur_env->tok_arr;
   struct psh_input* psh_input = cur_input(cur_env);
   if (tok_arr->nb_EOF_toks > 0)
      tok_arr->nb_EOF_toks--;
   else
      tok_arr->pushback++;
}

void
psh_delete_last_token(struct shell_env* cur_env) {
   struct dyn_arr_tok* tok_arr = cur_env->tok_arr;
   struct psh_input* psh_input = cur_input(cur_env);
   if (tok_arr->size == 0) {
      assert(0);
   }

   if (tok_arr->nb_EOF_toks > 0) {
      tok_arr->nb_EOF_toks--;
   } else {
      if (tok_arr->pushback)
         tok_arr->pushback--;
      if (tok_arr->tokens[tok_arr->size-1].psh_str) {
         psh_string_free(tok_arr->tokens[tok_arr->size-1].psh_str);
      }
      memset(&(tok_arr->tokens[tok_arr->size-1]), 0, sizeof(struct token));
      tok_arr->size--;
   }
}

enum token_type
psh_peek_token(struct shell_env* cur_env) {
   enum token_type ret_type = psh_get_token(cur_env);
   psh_unget_token(cur_env);
   return ret_type;
}

//The last parsed token needs to be the one
struct token*
psh_tokenise_heredoc(struct shell_env* cur_env, const char* here_string, bool is_dlessdash) {
   struct heredoc doc = {0};
   struct dyn_arr_tok* tok_arr = cur_env->tok_arr;
   struct psh_input* psh_input = cur_input(cur_env);
   assert(here_string);

   struct token* here_end_token = dyn_arr_tok_point_last(cur_env->tok_arr);
   //NEEDS TO REMOVE ALL TOKENS PREPARSED AFTER here_end_token
   delete_tokens_until_token(cur_env, here_end_token);

   assert(cur_env->tok_arr->pushback == 0);

   //advance till the next NEWLINE (and past the previous heredocs) till the beginning of the heredoc
   const char* begin_cur_line = psh_input->psh_str->str + psh_input->cur_pos;
   //offset from cur_pos to the beginning of the here_doc
   size_t offset = 0;
   while (*begin_cur_line && *begin_cur_line != '\n') {
      begin_cur_line++;
      offset++;
   }

   if (!*begin_cur_line) {
      //couldn't find a here_end, and therefore parse a heredoc
      return NULL;
   }
   
   begin_cur_line++;
   offset++;

   struct heredoc prev_doc = {0};
   while (1) {
      prev_doc = heredoc_from_begin_pos(tok_arr, begin_cur_line - psh_input->psh_str->str);
      if (memcmp(&prev_doc, &((struct heredoc){0, 0}), sizeof(struct heredoc))) {
         size_t here_doc_len = prev_doc.end_index - prev_doc.start_index + 1;
         offset += here_doc_len;
         begin_cur_line += here_doc_len;
      } else {
         break;
      }
   }
   

   /* fprintf(stderr, "psh_input->cur_pos: %lu\n", psh_input->cur_pos); */
   /* fprintf(stderr, "offset: %lu\n", offset); */
   doc.start_index = psh_input->cur_pos + offset;

   struct psh_string* heredoc_str = psh_string_create(NULL);
   //advance till the next newline and check if the line is equal to here_string
   //...
   
   uint64_t here_str_len = strlen(here_string);
   uint64_t i;
   uint64_t heredoc_len = 0;
   for (;;) {
      //check if current line is equal to here_string
      for (i = 0; i < here_str_len; i++) {
         if (!begin_cur_line[i] || here_string[i] != begin_cur_line[i])
            break;
      }
      if (i == here_str_len && (begin_cur_line[i] == '\n' || begin_cur_line[i] == '\0')) {
         //found the here_string
         break;
      }
      
      //if '<<-', skip all tabs
      if (is_dlessdash) {
         //technically, skipping spaces is not in the posix standard, but I don't care
         while (*begin_cur_line == '\t' || *begin_cur_line == ' ') {
            begin_cur_line++;
            heredoc_len++;
         }
      }

      //if not, onto the next line
      while (*begin_cur_line && *begin_cur_line != '\n') {
         psh_string_append_char(heredoc_str, *begin_cur_line);
         begin_cur_line++;
         heredoc_len++;
      }
      if (!*begin_cur_line) {
         //Failed to parse heredoc
         psh_string_free(heredoc_str);
         return NULL;
      } else {
         begin_cur_line++;
         heredoc_len++;
         psh_string_append_char(heredoc_str, '\n');
      }
   }
   doc.end_index = doc.start_index + heredoc_len + here_str_len;
   /* fprintf(stderr, "doc: {%lu, %lu}\n", doc.start_index, doc.end_index); */

   //the here_doc is between begin_here_doc and begin_cur_line[i]
   //add it to the token array
   /* fprintf(stderr, "parsed heredoc: '%s'\n", heredoc_str->str); */
   struct token here_tok = {
      .is_heredoc = true,
   };
   here_tok.psh_str = heredoc_str;
   here_tok.type = TOKTYPE_WORD ; //I think?
   dyn_arr_tok_push(tok_arr, here_tok);
   dyn_arr_heredoc_push(tok_arr->heredocs, doc);

   //when the output is delimited, implement a way for psh_input to skip the already tokenised here_doc
   //push the doc to the tok_arr

   return dyn_arr_tok_point_last(cur_env->tok_arr);
}

//This tokenises an expansion till its end, so anythin that starts with an unquoted '$' or '`' 
enum psh_error
psh_tokenise_expansion(struct token* cur_token, struct shell_env* cur_env, char first_c) {
   // If it's a $<not { or (>, you just advance will whatever is still a name
   char second_c;
   char third_c;
   char cur_c;
   psh_string_append_char(cur_token->psh_str, first_c);
   struct psh_input* psh_input = cur_input(cur_env);

   if (first_c == '$') {
      second_c = psh_getc(cur_env);
      if (second_c == '{') {
         psh_string_append_char(cur_token->psh_str, second_c);
         //TODO ask for more?
         while (1) {
            cur_c = psh_getc(cur_env);
            if (cur_c == EOF) {
               //TODO ask for more
               assert(0);
            }
            if (cur_c != '`' && cur_c != '$') psh_string_append_char(cur_token->psh_str, cur_c);
            switch (cur_c) {
               case '}': return PSH_SUCCESS;
               case '\'': { assert(psh_tokenise_single_quote(cur_token, cur_env) == 0); } break;
               case '\"': { assert(psh_tokenise_double_quote(cur_token, cur_env) == 0); } break;
               case '`': { case '$': assert(psh_tokenise_expansion(cur_token, cur_env, cur_c) == 0); } break;
            }
         }
      } else if (second_c == '(') {
         //works for both $() and $(())
         psh_string_append_char(cur_token->psh_str, second_c);

         uint64_t nb_parens = 1;
         while (1) {
            cur_c = psh_getc(cur_env);

            if (cur_c == EOF) {
               assert(0);
            } else if (cur_c == '$' || cur_c == '`') {
               enum psh_error err = psh_tokenise_expansion(cur_token, cur_env, cur_c);
               assert(err == 0);
            } else {
               psh_string_append_char(cur_token->psh_str, cur_c);
               switch (cur_c) {
                  case '(': { nb_parens++; } break;
                  case ')': { if (--nb_parens == 0) return PSH_SUCCESS; } break;
                  case '\'': { assert(psh_tokenise_single_quote(cur_token, cur_env) == 0); } break; 
                  case '\"': { assert(psh_tokenise_double_quote(cur_token, cur_env) == 0); } break;
                  case '\\': {
                     cur_c = psh_getc(cur_env);
                     if (cur_c == EOF) {
                        //TODO?
                        assert(0);
                     } else psh_string_append_char(cur_token->psh_str, cur_c);
                  } break;
               }
            }
         }
      } else {
         // It's a normal $, tokenise the rest of the word outside the function
         /* psh_ungetc(cur_env); */
         psh_string_append_char(cur_token->psh_str, second_c);
         return PSH_SUCCESS;
      }
   } else if (first_c == '`') {
      while (1) {
         cur_c = psh_getc(cur_env);

         if (cur_c == EOF) {
            assert(0);
         } else if (cur_c == '$') {
            assert(psh_tokenise_expansion(cur_token, cur_env, cur_c) == 0);
         } else {
            psh_string_append_char(cur_token->psh_str, cur_c);
            switch (cur_c) {
               case '\'': { assert(psh_tokenise_single_quote(cur_token, cur_env) == 0); } break; 
               case'\"': { assert(psh_tokenise_double_quote(cur_token, cur_env) == 0); } break;
               case '\\': {
                  cur_c = psh_getc(cur_env);
                  if (cur_c == EOF) {
                     //TODO?
                     assert(0);
                  } else psh_string_append_char(cur_token->psh_str, cur_c);
               } break;
               case '`': { return PSH_SUCCESS; } break;
            }
         }
      }
   } else assert(0);

   //TODO If it's a '$((', weird shit happens. Not sure what to do, probably count the parens
}

enum psh_error
psh_tokenise_single_quote(struct token* cur_token, struct shell_env* cur_env) {
   int c;
   enum psh_error err;

   while (1) {
      c = psh_getc(cur_env);
      if (c == EOF) {
         //TODO ask for more
         assert(0);
      } else if (c == '\'') {
         psh_string_append_char(cur_token->psh_str, c);
         return PSH_SUCCESS;
      }

      psh_string_append_char(cur_token->psh_str, c);
   }
}

enum psh_error
psh_tokenise_double_quote(struct token* cur_token, struct shell_env* cur_env) {
   int c;
   enum psh_error err;
   struct psh_input* psh_input = cur_input(cur_env);

   for (;;) {
      c = psh_getc(cur_env);
      
      switch (c) {
         case EOF: assert(0); //TODO ask for more
         case '"': {
            psh_string_append_char(cur_token->psh_str, c);
            return PSH_SUCCESS;
         }
         case '$': case '`': { assert(psh_tokenise_expansion(cur_token, cur_env, c) == 0); } break;
         case '\\': {
            c = psh_getc(cur_env);
            //Not very readable, but it looks really cool so I'm keeping it
            switch (c) {
               case EOF: assert(0);
               case '\n': break;
               default: psh_string_append_char(cur_token->psh_str, '\\'); /*fallthrough*/
               case '$': case '`': case '"': case '\\': psh_string_append_char(cur_token->psh_str, c); /*fallthrough*/
            }
         } break;
         default: { psh_string_append_char(cur_token->psh_str, c); } break;
      }
   }
}

bool
can_make_new_operator(const struct token* cur_token, const char in) {
   struct psh_string *tmp_str;
   if (cur_token == NULL) {
      tmp_str = psh_string_create("");
   } else {
      tmp_str = psh_string_create(cur_token->psh_str->str);
   }

   psh_string_append_char(tmp_str, in);

   bool ret = is_operator(tmp_str->str);
   psh_string_free(tmp_str);

   return ret;
}

//TODO: optimise this thing, cringe
bool
is_operator(const char *in) {
   for (size_t i = 0; i < (sizeof(operators) / sizeof(*operators)); i++) {
      if (!strcmp(in, operators[i]))
         return true;
   }
   return false;
}

enum token_type
get_operator_token_type(const char *in) {
   for (size_t i = 0; i < (sizeof(operators) / sizeof(*operators)); i++) {
      if (!strcmp(in, operators[i]))
         // TOKTYPE_DISOWN is the first operator token listed
         return i - TOKTYPE_AND;
   }
   // not an operator
   return TOKTYPE_TOKEN;
}

// TODO find a better way of doing this, this is cringe
enum token_type
get_token_type(const struct token* in_token) {
   const char* tok_str = in_token->psh_str->str;
// Control operators
   if (!strcmp(tok_str, "&"))        return TOKTYPE_AND;
   else if (!strcmp(tok_str, ";"))   return TOKTYPE_SEMICOLON;
   else if (!strcmp(tok_str, "|"))   return TOKTYPE_PIPE;
   else if (!strcmp(tok_str, "("))   return TOKTYPE_LPAREN;
   else if (!strcmp(tok_str, ")"))   return TOKTYPE_RPAREN;
   else if (!strcmp(tok_str, "\n"))  return TOKTYPE_NEWLINE;
   else if (!strcmp(tok_str, "&&"))  return TOKTYPE_AND_IF;
   else if (!strcmp(tok_str, "||"))  return TOKTYPE_OR_IF;
   else if (!strcmp(tok_str, ";;"))  return TOKTYPE_DSEMI;
// Redirection operators
   else if (!strcmp(tok_str, "<"))   return TOKTYPE_LESSTHAN;
   else if (!strcmp(tok_str, ">"))   return TOKTYPE_GREATTHAN;
   else if (!strcmp(tok_str, "<<"))  return TOKTYPE_DLESS;
   else if (!strcmp(tok_str, ">>"))  return TOKTYPE_DGREAT;
   else if (!strcmp(tok_str, "<&"))  return TOKTYPE_LESSAND;
   else if (!strcmp(tok_str, ">&"))  return TOKTYPE_GREATAND;
   else if (!strcmp(tok_str, "<>"))  return TOKTYPE_LESSGREAT;
   else if (!strcmp(tok_str, "<<-")) return TOKTYPE_DLESSDASH;
   else if (!strcmp(tok_str, ">|"))  return TOKTYPE_CLOBBER;
   else assert(0);
}

/*
   Basic "parsing" that can be done from the immediate context, the stage before actual parsing
   Happens after being "delimited"
*/
enum psh_error
token_preparse(struct token* in_token, bool is_following_char_less_or_great) {
   // If it is an operator, set the token identifier to that operator
   if (in_token->type == TOKTYPE_OPERATOR) {
      in_token->type = get_token_type(in_token);
      return 0;
   }

   // If the string is only digits and the delimiter (not sure if that means the previous/next token or the first/last character of the string? Only the first one makes sense imo), set to IO_NUMBER
   if (is_following_char_less_or_great) {
      bool is_number = true;
      for (size_t i = 0; i < in_token->psh_str->str_len; i++) {
         if (!isdigit(in_token->psh_str->str[i])) {
            is_number = false;
            break;
         }
      }
      if (is_number) {
         in_token->type = TOKTYPE_IO_NUMBER;
         return 0;
      }
   }

   // Otherwise, it's just a TOKEN for now
   in_token->type = TOKTYPE_TOKEN;
   return 0;
}

void
delete_tokens_until_token(struct shell_env* cur_env, struct token* in_token) {
   if (!in_token)
      return;

   struct dyn_arr_tok* tok_arr = cur_env->tok_arr;
   struct dyn_arr_heredoc* dyn_heredocs = tok_arr->heredocs;
   // Delete all tokens in tok_arr until in_token
   struct token* cur_tok_in_list = dyn_arr_tok_point_last_no_pushback(tok_arr);
   while (cur_tok_in_list && cur_tok_in_list != in_token) {
      if (cur_tok_in_list == NULL) {
         cur_tok_in_list = dyn_arr_tok_point_last_no_pushback(tok_arr);
         dyn_arr_tok_delete_last_no_pushback(tok_arr);
         continue;
      }

      //The last heredoc is necessarily from the last heredoc token, so discard this one
      if (cur_tok_in_list->is_heredoc) {
         dyn_arr_heredoc_pop(dyn_heredocs);
      }

      // Rewind the psh_input (not sure if works well)
      cur_input(cur_env)->cur_pos = cur_tok_in_list->index_start;
      psh_reverse_skip_whitespace(cur_input(cur_env));
      // Remove the last token from the list
      dyn_arr_tok_delete_last_no_pushback(tok_arr);
      
      cur_tok_in_list = dyn_arr_tok_point_last_no_pushback(tok_arr);
   }
   if (!cur_tok_in_list) {
      DEBUG_ASSERT(0);
   }
}

char* sym_get_alias_value(struct sym_table*, char*);
#define UNSET ((void*)0x1)

/// If token is alias, expand and return true.
/// Else return false
/// Aliases are only applied for tokens that are 'command name'
/// Resets in_token
bool
token_alias_expand(struct shell_env* cur_env, struct token* in_token) {
   char* alias_cmd = sym_get_alias_value(cur_env->alias_table, in_token->psh_str->str);
   struct dyn_arr_tok* tok_arr = cur_env->tok_arr;

   if (alias_cmd == NULL) {
      return false;
   }

   // Delete all tokens in tok_arr until in_token
   struct token* cur_tok_in_list = dyn_arr_tok_point_last_no_pushback(tok_arr);
   while (cur_tok_in_list && cur_tok_in_list != in_token) {
      if (cur_tok_in_list == NULL) {
         cur_tok_in_list = dyn_arr_tok_point_last_no_pushback(tok_arr);
         dyn_arr_tok_delete_last_no_pushback(tok_arr);
         continue;
      }
      // Rewind the psh_input (not sure if works well)
      cur_input(cur_env)->cur_pos = cur_tok_in_list->index_start;
      psh_reverse_skip_whitespace(cur_input(cur_env));
      // Remove the last token from the list
      dyn_arr_tok_delete_last_no_pushback(tok_arr);
      
      cur_tok_in_list = dyn_arr_tok_point_last_no_pushback(tok_arr);
   }
   if (!cur_tok_in_list) {
      assert(0);
   }

   psh_reverse_skip_whitespace(cur_input(cur_env));
   cur_input(cur_env)->cur_pos = in_token->index_start;

   //remove the token string from the psh_input
   psh_string_delete_chars(cur_input(cur_env)->psh_str, in_token->index_start, in_token->psh_str->str_len);

   //reinsert the alias text at the same index
   psh_string_insert_str(cur_input(cur_env)->psh_str, alias_cmd, in_token->index_start);

   //free token (it will be recreated in psh_get_token())
   psh_string_free(in_token->psh_str);
   memset(in_token, 0, sizeof(struct token));

   return true;
}
