#include "dyn_arr.h"

//returns the heredoc that has the same begin index, or the 0 heredoc
struct heredoc
heredoc_from_begin_pos(struct dyn_arr_tok* in, size_t cur_pos) {
   struct dyn_arr_heredoc* here = in->heredocs;
   for (size_t i = 0; i < here->size; i++) {
      if (cur_pos == here->heredocs[i].start_index) {
         return here->heredocs[i];
      } else if (cur_pos < here->heredocs[i].start_index)
         break;
   }

   return (struct heredoc){0, 0};
}

struct heredoc
heredoc_from_end_pos(struct dyn_arr_tok* in, size_t cur_pos) {
   struct dyn_arr_heredoc* here = in->heredocs;
   for (size_t i = 0; i < here->size; i++) {
      if (cur_pos == here->heredocs[i].end_index) {
         return here->heredocs[i];
      } else if (cur_pos < here->heredocs[i].end_index)
         break;
   }

   return (struct heredoc){0, 0};
}

void
dyn_arr_heredoc_init(struct dyn_arr_heredoc *arr) {
   *arr = (struct dyn_arr_heredoc) {
      .heredocs = psh_malloc(sizeof(struct heredoc) * 4),
      .size = 0,
      .capacity = 4
   };
}

struct dyn_arr_heredoc*
dyn_arr_heredoc_create(void) {
   struct dyn_arr_heredoc* ret = psh_malloc(sizeof(struct dyn_arr_heredoc));
   dyn_arr_heredoc_init(ret);
   return ret;
}

void
dyn_arr_heredoc_free(struct dyn_arr_heredoc *arr) {
   if (!arr)
      return;
   if (arr->heredocs)
      free(arr->heredocs);
   memset(arr, 0, sizeof(struct dyn_arr_heredoc));
}

void
dyn_arr_heredoc_push(struct dyn_arr_heredoc *arr, struct heredoc in) {
   if (arr->size >= arr->capacity - 1) {
      arr->capacity *= 2;
      arr->heredocs = realloc(arr->heredocs, sizeof(struct heredoc) * arr->capacity);
      assert(arr->heredocs);
   }

   arr->heredocs[arr->size] = in;
   arr->size++;
}

struct heredoc
dyn_arr_heredoc_pop(struct dyn_arr_heredoc *arr) {
   if (arr->size == 0)
      assert(0);

   if (arr->size < (arr->capacity / 2) - 1) {
      arr->capacity /= 2;
      arr->heredocs = realloc(arr->heredocs, sizeof(struct heredoc) * arr->capacity);
      assert(arr->heredocs);
   }

   arr->size--;
   return arr->heredocs[arr->size];
}




void
dyn_arr_tok_init(struct dyn_arr_tok *arr) {
   *arr = (struct dyn_arr_tok) {
      .tokens = psh_malloc(sizeof(struct token) * 8),
      .size = 0,
      .capacity = 8,
      .heredocs = dyn_arr_heredoc_create()
   };
}

struct dyn_arr_tok*
dyn_arr_tok_create(void) {
   struct dyn_arr_tok* ret = psh_malloc(sizeof(struct dyn_arr_tok));
   dyn_arr_tok_init(ret);
   return ret;
}

void
dyn_arr_tok_free(struct dyn_arr_tok *arr) {
   if (!arr)
      return;
   if (arr->tokens) {
      for (uint i = 0; i < arr->size; i++)
         if (arr->tokens[i].psh_str)
            psh_string_free(arr->tokens[i].psh_str);

      free(arr->tokens);

      dyn_arr_heredoc_free(arr->heredocs);
      free(arr->heredocs);
   }
   memset(arr, 0, sizeof(struct dyn_arr_tok));
}

void
dyn_arr_tok_push(struct dyn_arr_tok *arr, struct token in) {
   if (arr->size >= arr->capacity - 1) {
      arr->capacity *= 2;
      arr->tokens = realloc(arr->tokens, sizeof(struct token) * arr->capacity);
      assert(arr->tokens);
   }

   arr->tokens[arr->size] = in;
   arr->size++;
}

struct token*
dyn_arr_tok_pop(struct dyn_arr_tok *arr) {
   if (arr->size == 0)
      return NULL;

   if (arr->size < (arr->capacity / 2) - 1) {
      arr->capacity /= 2;
      arr->tokens = realloc(arr->tokens, sizeof(struct token) * arr->capacity);
      assert(arr->tokens);
   }

   arr->size--;
   return &arr->tokens[arr->size];
}

inline
struct token*
dyn_arr_tok_point_last(struct dyn_arr_tok *arr) {
   if (!arr || arr->nb_EOF_toks != 0 || arr->size == 0)
      return NULL;
   else
      return &(arr->tokens[arr->size - arr->pushback - 1]);
}

inline
struct token*
dyn_arr_tok_point_last_no_pushback(struct dyn_arr_tok *arr) {
   if (!arr || arr->nb_EOF_toks != 0 || arr->size == 0)
      return NULL;
   else
      return &(arr->tokens[arr->size - 1]);
}

void
dyn_arr_tok_delete_last_no_pushback(struct dyn_arr_tok *arr) {
   if (arr->nb_EOF_toks > 0) {
      arr->nb_EOF_toks--;
      return;
   }

   struct token* last = dyn_arr_tok_point_last_no_pushback(arr);
   if (last) {
      if (arr->pushback > 0)
         arr->pushback--;

      psh_string_free(last->psh_str);
      memset(last, 0, sizeof(struct token));
      arr->size--;
   }
}

enum psh_error
debug_dyn_arr_tok_print(struct dyn_arr_tok *arr) {
   printf("DEBUG: dyn arr:\n");
   printf("   - size_t size: %zu\n", arr->size);
   printf("   - size_t capacity: %zu\n", arr->capacity);
   printf("   - size_t pushback: %zu\n", arr->pushback);
   printf("   - size_t nb_EOF_toks: %zu\n", arr->nb_EOF_toks);
   printf("   - struct token* tokens: %p\n", (void*)arr->tokens);
   if (arr->tokens != NULL) {
      for (int i = 0; i < (int) arr->size; i++) {
         printf("      # struct token number #%d\n", i);
         printf("         - enum token_type: %i\n", ((arr->tokens)[i]).type);
         printf("         - struct psh_string* psh_str: %p\n", (void*)((arr->tokens)[i]).psh_str);
         printf("            - size_t arr_size: %zu\n", ((arr->tokens)[i]).psh_str->arr_size);
         printf("            - size_t str_len: %zu\n", ((arr->tokens)[i]).psh_str->str_len);
         printf("            - char str[]: '%s'\n", ((arr->tokens)[i]).psh_str->str);
         printf("            - size_t index_start[]: '%lu'\n", ((arr->tokens)[i]).index_start);
      }
   }
   printf("END DEBUG\n");

   return 0;
}

void
dyn_arr_str_init(struct dyn_arr_str *arr) {
   *arr = (struct dyn_arr_str) {
      .strs = psh_malloc(sizeof(char*) * 8),
      .size = 0,
      .capacity = 8
   };
}

struct dyn_arr_str*
dyn_arr_str_create(void) {
   struct dyn_arr_str* ret = psh_malloc(sizeof(*ret));
   dyn_arr_str_init(ret);
   return ret;
}

void
dyn_arr_str_free(struct dyn_arr_str *arr, bool free_all) {
   if (!arr)
      return;
   if (arr->strs) {
      if (free_all) {
         for (uint64_t i = 0; i < arr->size; i++)
            free(arr->strs[i]);
      }
      free(arr->strs);
   }
   memset(arr, 0, sizeof(struct dyn_arr_str));
}

enum psh_error
dyn_arr_str_push(struct dyn_arr_str *arr, char* in, bool copy_str) {
   if (arr->size >= arr->capacity - 1) {
      arr->capacity *= 2;
      arr->strs = realloc(arr->strs, sizeof(char*) * arr->capacity);
      if (arr->strs == NULL)
         assert(0);
   }

   char* pushed_str = (copy_str) ? psh_strdup(in) : in;

   arr->strs[arr->size] = pushed_str;
   arr->size++;

   return PSH_SUCCESS;
}

void
dyn_arr_str_insert_arr_at(struct dyn_arr_str *arr, const struct dyn_arr_str *cpy, size_t index) {
   size_t old_size = arr->size;
   arr->size += cpy->size;

   while (arr->size >= arr->capacity - 1) {
      arr->capacity *= 2;
      arr->strs = realloc(arr->strs, sizeof(char*) * arr->capacity);
      if (arr->strs == NULL)
         assert(0);
   }

   /* memmove(arr->strs + index + cpy->size, arr->strs + index, sizeof(char**) * (old_size)); */
   
   for (size_t i = index; i < old_size; i++) {
      arr->strs[i+cpy->size] = arr->strs[i];
   }

   for (size_t i = 0; i < cpy->size; i++) {
      arr->strs[index + i] = psh_strdup(cpy->strs[i]);
   }
}

//Doesn't change cpy, only copies
void
dyn_arr_str_delete(struct dyn_arr_str *arr, size_t index, size_t nb, bool do_free) {
   /* memmove(arr->strs + index, arr->strs + index + nb, sizeof(char**) * arr->size); */

   if (do_free) {
      for (size_t i = 0; i < nb; i++) {
         free(arr->strs[i+index]);
      }
   }
   
   for (size_t i = index + nb; i < arr->size; i++) {
      arr->strs[i-nb] = arr->strs[i];
   }

   arr->size -= nb;
}

char*
dyn_arr_str_pop(struct dyn_arr_str *arr) {
   if (arr->size == 0)
      return NULL;

   if (arr->size < (arr->capacity / 2) - 1) {
      arr->capacity /= 2;
      arr->strs = realloc(arr->strs, sizeof(char*) * arr->capacity);
      if (arr->strs == NULL)
         assert(0);
   }

   arr->size--;
   return arr->strs[arr->size];
}

//Note: You have to free the memory for the structure yourself (or not at all if on the stack)
char**
dyn_arr_to_cstr_arr(struct dyn_arr_str* arr) {
   if (arr->size != arr->capacity) {
      arr->strs = realloc(arr->strs, sizeof(char*) * arr->size);
      assert(arr->strs);
   }

   return arr->strs;
}

/*
   * DYN_ARR_PTR FUNCTIONS *
*/
void
dyn_arr_ptr_init(struct dyn_arr_ptr *arr) {
   *arr = (struct dyn_arr_ptr) {
      .ptrs = psh_calloc(sizeof(void*) * 8),
      .size = 0,
      .capacity = 8
   };
}

struct dyn_arr_ptr* dyn_arr_ptr_create(void) {
   struct dyn_arr_ptr* ret = psh_malloc(sizeof(struct dyn_arr_ptr));
   dyn_arr_ptr_init(ret);
   return ret;
}

void dyn_arr_ptr_free(struct dyn_arr_ptr *arr, bool free_self) {
   if (!arr)
      return;
   dyn_arr_ptr_free_elems(arr);
   memset(arr, 0, sizeof(struct dyn_arr_ptr));
   if (free_self)
      free(arr);
}

void
dyn_arr_ptr_push(struct dyn_arr_ptr *arr, void* in) {
   if (arr->size >= arr->capacity - 1) {
      arr->capacity *= 2;
      arr->ptrs = realloc(arr->ptrs, sizeof(void*) * arr->capacity);
      assert(arr->ptrs == NULL);
   }

   arr->ptrs[arr->size] = in;
   arr->size++;
}

void dyn_arr_ptr_free_elems(struct dyn_arr_ptr *arr) {
   if (arr->ptrs) {
      for (size_t i = 0; i < arr->size; i++)
         free(arr->ptrs[i]);
      free(arr->ptrs);
   }
   arr->size = 0;
   arr->capacity = 4;
   arr->ptrs = realloc(arr->ptrs, sizeof(void*) * arr->capacity);
   assert(arr->ptrs);
   memset(arr->ptrs, 0, sizeof(void*) * arr->capacity);
}


/*
   * DYN_ARR_NODE FUNCTIONS *
*/
void
dyn_arr_node_init(struct dyn_arr_node *arr) {
   *arr = (struct dyn_arr_node) {
      .nodes = psh_calloc(sizeof(struct psh_node*) * 8),
      .size = 0,
      .capacity = 8
   };
}

struct dyn_arr_node*
dyn_arr_node_create(void) {
   struct dyn_arr_node* ret = psh_malloc(sizeof(struct dyn_arr_node));
   dyn_arr_node_init(ret);
   return ret;
}

void psh_node_free(struct psh_node*, bool);

void
dyn_arr_node_free(struct dyn_arr_node *arr, bool free_each_node) {
   if (!arr)
      return;
   if (arr->nodes) {
      if (free_each_node) {
         for (size_t i = 0; i < arr->size; i++)
            psh_node_free(arr->nodes[i], true);
      }
      free(arr->nodes);
   }
   memset(arr, 0, sizeof(struct dyn_arr_node));
}

void
dyn_arr_node_push(struct dyn_arr_node *arr, struct psh_node* in) {
   while (arr->size >= arr->capacity - 1) {
      arr->capacity *= 2;
      arr->nodes = realloc(arr->nodes, sizeof(struct psh_node*) * arr->capacity);
      if (arr->nodes == NULL)
         assert(0);
   }

   arr->nodes[arr->size] = in;
   arr->size++;
}

struct psh_node*
dyn_arr_node_pop(struct dyn_arr_node *arr) {
   if (arr->size == 0)
      return NULL;

   if (arr->size < (arr->capacity / 2) - 1) {
      arr->capacity /= 2;
      arr->nodes = realloc(arr->nodes, sizeof(struct psh_node*) * arr->capacity);
      if (arr->nodes == NULL)
         assert(0);
   }

   arr->size--;
   struct psh_node* ret = arr->nodes[arr->size];
   arr->nodes[arr->size] = NULL;
   return ret;
}



/*
   * dyn_arr_uint8 functions
*/
void
dyn_arr_uint8_init(struct dyn_arr_uint8 *arr) {
   *arr = (struct dyn_arr_uint8) {
      .uint8s = psh_malloc(sizeof(uint8_t) * 8),
      .size = 0,
      .capacity = 8
   };
}

struct dyn_arr_uint8*
dyn_arr_uint8_create(void) {
   struct dyn_arr_uint8* ret = psh_malloc(sizeof(*ret));
   dyn_arr_uint8_init(ret);
   return ret;
}

void
dyn_arr_uint8_free(struct dyn_arr_uint8 *arr) {
   if (!arr) return;
   if (arr->uint8s) {
      free(arr->uint8s);
   }
   arr->size = 0;
   arr->capacity = 0;
}

void
dyn_arr_uint8_push(struct dyn_arr_uint8 *arr, uint8_t in) {
   if (arr->size >= arr->capacity - 1) {
      arr->capacity *= 2;
      arr->uint8s = realloc(arr->uint8s, sizeof(uint8_t) * arr->capacity);
      assert(arr->uint8s);
   }

   arr->uint8s[arr->size] = in;
   arr->size++;
}

enum psh_error
dyn_arr_uint8_pop(struct dyn_arr_uint8 *arr, uint8_t* in) {
   if (arr->size == 0)
      return PSH_ERROR;

   if (arr->size < (arr->capacity / 2) - 1) {
      arr->capacity /= 2;
      arr->uint8s = realloc(arr->uint8s, sizeof(uint8_t) * arr->capacity);
      if (arr->uint8s == NULL)
         assert(0);
   }

   arr->size--;
   *in = arr->uint8s[arr->size];
   arr->uint8s[arr->size] = 0;

   return PSH_SUCCESS;
}


/*
   * dyn_arr_psh_input functions
*/

void
dyn_arr_psh_input_init(struct dyn_arr_psh_input* _Nonnull arr) {
   *arr = (struct dyn_arr_psh_input) {
      .ins = psh_malloc(sizeof(struct psh_input*) * 8),
      .size = 0,
      .capacity = 8,
      .cur_input = 0
   };
}

//Inits the array with one empty psh_input
struct dyn_arr_psh_input* _Nonnull
dyn_arr_psh_input_create(void) {
   struct dyn_arr_psh_input* ret = psh_malloc(sizeof(*ret));
   dyn_arr_psh_input_init(ret);
   dyn_arr_psh_input_push(ret, psh_input_create());
   return ret;
}

//Takes ownership of psh_string*
struct dyn_arr_psh_input* _Nonnull
dyn_arr_psh_input_create_from(struct psh_string* _Nonnull in) {
   struct dyn_arr_psh_input* ret = psh_malloc(sizeof(*ret));
   dyn_arr_psh_input_init(ret);
   dyn_arr_psh_input_push(ret, psh_input_create_from(in));
   return ret;
}

//Takes ownership of psh_input*
struct dyn_arr_psh_input* _Nonnull
dyn_arr_psh_input_create_with(struct psh_input* _Nonnull in) {
   struct dyn_arr_psh_input* ret = psh_malloc(sizeof(*ret));
   dyn_arr_psh_input_init(ret);
   dyn_arr_psh_input_push(ret, in);
   return ret;
}

void
dyn_arr_psh_input_free(struct dyn_arr_psh_input _Nullable *arr, bool free_psh_inputs) {
   if (!arr) return;
   if (arr->ins) {
      if (free_psh_inputs) {
         for (uint i = 0; i < arr->size; i++) {
            psh_input_free(arr->ins[i], true);
         }
      }
      free(arr->ins);
   }
   arr->size = 0;
   arr->capacity = 0;
}

void
dyn_arr_psh_input_push(struct dyn_arr_psh_input* _Nonnull arr, struct psh_input* _Nonnull in) {
   if (arr->size >= arr->capacity - 1) {
      arr->capacity *= 2;
      arr->ins = psh_realloc(arr->ins, sizeof(struct psh_input*) * arr->capacity);
   }

   arr->ins[arr->size] = in;
   arr->size++;
}

void
dyn_arr_psh_input_delete_index(struct dyn_arr_psh_input _Nonnull *arr, size_t index, bool free) {
   struct psh_input** psh_inputs = arr->ins;
   if (free)
      psh_input_free(psh_inputs[index], true);

   for (size_t i = index+1; i < arr->size; i++)
      psh_inputs[i-1] = psh_inputs[i];

   arr->size--;
   memset(&arr->ins[arr->size], 0, sizeof(struct psh_input*));
}

struct psh_input* _Nullable
dyn_arr_psh_input_pop(struct dyn_arr_psh_input *arr) {
   if (arr->size == 0)
      return NULL;

   if (arr->size < (arr->capacity / 2) - 1) {
      arr->capacity /= 2;
      arr->ins = psh_realloc(arr->ins, sizeof(struct psh_input*) * arr->capacity);
   }

   arr->size--;
   struct psh_input* ret = arr->ins[arr->size];
   arr->ins[arr->size] = NULL;
   return ret;
}


/*
   * dyn_arr_jobs functions
*/
void
dyn_arr_job_init(struct dyn_arr_job *arr) {
   *arr = (struct dyn_arr_job) {
      .jobs = psh_malloc(sizeof(struct job*) * 8),
      .size = 0,
      .capacity = 8
   };
}

struct dyn_arr_job*
dyn_arr_job_create(void) {
   struct dyn_arr_job* ret = psh_malloc(sizeof(*ret));
   dyn_arr_job_init(ret);
   return ret;
}

void
dyn_arr_job_free(struct dyn_arr_job *arr, bool free_jobs) {
   if (!arr) return;
   if (arr->jobs) {
      if (free_jobs) {
         for (uint i = 0; i < arr->size; i++) {
            job_free(arr->jobs[i]);
         }
      }
      free(arr->jobs);
   }
   arr->size = 0;
   arr->capacity = 0;
}

void
dyn_arr_job_push(struct dyn_arr_job *arr, struct job* in) {
   if (arr->size >= arr->capacity - 1) {
      arr->capacity *= 2;
      arr->jobs = realloc(arr->jobs, sizeof(struct job*) * arr->capacity);
      assert(arr->jobs);
   }

   arr->jobs[arr->size] = in;
   arr->size++;
}

void
dyn_arr_job_delete_index(struct dyn_arr_job *arr, size_t index, bool free) {
   if (index >= arr->size)
      assert(0);

   struct job** jobs = arr->jobs;
   if (free)
      job_free(jobs[index]);

   for (size_t i = index+1; i < arr->size; i++)
      jobs[i-1] = jobs[i];

   arr->size--;
   arr->jobs[arr->size] = NULL;
}

void
dyn_arr_job_delete(struct dyn_arr_job *arr, struct job* job, bool free) {
   int index = -1;
   for (size_t i = 0; i < arr->size; i++) {
      if (arr->jobs[i] == job) {
         if (free) {
            job_free(job);
         }
         index = i;
         break;
      }
   }
   if (index == -1)
      return;
   
   dyn_arr_job_delete_index(arr, index, free);
}

struct job*
dyn_arr_job_pop(struct dyn_arr_job *arr) {
   if (arr->size == 0) return NULL;

   if (arr->size < (arr->capacity / 2) - 1) {
      arr->capacity /= 2;
      arr->jobs = realloc(arr->jobs, sizeof(struct job*) * arr->capacity);
      assert(arr->jobs);
   }

   arr->size--;
   struct job* ret = arr->jobs[arr->size];
   arr->jobs[arr->size] = NULL;
   return ret;
}

struct job*
dyn_arr_job_get_nb(const struct dyn_arr_job *arr, size_t job_nb) {
   if (!arr) return NULL;
   for (size_t i = 0; i < arr->size; i++) {
      if (arr->jobs[i]->job_nb == job_nb)
         return arr->jobs[i];
   }
   return NULL;
}

//Needs to be async-saf
struct job*
dyn_arr_job_get_pid(const struct dyn_arr_job *arr, pid_t pid) {
   if (!arr) return NULL;
   for (size_t i = 0; i < arr->size; i++) {
      if (arr->jobs[i]->pid == pid)
         return arr->jobs[i];
   }
   return NULL;
}

struct job*
dyn_arr_job_get_default(const struct dyn_arr_job *arr) {
   /* psh_block_signals(cur_env); */
   /* psh_unblock_signals(cur_env); */
   for (size_t i = 0; i < arr->size; i++) {
      if (arr->jobs[i]->current == JOB_CUR_DEFAULT)
         return arr->jobs[i];
   }
   return NULL;
}

struct job*
dyn_arr_job_get_foreground(const struct dyn_arr_job *arr) {
   /* psh_block_signals(cur_env); */
   /* psh_unblock_signals(cur_env); */
   for (size_t i = 0; i < arr->size; i++) {
      if (arr->jobs[i]->is_foreground)
         return arr->jobs[i];
   }
   return NULL;
}

struct job*
dyn_arr_job_get_next(const struct dyn_arr_job *arr) {
   /* psh_block_signals(cur_env); */
   /* psh_unblock_signals(cur_env); */
   for (size_t i = 0; i < arr->size; i++) {
      if (arr->jobs[i]->current == JOB_CUR_NEXT)
         return arr->jobs[i];
   }
   return NULL;
}

//jobs needs to be have no "JOB_CUR_NEXT" job, call this after "dyn_arr_job_get_next"
void
dyn_arr_job_find_new_next(struct dyn_arr_job *jobs) {
   /* psh_block_signals(cur_env); */
   /* psh_unblock_signals(cur_env); */
   for (size_t i = 0; i < jobs->size; i++) {
      if (jobs->jobs[i]->current == JOB_CUR_REST) {
         jobs->jobs[i]->current = JOB_CUR_NEXT;
         break;
      }
   }
}

///Gets jobs from a job id string
///Returns NULL if it doesn't find any
struct job*
dyn_arr_job_get(const struct dyn_arr_job *jobs, const char* id) {
   /* psh_block_signals(cur_env); */
   /* psh_unblock_signals(cur_env); */
   if (!jobs || !id || id[0] != '%')
      return NULL;
   int len_id = strlen(id);
   if (len_id < 2)
      return NULL;

   //Current job
   if (len_id == 2 && (id[1] == '%' || id[1] == '+')) {
      return dyn_arr_job_get_default(jobs);
   }

   //Next job
   if (len_id == 2 && id[1] == '-') {
      return dyn_arr_job_get_next(jobs);
   }

   //Get job nb
   bool is_nb = true;
   for (int i = 1; i < len_id; i++) {
      if (!isdigit(id[i])) { is_nb = false; break; }
   }

   if (is_nb) {
      errno = 0;
      uint64_t nb = strtoll(id+1, NULL, 10);
      if (errno != 0) {
         return NULL;
      }
      return dyn_arr_job_get_nb(jobs, nb);;
   }

   //If it's '%?string'
   if (id[1] == '?') {
      for (size_t i = 0; i < jobs->size; i++) {
         //I think it's right?
         if (strcasestr(jobs->jobs[i]->in_command, id+2)) {
            return jobs->jobs[i];
         }
      }
   }

   //Else it's '%string', needs to start with the string
   for (size_t i = 0; i < jobs->size; i++) {
      //I think it's right?
      if (strcasestr(jobs->jobs[i]->in_command, id+2) == jobs->jobs[i]->in_command) {
         return jobs->jobs[i];
      }
   }

   return NULL;
}

//No one can mess with the array, maybe block some signals?
void
dyn_arr_job_clean_done(struct dyn_arr_job* arr) {
   /* psh_block_signals(cur_env); */
   /* psh_unblock_signals(cur_env); */
   dyn_arr_job_clean_waited_for(arr);
   
   for (size_t i = 0; i < arr->size; i++) {
      struct job* job = arr->jobs[i];
      if (job->state == JOB_STATE_DONE) {
         printf("[%lu]%c %s %s\n", job->job_nb,
            (job->current == JOB_CUR_DEFAULT) ? '+' : (job->current == JOB_CUR_NEXT) ? '-' : ' ',
            //TODO: add Stopped(...) and Done(...)
            (job->state == JOB_STATE_RUNNING) ? "Running"
               : (job->state == JOB_STATE_DONE) ? "Done" : "Stopped",
            job->in_command);

         dyn_arr_job_delete_index(arr, i, true);
      }
   }
}

//No one can mess with the array, maybe block some signals?
void
dyn_arr_job_clean_waited_for(struct dyn_arr_job* arr) {
   /* psh_block_signals(cur_env); */
   /* psh_unblock_signals(cur_env); */
   for (size_t i = 0; i < arr->size; i++) {
      struct job* job = arr->jobs[i];
      if (job->finished_wait) {
         dyn_arr_job_delete_index(arr, i, true);
      }
   }
}
