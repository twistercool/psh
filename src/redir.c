#include "redir.h"

//TODO "If word evaluates to '-', file descriptor n, or standard input if n is not specified, shall be closed. Attempts to close a file descriptor that is not open shall not constitute an error. If word evaluates to something else, the behavior is unspecified."
//TODO needs to return exactly what all redirections are
enum redirect_output
perform_redirections(struct dyn_arr_node* io_redirects, int fd_in, int fd_out) {
   bool did_redir_input = false;
   bool did_redir_output = false;
   int new_fd;
   size_t nb_elems = (io_redirects == NULL) ? 0 : io_redirects->size;
   for (size_t i = 0; i < nb_elems; i++) {
      struct psh_node* cur_node_io_redirect = io_redirects->nodes[i];
      bool is_heredoc = cur_node_io_redirect->node_io_redirect.is_heredoc;
      int64_t redir_nb = cur_node_io_redirect->node_io_redirect.io_number;
      struct psh_node* cur_io_file = cur_node_io_redirect->node_io_redirect.node_io_file;
      struct psh_node* cur_io_here = cur_node_io_redirect->node_io_redirect.node_io_here;
      char* redir_filename = cur_io_file->node_io_file.filename;
      int64_t redir_filename_nb;
      enum token_type redir_toktype = cur_io_file->node_io_file.redirect_op;
      int err;
      if (is_heredoc) {
         /* fprintf(stderr, "performing redirection for heredoc\n"); */
         //just make a pipe, stuff the string into the input
         //close the input fd and set the output fd as stdin
         int pipefd[2];
         if (pipe(pipefd) == -1) {
            fprintf(stderr, "piersh, failed to perform redirection of heredoc\n");
            //exit?
         }
         char* heredoc = cur_io_here->node_io_here.heredoc;
         redir_nb = (redir_nb == -1) ? 0 : redir_nb;
         ssize_t write_ret = write(pipefd[1], heredoc, strlen(heredoc));
         if (write_ret == -1) {
            TODO();
         }
         close(pipefd[1]);
         errno = 0;
         err = dup2(pipefd[0], redir_nb);
         assert(err >= 0);
         close(pipefd[0]);
      } else {
         switch (cur_io_file->node_io_file.redirect_op) {
            case TOKTYPE_LESSTHAN: // <
               redir_nb = (redir_nb >= 0) ? redir_nb : 0;
               new_fd = open(redir_filename, O_RDONLY);
               assert(new_fd != -1);
               err = dup2(new_fd, redir_nb);
               assert(err >= 0);
               close(new_fd);
               did_redir_input = true;
               break;
            case TOKTYPE_GREATTHAN: // >
               //TODO check if noclobber is set, if yes, fail if file exists
               redir_nb = (redir_nb >= 0) ? redir_nb : 1;
               new_fd = creat(redir_filename, 0644);
               assert(new_fd != -1);
               err = dup2(new_fd, redir_nb);
               assert(err >= 0);
               close(new_fd);
               did_redir_output = true;
               break;
            case TOKTYPE_CLOBBER: // >|
               redir_nb = (redir_nb >= 0) ? redir_nb : 1;
               new_fd = creat(redir_filename, 0644);
               assert(new_fd != -1);
               err = dup2(new_fd, redir_nb);
               assert(err >= 0);
               close(new_fd);
               did_redir_output = true;
               break;
            case TOKTYPE_DGREAT: // >>
               redir_nb = (redir_nb >= 0) ? redir_nb : 1;
               new_fd = open(redir_filename, O_WRONLY | O_APPEND | O_CREAT, 0644); //TODO 0644 should be affected by the umask
               assert(new_fd != -1);
               err = dup2(new_fd, redir_nb);
               assert(err >= 0);
               close(new_fd);
               did_redir_output = true;
               break;
            case TOKTYPE_LESSGREAT: // <>
               redir_nb = (redir_nb >= 0) ? redir_nb : 1;
               new_fd = open(redir_filename, O_TRUNC | O_WRONLY | O_RDONLY | O_CREAT, 0644);
               assert(new_fd != -1);
               err = dup2(new_fd, redir_nb);
               assert(err >= 0);
               close(new_fd);
               did_redir_output = true;
               break;
            case TOKTYPE_LESSAND: // <&
               redir_nb = (redir_nb >= 0) ? redir_nb : 0;
               errno = 0;
               redir_filename_nb = strtoll(redir_filename, NULL, 10);
               if (errno != 0 || redir_filename_nb < 0) {
                  fprintf(stderr, "piersh: '%s': bad file descriptor\n", redir_filename);
               }
               else {
                  if (dup2(redir_filename_nb, redir_nb) == -1) {
                     fprintf(stderr, "piersh: '%s': failed to redirect\n", redir_filename);
                  }
               }
               break;
            case TOKTYPE_GREATAND: // >&
               //TODO did I implement this right?
               redir_nb = (redir_nb >= 0) ? redir_nb : 1;
               redir_filename_nb = strtoll(redir_filename, NULL, 10);
               errno = 0;
               if (errno != 0 || redir_filename_nb < 0) {
                  fprintf(stderr, "piersh: '%s': bad file descriptor\n", redir_filename);
               }
               else {
                  if (dup2(redir_nb, redir_filename_nb) == -1) {
                     fprintf(stderr, "piersh: '%s': failed to redirect\n", redir_filename);
                  }
               }
               break;
            default:
               assert(0);
         }
      }
   }

   enum redirect_output ret;
   if (!did_redir_input && !did_redir_output)
      ret = REDIR_NONE;
   else if (!did_redir_input && did_redir_output)
      ret = REDIR_OUT;
   else if (did_redir_input && !did_redir_output)
      ret = REDIR_IN;
   else
      ret = REDIR_BOTH;

   if (fd_in != STDIN_FILENO) {
      if (ret == false) {
         dup2(fd_in, STDIN_FILENO);
      }
      close(fd_in);
   }
   if (fd_out != STDOUT_FILENO) {
      if (ret == false) {
         dup2(fd_out, STDOUT_FILENO);
      }
      close(fd_out);
   }
   return ret;
}

int*
save_fds(void) {
   //fds that are saved by save_fd shouldn't be backed up
   bool is_owned[MAX_FD] = {0};
   int cur_fd = MAX_FD - 1;
   while (cur_fd > 2 && fcntl(cur_fd, F_GETFD) != -1) {
      cur_fd--;
   }
   if (cur_fd <= 2) {
      fprintf(stderr, "piersh: failed to save file descriptors: no fds left\n");
      assert(0);
   }
   int* arr_fd = psh_malloc(sizeof(int) * MAX_FD);
   memset(arr_fd, -1, sizeof(int) * MAX_FD);
   //check if we created the fd or else they duplicate themselves
   for (int i = 0; i < MAX_FD; i++) {
      if (fcntl(i, F_GETFD) != -1 && !is_owned[i]) {
         arr_fd[i] = dup2(i, cur_fd);
         is_owned[cur_fd] = true;
         cur_fd--;
         while (cur_fd > 2 && arr_fd[cur_fd] != -1) {
            cur_fd--;
         }
         if (cur_fd <= 2) {
            fprintf(stderr, "piersh: failed to save file descriptors: no fds left\n");
            assert(0);
         }
      }
      else
         arr_fd[i] = -1;
   }

   return arr_fd;
}

void
restore_fds(int* arr_fd) {
   for (uint32_t i = 0; i < MAX_FD; i++) {
      if (arr_fd[i] != -1) {
         dup2(arr_fd[i], i);
         close(arr_fd[i]);
      }
   }
}
