#include "struct.h"

///Guaranteed to work or panic
struct psh_string* _Nonnull
psh_string_create(const char* input _Nullable) {
   if (input == NULL)
      input = "";

   size_t str_len = (size_t) strlen(input);
   size_t arr_size = 8;

   while (arr_size <= str_len + 1)
      arr_size *= 2;

   struct psh_string* ret = psh_calloc(sizeof(struct psh_string));
   char* ret_str = psh_calloc(arr_size);
   ret->str = ret_str;
   ret->str_len = str_len;
   ret->arr_size = arr_size;
   strcpy(ret->str, input);

   return ret;
}

struct psh_string* _Nonnull
psh_string_clone(const struct psh_string* in_psh_str _Nonnull) {
   struct psh_string* ret = psh_calloc(sizeof(struct psh_string));
   ret->arr_size = 8;
   while (ret->arr_size <= in_psh_str->str_len) {
      ret->arr_size *= 2;
   }
   ret->str = psh_malloc(ret->arr_size);
   strcpy(ret->str, in_psh_str->str);
   ret->str_len = in_psh_str->str_len;

   return ret;
}

void
psh_string_free(struct psh_string* input) {
   if (input != NULL) {
      free(input->str);
      free(input);
   }
}

inline void
_psh_resize_increase(struct psh_string* in_psh_str, uint64_t size) {
   while (in_psh_str->arr_size <= size) {
      in_psh_str->arr_size *= 2;
      in_psh_str->str = realloc(in_psh_str->str, in_psh_str->arr_size);
      if (in_psh_str->str == NULL)
         assert(0);
   }
/*    return PSH_SUCCESS; */
}

void
psh_string_append_char(struct psh_string* in_str, char input) {
   /*TODO _psh_resize_increase(in_psh_str, in_psh_str->str_len + 2); */
   if (!in_str)
      return;
   while (in_str->arr_size <= in_str->str_len + 2) {
      in_str->arr_size *= 2;
      in_str->str = psh_realloc(in_str->str, in_str->arr_size);
   }

   in_str->str[in_str->str_len] = input;
   in_str->str[in_str->str_len + 1] = '\0';

   in_str->str_len++;
}

void
psh_string_append_str(struct psh_string* in_psh_str, const char* in_str) {
   if (!in_str)
      return;
   size_t str_len = (size_t) strlen(in_str);
   /*TODO _psh_resize_increase(in_psh_str, in_psh_str->str_len + str_len + 1); */
   while (in_psh_str->arr_size <= in_psh_str->str_len + str_len + 1) {
      in_psh_str->arr_size *= 2;
      in_psh_str->str = realloc(in_psh_str->str, in_psh_str->arr_size);
      if (in_psh_str->str == NULL)
         assert(0);
   }

   strcat(in_psh_str->str + in_psh_str->str_len, in_str);

   in_psh_str->str_len += str_len;
}

void
psh_string_append_psh_str(struct psh_string* in_psh_str, const struct psh_string* const in_str) {
   if (!in_str)
      return;
   size_t str_len = in_str->str_len;
   /*TODO _psh_resize_increase(in_psh_str, in_psh_str->str_len + str_len + 1); */
   while (in_psh_str->arr_size <= in_psh_str->str_len + str_len + 1) {
      in_psh_str->arr_size *= 2;
      in_psh_str->str = realloc(in_psh_str->str, in_psh_str->arr_size);
      assert(in_psh_str->str);
   }

   strcat(in_psh_str->str + in_psh_str->str_len, in_str->str);

   in_psh_str->str_len += str_len;
}

void
psh_string_insert_char(struct psh_string* in_psh_str, char c, uint64_t index) {
   if (index == in_psh_str->str_len) {
      psh_string_append_char(in_psh_str, c);
      return;
   } else if (index > in_psh_str->str_len) {
      return;
   }

   /*TODO _psh_resize_increase(in_psh_str, in_psh_str->str_len + str_len + 2); */
   while (in_psh_str->arr_size <= in_psh_str->str_len + 2) {
      in_psh_str->arr_size *= 2;
      in_psh_str->str = realloc(in_psh_str->str, in_psh_str->arr_size);
      if (in_psh_str->str == NULL)
         assert(0);
   }

   memmove(in_psh_str->str + index + 1, in_psh_str->str + index, in_psh_str->str_len - index + 1);
   in_psh_str->str[index] = c;
   
   in_psh_str->str_len++;
}

void
psh_string_insert_str(struct psh_string* in_psh_str, const char* str, uint64_t index) {
   if (index == in_psh_str->str_len) {
      psh_string_append_str(in_psh_str, str);
      return;
   } else if (index > in_psh_str->str_len) {
      return;
   }

   size_t str_len = strlen(str);

   /*TODO _psh_resize_increase(in_psh_str, in_psh_str->str_len + str_len + 2); */
   while (in_psh_str->arr_size <= in_psh_str->str_len + str_len + 1) {
      in_psh_str->arr_size *= 2;
      in_psh_str->str = realloc(in_psh_str->str, in_psh_str->arr_size);
      if (in_psh_str->str == NULL)
         assert(0);
   }

   memmove(in_psh_str->str + index + str_len, in_psh_str->str + index, in_psh_str->str_len - index + str_len);
   memcpy(in_psh_str->str + index, str, str_len);
   
   in_psh_str->str_len += str_len;
}

void
psh_string_delete_char(struct psh_string* in_psh_str, uint64_t index) {
   if (index >= in_psh_str->str_len) {
      return;
   }

   /*TODO _psh_resize_decrease(in_psh_str, in_psh_str->str_len + str_len + 2); */

   memmove(in_psh_str->str + index, in_psh_str->str + index + 1, in_psh_str->str_len - index);
   
   in_psh_str->str_len--;
   in_psh_str->str[in_psh_str->str_len] = '\0'; //idk if necessary?
}

void
psh_string_delete_chars(struct psh_string* in_psh_str, uint64_t index, size_t len) {
   if (index + len >= in_psh_str->str_len) {
      return;
   }

   /*TODO _psh_resize_decrease(in_psh_str, in_psh_str->str_len + str_len + 2); */

   memmove(in_psh_str->str + index, in_psh_str->str + index + len, in_psh_str->str_len - index + 1);
   
   in_psh_str->str_len -= len;
   in_psh_str->str[in_psh_str->str_len] = '\0'; //idk if necessary?
}

void
psh_string_reset(struct psh_string* _Nonnull in_psh_str) {
   if (in_psh_str->arr_size == 0) assert(0);
   in_psh_str->str[0] = '\0';
   in_psh_str->str_len = 0;
}


///Either frees the psh_string and returns the equivalent char* or strdups the char*
inline char*
psh_string_to_cstr(struct psh_string* in_psh_str, bool free_psh_string) {
   if (!in_psh_str)
      return NULL;

   if (free_psh_string) {
      char* ret = realloc(in_psh_str->str, in_psh_str->str_len + 1);
      assert(ret);
      free(in_psh_str);
      return ret;
   } else {
      char* ret = psh_malloc(in_psh_str->str_len + 1);
      strcpy(ret, in_psh_str->str);
      return ret;
   }
}

struct psh_string* _Nullable
psh_string_from_nb(uint64_t nb) {
   struct psh_string* ret_str = psh_string_create("");

   if (nb == 0) {
      psh_string_append_char(ret_str, '0');
      return ret_str;
   }

   while (nb) {
      char cur_digit = nb % 10 + '0';
      psh_string_append_char(ret_str, cur_digit);
      nb /= 10;
   }

   char* str = ret_str->str;
   uint64_t len = ret_str->str_len;
   for (uint64_t i = 0; i < len / 2; i++) {
      char tmp = str[len - i - 1];
      str[len - i - 1] = str[i];
      str[i] = tmp;
   }

   return ret_str;
}


//

struct psh_input*
psh_input_create(void) {
   struct psh_input* ret = psh_malloc(sizeof(struct psh_input));
   *ret = (struct psh_input) {
      .cur_pos = 0,
      .is_last_EOF = false,
      .psh_str = psh_string_create(NULL)
   };
   return ret;
}

///Takes ownership of psh_string
struct psh_input* _Nullable
psh_input_create_from(struct psh_string* _Nonnull in) {
   struct psh_input* ret = psh_malloc(sizeof(struct psh_input));
   *ret = (struct psh_input) {
      .cur_pos = 0,
      .is_last_EOF = false,
      .psh_str = in
   };
   return ret;
}

void
psh_input_reset(struct psh_input* in) {
   psh_string_free(in->psh_str);
   in->psh_str = psh_string_create(NULL);
   in->cur_pos = 0;
}

void
psh_input_free(struct psh_input* in, bool free_pstr) {
   if (free_pstr)
      psh_string_free(in->psh_str);
   free(in);
}

inline int
psh_peekc(struct shell_env* cur_env) {
   struct psh_input* input = cur_input(cur_env);
   if (input->cur_pos >= input->psh_str->str_len)
      return EOF;
   else
      return input->psh_str->str[input->cur_pos];
}

struct heredoc {
   size_t start_index;
   size_t end_index;
};
struct heredoc heredoc_from_begin_pos(struct dyn_arr_tok* in, size_t cur_pos);
struct heredoc heredoc_from_end_pos(struct dyn_arr_tok* in, size_t cur_pos);

inline int
psh_getc(struct shell_env* cur_env) {
   struct psh_input* input = cur_input(cur_env);
   struct heredoc here = heredoc_from_begin_pos(cur_env->tok_arr, input->cur_pos);
   if (here.start_index != 0) {
      input->cur_pos += here.end_index - here.start_index;
   }

   if (input->cur_pos >= input->psh_str->str_len) {
      if (!input->is_last_EOF)
         input->is_last_EOF = true;
      return EOF;
   }
   else
      return input->psh_str->str[input->cur_pos++];
}

inline void
psh_ungetc(struct shell_env* cur_env) {
   struct psh_input* input = cur_input(cur_env);
   struct heredoc here = heredoc_from_end_pos(cur_env->tok_arr, input->cur_pos);
   if (here.end_index != 0)
      input->cur_pos += here.end_index - here.start_index;

   if (input->is_last_EOF) {
      input->is_last_EOF = false;
      return;
   }
      
   if (input->cur_pos > 0)
      --input->cur_pos;
}

void
psh_skip_whitespace(struct shell_env* cur_env) {
   int next_c;
   while (1) {
      next_c = psh_peekc(cur_env);
      if (next_c != ' ' && next_c != '\t')
         return;
      psh_getc(cur_env);
   }
}

void
psh_reverse_skip_whitespace(struct psh_input* input) {
   int cur_c;
   int cur_pos;
   input->is_last_EOF = false;
   if ((cur_pos = input->cur_pos) > 0)
      input->cur_pos--;

   while ((cur_pos = input->cur_pos) > 0) {
      cur_c = input->psh_str->str[cur_pos];
      if (cur_c != ' ' && cur_c != '\t')
         return;
      input->cur_pos--;
   }
}

inline char* _Nonnull
psh_strdup(const char* in) {
   if (!in) return NULL;
   char* ret = psh_malloc(strlen(in) + 1);
   strcpy(ret, in);
   return ret;
}

///Cannot fail, panics if out of memory
void* _Nonnull
psh_malloc(size_t in_len) {
   void* ret = malloc(in_len);
   assert(ret != NULL);
   return ret;
}

///Cannot fail, panics if out of memory
void* _Nonnull
psh_calloc(size_t in_len) {
   void* ret = calloc(1, in_len);
   assert(ret != NULL);
   return ret;
}

void* _Nonnull
psh_realloc(void* in, size_t len) {
   void* ret = realloc(in, len);
   assert(ret != NULL);
   return ret;
}


