#include "pattern_match.h"


bool
is_pattern_match(const char* pat, const char* str) {
   if (!pat || !str)
      return false;

   //* doesn't match hidden files
   if (pat[0] == '*') {
      const char* patt = pat;
      while (*patt == '*')
         patt++;
      if (!*patt)
         return str[0] != '.';
   }

   //This is because pattern ".*" does match "." and ".." but not "*." for some reason
   if (!strcmp(str, ".")) {
      if (pat[0] != '.')
         return false; 
   } else if (!strcmp(str, "..")) {
      if (pat[0] != '.')
         return false;
   }

   while (*pat && *str) {
      switch (*pat) {
         case '\\': {
            pat++;
            if (!*pat) {
               //error?
               return false;
            }
            if (*pat != *str) return false;
            pat++; str++;
         } break;
         case '*': {
            if (is_pattern_match(pat+1, str))
               return true;
            str++;
         } break;
         case '[': {
            pat++;
            bool is_reversed = false;
            if (!*pat) {
               //error?
               return false;
            } else if (*pat == '!') {
               is_reversed = true;
               pat++;
            }
            bool found = false;
            while (*pat != ']') {
               if (found) {
                  pat++;
                  continue;
               } else if (!*pat) {
                  //error?
                  return false;
               } else if (*pat == *str) {
                  if (is_reversed)
                     return false;
                  else
                     found = true;
               }
               pat++;
            }
            if (!found && !is_reversed) return false;
            else { pat++; str++; }
         } break;
         case '?': { pat++; str++; } break;
         default: {
            if (*pat != *str) return false;
            pat++; str++;
         } break;
      }
   }

   if (!*str) {
      while (*pat == '*') pat++;
      return !*pat;
   } else return false;
}

//TODO maybe need to worry about quoting?
//shouldn't return the empty string ever normally
//if path starts with '/', it's absolute
struct psh_string*
next_word_in_path(const char* in) {
   if (!in || !*in)
      return NULL;

   struct psh_string* ret = psh_string_create("");
   if (*in == '/') {
      psh_string_append_char(ret, '/');
      while (*in == '/')
         in++;
   }
   while (1) {
      if (!*in || *in == '/') {
         return ret;
      } else {
         psh_string_append_char(ret, *in);
      }
      in++;
   }
}

struct psh_string*
remove_max_suffix(const char* str, const char* pat) {
   for (const char* cur_str = str; *cur_str; cur_str++) {
      if (is_pattern_match(pat, cur_str)) {
         //return up until cur_str - 1
         size_t ret_len = cur_str - str;
         char* tmp_ret = malloc(ret_len + 1);
         memcpy(tmp_ret, str, ret_len);
         tmp_ret[ret_len] = '\0';
         struct psh_string* ret = psh_string_create(tmp_ret);
         free(tmp_ret);
         return ret;
      }
   }

   return psh_string_create(str);
}

struct psh_string*
remove_min_suffix(const char* str, const char* pat) {
   const char* cur_str = str;
   while (*cur_str) {
      cur_str++;
   }

   for (; cur_str >= str; cur_str--) {
      if (is_pattern_match(pat, cur_str)) {
         //return up until cur_str - 1
         size_t ret_len = cur_str - str;
         char* tmp_ret = malloc(ret_len + 1);
         memcpy(tmp_ret, str, ret_len);
         tmp_ret[ret_len] = '\0';
         struct psh_string* ret = psh_string_create(tmp_ret);
         free(tmp_ret);
         return ret;
      }
   }

   return psh_string_create(str);
}

struct psh_string*
remove_max_prefix(const char* str, const char* pat) {
   const char* cur_str = str;
   while (*cur_str) {
      cur_str++;
   }

   for (; cur_str >= str; cur_str--) {
      //construct prefix: str till cur_str
      size_t cur_str_len = cur_str - str;
      char* tmp_cur_str = malloc(cur_str_len + 1);
      memcpy(tmp_cur_str, str, cur_str_len);
      tmp_cur_str[cur_str_len] = '\0';

      if (is_pattern_match(pat, tmp_cur_str)) {
         free(tmp_cur_str);
         return psh_string_create(cur_str);
      }
      free(tmp_cur_str);
   }

   return psh_string_create(str);
}

struct psh_string*
remove_min_prefix(const char* str, const char* pat) {
   for (const char* cur_str = str; *cur_str; cur_str++) {
      //construct prefix: str till cur_str
      size_t cur_str_len = cur_str - str;
      char* tmp_cur_str = malloc(cur_str_len + 1);
      memcpy(tmp_cur_str, str, cur_str_len);
      tmp_cur_str[cur_str_len] = '\0';

      if (is_pattern_match(pat, tmp_cur_str)) {
         free(tmp_cur_str);
         return psh_string_create(cur_str);
      }
      free(tmp_cur_str);
   }

   return psh_string_create(str);
}
