#include "psh.h"

//Only call once per process (I think)
void
init_shell_env(struct shell_env* in_env, int argc, char** argv, char** env) {
   memset(in_env, 0, sizeof(struct shell_env));
   psh_log_init();
   setup_sighandlers(in_env);
   in_env->var_table = sym_table_create(4096 / sizeof(struct sym_table));
   in_env->alias_table = sym_table_create(4096 / sizeof(struct sym_table));
   in_env->func_table = sym_table_create(4096 / sizeof(struct sym_table));
   /* in_env->psh_input = psh_input_create(); */

   if (env) {
      while (*env) {
         char* cur_str = psh_strdup(*env);
         const char* name = strtok(cur_str, "=");
         const char* value = strtok(NULL, "");
         if (value == NULL)
            value = "";
         sym_var_insert(in_env->var_table, name, value, true);
         env++;
         free(cur_str);
      }
   }
   if (argv) {
      in_env->arg0 = psh_strdup(argv[0]);
   }
   in_env->pid_invoked_shell = getpid();

   in_env->args = dyn_arr_str_create();

   sym_init_undefined_variables(in_env->var_table);
   sym_init_builtin_functions(in_env->func_table);

   in_env->tok_arr = dyn_arr_tok_create();
   in_env->jobs = dyn_arr_job_create();
   in_env->psh_inputs = dyn_arr_psh_input_create();
   /* in_env->tmp_alloc = tmp_alloc_create(); */
}

///Destroys shell_env
void
free_shell_env(struct shell_env* in_env, bool free_input) {
   destroy_sighandlers(in_env);
   /* tmp_alloc_free(in_env->tmp_alloc); */
   dyn_arr_psh_input_free(in_env->psh_inputs, true);
   free(in_env->psh_inputs);
   dyn_arr_tok_free(in_env->tok_arr);
   free(in_env->tok_arr);
   dyn_arr_job_free(in_env->jobs, true);
   free(in_env->jobs);
   dyn_arr_str_free(in_env->args, true);
   free(in_env->arg0);
   free(in_env->args);
   /* if (in_env->psh_input) */
   /*    psh_string_free(in_env->psh_input->psh_str); */
   /* free(in_env->psh_input); */
   sym_table_free(in_env->alias_table);
   sym_table_free(in_env->var_table);
   sym_table_free(in_env->func_table);
   
   memset(in_env, 0, sizeof(struct shell_env));

   if (free_input)
      free(in_env);
}


struct psh_string* _Nullable
read_file_to_psh_string(const char* path) {
   FILE* file_ptr = NULL;
   struct psh_string* ret_pstr = NULL;

   file_ptr = fopen(path, "r");
   if (!file_ptr)
      goto error;

   ret_pstr = psh_string_create(NULL);
   
   int c = EOF;
   while ((c = fgetc(file_ptr)) != EOF) {
      if (ferror(file_ptr) != 0) {
         fprintf(stderr, "piersh: error reading '%s'\n", path);
         goto error;
      }
      psh_string_append_char(ret_pstr, (uint8_t) c);
   }
   
   fclose(file_ptr);

   return ret_pstr;
error:
   free(ret_pstr);
   if (file_ptr)
      fclose(file_ptr);

   return NULL;
}

/*
   * Sets errno to the error of the open if returns NULL
*/
struct psh_input* _Nullable
read_file_to_psh_input(const char* path) {
   struct psh_string* _Nullable file_pstr = read_file_to_psh_string(path);
   if (!file_pstr) {
      return NULL;
   }

   return psh_input_create_from(file_pstr);
}

enum psh_error
source_profile_files(struct shell_env* cur_env) {
   if (cur_env->is_interactive) {
      if (cur_env->is_login) {
         struct psh_result* ret_result = exec_dot_script(cur_env, "/etc/profile");
         if (ret_result->error != PSH_SUCCESS || ret_result->exit_status != 0) {
            perror("read_profile_files");
         }
         free(ret_result);

         DIR* dir = opendir("/etc/profile.d");
         if (dir == NULL)
            return PSH_SUCCESS;
         
         struct dirent* cur_ent;
         while ( (cur_ent = readdir(dir)) != NULL ) {
            struct stat st_file;
            struct psh_string* filename_pstr = psh_string_create("/etc/profile.d/");
            pstr_append(filename_pstr, cur_ent->d_name);
            if (stat(filename_pstr->str, &st_file) == -1) {
               perror("read_profile_files: stat");
               psh_string_free(filename_pstr);
               continue;
            }
      
            if (st_file.st_mode & S_IFDIR) {
               continue;
            }

            struct psh_result* cur_res = exec_dot_script(cur_env, filename_pstr->str);
            free(cur_res);
            psh_string_free(filename_pstr);
         }

         struct sym_var_entry* cur_entry = sym_get_var_entry(cur_env->var_table, "HOME");
         if (cur_entry == NULL || strlen(cur_entry->value) == 0) {
            perror("read_profile_files: sym_get_var_entry");
            return PSH_ERROR;
         }

         struct psh_string* pstr_profile = psh_string_create(cur_entry->value);
         pstr_append(pstr_profile, "/.profile");
         struct stat pstr_profile_st;
         if (stat(pstr_profile->str, &pstr_profile_st) == -1) {
            psh_string_free(pstr_profile);
            return PSH_SUCCESS;
         }
         struct psh_result* cur_res = exec_dot_script(cur_env, pstr_profile->str);
         free(cur_res);
      } else {
         struct sym_var_entry* cur_entry = sym_get_var_entry(cur_env->var_table, "HOME");
         if (cur_entry == NULL || strlen(cur_entry->value) == 0) {
            perror("read_profile_files: sym_get_var_entry");
            return PSH_ERROR;
         }

         struct psh_string* pstr_pshrc = psh_string_create(cur_entry->value);
         pstr_append(pstr_pshrc, "/.pshrc");
         struct stat pstr_pshrc_st;
         if (stat(pstr_pshrc->str, &pstr_pshrc_st) == -1) {
            psh_string_free(pstr_pshrc);
            return PSH_SUCCESS;
         }
         struct psh_result* cur_res = exec_dot_script(cur_env, pstr_pshrc->str);
         free(cur_res);
      }
   }

   return PSH_SUCCESS;
}



//The same as fgetc(stdin), but if it's interuppted, it checks if there are awaiting traps immediately
int
psh_fgetc(struct shell_env* cur_env) {
   int ret;
   while ((ret = fgetc(stdin)) == EOF) {
      if (ferror(stdin)) {
         //Should not get EINTR since sigaction has SA_RESTART flag, but we never know
         if (errno == EINTR) {
            clearerr(stdin);
            check_and_exec_traps(cur_env);
            continue;
         } else {
            perror("psh: reading err reading stdin");
            continue;
         }
      //or it's just an actual EOF
      } else {
         return ret;
      }
   }

   return ret;
}

/* void */
/* set_psh_input(struct shell_env* cur_env, size_t nb) { */
   
/* } */

static void
erase_line(struct shell_env* cur_env, uint64_t cur_pos) {
   for (uint i = cur_pos; i < cur_input(cur_env)->psh_str->str_len; i++) {
      printf(CURSOR_RIGHT);
   }
   for (uint i = 0 ; i < cur_input(cur_env)->psh_str->str_len; i++) {
      printf("\b \b");
   }
}

static void
paint_line(struct shell_env* cur_env, uint64_t* cur_pos) {
   printf("%s", cur_input(cur_env)->psh_str->str);
   *cur_pos = cur_input(cur_env)->psh_str->str_len;
}

enum psh_error
psh_start_interactive_shell(struct shell_env* cur_env) {
   //read all input to the psh_input char by char
   struct psh_node* cur_node = NULL;

   read_histfile(cur_env);

   //Has to be at least 1
   cur_env->psh_inputs->cur_input = cur_env->psh_inputs->size - 1;


   //Sets terminal settings
   struct termios term;
   tcgetattr(STDIN_FILENO, &term);
   term.c_lflag &= ~(ICANON | ECHO);
   tcsetattr(STDIN_FILENO, TCSANOW, &term);

   /* setbuf(stdin, NULL); */

   goto skip;
bubble_error:
   psh_input_reset(cur_input(cur_env));

   dyn_arr_tok_free(cur_env->tok_arr);
   memset(cur_env->tok_arr, 0, sizeof(struct dyn_arr_tok));
   dyn_arr_tok_init(cur_env->tok_arr);

   //check for finished background jobs
   dyn_arr_job_clean_done(cur_env->jobs);
skip:;

   const char* PS1 = sym_get_value(cur_env->var_table, "PS1");
   if (!PS1)
      PS1 = "";

   printf("%s", PS1);
   fflush(stdout);

   uint64_t cur_pos = 0;

   int c;
   while (1) {
      c = psh_fgetc(cur_env);
      /* PSH_LOG(LOG_DEBUG, "Getting char: '%d'\n", c); */

      switch (c) {
         case ESC_C: {
            c = psh_fgetc(cur_env);
            if (c == EOF)
               assert(0);
            else if (c == '[') {
               while ((c = psh_fgetc(cur_env)) == EOF)
                  ;
               if (c == 'C') {
                  if (cur_pos < cur_input(cur_env)->psh_str->str_len) {
                     printf(CURSOR_RIGHT);
                     cur_pos++;
                  }
               } else if (c == 'D') {
                  if (cur_pos > 0) {
                     printf("\b");
                     cur_pos--;
                  }
               //TODO: finish logic of those
               } else if (c == 'A') { //UP
                  if (cur_env->psh_inputs->cur_input > 0) {
                     erase_line(cur_env, cur_pos);
                     cur_env->psh_inputs->cur_input--;
                     paint_line(cur_env, &cur_pos);
                  }
               } else if (c == 'B') { //DOWN
                  if (cur_env->psh_inputs->cur_input < cur_env->psh_inputs->size - 1) {
                     erase_line(cur_env, cur_pos);
                     cur_env->psh_inputs->cur_input++;
                     paint_line(cur_env, &cur_pos);
                  }
               }
            }
         } break;

         case 127: { // delete
            if (cur_pos > 0) {
               if (cur_pos == cur_input(cur_env)->psh_str->str_len) {
                  psh_string_delete_char(cur_input(cur_env)->psh_str, cur_pos - 1);
                  printf("\b \b");
               } else {
                  psh_string_delete_char(cur_input(cur_env)->psh_str, cur_pos - 1);
                  printf(" \b");
                  printf("%s", cur_input(cur_env)->psh_str->str + cur_pos - 1);
                  printf("  \b");
                  for (int i = cur_input(cur_env)->psh_str->str_len; i >= (int)cur_pos; i--) {
                     printf("\b");
                  }

               }
               cur_pos--;
            }
         } break;

         case 10: { // line feed
            putc('\n', stdout);

            /* fprintf(stderr, "Parsed input: '%s'\n", cur_input(cur_env)->psh_str->str); */

            //run the input
            if (cur_input(cur_env)->psh_str->str_len > 0) {
               cur_node = parse_program(cur_env);
               if (IS_PARSE_ERR(cur_node)) {
                  fprintf(stderr, "Failed to parse program from input\n");
                  goto bubble_error;
               } else {
                  if (cur_input(cur_env)->cur_pos < cur_input(cur_env)->psh_str->str_len - 1) {
                     fprintf(stderr, "Didn't fully parse everything, parse error?\n");
                  }

                  struct psh_result* result = eval_program(cur_env, cur_node);
                  free(result);
               }

               cur_input(cur_env)->is_last_EOF = 0;
               cur_input(cur_env)->cur_pos = 0;

               if (cur_env->psh_inputs->ins[cur_env->psh_inputs->size - 1]->psh_str->str_len > 0) {
                  dyn_arr_psh_input_push(cur_env->psh_inputs, psh_input_create());
               }

               dyn_arr_tok_free(cur_env->tok_arr);
               memset(cur_env->tok_arr, 0, sizeof(struct dyn_arr_tok));
               dyn_arr_tok_init(cur_env->tok_arr);
            }

            PS1 = sym_get_value(cur_env->var_table, "PS1");
            if (!PS1)
               PS1 = "";
            printf("%s", PS1);

            cur_env->psh_inputs->cur_input = cur_env->psh_inputs->size - 1;
            cur_pos = 0;

            //check for finished background jobs
            dyn_arr_job_clean_done(cur_env->jobs);
         } break;

         case EOF: { // NULL terminator
            if (!cur_env->flag_ignoreeof) {
               if (cur_input(cur_env)->psh_str->str_len == 0) {
                  PSH_LOG(LOG_DEBUG, "Exiting\n");
                  exit(0);
               } else {
                  c = ungetc(c, stdin);
                  clearerr(stdin);
               }
            }      
         } break;

         default: {
            putc(c, stdout);
            if (cur_pos == cur_input(cur_env)->psh_str->str_len) {
               pstr_append(cur_input(cur_env)->psh_str, c);
            } else {
               //TODO: improve multiline-handling
               psh_string_insert_char(cur_input(cur_env)->psh_str, c, cur_pos);
               printf("%s", cur_input(cur_env)->psh_str->str + cur_pos + 1);

               for (int i = cur_input(cur_env)->psh_str->str_len - 1; i > (int)cur_pos; i--) {
                  printf("\b");
               }
            }

            DEBUG_ASSERT(cur_pos <= cur_input(cur_env)->psh_str->str_len);

            cur_pos++;
         } break;
      }
   }

   return 0;
}

enum psh_error
psh_secondary_prompt(struct shell_env* cur_env) {
   struct psh_input* psh_in = psh_calloc(sizeof(struct psh_input));
   psh_in->psh_str = psh_string_create(NULL);
   struct psh_string* psh_str = psh_in->psh_str;
   //read all input to the psh_input char by char
   struct psh_node* cur_node = NULL;

   const char *PS2 = sym_get_value(cur_env->var_table, "PS2");
   if (!PS2)
      PS2 = "";
   printf("%s", PS2);
   fflush(stdout);

   uint64_t cur_pos = 0;

   int c;
   while (1) {
      c = psh_fgetc(cur_env);

      switch (c) {
         case ESC_C: {
            c = psh_fgetc(cur_env);
            if (c == EOF)
               assert(0);
            else if (c == '[') {
               while ((c = psh_fgetc(cur_env)) == EOF)
                  ;
               if (c == 'C') {
                  if (cur_pos < psh_in->psh_str->str_len) {
                     printf(CURSOR_RIGHT);
                     cur_pos++;
                  }
               }
               else if (c == 'D') {
                  if (cur_pos > 0) {
                     printf(CURSOR_LEFT);
                     cur_pos--;
                  }
               }
               else {
                  //ignore for now
               }
            }
         } break;

         case 127: { /*delete*/
            //only works at the end of the input for now
            if (cur_pos > 0 && cur_pos == psh_in->psh_str->str_len) {
               psh_string_delete_char(psh_in->psh_str, cur_pos - 1);
               printf("\b " CURSOR_LEFT);
               cur_pos--;
            }
         } break;

         case 10: { // line feed
            putc('\n', stdout);
            fflush(stdout);

            //append to
            pstr_append(cur_input(cur_env)->psh_str, psh_in->psh_str->str);
            if (psh_in->psh_str->str_len > 0) {
               cur_env->tok_arr->nb_EOF_toks = 0;
               cur_input(cur_env)->is_last_EOF = 0;
            }
            //TODO needs to remove all of the cached tokens or not? (I don't think so)

            /* printf("cur_env psh_input: %s\n", cur_env->psh_input->psh_str->str); */
            /* printf("psh_in: %s\n", psh_in->psh_str->str); */
            psh_string_free(psh_in->psh_str);
            free(psh_in);

         } return 0;

         case EOF: { // NULL terminator
            //Always ignore unless zero length?
         } break;

         default: {
            putc(c, stdout);
            if (cur_pos > 0 && cur_pos != psh_in->psh_str->str_len) {
               psh_string_delete_char(psh_in->psh_str, cur_pos);
            }
            DEBUG_ASSERT(cur_pos <= psh_in->psh_str->str_len);
            psh_string_insert_char(psh_in->psh_str, (uint8_t) c, cur_pos);
            cur_pos++;
         } break;
      }
   }

   return 0;
}



enum psh_error
sym_init_undefined_variables(struct sym_table* _Nonnull sym_table) {
   const char* cur_str = NULL;
   if (!(cur_str = sym_get_value(sym_table, "PS1"))) (void)sym_var_insert(sym_table, "PS1", "$ ", false);
   if (!(cur_str = sym_get_value(sym_table, "PS2"))) (void)sym_var_insert(sym_table, "PS2", "> ", false);
   if (!(cur_str = sym_get_value(sym_table, "PS4"))) (void)sym_var_insert(sym_table, "PS4", "+ ", false);
   if (!(cur_str = sym_get_value(sym_table, "IFS"))) (void)sym_var_insert(sym_table, "IFS", " \n\t", false);
   if (!(cur_str = sym_get_value(sym_table, "HISTFILE"))) {
      const char* home = sym_get_value(sym_table, "HOME");
      if (!home) { assert(0 && "TODO: implement"); }
      struct psh_string* pstr_histfile = psh_string_create(home);
      pstr_append(pstr_histfile, "/.psh_history");
      (void)sym_var_insert(sym_table, "HISTFILE", pstr_histfile->str, false);
      psh_string_free(pstr_histfile);
   }
   if (!(cur_str = sym_get_value(sym_table, "HISTSIZE"))) (void)sym_var_insert(sym_table, "HISTSIZE", "1024", false);

   //TODO reset PWD and PPID?


   return PSH_SUCCESS;
}


enum gl_ret
psh_getline(FILE* _Nonnull file, struct psh_string* _Nonnull out) {
   int prev_c = EOF, prev_prev_c = EOF;
   int cur_c = EOF;
   //TODO: Actually needs to do better quoting/parsing (I think)
   while ( (cur_c = fgetc(file)) != EOF && (cur_c != '\n' || (prev_c == '\\' && prev_c == prev_prev_c)) ) {
      pstr_append(out, cur_c);
      prev_c = cur_c;
      prev_prev_c = prev_c;
   }
   if (cur_c == EOF) {
      if (ferror(file)) {
         return PSH_GL_ERROR;
      }
      if (out->str_len == 0) {
         return PSH_GL_EOF;
      } else {
         return PSH_GL_SUCCESS;
      }
   }

   return PSH_GL_SUCCESS;
}

//TODO: read histfile with multiline inputs
void
read_histfile(struct shell_env* _Nonnull cur_env) {
   //Should always be defined at this point
   const char* histfile = sym_get_value(cur_env->var_table, "HISTFILE");

   FILE* hist_fp = fopen(histfile, "r");
   if (!hist_fp) {
      //TODO: create file here?
      return;
   }

   struct psh_input* last = dyn_arr_psh_input_pop(cur_env->psh_inputs);

   struct psh_string* cur_line = psh_string_create(NULL);
   enum gl_ret cur_ret;
   while ( (cur_ret = psh_getline(hist_fp, cur_line)) == PSH_GL_SUCCESS) {
      dyn_arr_psh_input_push(cur_env->psh_inputs, psh_input_create_from(psh_string_clone(cur_line)));
      psh_string_reset(cur_line);
   }
   if (cur_ret == PSH_GL_ERROR) {
      perror("psh: failed to read history file");
   }
   psh_string_free(cur_line);

   dyn_arr_psh_input_push(cur_env->psh_inputs, last);
}

void
sleep_ms(const uint64_t ms) {
   struct timespec ts;
   ts.tv_sec = ms / 1000;
   ts.tv_nsec = (ms % 1000) * 1000000;
   nanosleep(&ts, NULL);
}

struct psh_input* _Nonnull
cur_input(struct shell_env* cur_env) {
   return cur_env->psh_inputs->ins[cur_env->psh_inputs->cur_input];
}
