#include "sym_table.h"

static
inline
uint64_t
hash_func(const char* str, const uint64_t arr_size) {
   uint64_t sum = 7879;

   if (!str)
      return sum % arr_size;

   while (*str) {
      sum *= ((*str * 1337) + 5531) * 2333;
      str++;
   }

   return sum % arr_size;
}

struct sym_table*
sym_table_create(size_t size) {
   struct sym_table* ret_table = psh_calloc(sizeof(struct sym_table));

   ret_table->sym_node_arr = psh_calloc(size * sizeof(struct sym_node*));
   ret_table->nb_sym_nodes = size;

   return ret_table;
}

//TODO rename to sym_var_insert
///inserts a var
enum psh_error
sym_var_insert(struct sym_table* in_table, const char* name, const char* value, bool is_env) {
   // check if it's already there
   struct sym_var_entry* prev_entry = sym_get_var_entry(in_table, name);
   if (prev_entry != NULL) {
      free(prev_entry->value);
      if (value)
         prev_entry->value = psh_strdup(value);
      else
         prev_entry->value = NULL;
      prev_entry->is_env = is_env;
      return PSH_SUCCESS;
   }

   uint64_t hash_index = hash_func(name, in_table->nb_sym_nodes);
   DEBUG_ASSERT(hash_index < in_table->nb_sym_nodes);


   struct sym_node* new_node = psh_calloc(sizeof(struct sym_node));
   new_node->type = SYM_NODE_VAR;

   struct sym_var_entry* new_entry = psh_calloc(sizeof(struct sym_var_entry));
   new_node->var_entry = new_entry;

   if (value == NULL)
      new_entry->value = NULL;
   else
      new_entry->value = psh_strdup(value);

   new_entry->name = psh_strdup(name);
   new_entry->is_env = is_env;

   struct sym_node* cur_node = in_table->sym_node_arr[hash_index];
   if (cur_node == NULL) {
      in_table->sym_node_arr[hash_index] = new_node;
   }
   else {
      while (cur_node->next_sym_node)
         cur_node = cur_node->next_sym_node;

      cur_node->next_sym_node = new_node;
   }

   in_table->nb_sym_entries++;

   return PSH_SUCCESS;
}

struct sym_var_entry*
sym_get_var_entry(const struct sym_table* in_table, const char* in_name) {
   uint64_t hash_index = hash_func(in_name, in_table->nb_sym_nodes);

   struct sym_node* cur_node = in_table->sym_node_arr[hash_index];
   while (cur_node != NULL) {
      struct sym_var_entry* cur_entry = cur_node->var_entry;
      if (!strcmp(in_name, cur_entry->name))
         return cur_entry;

      cur_node = cur_node->next_sym_node;
   }

   return NULL;
}


struct sym_func_entry*
sym_get_func_entry(const struct sym_table* in_table, const char* in_name) {
   uint64_t hash_index = hash_func(in_name, in_table->nb_sym_nodes);

   struct sym_node* cur_node = in_table->sym_node_arr[hash_index];
   while (cur_node != NULL) {
      struct sym_func_entry* cur_entry = cur_node->func_entry;
      if (!strcmp(in_name, cur_entry->name))
         return cur_entry;

      cur_node = cur_node->next_sym_node;
   }

   return NULL;
}

struct sym_alias_entry*
sym_get_alias_entry(const struct sym_table* in_table, const char* in_name) {
   uint64_t hash_index = hash_func(in_name, in_table->nb_sym_nodes);

   struct sym_node* cur_node = in_table->sym_node_arr[hash_index];
   while (cur_node != NULL) {
      struct sym_alias_entry* cur_entry = cur_node->alias_entry;
      if (!strcmp(in_name, cur_entry->name))
         return cur_entry;

      cur_node = cur_node->next_sym_node;
   }

   return NULL;
}


//TODO rename to sym_get_var_value
//TODO propagate the addition of UNSET to the codebase
const char*
sym_get_value(const struct sym_table* in_table, const char* in_name) {
   struct sym_var_entry* entry = sym_get_var_entry(in_table, in_name);
   if (entry) {
      if (entry->value)
         return entry->value;
      else
         return UNSET;
   }
   else
      return NULL;
}

char*
sym_get_alias_value(const struct sym_table* in_table, const char* in_name) {
   struct sym_alias_entry* entry = sym_get_alias_entry(in_table, in_name);
   if (entry) {
      if (entry->cmd)
         return entry->cmd;
      else
         return UNSET;
   }
   else
      return NULL;
}

enum psh_error
sym_var_delete(struct sym_table* in_table, const char* in_name) {
   uint64_t hash_index = hash_func(in_name, in_table->nb_sym_nodes);
   DEBUG_ASSERT(hash_index < in_table->nb_sym_nodes);

   struct sym_node* prev_node = NULL;
   struct sym_node* cur_node = in_table->sym_node_arr[hash_index];

   while (cur_node) {
      if (!strcmp(cur_node->var_entry->name, in_name)) {
         free(cur_node->var_entry->name);
         free(cur_node->var_entry->value);
         
         if (prev_node)
            prev_node->next_sym_node = cur_node->next_sym_node;
         else
            in_table->sym_node_arr[hash_index] = cur_node->next_sym_node;

         free(cur_node->var_entry);
         free(cur_node);
         in_table->nb_sym_entries--;
         break;
      }

      prev_node = cur_node;
      cur_node = cur_node->next_sym_node;
   }

   return PSH_SUCCESS;
}

struct dyn_arr_str*
sym_export_all_variables(struct sym_table* in_table) {
   struct dyn_arr_str* ret = dyn_arr_str_create();

   for (size_t i = 0; i < in_table->nb_sym_nodes; i++) {
      struct sym_node* cur_node = in_table->sym_node_arr[i];

      while (cur_node) {
         struct sym_var_entry* cur_entry = cur_node->var_entry;

         size_t cur_name_len = strlen(cur_entry->name);
         size_t cur_value_len = (cur_entry == NULL) ? 0 : strlen(cur_entry->value);
         char* env_entry = psh_calloc(cur_name_len + cur_value_len + 2);
         strcpy(env_entry, cur_entry->name);
         strcpy(env_entry + cur_name_len, "=");
         if (cur_value_len == 0)
            *(env_entry + cur_name_len + cur_name_len + 1) = '\0';
         else
            strcpy(env_entry + cur_name_len + 1, cur_entry->value);

         dyn_arr_str_push(ret, env_entry, false);

         cur_node = cur_node->next_sym_node;
      }
   }

   return ret;
}

struct dyn_arr_str*
sym_export_env(struct sym_table* in_table) {
   struct dyn_arr_str* ret = dyn_arr_str_create();

   for (size_t i = 0; i < in_table->nb_sym_nodes; i++) {
      struct sym_node* cur_node = in_table->sym_node_arr[i];

      while (cur_node) {
         struct sym_var_entry* cur_entry = cur_node->var_entry;

         if (cur_entry->is_env) {
            size_t cur_name_len = strlen(cur_entry->name);
            size_t cur_value_len = (cur_entry == NULL) ? 0 : strlen(cur_entry->value);
            char* env_entry = psh_calloc(cur_name_len + cur_value_len + 2);
            strcpy(env_entry, cur_entry->name);
            strcpy(env_entry + cur_name_len, "=");
            if (cur_value_len == 0)
               *(env_entry + cur_name_len + cur_name_len + 1) = '\0';
            else
               strcpy(env_entry + cur_name_len + 1, cur_entry->value);

            dyn_arr_str_push(ret, env_entry, false);
         }

         cur_node = cur_node->next_sym_node;
      }
   }

   return ret;
}

struct dyn_arr_str*
sym_export_aliases(struct sym_table* in_table) {
   struct dyn_arr_str* ret = dyn_arr_str_create();

   for (size_t i = 0; i < in_table->nb_sym_nodes; i++) {
      struct sym_node* cur_node = in_table->sym_node_arr[i];

      while (cur_node) {
         struct sym_alias_entry* cur_entry = cur_node->alias_entry;

         size_t cur_name_len = strlen(cur_entry->name);
         size_t cur_cmd_len = (cur_entry == NULL) ? 0 : strlen(cur_entry->cmd);
         char* env_entry = psh_calloc(cur_name_len + cur_cmd_len + 4);
         strcpy(env_entry, cur_entry->name);
         strcpy(env_entry + cur_name_len, "=");
         if (cur_cmd_len == 0)
            strcpy(env_entry + cur_name_len, "=''");
         else
            strcpy(env_entry + cur_name_len + 1, cur_entry->cmd);

         dyn_arr_str_push(ret, env_entry, false);

         cur_node = cur_node->next_sym_node;
      }
   }

   return ret;
}

enum psh_error
sym_func_insert(struct sym_table* in_table, const char* name, enum func_type func_type, ...) {
   // check if it's already there
   struct sym_func_entry* prev_entry = sym_get_func_entry(in_table, name);
   if (prev_entry != NULL) {
      if (func_type == FUNC_SPECIAL_BUILTIN || func_type == FUNC_BUILTIN) {
         assert(0);
      } else {
         if (prev_entry->func_type == FUNC_SPECIAL_BUILTIN) {
            assert(0);
         } else {
            //TODO if builtin, hide the definition but don't remove it
            //TODO if function, replace the previous entry
            assert(0);
         }
      }
   }

   uint64_t hash_index = hash_func(name, in_table->nb_sym_nodes);
   DEBUG_ASSERT(hash_index < in_table->nb_sym_nodes);


   struct sym_node* new_node = psh_calloc(sizeof(struct sym_node));
   new_node->type = SYM_NODE_FUNC;

   struct sym_func_entry* new_entry = psh_calloc(sizeof(struct sym_func_entry));
   new_node->func_entry = new_entry;

   char* entry_name = psh_strdup(name);

   new_entry->name = entry_name;
   new_entry->func_type = func_type;

   va_list args;
   va_start(args, func_type);
   if (func_type == FUNC_BUILTIN || func_type == FUNC_SPECIAL_BUILTIN) {
      new_entry->func = va_arg(args, int(*)(struct shell_env*, struct dyn_arr_str*, ...));
   } else {
      new_entry->node_func = va_arg(args, struct psh_node*);
   }

   if (in_table->sym_node_arr[hash_index] == NULL) {
      in_table->sym_node_arr[hash_index] = new_node;
   } else {
      struct sym_node* cur_node = in_table->sym_node_arr[hash_index];
      while (cur_node->next_sym_node)
         cur_node = cur_node->next_sym_node;

      cur_node->next_sym_node = new_node;
   }

   in_table->nb_sym_entries++;

   va_end(args);

   return PSH_SUCCESS;
}

enum psh_error
sym_alias_insert(struct sym_table* in_table, const char* name, const char* cmd) {
   // check if it's already there
   struct sym_alias_entry* prev_entry = sym_get_alias_entry(in_table, name);
   if (prev_entry != NULL) {
      //TODO can delete the entry right away
      sym_alias_delete(in_table, name);
   }

   uint64_t hash_index = hash_func(name, in_table->nb_sym_nodes);
   DEBUG_ASSERT(hash_index < in_table->nb_sym_nodes);

   struct sym_node* new_node = psh_calloc(sizeof(struct sym_node));
   new_node->type = SYM_NODE_ALIAS;

   struct sym_alias_entry* new_entry = psh_calloc(sizeof(struct sym_alias_entry));
   new_node->alias_entry = new_entry;

   new_entry->name = psh_strdup(name);
   new_entry->cmd = psh_strdup(cmd);

   if (in_table->sym_node_arr[hash_index] == NULL) {
      in_table->sym_node_arr[hash_index] = new_node;
   } else {
      struct sym_node* cur_node = in_table->sym_node_arr[hash_index];
      while (cur_node->next_sym_node)
         cur_node = cur_node->next_sym_node;

      cur_node->next_sym_node = new_node;
   }

   in_table->nb_sym_entries++;

   return PSH_SUCCESS;
}

void
sym_alias_delete(struct sym_table* in_table, const char* in_name) {
   uint64_t hash_index = hash_func(in_name, in_table->nb_sym_nodes);
   DEBUG_ASSERT(hash_index < in_table->nb_sym_nodes);

   struct sym_node* prev_node = NULL;
   struct sym_node* cur_node = in_table->sym_node_arr[hash_index];

   while (cur_node) {
      if (!strcmp(cur_node->alias_entry->name, in_name)) {
         free(cur_node->alias_entry->name);
         free(cur_node->alias_entry->cmd);

         if (prev_node)
            prev_node->next_sym_node = cur_node->next_sym_node;
         else
            in_table->sym_node_arr[hash_index] = cur_node->next_sym_node;

         free(cur_node->alias_entry);
         free(cur_node);
         in_table->nb_sym_entries--;
         break;
      }

      prev_node = cur_node;
      cur_node = cur_node->next_sym_node;
   }
}


void
sym_func_delete(struct sym_table* in_table, const char* in_name) {
   uint64_t hash_index = hash_func(in_name, in_table->nb_sym_nodes);
   DEBUG_ASSERT(hash_index < in_table->nb_sym_nodes);

   struct sym_node* prev_node = NULL;
   struct sym_node* cur_node = in_table->sym_node_arr[hash_index];

   while (cur_node) {
      if (!strcmp(cur_node->func_entry->name, in_name)) {
         //TODO if original is a builtin with a function on top, deal with it specifically
         if (cur_node->func_entry->func_type != FUNC_NORMAL)
            break; //Can't remove a builtin or a special builtin

         psh_node_free(cur_node->func_entry->node_func, true);
         free(cur_node->func_entry->name);

         if (prev_node)
            prev_node->next_sym_node = cur_node->next_sym_node;
         else
            in_table->sym_node_arr[hash_index] = cur_node->next_sym_node;

         free(cur_node->func_entry);
         free(cur_node);
         in_table->nb_sym_entries--;
         break;
      }

      prev_node = cur_node;
      cur_node = cur_node->next_sym_node;
   }
}


///frees everything in the sym_node, including the entry and itself
void
sym_node_free(struct sym_node* in_node) {
   if (!in_node)
      return;

   if (in_node->type == SYM_NODE_VAR) {
      struct sym_var_entry* entry = in_node->var_entry;
      if (entry) {
         free(entry->name);
         free(entry->value);
      }
      free(entry);
   } else if (in_node->type == SYM_NODE_FUNC) {
      struct sym_func_entry* entry = in_node->func_entry;
      if (entry) {
         free(entry->name);
         if (entry->func_type == FUNC_BUILTIN || entry->func_type == FUNC_SPECIAL_BUILTIN) {
            //don't free the function no?
         } else if (entry->func_type == FUNC_NORMAL) {
            psh_node_free(entry->node_func, true);
         } else {
            DEBUG_ASSERT(0);
         }
      }
      free(entry);
   } else if (in_node->type == SYM_NODE_ALIAS) {
      struct sym_alias_entry* entry = in_node->alias_entry;
      if (entry) {
         free(entry->name);
         free(entry->cmd);
      }
      free(entry);
   } else {
      DEBUG_ASSERT(0);
   }
   free(in_node);
}

void
sym_table_empty(struct sym_table* in_table) {
   if (!in_table)
      return;

   for (size_t i = 0; i < in_table->nb_sym_nodes; i++) {
      struct sym_node* cur_node = in_table->sym_node_arr[i];
      struct sym_node* next_node = NULL;
      
      in_table->sym_node_arr[i] = 0;

      while (cur_node) {
         next_node = cur_node->next_sym_node;
         sym_node_free(cur_node);
         cur_node = next_node;
      }
   }

   in_table->nb_sym_entries = 0;
}

void
sym_table_free(struct sym_table* in_table) {
   if (!in_table)
      return;

   for (size_t i = 0; i < in_table->nb_sym_nodes; i++) {
      struct sym_node* cur_node = in_table->sym_node_arr[i];
      struct sym_node* next_node = NULL;
      
      while (cur_node) {
         next_node = cur_node->next_sym_node;
         sym_node_free(cur_node);
         cur_node = next_node;
      }
   }
   free(in_table->sym_node_arr);
   free(in_table);
}
