#include "job_ctl.h"

void dyn_arr_job_push(struct dyn_arr_job *arr, struct job* in);
void dyn_arr_job_delete(struct dyn_arr_job *arr, struct job* job, bool free);

///Note: the dyn_arr_job owns the memory, don't free the job directly
struct job*
job_create_fg(struct dyn_arr_job* arr, const char* cmd, pid_t pid) {
   size_t highest_job_nb = 0;
   for (size_t i = 0; i < arr->size; i++) {
      size_t cur_nb = arr->jobs[i]->job_nb;
      if (cur_nb > highest_job_nb)
         highest_job_nb = cur_nb;
   }

   struct job* ret = psh_malloc(sizeof(*ret));
   *ret = (struct job) {
      .in_command = psh_strdup(cmd),
      .job_nb = highest_job_nb+1,
      .pid = pid,
      .gpid = -1, //TODO ?
      .state = JOB_STATE_RUNNING, //TODO or is it?
      .extra_state = 0,
      .current = JOB_CUR_DEFAULT,
      .is_foreground = true,
      .finished_wait = false,
   };

   dyn_arr_job_push(arr, ret);
   
   return ret;
}

struct job* job_create_bg(struct dyn_arr_job* arr, const char* cmd, pid_t pid) {
   size_t highest_job_nb = 0;
   for (size_t i = 0; i < arr->size; i++) {
      size_t cur_nb = arr->jobs[i]->job_nb;
      if (cur_nb > highest_job_nb)
         highest_job_nb = cur_nb;
   }

   struct job* ret = psh_malloc(sizeof(*ret));
   *ret = (struct job) {
      .in_command = psh_strdup(cmd),
      .job_nb = highest_job_nb+1,
      .pid = pid,
      .gpid = -1, //TODO ?
      .state = JOB_STATE_RUNNING, //TODO or is it?
      .extra_state = 0,
      .current = JOB_CUR_REST,
      .is_foreground = false,
      .finished_wait = false,
   };

   dyn_arr_job_push(arr, ret);

   fprintf(stderr, "[%lu] %d\n", ret->job_nb, ret->pid);
   
   return ret;
}

void
job_delete(struct dyn_arr_job* arr, struct job* job) {
   dyn_arr_job_delete(arr, job, true);
}

//this function needs to wait for a job, and if I receive a SIGSTP (ctrl+z), I need to somehow manage that
int //maybe return an enum with the possible return values?
job_wait_fg(struct shell_env* cur_env, struct job* job) {
   int status;
   int ret;
   while ((ret = waitpid(job->pid, &status, WUNTRACED)) == -1) {
      if (errno == EINTR) {
         PSH_LOG(LOG_INFO, "waitpid was interrupted\n");
         check_and_exec_traps(cur_env);
      }
   }

   if (WIFSTOPPED(status) && !job->is_foreground) {
      //received a SIGTSTP, so the foreground process has been stopped
      job->extra_state = WSTOPSIG(status);
      return 0;
   } else { //finished normally
      PSH_LOG(LOG_INFO, "finished waiting for job at pid '%d'\n", job->pid);
      job->finished_wait = true;
      return status;
   }
}

void
job_free(struct job* job) {
   if (job) {
      free(job->in_command);
   }
   free(job);
}

void
psh_block_signals(struct shell_env* cur_env) {
   sigprocmask(SIG_BLOCK, &(cur_env->sa->sa_mask), NULL);
   cur_env->_blocking_signals = true;
}

void
psh_unblock_signals(struct shell_env* cur_env) {
   cur_env->_blocking_signals = false;
   sigprocmask(SIG_UNBLOCK, &(cur_env->sa->sa_mask), NULL);
}

void
check_and_exec_traps(struct shell_env* cur_env) {
   psh_block_signals(cur_env);

   if (cur_env->nb_awaiting_traps != 0) {
      for (size_t i = 0; i < cur_env->nb_awaiting_traps; i++) {
         size_t trap_ind = cur_env->awaiting_traps[i];
         const char* trap_str = cur_env->traps[trap_ind];
         
         //TODO evaluate trap
         if (trap_str != NULL) {
            PSH_LOG(LOG_INFO, "Eval '%s' (TODO: actually evaluate it) \n", trap_str);
         }
         
      }
      memset(cur_env->awaiting_traps, 0, sizeof(*(cur_env->awaiting_traps)) * cur_env->nb_awaiting_traps);
      cur_env->nb_awaiting_traps = 0;
   }
   

   psh_unblock_signals(cur_env);
}
