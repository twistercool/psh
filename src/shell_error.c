#include "shell_error.h"

//NOTE: In an interactive session, you should only call this if the error is not recoverable
void
shell_language_syntax_error(struct shell_env* cur_env) {
   //TODO work out how to get the line number
   size_t line_no = 1000;

   enum token_type psh_get_token(struct shell_env*);
   struct token* dyn_arr_tok_point_last(struct dyn_arr_tok*);
   //Shell language syntax error: shall exit with diagnostic
   enum token_type tok_type = psh_get_token(cur_env);
   if (tok_type == TOKTYPE_EOF) {
      fprintf(stderr, "psh: line '%lu': syntax error: unexpected end of file\n", line_no);
   } else {
      struct token* cur_tok = dyn_arr_tok_point_last(cur_env->tok_arr);
      fprintf(stderr, "psh: line '%lu': syntax error near unexpected token '%s'\n", line_no, cur_tok->psh_str->str);
   }

   if (cur_env->is_interactive) {
      /* longjmp(cur_env->error_jmp, JMP_ERR_SYNTAX); */
   } else {
      exit(2);
   }
}

void special_builtin_error();
void special_builtin_redirection_error();

void normal_builtin_error();
void normal_builtin_redirection_error();

void compound_command_redirection_error();

void function_redirection_error();

void variable_assignment_error();




void
expansion_error(struct shell_env* cur_env, size_t line_no, const char* const msg) {
   fprintf(stderr, "psh: %s\n", msg);
   if (cur_env->is_interactive) {
      /* fprintf(stderr, "psh: %s\n", msg); */
      /* longjmp(cur_env->error_jmp, JMP_ERR_EXPANSION); */
   } else {
      /* fprintf(stderr, "psh: line '%lu': %s\n", line_no, msg); */
      exit(1);
   }
}

//NOTE: You need to set the last_pipeline_status before
void
command_not_found_error(struct shell_env* cur_env, size_t line_no, const char* const cmd_name) {
   if (cur_env->is_interactive) {
      fprintf(stderr, "psh: '%s' command not found\n", cmd_name);
   } else {
      fprintf(stderr, "psh: line '%lu': '%s' command not found\n", line_no, cmd_name);
   }
   //Do nothing else
}
