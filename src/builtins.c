#include "builtins.h"

/// Implements "break"
static int
special_builtin_break(struct shell_env* cur_env, const struct dyn_arr_str* argv, ...) {
   uint64_t special_val = 1;

   va_list vargs;
   va_start(vargs, argv);
   struct psh_result* ret_result = va_arg(vargs, struct psh_result*);
   va_end(vargs);

   if (argv->size <= 2) {
      special_val = 0;
   } else {
      errno = 0;
      char* endptr;
      special_val = strtoll(argv->strs[1], &endptr, 10);
      if (errno || endptr == argv->strs[1]) {
         fprintf(stderr, "psh: break: %s: numeric argument required\n", argv->strs[1]);
         special_val = -1;
      }
   }

   if (special_val <= 0) {
      return 1;
   } else {
      ret_result->special_code = CODE_BREAK;
      ret_result->special_val = special_val;
      return 0;
   }
}

/// Implements "continue"
static int
special_builtin_continue(struct shell_env* cur_env, const struct dyn_arr_str* argv, ...) {
   int64_t special_val = 1;

   va_list vargs;
   va_start(vargs, argv);
   struct psh_result* ret_result = va_arg(vargs, struct psh_result*);
   va_end(vargs);

   if (argv->size <= 2) {
      special_val = 0;
   } else {
      errno = 0;
      char* endptr;
      special_val = strtoll(argv->strs[1], &endptr, 10);
      if (errno || endptr == argv->strs[1]) {
         fprintf(stderr, "psh: continue: %s: numeric argument required\n", argv->strs[1]);
         special_val = -1;
      }
   }

   if (special_val <= 0) {
      return 1;
   } else {
      ret_result->special_code = CODE_CONTINUE;
      ret_result->special_val = special_val;
      return 0;
   }
}

/// Implements "return"
static int
special_builtin_return(struct shell_env* cur_env, const struct dyn_arr_str* argv, ...) {
   int64_t special_val = 1;

   va_list vargs;
   va_start(vargs, argv);
   struct psh_result* ret_result = va_arg(vargs, struct psh_result*);
   va_end(vargs);

   if (argv->size <= 2) {
      special_val = 0;
   } else {
      errno = 0;
      char* endptr;
      special_val = strtoll(argv->strs[1], &endptr, 10);
      if (errno || endptr == argv->strs[1]) {
         fprintf(stderr, "psh: return: %s: numeric argument required\n", argv->strs[1]);
         special_val = -1;
      }
   }

   if (special_val <= 0) {
      return 1;
   } else {
      ret_result->special_code = CODE_RETURN;
      ret_result->special_val = special_val;
      return 0;
   }
}

/// Implements ":"
static int
special_builtin_colon(struct shell_env* cur_env, struct dyn_arr_str* argv) {
   return 0;
}

/// Implements "."
static int
special_builtin_dot(struct shell_env* cur_env, const struct dyn_arr_str* argv, ...) {
   //TODO parse arguments properly

   va_list vargs;
   va_start(vargs, argv);
   struct psh_result* ret_result = va_arg(vargs, struct psh_result*);
   va_end(vargs);

   fprintf(stderr, "'%s'\n", argv->strs[0]);
   
   if (argv->size <= 2) {
      fprintf(stderr, "psh: .: filename argument required\n");
      return 1;
   } else if (argv->size >= 4) {
      fprintf(stderr, "psh: .: only one argument\n");
      return 1;
   }

   //TODO search in path if doesn't have a '/'
   struct psh_string* script_path = NULL;
   if (strchr(argv->strs[1], '/') != NULL) {
      script_path = psh_string_create(argv->strs[1]);
      if (!script_path) {
         //TODO properly error handle
         assert(0);
      }
   } else {
      script_path = find_in_PATH(cur_env, argv->strs[1], NULL);
      if (!script_path) {
         //TODO properly error handle
         assert(0);
      }
   }
   //TODO check 

   struct psh_result* dot_ret_result = exec_dot_script(cur_env, script_path->str);
   if (!dot_ret_result) {
      fprintf(stderr, "Failed exec_dot_script\n");
   }

   //TODO make sure that the special_code is cleared and 
   if (dot_ret_result->special_code != CODE_NORMAL) {
      
   }

   ret_result->error = dot_ret_result->error;
   /* ret_result->special_code = dot_ret_result->special_code; */ //what to do with this?
   /* ret_result->special_val = dot_ret_result->special_val; */

   return dot_ret_result->exit_status;
}

/// Implements "eval"
static int
special_builtin_eval(struct shell_env* cur_env, struct dyn_arr_str* argv) {
   //construct a command by concatenating the arguments, separated with a space
   int retval = 0;
   struct psh_string* args = psh_string_create(NULL);

   for (size_t i = 1; i < argv->size - 1; i++) {
      psh_string_append_str(args, argv->strs[i]);
      if (i != argv->size - 2)
         psh_string_append_char(args, ' ');
   }

   /* fprintf(stderr, "The command eval would execute is: '%s'\n", args->str); */

   struct shell_env tmp_env;
   memcpy(&tmp_env, cur_env, sizeof(struct shell_env));
   tmp_env.tok_arr = dyn_arr_tok_create();
   tmp_env.psh_inputs = dyn_arr_psh_input_create_from(args);
   struct psh_node* node_simple_command = parse_simple_command(&tmp_env);

   struct psh_result* ret_result = eval_simple_command(&tmp_env, node_simple_command, STDIN_FILENO, STDOUT_FILENO);

   dyn_arr_tok_free(tmp_env.tok_arr);
   dyn_arr_psh_input_free(tmp_env.psh_inputs, true);
   free(ret_result);
   free(node_simple_command);

   return retval;
}

//TODO implement other half of exec
/// Implements "exec"
static int
special_builtin_exec(struct shell_env* cur_env, const struct dyn_arr_str* argv, ...) {

   va_list vargs;
   va_start(vargs, argv);
   struct dyn_arr_node* nodes_redirections = va_arg(vargs, struct dyn_arr_node*);
   va_end(vargs);

   //If there are no args after that, simply perform the redirections and continue
   //else, perform execve

   //No args means just perform the redirections
   if (argv->size == 2) {
      //TODO perform all redirections specified in the 
      assert(0);

   } else { //there areargs, so actually execve
      char* cmd_str = argv->strs[1];
      struct psh_string* exec_path = NULL;
      if (strchr(cmd_str, '/') != NULL) {
         exec_path = psh_string_create(cmd_str);
      } else {
         //search PATH if doesn't have a '/'
         exec_path = find_in_PATH(cur_env, cmd_str, NULL);
         if (!exec_path) {
            fprintf(stderr, "psh: command not found: '%s'\n", cmd_str);
            return 127;
         }
      }

      struct dyn_arr_str* exec_argv = dyn_arr_str_create();
      dyn_arr_str_push(exec_argv, exec_path->str, true);
      //Will push NULL as well
      for (size_t i = 2; i < argv->size; i++) {
         dyn_arr_str_push(exec_argv, argv->strs[i], true);
      }

      struct dyn_arr_str* dyn_env = sym_export_env(cur_env->var_table);
      dyn_arr_str_push(dyn_env, NULL, false);
      char** env_cstrs = dyn_arr_to_cstr_arr(dyn_env);

      errno = 0;
      int exec_ret = execve(exec_path->str, exec_argv->strs, env_cstrs);
      if (exec_ret == -1) {
         switch (errno) {
            case EACCES: fprintf(stderr, "psh: cannot access: '%s'\n", exec_path->str); break;
            case ENOENT: fprintf(stderr, "psh: no such file or directory: '%s'\n", exec_path->str); break;
            default: fprintf(stderr, "psh: exec error: '%d' for '%s'\n", errno, exec_path->str); break;
         }
      }
      return 126;
   }
   return 0;
}

/// Implements "exit"
static int
special_builtin_exit(struct shell_env* cur_env, struct dyn_arr_str* argv) {
   //TODO I think that I need to kill all the children processes manually? and maybe setting the variable assignments before matters
   int64_t exit_val;
   if (argv->size <= 2) {
      exit_val = 0;
   } else {
      errno = 0;
      exit_val = strtoll(argv->strs[1], NULL, 10);
      if (errno != 0) {
         fprintf(stderr, "psh: exit: %s: numeric argument required\n", argv->strs[1]);
      }
   }
   exit(exit_val);
}

/// Implements "export"
static int
special_builtin_export(struct shell_env* cur_env, struct dyn_arr_str* argv) {
   if (argv->size <= 2) {
      struct dyn_arr_str* env_export = sym_export_env(cur_env->var_table);
      for (size_t i = 0; i < env_export->size; i++) {
         struct psh_string* name_str = psh_string_create("");
         char* cur_str = env_export->strs[i];
         while (*cur_str && *cur_str != '=') {
            psh_string_append_char(name_str, *cur_str);
            cur_str++;
         }

         cur_str++;
         struct psh_string* value_str = psh_string_create(cur_str);

         printf("export %s=\"%s\"\n", name_str->str, value_str->str);
         psh_string_free(name_str);
         psh_string_free(value_str);
      }
      dyn_arr_str_free(env_export, true);
   }
   else {
      for (size_t i = 1; i < argv->size - 1; i++) {
         //TODO check if they're assignments, expand and set as export vars, else set to empty string
         struct psh_string* name_str = psh_string_create("");
         char* cur_str = argv->strs[i];
         while (*cur_str && *cur_str != '=') {
            psh_string_append_char(name_str, *cur_str);
            cur_str++;
         }

         struct psh_string* value_str = psh_string_create("");
         if (*cur_str == '=') {
            cur_str++;
            psh_string_append_str(value_str, cur_str);

            sym_var_insert(cur_env->var_table, name_str->str, value_str->str, true);
         } else {
            struct sym_var_entry* cur_entry = sym_get_var_entry(cur_env->var_table, name_str->str);
            if (cur_entry) {
               cur_entry->is_env = true;
            } else {
               sym_var_insert(cur_env->var_table, name_str->str, value_str->str, true);
            }
         }

         psh_string_free(name_str);
         psh_string_free(value_str);
      }
   }

   return 0;
}

/// Implements "readonly"
static int
special_builtin_readonly(struct shell_env* cur_env, const struct dyn_arr_str* argv) {
   int ret_val = 0;

   if (argv->size == 2 || !strcmp(argv->strs[1], "-p")) {
      //print all values
      for (size_t j = 0; j < cur_env->var_table->nb_sym_nodes; j++) {
         struct sym_node* cur_node = cur_env->var_table->sym_node_arr[j];
         while (cur_node) {
            if (cur_node->var_entry->is_readonly) {
               char* cur_name = cur_node->var_entry->name;
               char* cur_value = cur_node->var_entry->value;
               if (cur_value)
                  printf("readonly %s=\"%s\"\n", cur_name, cur_value);
               else
                  printf("readonly %s\n", cur_name);
            }
            cur_node = cur_node->next_sym_node;
         }
      }
      return 0;
   }

   for (size_t i = 1; i < argv->size - 1; i++) {
      char* cur_str = psh_strdup(argv->strs[i]);
      char* equal_ptr = strchr(cur_str, '=');
      if (equal_ptr) {
         //there's an assignment
         *equal_ptr = '\0';
         //perform assignment if not already in var_table
         struct psh_string* name_psh_str = psh_string_create(cur_str);

         struct psh_string* value_psh_str = psh_string_create(equal_ptr + 1);

         struct sym_var_entry* cur_entry = sym_get_var_entry(cur_env->var_table, name_psh_str->str);
         
         if (cur_entry) {
            if (cur_entry->is_readonly) {
               fprintf(stderr, "psh: '%s': readonly variable\n", name_psh_str->str);
               psh_string_free(name_psh_str);
               psh_string_free(value_psh_str);
               free(cur_str);
               return 1;
            } else {
               if (cur_entry->value) {
                  free(cur_entry->value);
               }

               cur_entry->value = psh_string_to_cstr(value_psh_str, true);
            }
         } else {
            //no entry exists
            sym_var_insert(cur_env->var_table, name_psh_str->str, value_psh_str->str, 0);
            cur_entry = sym_get_var_entry(cur_env->var_table, name_psh_str->str);
            cur_entry->is_readonly = true;
         }

         psh_string_free(name_psh_str);
         psh_string_free(value_psh_str);
         free(cur_str);
      } else {
         //no assignment
         struct sym_var_entry* cur_entry = sym_get_var_entry(cur_env->var_table, cur_str);
         if (!cur_entry) {
            //no previous entry
            sym_var_insert(cur_env->var_table, cur_str, "", 0);
            cur_entry = sym_get_var_entry(cur_env->var_table, cur_str);
         }
         cur_entry->is_readonly = true;
         free(cur_str);
      }
   }

   return ret_val;
}

/// Implements "set"
static int
special_builtin_set(struct shell_env* cur_env, const struct dyn_arr_str* argv) {
   //TODO finish this 

   if (argv->size == 2) {
      //show all variables
      //TODO show in quotes if there are invisible characters
      //TODO maybe sort them?
      struct dyn_arr_str* dyn_vars = sym_export_all_variables(cur_env->var_table);

      for (size_t i = 0; i < dyn_vars->size; i++)
         printf("%s\n", dyn_vars->strs[i]);

      dyn_arr_str_free(dyn_vars, true);

      return 0;
   }

   //TODO parse options
   if (argv->strs[1][0] == '-' && argv->strs[1][1] != '-') {
      fprintf(stderr, "No options available for set yet\n");
      return 0;
   }

   uint8_t skip_first = 0;
   if (!strcmp(argv->strs[1], "--")) {
      skip_first++;
   }
   bool unset_previous_args = false;
   for (uint64_t i = 1 + skip_first; i < argv->size - 1; i++) {
      if (!unset_previous_args) {
         for (uint64_t j = 0; j < cur_env->args->size; j++) {
            free(cur_env->args->strs[j]);
            cur_env->args->strs[j] = NULL;
            cur_env->args->size = 0;
         }
         unset_previous_args = true;
      }
      dyn_arr_str_push(cur_env->args, argv->strs[i], true);
   }

   return 0;
}

/// Implements "shift"
static int
special_builtin_shift(struct shell_env* cur_env, const struct dyn_arr_str* argv) {
   uint64_t shift_val;

   if (argv->size <= 2) {
      shift_val = 1;
   } else if (argv->size == 3) {
      errno = 0;
      shift_val = strtoull(argv->strs[1], NULL, 10);
      if (errno == ERANGE) {
         fprintf(stderr, "psh: shift: '%s': shift count out of range\n", argv->strs[1]);
         return 1;
      } else if (errno != 0) {
         fprintf(stderr, "psh: shift: '%s': invalid argument\n", argv->strs[1]);
         return 1;
      }
   } else {
      fprintf(stderr, "psh: shift: too many arguments\n");
      return 1;
   }

   struct dyn_arr_str* args = cur_env->args;

   //perform the shift
   if (shift_val > args->size) {
      fprintf(stderr, "psh: shift: '%lu': shift count out of range\n", shift_val);
      return 1;
   }

   if (shift_val == 0) {
      return 0;
   }

   PSH_LOG(LOG_INFO, "shift_val: %lu\n", shift_val);

   for (uint64_t i = 0; i < args->size; i++) {
      if (i < shift_val)
         free(args->strs[i]);

      if (i < args->size - shift_val)
         args->strs[i] = args->strs[i + shift_val];
      else
         cur_env->args->strs[i] = NULL;
   }
   cur_env->args->size -= shift_val;

   //TODO realloc the array so it can be smaller?


   return 0;
}

/// Implements "times"
static int
special_builtin_times(struct shell_env* cur_env, struct dyn_arr_str* argv) {

   const long TICKS_PER_SECOND = sysconf(_SC_CLK_TCK);
   struct tms time_ticks = {0};
   clock_t times_ret = times(&time_ticks);
   if (times_ret == 0) {
      //what error can even happen?
      perror("psh: times");
      return 1;
   }

   int shell_user_minutes = time_ticks.tms_utime / TICKS_PER_SECOND / 60;
   double shell_user_seconds = (double)time_ticks.tms_utime / TICKS_PER_SECOND;

   int shell_system_minutes = time_ticks.tms_stime / TICKS_PER_SECOND / 60;
   double shell_system_seconds = (double)time_ticks.tms_stime / TICKS_PER_SECOND;

   int children_user_minutes = time_ticks.tms_cutime / TICKS_PER_SECOND / 60;
   double children_user_seconds = (double)time_ticks.tms_cutime / TICKS_PER_SECOND;

   int children_system_minutes = time_ticks.tms_cstime / TICKS_PER_SECOND / 60;
   double children_system_seconds = (double)time_ticks.tms_cstime / TICKS_PER_SECOND;

   printf("%dm%.2lfs %dm%.2lfs\n%dm%.2lfs %dm%.2lfs\n", shell_user_minutes, shell_user_seconds, shell_system_minutes, shell_system_seconds,
                                                children_user_minutes, children_user_seconds, children_system_minutes, children_system_seconds);

   return 0;
}

bool
is_signame(char* str) {
   uint8_t sig_nb = sig_str_len;
   for (uint8_t j = 0; j < sig_str_len; j++) {
      if (uppercase_strequal(str, sig_str[j])) {
         sig_nb = j;
         break;
      }
   }
   return sig_nb != sig_str_len;
}

//returns the sig index of the sig_str array, or -1 if it's not right
static
int
sig_index(const char* condition) {
   char* endptr;
   errno = 0;
   uint64_t arg_nb = strtol(condition, &endptr, 10);
   if (errno != 0 || (errno == 0 && arg_nb >= sig_str_len)) {
      return -1;
   } else if (endptr != condition) {
      return (int) arg_nb;
   }

   for (int i = 0; i < sig_str_len; i++) {
      if (uppercase_strequal(condition, sig_str[i])) {
         return i;
      }
   }

   return -1;
}

/// Implements "trap"
static int
special_builtin_trap(struct shell_env* cur_env, const struct dyn_arr_str* argv) {
   int retval = 0;

   if (argv->size <= 2 || (argv->size == 3 && !strcmp(argv->strs[1], "--"))) {
      //print all
      for (uint8_t i = 0; i < NB_SIGNALS; i++) {
         if (cur_env->traps[i]) {
            printf("trap -- '%s' %s\n", cur_env->traps[i], sig_str[i]);
         }
      }
      return 0;
   }

   //check if first arg is "-"
   if (!strcmp(argv->strs[1], "-")) {
      //reset all 
      for (size_t i = 2; i < argv->size - 1; i++) {
         int index = sig_index(argv->strs[i]);
         if (index > -1) {
            free(cur_env->traps[index]);
            cur_env->traps[index] = NULL;
         } else {
            fprintf(stderr, "psh: trap: '%s': invalid signal specification\n", argv->strs[i]);
            retval = 1;
         }
      }
      return retval;
   }

   //check if first arg a nb
   char* endptr;
   errno = 0;
   uint64_t arg_nb = strtol(argv->strs[1], &endptr, 10);
   if (errno == 0 && arg_nb >= sig_str_len) {
      fprintf(stderr, "psh: trap: '%s': invalid signal specification\n", argv->strs[1]);
      return 1;
   }
   else if (endptr != argv->strs[1]) {
      //rest are "conditions" (signal numbers or signal handles like HUP)
      for (size_t i = 1; i < argv->size - 1; i++) {
         int index = sig_index(argv->strs[i]);
         if (index > -1) {
            free(cur_env->traps[index]);
            cur_env->traps[index] = NULL;
         } else {
            fprintf(stderr, "psh: trap: '%s': invalid signal specification\n", argv->strs[i]);
            retval = 1;
         }
      }
      return retval;
   }

   uint16_t i = 2;
   //ingore "--" if it's the first arg
   if (!strcmp(argv->strs[1], "--")) {
      i++;
   }

   //argv->strs[1] is now an "action" (the function/thing to eval), the rest are "conditions"
   for (; i < argv->size - 1; i++) {
      int index = sig_index(argv->strs[i]);
      if (index > -1) {
         free(cur_env->traps[i]);
         cur_env->traps[index] = psh_strdup(argv->strs[1]);
      } else {
         fprintf(stderr, "psh: trap: '%s': invalid signal specification\n", argv->strs[i]);
         retval = 1;
      }
   }

   return retval;
}

/// Implements "unset"
static int
special_builtin_unset(struct shell_env* cur_env, const struct dyn_arr_str* argv) {
   //can't unset read only variables
   //can parse -v for only values, or -f for only functions
   bool unset_variables = true; //else unset functions

   size_t i = 1;
   if (argv->size > 2) {
      if (!strcmp(argv->strs[i], "-f")) {
         unset_variables = false;
         i++;
      } else if (!strcmp(argv->strs[i], "-v"))
         i++;
   }

   for (; i < argv->size - 1; i++) {
      if (unset_variables) {
         struct sym_var_entry* cur_entry = sym_get_var_entry(cur_env->var_table, argv->strs[i]);
         //TODO /* if (cur_entry->is_readonly) */
            sym_var_delete(cur_env->var_table, argv->strs[i]);
      } else {
         //TODO /* if (cur_entry->is_readonly) */
            sym_var_delete(cur_env->func_table, argv->strs[i]);
      }
   }

   return 0;
}

//Regular builtins

/*
   NOTE: alias works by replacing the input cmd_name by the text if it parses it and it matches an alias, make sure that it doesn't interfere with other methods of messing with the psh_input (alias might need to be reworked in the future)
   ALSO: the actual assignments are dealt with at parse time, since it affects the parsing of 
*/
static int
builtin_alias(struct shell_env* cur_env, const struct dyn_arr_str* argv) {
   int retval = 0;
   if (argv->size == 2) {
      struct dyn_arr_str* alias_strs = sym_export_aliases(cur_env->alias_table);
      for (size_t i = 0; i < alias_strs->size; i++) {
         printf("%s='%s'\n", argv->strs[i], alias_strs->strs[i]);
      }
      dyn_arr_str_free(alias_strs, true);
   } else {
      for (size_t i = 1; i < argv->size - 1; i++) {
         //TODO not sure if quoting/other expansions have to happen or if it's already dealt with
         char* end_word = strchr(argv->strs[i], '=');
         if (end_word) {
            //assignment has already been taken care of at parse time
            continue;
         } else {
            //print alias
            char* alias_str = sym_get_alias_value(cur_env->alias_table, argv->strs[i]);
            if (alias_str == NULL) {
               fprintf(stderr, "psh: alias: '%s': not found\n", argv->strs[i]);
               retval = PSH_ERROR;
            } else if (alias_str == UNSET) {
               printf("%s=''\n", argv->strs[i]);
            } else {
               printf("%s='%s'\n", argv->strs[i], alias_str);
            }
         }
      }
   }

   return retval;
}

int
builtin_unalias(struct shell_env* cur_env, const struct dyn_arr_str* argv) {
   int retval = 0;
   /* if (argv->size == 2) { */
   /*    fprintf(stderr, "unalias: usage: unalias [-a] name [name ...]\n"); */
   /*    return PSH_ERROR; */
   /* } */

   /* if (!strcmp(argv->strs[1], "-a")) { */
   /*    sym_table_empty(cur_env->alias_table); */
   /*    return 0; */
   /* } */

   /* for (size_t i = 1; i < argv->size - 1; i++) { */
   /*    char* alias_str = sym_get_alias_value(cur_env->alias_table, argv->strs[i]); */
   /*    if (!alias_str) { */
   /*       fprintf(stderr, "psh: unalias: '%s': not found\n", argv->strs[i]); */
   /*       retval = 1; */
   /*    } else { */
   /*       sym_alias_delete(cur_env->alias_table, argv->strs[i]); */
   /*    } */
   /* } */

   return retval;
}

enum jobs_flag {
   JOBS_NO_FLAG = 0,
   JOBS_L,
   JOBS_F,
};

//Maybe print on stderr?
static
void
builtin_jobs_print_job_details(const struct job* job, const enum jobs_flag flag) {
   switch (flag) {
      case JOBS_NO_FLAG: {
         printf("[%lu]%c %s %s\n", job->job_nb,
            (job->current == JOB_CUR_DEFAULT) ? '+' : (job->current == JOB_CUR_NEXT) ? '-' : ' ',
            //TODO: add Stopped(...) and Done(...)
            (job->state == JOB_STATE_RUNNING) ? "Running"
               : (job->state == JOB_STATE_DONE) ? "Done" : "Stopped",
            job->in_command);
      } break;
      case JOBS_L: {
         printf("[%lu]%c %d %s %s\n", job->job_nb,
            (job->current == JOB_CUR_DEFAULT) ? '+' : (job->current == JOB_CUR_NEXT) ? '-' : ' ',
            job->pid,
            //TODO: add Stopped(...) and Done(...)
            (job->state == JOB_STATE_RUNNING) ? "Running"
               : (job->state == JOB_STATE_DONE) ? "Done" : "Stopped",
            job->in_command);
      } break;
      case JOBS_F: {
         printf("%d\n", job->pid);
      } break;
   }
}

static int
builtin_jobs(struct shell_env* cur_env, const struct dyn_arr_str* argv) {
   if (cur_env->_blocking_signals)
      sigprocmask(SIG_BLOCK, &(cur_env->sa->sa_mask), NULL);

   //Clean finished jobs before anything
   dyn_arr_job_clean_done(cur_env->jobs);

   int retval = 0;
   struct dyn_arr_job* jobs = cur_env->jobs;

   enum jobs_flag flag = JOBS_NO_FLAG;

   size_t i = 1;
   while (argv->strs[i] && argv->strs[i][0] == '-') {
      for (const char* c = argv->strs[i] + 1; *c; c++) {
         switch (*c) {
            case 'l': flag = JOBS_L; break;
            case 'p': flag = JOBS_F; break;
            default: {
               fprintf(stderr, "psh: jobs: '-%c': invalid option", *c);
               retval = 1;
               goto ret;
            };
         }
      }
      i++;
   }

   //if there are no args
   if (!argv->strs[i]) {
      for (size_t j = 0; j < jobs->size; j++) {
         struct job* job = jobs->jobs[j];
         builtin_jobs_print_job_details(job, flag);
      }
      goto ret;
   }

   //if there are pid/jobs args
   for (; i < argv->size - 1; i++) {
      const char* cur_arg = argv->strs[i];
      const struct job* job = dyn_arr_job_get(jobs, cur_arg);
      if (!job) {
         fprintf(stderr, "psh: jobs: '%s': no such job\n", cur_arg);
         retval = 1;
      } else {
        builtin_jobs_print_job_details(job, flag);
      }
   }

ret:
   if (cur_env->_blocking_signals)
      sigprocmask(SIG_UNBLOCK, &(cur_env->sa->sa_mask), NULL);
   return retval;
}

static int
builtin_bg(struct shell_env* cur_env, const struct dyn_arr_str* argv) {
   int retval = 0;
   struct dyn_arr_job* jobs = cur_env->jobs;

   if (cur_env->_blocking_signals)
      sigprocmask(SIG_BLOCK, &(cur_env->sa->sa_mask), NULL);

   if (argv->size == 2) {
      //bg the default job
      struct job* bg_job = dyn_arr_job_get_default(jobs);
      if (!bg_job) {
         fprintf(stderr, "psh: bg: current: no such job\n");
         retval = 1;
         goto ret;
      } else {
         PSH_LOG(LOG_INFO, "bg job nb: '%lu' and pid: '%d'\n", bg_job->job_nb, bg_job->pid);
         if (bg_job->state == JOB_STATE_DONE) {
            //not sure how to handle that
            retval = 1;
            goto ret;
         } else if (bg_job->state == JOB_STATE_STOPPED) {
            bg_job->extra_state = 0;
            kill(bg_job->pid, SIGCONT);
         }
         goto ret;
      }
   
      goto ret;
   }

   for (size_t i = 1; i < argv->size - 1; i++) {
      struct job* bg_job = dyn_arr_job_get(jobs, argv->strs[i]);

      if (!bg_job) {
         fprintf(stderr, "psh: bg: '%s': no such job\n", argv->strs[i]);
         retval = 1;
         continue;
      } else {
         PSH_LOG(LOG_INFO, "bg job nb: '%lu' and pid: '%d'\n", bg_job->job_nb, bg_job->pid);
         if (bg_job->state == JOB_STATE_DONE) {
            //not sure how to handle that
            retval = 1;
            goto ret;
         } else if (bg_job->state == JOB_STATE_STOPPED) {
            bg_job->extra_state = 0;
            kill(bg_job->pid, SIGCONT);
         }
      }
   }

ret:
   if (cur_env->_blocking_signals)
      sigprocmask(SIG_UNBLOCK, &(cur_env->sa->sa_mask), NULL);
   return retval;
}

static int
builtin_fg(struct shell_env* cur_env, const struct dyn_arr_str* argv) {
   int retval = 0;
   struct dyn_arr_job* jobs = cur_env->jobs;

   if (cur_env->_blocking_signals)
      sigprocmask(SIG_BLOCK, &(cur_env->sa->sa_mask), NULL);

   struct job* fg_job = NULL;
   if (argv->size == 2) {
      //fg the default job
      fg_job = dyn_arr_job_get_default(jobs);
      if (!fg_job) {
         fprintf(stderr, "psh: fg: current: no such job\n");
         retval = 1;
         goto ret;
      } else {
         PSH_LOG(LOG_INFO, "fg job nb: '%lu' and pid: '%d'\n", fg_job->job_nb, fg_job->pid);
         if (fg_job->state == JOB_STATE_DONE) {
            //not sure how to handle that
            retval = 1;
            goto ret;
         } else if (fg_job->state == JOB_STATE_STOPPED) {
            fg_job->extra_state = 0;
            kill(fg_job->pid, SIGCONT);
         }
         int status = job_wait_fg(cur_env, fg_job);
         goto ret;
      }
   } else { //ignore all the ones after
      fg_job = dyn_arr_job_get(jobs, argv->strs[1]);
      if (!fg_job) {
         fprintf(stderr, "psh: fg: '%s': no such job\n", argv->strs[1]);
         retval = 1;
         goto ret;
      } else {
         PSH_LOG(LOG_INFO, "fg job nb: '%lu' and pid: '%d'\n", fg_job->job_nb, fg_job->pid);
         if (fg_job->state == JOB_STATE_DONE) {
            //not sure how to handle that
            retval = 1;
            goto ret;
         } else if (fg_job->state == JOB_STATE_STOPPED) {
            fg_job->extra_state = 0;
            kill(fg_job->pid, SIGCONT);
         }
         int status = job_wait_fg(cur_env, fg_job);
         goto ret;
      }
   }

ret:
   if (cur_env->_blocking_signals)
      sigprocmask(SIG_UNBLOCK, &(cur_env->sa->sa_mask), NULL);
   return retval;
}

static int
builtin_false(struct shell_env* cur_env, const struct dyn_arr_str* argv) {
   return 1;
}

static int
builtin_true(struct shell_env* cur_env, const struct dyn_arr_str* argv) {
   return 0;
}

static int
builtin_cd(struct shell_env* cur_env, const struct dyn_arr_str* argv) {
   const char* newpwd;
   if (argv->size == 2) {
      newpwd = sym_get_value(cur_env->var_table, "HOME");
      if (!newpwd) {
         fprintf(stderr, "psh: cd: HOME not set\n");
         return 1;
      }
   } else {
      if (argv->size != 3) {
         fprintf(stderr, "psh: cd: too many arguments\n");
         return 1;
      }
      newpwd = argv->strs[1];
   }

   // Check if the arg is '-'
   char* oldpwd_backup = NULL;
   if (!strcmp(newpwd, "-")) {
      const char* old = sym_get_value(cur_env->var_table, "OLDPWD");
      if (!old) {
         fprintf(stderr, "psh: cd: OLDPWD not set\n");
         return 1;
      }
      newpwd = psh_strdup(old);
      oldpwd_backup = psh_strdup(old);
   }

   struct stat newpwd_stat;
   int stat_err = stat(newpwd, &newpwd_stat);
   if (stat_err) {
      fprintf(stderr, "psh: cd: error stating: %s\n", newpwd);
      return 1;
   }

   if ((newpwd_stat.st_mode & S_IFMT) != S_IFDIR) {
      fprintf(stderr, "psh: cd: not a directory: %s\n", newpwd);
      return 1;
   }

   char* oldpwd = getcwd(NULL, 0);
   sym_var_insert(cur_env->var_table, "OLDPWD", oldpwd, true);
   free(oldpwd);

   int cd_err = chdir(newpwd);
   if (cd_err) {
      fprintf(stderr, "psh: cd: can't cd to dir: %s\n", newpwd);
      return 1;
   }

   char* pwd = getcwd(NULL, 0);
   sym_var_insert(cur_env->var_table, "PWD", pwd, true);
   free(pwd);

   if (argv->size == 3 && !strcmp(argv->strs[1], "-")) {
      printf("%s\n", oldpwd_backup);
      free((char*)newpwd);
      free(oldpwd_backup);
   }

   return 0;
}

//Forward decl
static int print_type_of_cmd(struct shell_env* cur_env, _Nonnull const char* cmd, _Nullable const char* PATH_override, bool is_type_cmd);

static int
builtin_command(struct shell_env* cur_env, struct dyn_arr_str* argv, ...) {
   va_list vargs;
   va_start(vargs, argv);
   struct dyn_arr_node* nodes_redirections = va_arg(vargs, struct dyn_arr_node*);
   struct psh_result* ret_result = va_arg(vargs, struct psh_result*);
   int fd_in = va_arg(vargs, int), fd_out = va_arg(vargs, int);
   va_end(vargs);

   int ret_val = 0;

   if (argv->size <= 2) {
      return ret_val;
   }

   bool flag_p = false;
   bool flag_v = false;
   bool flag_V = false;

   int cur_arg = 1;

   //support -v, -V and -p
   while (argv->strs[cur_arg]) {
      if (argv->strs[cur_arg][0] != '-') {
         break;
      }
      for (uint i = 1; i < strlen(argv->strs[cur_arg]); i++) {
         switch (argv->strs[cur_arg][i]) {
            case 'V': flag_V = true; flag_v = false; break;
            case 'v': flag_v = true; flag_V = false; break;
            case 'p': flag_p = true; break;
            default: goto exit_while;
         }
      }
      cur_arg++;
   }
exit_while:;

   const char* cmd_str = argv->strs[cur_arg];
   //NOTE: array ends with NULL
   struct dyn_arr_str* new_argv = dyn_arr_str_create();
   for (uint i = cur_arg; i < argv->size; i++) {
      dyn_arr_str_push(new_argv, argv->strs[i], true);
   }

   const char* path_override = NULL;
   if (flag_p) {
      path_override = "/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl"; //TODO: not sure what the "standard PATH" is
   }

   //NOTE: Delayed PATH search for speed
   const struct psh_string* exec_path = NULL;

   if (flag_V) {
      for (uint i = cur_arg; i < argv->size - 1; i++) {
         ret_val |= print_type_of_cmd(cur_env, argv->strs[i], path_override, false);
      }
      goto free_and_ret;
   } else if (flag_v) {
      const char* alias_val;
      if (!flag_p && ((alias_val = sym_get_alias_value(cur_env->alias_table, cmd_str)) != NULL)) {
         printf("alias %s='%s'\n", cmd_str, alias_val);
      } else if (!flag_p && (sym_get_func_entry(cur_env->func_table, cmd_str) != NULL)) {
         printf("%s\n", cmd_str);
      } else if ((exec_path = find_in_PATH(cur_env, cmd_str, path_override)) != NULL) {
         printf("%s\n", exec_path->str);
      } else {
         ret_val = 1;
      }
      goto free_and_ret;
   }
   exec_path = find_in_PATH(cur_env, cmd_str, path_override);

   if (!exec_path) {
      /* fprintf(stderr, "piersh: command not found: '%s'\n", cmd_str); */
      ret_result->exit_status = 127;
      //TODO: find the line number
      command_not_found_error(cur_env, 1000, argv->strs[cur_arg]);
      goto free_and_ret;
   }

   //Ignore ret val?
   int ret = execute_command_in_context(cur_env, exec_path, new_argv, nodes_redirections, ret_result, fd_in, fd_out);

free_and_ret:
   psh_string_free((struct psh_string*)exec_path);
   dyn_arr_str_free(new_argv, true);
   return ret_val;
}

static int
builtin_fc(struct shell_env* cur_env, struct dyn_arr_str* argv) {
   TODO();
}

static int
builtin_getopts(struct shell_env* cur_env, struct dyn_arr_str* argv) {
   TODO();
}

//TODO create data

/* static void */
/* builtin_hash_init() { } */

/* static void */
/* builtin_hash_destroy() { } */

/* static void */
/* builtin_hash_reset() { } */

static int
builtin_hash(struct shell_env* cur_env, struct dyn_arr_str* argv) {
   TODO();
}

inline
bool
uppercase_strequal(const char* str0, const char* str1) {
   while (*str0 && *str1 && toupper(*str0++) == toupper(*str1++))
      ;
   return !*str0 && !*str1;
}

static int
builtin_kill(struct shell_env* cur_env, const struct dyn_arr_str* argv) {
   if (argv->size <= 2) {
      fprintf(stderr, "kill: not enough arguments\n");
      return 1;
   }

//kill -l [exit_status]
   if (!strcmp(argv->strs[1], "-l")) {
      int ret_val = 0;
      if (argv->size <= 3) {
         for (uint8_t i = 0; i < sig_str_len; i++)
            printf("%s%s", sig_str[i], (i == sig_str_len - 1) ? "\n" : " ");
      } else {
         //parse the 3rd arg and print the name of the string (and do something weird in a weird case?)
         bool printed_nb = false;
         for (size_t i = 2; i < argv->size - 1; i++) {
            if (argv->strs[i][0] == '-') {
               continue;
            }
            printed_nb = true;

            for (uint8_t j = 0; j < sig_str_len; j++) {
               if (uppercase_strequal(argv->strs[i], sig_str[j])) {
                  printf("%u\n", j);
                  goto continue_loop;
               }
            }

            errno = 0;
            uint64_t arg_nb = strtol(argv->strs[i], NULL, 10);
            if (errno != 0 || arg_nb >= sig_str_len) {
               fprintf(stderr, "psh: kill: '%s': invalid signal specification\n", argv->strs[i]);
               ret_val = 1;
            } else {
               printf("%s\n", sig_str[arg_nb]);
            }
         }
         if (!printed_nb) {
            for (uint8_t i = 0; i < sig_str_len; i++)
               printf("%s%s", sig_str[i], (i == sig_str_len - 1) ? "\n" : " ");
         }
         continue_loop:;
      }
      return ret_val;
   }

//kill -s signal_name pid...
   if (!strcmp(argv->strs[1], "-s")) {
      if (argv->size <= 3) {
         fprintf(stderr, "psh: kill: -s: option requires an argument\n");
         return 1;
      }

      uint8_t sig_nb = sig_str_len;
      for (uint8_t j = 0; j < sig_str_len; j++) {
         if (uppercase_strequal(argv->strs[2], sig_str[j])) {
            sig_nb = j;
            break;
         }
      }
      if (sig_nb == sig_str_len) {
         fprintf(stderr, "psh: kill: '%s': invalid signal specification\n", argv->strs[2]);
         return 1;
      }

      int ret_val = 0;
      for (uint64_t i = 2; i < argv->size - 1; i++) {
         //TODO implement job IDs
         if (argv->strs[i][0] == '%') {
            assert(0);
         }
      
         errno = 0;
         int64_t pid_arg = strtoll(argv->strs[i], NULL, 10);
         if (errno != 0) {
            fprintf(stderr, "psh: kill: '%s': arguments must be process or job IDs\n", argv->strs[i]);
         }
         int kill_ret = kill(pid_arg, sig_nb);
         if (kill_ret == -1) {
            if (errno == EPERM) { fprintf(stderr, "psh: kill: '%s': Operation not permitted\n", argv->strs[i]); }
            else if (errno == ESRCH) { fprintf(stderr, "psh: kill: '%s': No such process\n", argv->strs[i]); }
            else { fprintf(stderr, "psh: kill: '%s': Unknown error\n", argv->strs[i]); }
            ret_val = 1;
         }
      }

      return ret_val;
   }

/*
kill [-signal_name] pid...
kill [-signal_number] pid...
*/
   uint8_t sig_nb = 15; //SIGTERM as default
   if (argv->strs[1][0] == '-') {
      for (uint8_t j = 0; j < sig_str_len; j++) {
         if (uppercase_strequal(argv->strs[1]+1, sig_str[j])) {
            sig_nb = j;
            goto begin_for_loop;
         }
      }

      errno = 0;
      uint64_t arg_nb = strtoull(argv->strs[1]+1, NULL, 10);
      if (errno != 0 || arg_nb >= sig_str_len) {
         fprintf(stderr, "psh: kill: '%s': invalid signal specification\n", argv->strs[1]);
         return 1;
      }
      sig_nb = arg_nb;
   }
begin_for_loop:;

   int ret_val = 0;
   for (uint64_t i = 2; i < argv->size - 1; i++) {
      //TODO implement job IDs
      if (argv->strs[i][0] == '%') {
         assert(0);
      }
   
      errno = 0;
      char* end_ptr = argv->strs[i];
      int64_t pid_arg = strtoll(argv->strs[i], &end_ptr, 10);
      if (end_ptr == argv->strs[i]) {
         fprintf(stderr, "psh: kill: '%s': arguments must be process or job IDs\n", argv->strs[i]);
         continue;
      }
      int kill_ret = kill(pid_arg, sig_nb);
      if (kill_ret == -1) {
         /* if (errno == EPERM) { fprintf(stderr, "psh: kill: '%s': Operation not permitted\n", argv->strs[i]); } */
         /* else if (errno == ESRCH) { fprintf(stderr, "psh: kill: '%s': No such process\n", argv->strs[i]); } */
         /* else { fprintf(stderr, "psh: kill: '%s': Unknown error\n", argv->strs[i]); } */
         perror("psh: kill");
         ret_val = 1;
      }
   }

   return ret_val;
}

/* int */
/* builtin_newgrp(struct shell_env* cur_env, struct dyn_arr_str* argv) { */
/*    newgrp is NOT a builtin, it's implemented as a binary! */
/* } */

//TODO implement parsing of -L and -P
static int
builtin_pwd(struct shell_env* cur_env, const struct dyn_arr_str* argv) {
   char* pwd = getcwd(NULL, 0);
   assert(pwd);
   puts(pwd);
   free(pwd);

   return 0;
}

//TODO: feels buggy
static int
builtin_read(struct shell_env* cur_env, const struct dyn_arr_str* argv) {
   bool ignore_escape = false;
   if (argv->size >= 3 && !strcmp(argv->strs[1], "-r"))
      ignore_escape = true;

   struct psh_string* prev_line = psh_string_create(NULL); //for multiple lines of inputs
   struct psh_string* line = psh_string_create(NULL);

   bool is_EOF = false;

   const bool is_stdin_a_tty = isatty(STDIN_FILENO);

   int c;
   while ((c = fgetc(stdin))) {
      switch (c) {
         //TODO deal with escape codes way better (can move around in the input in a weird way, which breaks the code)
         case 127: {
            if (line->str_len == 0)
               continue;
            psh_string_delete_char(line, line->str_len-1);
            printf("\b " CURSOR_LEFT);
         } break;
         case '\\': {
               if (ignore_escape) {
                  if (is_stdin_a_tty) putc('\\', stdout);
                  psh_string_append_char(line, (uint8_t) c);
                  continue;
               } else {
                  if (is_stdin_a_tty) putc(c, stdout);
                  c = fgetc(stdin);
                  if (!c) {
                     continue;
                  } else if (c == '\n') {
                     psh_string_append_str(prev_line, line->str);
                     psh_string_free(line);
                     line = psh_string_create("");
                     if (is_stdin_a_tty) putc('\n', stdout);
                     const char* PS2 = sym_get_value(cur_env->var_table, "PS2");
                     if (PS2 == NULL || PS2 == UNSET)
                        PS2 = "";
                     continue;
                  } else {
                     if (is_stdin_a_tty) putc(c, stdout);
                     psh_string_append_char(line, (uint8_t) c);
                     continue;
                  }
               }
         } break;
         case '\n': {
            if (is_stdin_a_tty) putc('\n', stdout);
            goto exit_while;
         } break;
         case EOF: {
            is_EOF = true;
            goto exit_while;
         } break;
         default: {
            if (is_stdin_a_tty) putc(c, stdout);
            psh_string_append_char(line, (uint8_t) c);
         } break;
      }
   }
exit_while:

   psh_string_append_str(prev_line, line->str);
   psh_string_free(line);

   if (is_EOF) {
      //Discard input
      psh_string_free(prev_line);
      return 1;
   }

   const size_t nb_args = (ignore_escape) ? argv->size-3 : (argv->size - 2);
   if (nb_args == 0) {
      //Discard input
      psh_string_free(prev_line);
      return 0;
   }

   struct dyn_arr_str* fields = dyn_arr_str_create();

   const char* IFS = sym_get_value(cur_env->var_table, "IFS");
   if (IFS == NULL || IFS == UNSET)
      IFS = " \n\t";

   //go to the first word
   const char* cur_c = prev_line->str;

   //if there are more
   for (size_t i = 0; i < nb_args; i++) {
      while (*cur_c && is_in_IFS(*cur_c, IFS)) cur_c++;
      if (*cur_c == '\0') break;

      //delimit first 
      struct psh_string* cur_word = psh_string_create(NULL);
      while (*cur_c && !is_in_IFS(*cur_c, IFS)) {
         psh_string_append_char(cur_word, *cur_c);
         cur_c++;
      }

      //Last arg, if there's still inputs lefts, will have the whole rest of the input
      if (*cur_c && i == nb_args - 1) {
         while (*cur_c) {
            psh_string_append_char(cur_word, *cur_c);
            cur_c++;
         }
      }

      dyn_arr_str_push(fields, psh_string_to_cstr(cur_word, true), false);
   }

   size_t begin_index = (ignore_escape) ? 2 : 1;
   for (size_t i = 0; i < fields->size; i++) {
      sym_var_insert(cur_env->var_table, argv->strs[begin_index + i], fields->strs[i], false);
   }

   if (fields->size < nb_args) {
      //set all args after i to the empty string
      /* fprintf(stderr, "Set all remaining vars to empty string\n"); */
      for (size_t i = fields->size; i < nb_args; i++) {
         sym_var_insert(cur_env->var_table, argv->strs[begin_index + i], "", false);
      }
   }

   psh_string_free(prev_line);
   dyn_arr_str_free(fields, true);

   return 0;
}

static int
print_type_of_cmd(struct shell_env* cur_env, _Nonnull const char* cmd, _Nullable const char* PATH_override, bool is_type_cmd) {
   int ret_val = 0;

   if (strchr(cmd, '/') != NULL) {
      struct stat st;
      if (stat(cmd, &st)) {
         perror("psh:");
         ret_val = -1;
      } else {
         printf("%s is %s\n", cmd, cmd);
      }

      return ret_val;
   }

   const char* alias_val;
   if (!PATH_override && (alias_val = sym_get_alias_value(cur_env->alias_table, cmd)) != NULL) {
      printf("'%s' is an alias for '%s'\n", cmd, alias_val);
      return ret_val;
   }
   
   const struct sym_func_entry* func_entry;
   if (!PATH_override && (func_entry = sym_get_func_entry(cur_env->func_table, cmd)) != NULL) {
      if (func_entry->func_type == FUNC_BUILTIN || func_entry->func_type == FUNC_SPECIAL_BUILTIN) {
         printf("%s is a shell builtin\n", cmd);
      } else /*if (func_entry->func_type == FUNC_NORMAL)*/ {
         printf("%s is a shell function\n", cmd);
      }
   } else {
      //look through PATH
      struct psh_string* exec_path = find_in_PATH(cur_env, cmd, PATH_override);
      if (exec_path) {
         printf("%s is %s\n", cmd, exec_path->str);
      } else {
         if (is_type_cmd) {
            fprintf(stderr, "type: '%s': not found\n", cmd);
         } else {
            fprintf(stderr, "command: '%s': not found\n", cmd);
         }
         ret_val = 1;
      }
      psh_string_free(exec_path);
   }

   return ret_val;
}

static int
builtin_type(struct shell_env* cur_env, const struct dyn_arr_str* argv) {
   //checks if it's a builtin (special or normal is the same here), a shell function, an alias, and if not, looks in PATH.
   int ret_val = 0;

   for (size_t i = 1; i < argv->size - 1; i++) {
      ret_val |= print_type_of_cmd(cur_env, argv->strs[i], NULL, true);
   }

   return ret_val;
}

#define BLOCK_SIZE 512 /*idk why but that's what it is for this program*/
#define KB_SIZE 1024

static int
print_rlimit(int resource, const char *msg, bool hard_flag) {
   __rlim_t out_nb;
   struct rlimit cur_lim = {0};
   int ret = getrlimit(resource, &cur_lim);
   if (ret != 0) {
      perror("Error getting/setting limit");
      return ret;
   }

   printf("%s", msg);
   if (hard_flag) out_nb = cur_lim.rlim_max;
   else out_nb = cur_lim.rlim_cur;

   if (out_nb == RLIM_INFINITY) {
      printf("unlimited\n");
   } else {
      switch (resource) {
         case RLIMIT_FSIZE: out_nb /= BLOCK_SIZE; break;
         case RLIMIT_STACK:
         case RLIMIT_MEMLOCK: out_nb /= KB_SIZE; break;
         default: break;
      }
      printf("%lu\n", out_nb);
   }

   return 0;
}

int
builtin_ulimit(struct shell_env* cur_env, struct dyn_arr_str* argv) {

   bool hard_flag = false, soft_flag = false;
   bool all_flag = false;

   int ret;
   struct rlimit cur_lim;
   long in_nb;

   static struct {
      bool flag;
      enum __rlimit_resource rlim;
      size_t nb_scale;
      const char* const str;
   } vals[] = {
      { 0, RLIMIT_CPU,        1,          "-t: cpu time (seconds)            " }, //cputime
      { 0, RLIMIT_FSIZE,      BLOCK_SIZE, "-f: file size (blocks)            " }, //filesize
      { 0, RLIMIT_DATA,       1,          "-d: data seg size (kbytes)        " }, //datasegsize
      { 0, RLIMIT_STACK,      KB_SIZE,    "-s: stack size (kbytes)           " }, //stacksize
      { 0, RLIMIT_CORE,       1,          "-c: core file size (blocks)       " }, //coresize
      { 0, RLIMIT_RSS,        1,          "-m: resident set size (kbytes)    " }, //rss
      { 0, RLIMIT_NPROC,      1,          "-u: processes                     " }, //procnb
      { 0, RLIMIT_NOFILE,     1,          "-n: file descriptors              " }, //fdnb
      { 0, RLIMIT_MEMLOCK,    1,          "-l: locked-in-memory size (kbytes)" }, //lockedinmemsize
      { 0, RLIMIT_AS,         1,          "-v: address space (kbytes)        " }, //addrspaesize
      { 0, RLIMIT_LOCKS,      1,          "-x: file locks                    " }, //filelocknb
      { 0, RLIMIT_SIGPENDING, 1,          "-i: pending signals               " }, //pendsignb
      { 0, RLIMIT_MSGQUEUE,   1,          "-q: bytes in POSIX msg queues     " }, //msgqueuesize
      { 0, RLIMIT_NICE,       1,          "-e: max nice                      " }, //nice
      { 0, RLIMIT_RTPRIO,     1,          "-r: max rt priority               " }, //rtprio
      { 0, RLIMIT_RTTIME,     1,          "-N 15: rt cpu time (microseconds) " }, //rtcputime
   };


#define PRINT_RLIM(lim, txt) do { if ((ret = print_rlimit(lim, txt, all_flag)) != 0) { perror("psh: ulimit: print error"); return ret; } } while(0)
#define GET_RLIM(lim) do { if (getrlimit(lim, &cur_lim)) { perror("psh: ulimit: error getting limit"); return 1; } } while(0)
#define SET_RLIM(lim) do { if (setrlimit(lim, &cur_lim)) { perror("psh: ulimit: Error setting limit"); return 1; } } while(0)


   if (argv->size > 4) {
      fprintf(stderr, "Usage: ulimit -flags <nb|unlimited> || ulimit <number|unlimited>\n");
      return 1;
   }

   if (argv->size == 2) {
      GET_RLIM(RLIMIT_FSIZE);
      if (cur_lim.rlim_cur == RLIM_INFINITY) printf("unlimited\n");
      else printf("%lu\n", cur_lim.rlim_cur / BLOCK_SIZE);
      return 0;
   }

   /*no flags so sets FSIZE*/
   if (argv->strs[1][0] != '-') {
      /*checks if it's a number or unlimited*/
      if (!strcmp(argv->strs[1], "unlimited"))
         in_nb = RLIM_INFINITY;
      errno = 0;
      in_nb = strtol(argv->strs[1], NULL, 10);
      if (errno != 0) {
         fprintf(stderr, "ulimit: invalid number: %ld\n", in_nb);
         return 1;
      }

      GET_RLIM(RLIMIT_FSIZE);

      cur_lim.rlim_cur = in_nb;

      SET_RLIM(RLIMIT_FSIZE);

      return 0;
   }

   /*parses flags*/
   for (const char* c = argv->strs[1] + 1; *c; c++) {
      switch (*c) {
         case 'H': hard_flag     = true; break;
         case 'S': soft_flag     = true; break;
         case 'a': all_flag      = true; break;
         case 't': vals[0].flag  = true; break;
         case 'f': vals[1].flag  = true; break;
         case 'd': vals[2].flag  = true; break;
         case 's': vals[3].flag  = true; break;
         case 'c': vals[4].flag  = true; break;
         case 'm': vals[5].flag  = true; break;
         case 'u': vals[6].flag  = true; break;
         case 'n': vals[7].flag  = true; break;
         case 'l': vals[8].flag  = true; break;
         case 'v': vals[9].flag  = true; break;
         case 'x': vals[10].flag = true; break;
         case 'i': vals[11].flag = true; break;
         case 'q': vals[12].flag = true; break;
         case 'e': vals[13].flag = true; break;
         case 'r': vals[14].flag = true; break;
         case 'N': vals[15].flag = true; break;
         default: {
            fprintf(stderr, "ulimit: bad option: -%c\n", *c);
            return -1;
         } break;
      }
   }

   /*TODO: no limits allowed with -a*/

   /*print the options*/
   if (argv->size == 3) {
      for (uint i = 0; i < ARR_SIZE(vals); i++) {
         if (vals[i].flag || all_flag) {
            PRINT_RLIM(vals[i].rlim, vals[i].str);
         }
      }
      return 0;
   }

   /*checks if it's a number or unlimited*/
   if (!strcmp(argv->strs[1], "unlimited"))
      in_nb = RLIM_INFINITY;

   errno = 0;
   in_nb = strtol(argv->strs[2], NULL, 10);
   if (errno != 0) {
      fprintf(stderr, "psh: ulimit: invalid number: %ld\n", in_nb);
      return 1;
   }

   //make those change the value
   for (uint i = 0 ; i < ARR_SIZE(vals); i++) {
      if (vals[i].flag || all_flag) {
         GET_RLIM(vals[i].rlim);
         if (hard_flag) cur_lim.rlim_max = in_nb * vals[i].nb_scale;
         if (soft_flag || !(hard_flag || soft_flag)) cur_lim.rlim_cur = in_nb * vals[i].nb_scale;
         SET_RLIM(vals[i].rlim);
      }
   }

   return 0;
}

//TODO make the parsing
int
builtin_umask(struct shell_env* cur_env, const struct dyn_arr_str* argv) {
   if (argv->size <= 2) {
      //print the current umask
      mode_t cur_mask = umask(0);
      umask(cur_mask);

      printf("%o%o%o\n", (cur_mask & 0700) >> 6, (cur_mask & 070) >> 3, cur_mask & 07);
      return 0;
   } else if (argv->size == 3) {
      if (!strcmp(argv->strs[1], "-S")) {
         mode_t cur_mask = umask(0);
         umask(cur_mask);
         struct psh_string* output_str = psh_string_create("u=");
         assert(output_str);
         if (!(cur_mask & 0400)) psh_string_append_char(output_str, 'r');
         if (!(cur_mask & 0200)) psh_string_append_char(output_str, 'w');
         if (!(cur_mask & 0100)) psh_string_append_char(output_str, 'x');

         psh_string_append_str(output_str, ",g=");
         if (!(cur_mask & 0040)) psh_string_append_char(output_str, 'r');
         if (!(cur_mask & 0020)) psh_string_append_char(output_str, 'w');
         if (!(cur_mask & 0010)) psh_string_append_char(output_str, 'x');

         psh_string_append_str(output_str, ",o=");
         if (!(cur_mask & 0004)) psh_string_append_char(output_str, 'r');
         if (!(cur_mask & 0002)) psh_string_append_char(output_str, 'w');
         if (!(cur_mask & 0001)) psh_string_append_char(output_str, 'x');

         puts(output_str->str);
         return 0;
      } else {
         assert(0);
      }
   }

   assert(0);
}

//TODO seems unfinished
static int
builtin_wait(struct shell_env* cur_env, const struct dyn_arr_str* argv) {
   int retval = 0;
   for (uint64_t i = 2; i < argv->size - 1; i++) {
      const char* cur = argv->strs[i];
      int64_t pid_arg;
      struct job* job = dyn_arr_job_get(cur_env->jobs, cur);
      if (job) {
         pid_arg = job->pid;
      } else {
         errno = 0;
         char* end_ptr;
         pid_arg = strtoll(cur, &end_ptr, 10);
         if (end_ptr == cur) {
            fprintf(stderr, "psh: wait: '%s': not a pid or valid job spec\n", cur);
            continue;
         } else if (errno) {
            perror("wait");
            retval = 127;
            continue;
         }
      }

      int status;
      int wait_ret = waitpid(pid_arg, &status, 0);
   }
   return retval;
}


/*
   * Assumes the table is empty at the beginning
*/
void
sym_init_builtin_functions(_Nonnull struct sym_table* func_table) {
   sym_func_insert(func_table, "break", FUNC_SPECIAL_BUILTIN, &special_builtin_break);
   sym_func_insert(func_table, ":", FUNC_SPECIAL_BUILTIN, &special_builtin_colon);
   sym_func_insert(func_table, "continue", FUNC_SPECIAL_BUILTIN, &special_builtin_continue);
   sym_func_insert(func_table, ".", FUNC_SPECIAL_BUILTIN, &special_builtin_dot);
   sym_func_insert(func_table, "eval", FUNC_SPECIAL_BUILTIN, &special_builtin_eval);
   sym_func_insert(func_table, "exec", FUNC_SPECIAL_BUILTIN, &special_builtin_exec);
   sym_func_insert(func_table, "exit", FUNC_SPECIAL_BUILTIN, &special_builtin_exit);
   sym_func_insert(func_table, "export", FUNC_SPECIAL_BUILTIN, &special_builtin_export);
   sym_func_insert(func_table, "readonly", FUNC_SPECIAL_BUILTIN, &special_builtin_readonly);
   sym_func_insert(func_table, "return", FUNC_SPECIAL_BUILTIN, &special_builtin_return);
   sym_func_insert(func_table, "set", FUNC_SPECIAL_BUILTIN, &special_builtin_set);
   sym_func_insert(func_table, "shift", FUNC_SPECIAL_BUILTIN, &special_builtin_shift);
   sym_func_insert(func_table, "times", FUNC_SPECIAL_BUILTIN, &special_builtin_times);
   sym_func_insert(func_table, "trap", FUNC_SPECIAL_BUILTIN, &special_builtin_trap);
   sym_func_insert(func_table, "unset", FUNC_SPECIAL_BUILTIN, &special_builtin_unset);

   sym_func_insert(func_table, "type", FUNC_BUILTIN, &builtin_type);
   sym_func_insert(func_table, "ulimit", FUNC_BUILTIN, &builtin_ulimit);
   sym_func_insert(func_table, "alias", FUNC_BUILTIN, &builtin_alias);
   sym_func_insert(func_table, "bg", FUNC_BUILTIN, &builtin_bg);
   sym_func_insert(func_table, "cd", FUNC_BUILTIN, &builtin_cd);
   sym_func_insert(func_table, "command", FUNC_BUILTIN, &builtin_command);
   sym_func_insert(func_table, "false", FUNC_BUILTIN, &builtin_false);
   sym_func_insert(func_table, "fc", FUNC_BUILTIN, &builtin_fc);
   sym_func_insert(func_table, "fg", FUNC_BUILTIN, &builtin_fg);
   sym_func_insert(func_table, "getopts", FUNC_BUILTIN, &builtin_getopts);
   sym_func_insert(func_table, "hash", FUNC_BUILTIN, &builtin_hash);
   sym_func_insert(func_table, "jobs", FUNC_BUILTIN, &builtin_jobs);
   sym_func_insert(func_table, "kill", FUNC_BUILTIN, &builtin_kill);
   /* sym_func_insert(func_table, "newgrp", FUNC_BUILTIN, &builtin_newgrp); */
   sym_func_insert(func_table, "pwd", FUNC_BUILTIN, &builtin_pwd);
   sym_func_insert(func_table, "read", FUNC_BUILTIN, &builtin_read);
   sym_func_insert(func_table, "true", FUNC_BUILTIN, &builtin_true);
   sym_func_insert(func_table, "umask", FUNC_BUILTIN, &builtin_umask);
   sym_func_insert(func_table, "unalias", FUNC_BUILTIN, &builtin_unalias);
   sym_func_insert(func_table, "wait", FUNC_BUILTIN, &builtin_wait);
}
