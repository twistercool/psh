%{
//Includes
#include <stdio.h>
%}
//tokens

%token NUMBER
%token VARIABLE

%token PLUS_PLUS
%token MIN_MIN
%token LSHIFT_LSHIFT
%token RSHIFT_RSHIFT
%token LSHIFT_EQ
%token RSHIFT_EQ
%token LSHIFT_LSHIFT_EQ
%token RSHIFT_RSHIFT_EQ
%token EQ_EQ
%token NOT_EQ
%token AND_AND
%token OR_OR
%token TIMES_EQ
%token DIV_EQ
%token MOD_EQ
%token PLUS_EQ
%token MIN_EQ
%token AND_EQ
%token XOR_EQ
%token OR_EQ

%left '+' '-'
%left '*' '/' '%'
%left '&' '|'
%left '!' '~'
%left AND_AND OR_OR
%left UNARY
%left '<' LSHIFT_EQ '>' RSHIFT_EQ EQ_EQ NOT_EQ
%right '=' TIMES_EQ DIV_EQ MOD_EQ PLUS_EQ MIN_EQ LSHIFT_LSHIFT_EQ RSHIFT_RSHIFT_EQ AND_EQ XOR_EQ OR_EQ

%%
expr
   : NUMBER
   | VARIABLE
   | VARIABLE post_op
   | '(' expr ')'
   | unary_op expr %prec UNARY
   | expr logical_op expr
   | expr arithmetic_op expr
   | VARIABLE assignment_op expr
   ;

post_op
   : PLUS_PLUS
   | MIN_MIN
   ;

unary_op
   : PLUS_PLUS
   | '+'
   | MIN_MIN
   | '-'
   | '~'
   | '!'
   ;

arithmetic_op
   : '*'
   | '/'
   | '%'
   | '+'
   | '-'
   | LSHIFT_LSHIFT
   | RSHIFT_RSHIFT
   ;

logical_op
   : '<'
   | LSHIFT_EQ
   | '>'
   | RSHIFT_EQ
   | EQ_EQ
   | NOT_EQ
   | '&'
   | '^'
   | '|'
   | AND_AND
   | OR_OR
   ;

assignment_op
   : '='
   | TIMES_EQ
   | DIV_EQ
   | MOD_EQ
   | PLUS_EQ
   | MIN_EQ
   | LSHIFT_LSHIFT_EQ
   | RSHIFT_RSHIFT_EQ
   | AND_EQ
   | XOR_EQ
   | OR_EQ
   ;
%%

int yacc_parse_arthmetic_sub() {
   yyparse();
   return 0;
}
