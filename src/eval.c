#include "eval.h"


struct psh_result*
eval_program(struct shell_env* cur_env, const struct psh_node* node_program) {
   DEBUG_ASSERT(node_program);
   DEBUG_ASSERT(node_program->node_type == PSH_NODE_PROGRAM);

   struct psh_result* cur_result = NULL;
   if (!node_program->node_program.node_complete_commands) {
      cur_result = psh_calloc(sizeof(struct psh_result));
      return cur_result;
   } else
      cur_result = eval_complete_commands(cur_env, node_program->node_program.node_complete_commands);

   return cur_result;
}

struct psh_result*
eval_complete_commands(struct shell_env* cur_env, const struct psh_node* node_complete_commands) {
   DEBUG_ASSERT(node_complete_commands);
   DEBUG_ASSERT(node_complete_commands->node_type == PSH_NODE_COMPLETE_COMMANDS);

   struct psh_result* cur_result = NULL;

   struct dyn_arr_node* arr_nodes = node_complete_commands->node_complete_commands.nodes_complete_command;
   for (uint64_t i = 0; i < arr_nodes->size; i++) {
      free(cur_result);
      cur_result = eval_complete_command(cur_env, arr_nodes->nodes[i]);
      DEBUG_ASSERT(cur_result);
      if (cur_result->special_code != CODE_NORMAL) {
         fprintf(stderr, "Didn't implement return yet (or continue/break bug)\n");
         assert(0);
      }
   }

   return cur_result;
}

struct psh_result*
eval_complete_command(struct shell_env* cur_env, const struct psh_node* node_complete_command) {
   DEBUG_ASSERT(node_complete_command);
   DEBUG_ASSERT(node_complete_command->node_type == PSH_NODE_COMPLETE_COMMAND);

   const bool run_in_background = (node_complete_command->node_complete_command.node_separator_op == NULL) ? false : node_complete_command->node_complete_command.node_separator_op->node_separator_op.type == TOKTYPE_AND;

   struct psh_result* cur_result = NULL;
   if (run_in_background) {
      const pid_t forked_pid = fork_shell();
      //add it to a data struvture for
      if (forked_pid == 0) {
         //child
         PSH_LOG(LOG_INFO, "Starting running bg job complete command\n");
         cur_env->_is_inside_job = true;
         cur_result = eval_list(cur_env, node_complete_command->node_complete_command.node_list);
         PSH_LOG(LOG_INFO, "Finished running bg job complete command\n");
         exit(cur_result->exit_status);
      } else {
         //parent
         struct job* job = job_create_bg(cur_env->jobs, "TODO_cmd_from_complete_command", forked_pid);
         cur_result = psh_calloc(sizeof(struct psh_result));
      }
   } else { //foreground
      cur_result = eval_list(cur_env, node_complete_command->node_complete_command.node_list);
   }

   return cur_result;
}

struct psh_result*
eval_list(struct shell_env* cur_env, const struct psh_node* node_list) {
   DEBUG_ASSERT(node_list);
   DEBUG_ASSERT(node_list->node_type == PSH_NODE_LIST);

   const struct dyn_arr_node* nodes_separator_op = node_list->node_list.nodes_separator_op;
   const struct dyn_arr_node* nodes_and_or = node_list->node_list.nodes_and_or;
   const size_t nb_separator_ops = (nodes_separator_op) ? nodes_separator_op->size : 0;
   const size_t nb_and_ors = nodes_and_or->size;
   DEBUG_ASSERT(nb_and_ors > 0);

   struct psh_result* cur_result = NULL;

   for (uint i = 0; i < nb_and_ors; i++) {
      free(cur_result);
      cur_result = NULL;

      bool run_in_background;
      if (i >= nb_separator_ops) {
         run_in_background = false;
      } else {
         run_in_background = nodes_separator_op->nodes[i]->node_separator.type == TOKTYPE_AND;
      }

      if (run_in_background) {
         pid_t forked_pid = fork_shell();
         if (forked_pid == 0) {
            //child
            PSH_LOG(LOG_INFO, "Starting running job\n");
            cur_env->_is_inside_job = true;
            cur_result = eval_and_or(cur_env, nodes_and_or->nodes[i]);
            PSH_LOG(LOG_INFO, "Finished running job\n");
            exit(cur_result->exit_status);
         } else {
            struct job* job = job_create_bg(cur_env->jobs, "TODO_from_list", forked_pid);
            cur_result = psh_calloc(sizeof(struct psh_result));
         }
      } else { //foreground
         cur_result = eval_and_or(cur_env, nodes_and_or->nodes[i]);
      }

      if (cur_result && cur_result->special_code != CODE_NORMAL) {
         //TODO implement
         assert(0);
      }
   }

   return cur_result;
}

struct psh_result*
eval_and_or(struct shell_env* cur_env, const struct psh_node* node_and_or) {
   DEBUG_ASSERT(node_and_or && node_and_or->node_type == PSH_NODE_AND_OR);

   struct psh_result* ret_result = psh_calloc(sizeof(struct psh_result));

   struct dyn_arr_node* dyn_pipelines = node_and_or->node_and_or.nodes_pipeline;
   struct dyn_arr_uint8* dyn_is_AND_IF = node_and_or->node_and_or.dyn_is_AND_IF;

   DEBUG_ASSERT(dyn_pipelines != NULL && dyn_pipelines->size != 0);
   struct psh_result* cur_result = eval_pipeline(cur_env, dyn_pipelines->nodes[0]);
   //TODO check the special_code better (free the things that need to be freed)
   if (cur_result->special_code != CODE_NORMAL) {
      assert(0);
      free(cur_result);
      return ret_result;
   }
   ret_result->exit_status = cur_result->exit_status;

   bool did_succeed = (cur_result->exit_status == 0);

   const int nb_separators = (dyn_is_AND_IF == NULL) ? 0 : dyn_is_AND_IF->size;
   if (nb_separators == 0) {
      free(ret_result);
      return cur_result;
   }
   for (int i = 0; i < nb_separators; i++) {
      if (did_succeed != dyn_is_AND_IF->uint8s[i])
         continue;

      cur_result = eval_pipeline(cur_env, dyn_pipelines->nodes[i+1]);
      //TODO check the special_code better (free the things that need to be freed)
      if (cur_result->special_code != CODE_NORMAL) {
         assert(0);
         free(cur_result);
         return ret_result;
      }

      did_succeed = (cur_result->exit_status == 0);

      ret_result->exit_status = cur_result->exit_status;
      free(cur_result);
   }

   return ret_result;
}

struct psh_result*
eval_pipeline(struct shell_env* cur_env, const struct psh_node* node_pipeline) {
   DEBUG_ASSERT(node_pipeline && node_pipeline->node_type == PSH_NODE_PIPELINE);

   const int nb_sequences = node_pipeline->node_pipeline.nodes_command->size;

   if (nb_sequences == 1) {
      struct psh_result* ret_result = eval_command(cur_env, node_pipeline->node_pipeline.nodes_command->nodes[0], STDIN_FILENO, STDOUT_FILENO);
      cur_env->last_pipeline_status = ret_result->exit_status;
      return ret_result;
   }

   struct psh_result* cur_result = NULL;

   int pipe_fds[2] = {STDIN_FILENO, -1};

   // up to the pre-last one
   for (int i = 0; i < nb_sequences - 1; i++) {
      free(cur_result);

      int fd_in, fd_out;
      fd_in = pipe_fds[0];
      int ret = pipe(pipe_fds);
      assert(ret == 0);
      fd_out = pipe_fds[1];

      /* printf("pipe from %d to %d\n", fd_in, fd_out); */

      cur_result = eval_command(cur_env, node_pipeline->node_pipeline.nodes_command->nodes[i], fd_in, fd_out);
      if (cur_result->special_code != CODE_NORMAL) {
         close(fd_out);
         return cur_result;
      }
      //TODO ignore or free results

      /* if (fd_in != 0) */
      /*    close(fd_in); */
      close(fd_out);
   }
   free(cur_result);
   cur_result = eval_command(cur_env, node_pipeline->node_pipeline.nodes_command->nodes[nb_sequences - 1], pipe_fds[0], STDOUT_FILENO);
   close(pipe_fds[0]);

   //TODO only wait for the last one


   //bang applies logical NOT to result
   if (node_pipeline->node_pipeline.is_banged)
      cur_result->exit_status = ~cur_result->exit_status;

   cur_env->last_pipeline_status = cur_result->exit_status;

   return cur_result;
}

struct psh_result*
eval_command(struct shell_env* cur_env, const struct psh_node* node_command, int fd_in, int fd_out) {
   DEBUG_ASSERT(node_command && node_command->node_type == PSH_NODE_COMMAND);

   switch (node_command->node_command.node_type) {
      case PSH_NODE_SIMPLE_COMMAND: {
         return eval_simple_command(cur_env, node_command->node_command.node_simple_command, fd_in, fd_out);
      } break;
      case PSH_NODE_COMPOUND_COMMAND: {
         struct psh_result* ret_result = NULL;
         if (node_command->node_command.nodes_io_redirect) {
            int* orig_fds = save_fds();
            enum redirect_output out = perform_redirections(node_command->node_command.nodes_io_redirect, fd_in, fd_out);
            ret_result = eval_compound_command(cur_env, node_command->node_command.node_simple_command, fd_in, fd_out);
            restore_fds(orig_fds);
            free(orig_fds);
         } else {
            ret_result = eval_compound_command(cur_env, node_command->node_command.node_simple_command, fd_in, fd_out);
         }

         return ret_result;
      } break;
      case PSH_NODE_FUNCTION: {
         return eval_function_definition(cur_env, node_command->node_command.node_simple_command);
      } break;
      default:
         fprintf(stderr, "Unknown eval_command node_type\n");
      assert(0);
   }
}

struct psh_result*
eval_compound_command(struct shell_env* cur_env, const struct psh_node* node_compound_command, int fd_in, int fd_out) {
   DEBUG_ASSERT(node_compound_command && node_compound_command->node_type == PSH_NODE_COMPOUND_COMMAND);

   //TODO deal with redirect list somehow?
   switch (node_compound_command->node_compound_command.node_type) {
      case PSH_NODE_BRACE_GROUP: return eval_brace_group(cur_env, node_compound_command->node_compound_command.node_brace_group);
      case PSH_NODE_SUBSHELL:    return eval_subshell(cur_env, node_compound_command->node_compound_command.node_subshell);
      case PSH_NODE_FOR_CLAUSE:  return eval_for_clause(cur_env, node_compound_command->node_compound_command.node_for_clause);
      case PSH_NODE_CASE_CLAUSE: return eval_case_clause(cur_env, node_compound_command->node_compound_command.node_case_clause);
      case PSH_NODE_IF_CLAUSE:   return eval_if_clause(cur_env, node_compound_command->node_compound_command.node_if_clause);
      case PSH_NODE_LOOP:        return eval_loop(cur_env, node_compound_command->node_compound_command.node_loop);
      default:
         assert(0);
   }
}

struct psh_result*
eval_case_clause(struct shell_env* cur_env, const struct psh_node* node_case_clause) {
   DEBUG_ASSERT(node_case_clause && node_case_clause->node_type == PSH_NODE_CASE_CLAUSE);
   DEBUG_ASSERT(node_case_clause->node_case_clause.word_str);

   struct psh_result* ret_result = NULL;

   //TODO Don't know if right expansions are applied
   char* var = node_case_clause->node_case_clause.word_str;
   struct wordexp_t* wordexp_cmd = psh_calloc(sizeof(struct wordexp_t));
   int ret = wordexp(cur_env, var, wordexp_cmd, 0);
   char* exp_var = psh_strdup((wordexp_cmd->we_wordc == 0) ? var : wordexp_cmd->we_wordv[0]);
   wordfree(wordexp_cmd);

   struct dyn_arr_node* nodes_case_item = node_case_clause->node_case_clause.nodes_case_item;

   uint64_t nb_case_items = (nodes_case_item) ? nodes_case_item->size : 0;
   for (uint64_t i = 0; i < nb_case_items; i++) {
      //check if any pattern in the list matches the "var" variable
      //if yes, run the 
      struct dyn_arr_str* cur_patterns = nodes_case_item->nodes[i]->node_case_item.dyn_patterns;
      if (!cur_patterns)
         continue;

      for (uint64_t j = 0; j < cur_patterns->size; j++) {
         if (is_pattern_match(cur_patterns->strs[j], exp_var)) {
            free(exp_var);
            return eval_compound_list(cur_env, nodes_case_item->nodes[i]->node_case_item.node_compound_list);
         }
      }
   }

   if (!ret_result)
      ret_result = psh_calloc(sizeof(struct psh_result));

   free(exp_var);

   return ret_result;
}

struct psh_result*
eval_function_definition(struct shell_env* cur_env, const struct psh_node* node_function) {
   DEBUG_ASSERT(node_function && node_function->node_type == PSH_NODE_FUNCTION);

   //TODO needs to check if readonly, etc...
   //TODO NEEDS TO COPY THE NODE
   enum psh_error err = sym_func_insert(cur_env->func_table, node_function->node_function.function_name, FUNC_NORMAL, psh_node_copy(node_function));

   struct psh_result* ret_result = psh_calloc(sizeof(struct psh_result));
   ret_result->exit_status = err;
   return ret_result;
}

struct psh_result*
eval_function(struct shell_env* cur_env, struct psh_node* node_function, struct dyn_arr_str* args, int fd_in, int fd_out) {
   DEBUG_ASSERT(node_function);
   DEBUG_ASSERT(node_function->node_type == PSH_NODE_FUNCTION);

   struct dyn_arr_str* stored_args = cur_env->args;
   cur_env->args = args;

   //TODO deal with potential redirect list
   struct psh_result* cur_result = eval_compound_command(cur_env, node_function->node_function.node_compound_command, fd_in, fd_out);

   //note: old args are freed outside of the eval_function
   cur_env->args = stored_args;

   switch (cur_result->special_code) {
      case CODE_RETURN:
         cur_result->exit_status = cur_result->special_val;
         cur_result->special_code = CODE_NORMAL;
         cur_result->special_val = 0;
         return cur_result;
      case CODE_BREAK:
      case CODE_CONTINUE:
         //TODO not sure if a break/continue inside of a function goes outside of it
         assert(0);
         return cur_result;
      default:
         DEBUG_ASSERT(cur_result->special_code == CODE_NORMAL);
         return cur_result;
   }
}

struct psh_result*
eval_for_clause(struct shell_env* cur_env, const struct psh_node* node_for_clause) {
   assert(node_for_clause);
   assert(node_for_clause->node_type == PSH_NODE_FOR_CLAUSE);

   char* name = node_for_clause->node_for_clause.name;

   struct dyn_arr_str* dyn_wordlist = node_for_clause->node_for_clause.dyn_wordlist;
   struct psh_node* node_do_group = node_for_clause->node_for_clause.node_do_group;

   size_t nb_assigns = (dyn_wordlist == NULL) ? 0 : dyn_wordlist->size;

   struct psh_result* cur_result = NULL;
   //TODO maybe need to perform some expansions on the wordlist words
   for (size_t i = 0; i < nb_assigns; i++) {
      struct sym_var_entry* cur_entry = sym_get_var_entry(cur_env->var_table, name);
      bool is_env = (cur_entry == NULL) ? false : cur_entry->is_env;
      sym_var_insert(cur_env->var_table, name, dyn_wordlist->strs[i], is_env);

      cur_result = eval_do_group(cur_env, node_do_group);
      switch (cur_result->special_code) {
         case CODE_BREAK:
            DEBUG_ASSERT(cur_result->special_val > 0);
            cur_result->special_val--;
            if (cur_result->special_val <= 0)
               cur_result->special_code = CODE_NORMAL;
            goto exit_loop;
         case CODE_CONTINUE: {
            DEBUG_ASSERT(cur_result->special_val > 0);
            cur_result->special_val--;
            if (cur_result->special_val <= 0)
               cur_result->special_code = CODE_NORMAL;
            else
               goto exit_loop;
         } break;
         case CODE_RETURN:
            fprintf(stderr, "didn't implement return yet\n");
            assert(0);
            goto exit_loop;
         default: { //ignore
            DEBUG_ASSERT(cur_result->special_val == 0);
         } break;
      }

      //TODO free and figure out how the error values are bubbled up
      //TODO also handle the special_code for continue, break and return
   }
exit_loop:

   return cur_result;
}

struct psh_result*
exec_in_subshell(struct shell_env* cur_env, const struct psh_node* node_subshell) {
   struct psh_result* ret_result = psh_calloc(sizeof(struct psh_result));

   int fork_pid = fork();
   assert(fork_pid != -1);
   if (fork_pid == 0) {
      //child
      struct psh_result* res = eval_compound_list(cur_env, node_subshell->node_subshell.node_compound_list);
      
      exit(res->exit_status);
   } else {
      //parent
      int status;
      waitpid(fork_pid, &status, 0);

      ret_result->exit_status = status;
   }

   return ret_result;
}

struct psh_result*
eval_subshell(struct shell_env* cur_env, const struct psh_node* node_subshell) {
   DEBUG_ASSERT(node_subshell && node_subshell->node_type == PSH_NODE_SUBSHELL);
   return exec_in_subshell(cur_env, node_subshell);
}

struct psh_result*
eval_compound_list(struct shell_env* cur_env, const struct psh_node* node_compound_list) {
   DEBUG_ASSERT(node_compound_list);
   DEBUG_ASSERT(node_compound_list->node_type == PSH_NODE_COMPOUND_LIST);

   bool run_in_background = (node_compound_list->node_compound_list.node_separator == NULL) ? false : node_compound_list->node_compound_list.node_separator->node_separator.type == TOKTYPE_AND;

   struct psh_result* cur_result = NULL;

   if (run_in_background) {
      pid_t forked_pid = fork_shell();
      if (forked_pid == 0) {
         //child
         PSH_LOG(LOG_INFO, "Starting running bg job compound list\n");
         cur_env->_is_inside_job = true;
         cur_result = eval_term(cur_env, node_compound_list->node_compound_list.node_term);
         PSH_LOG(LOG_INFO, "Finished running bg job compound list\n");
         exit(cur_result->exit_status);
      } else {
         //parent
         struct job* job = job_create_bg(cur_env->jobs, "TODO_cmd_from_compound_list", forked_pid);
         cur_result = psh_calloc(sizeof(struct psh_result));
      }
   } else {
      cur_result = eval_term(cur_env, node_compound_list->node_compound_list.node_term);
   }

   return cur_result;
}

struct psh_result*
eval_term(struct shell_env* cur_env, const struct psh_node* node_term) {
   DEBUG_ASSERT(node_term && node_term->node_type == PSH_NODE_TERM);

   struct dyn_arr_node* nodes_separator = node_term->node_term.nodes_separator;
   struct dyn_arr_node* nodes_and_or = node_term->node_term.nodes_and_or;
   size_t nb_and_ors = nodes_and_or->size;
   DEBUG_ASSERT(nb_and_ors > 0);

   struct psh_result* cur_result = NULL;

   for (size_t i = 0; i < nb_and_ors; i++) {
      free(cur_result);

      bool run_in_background;
      if (i >= nb_and_ors - 1) {
         run_in_background = false;
      } else {
         run_in_background = (nodes_separator->nodes[i]->node_separator.type == TOKTYPE_AND);
      }

      if (run_in_background) {
         pid_t forked_pid = fork_shell();
         //add it to a data struvture for
         if (forked_pid == 0) {
            //child
            PSH_LOG(LOG_INFO, "Starting running bg job term\n");
            cur_env->_is_inside_job = true;
            cur_result = eval_and_or(cur_env, nodes_and_or->nodes[i]);
            exit(cur_result->exit_status);
         PSH_LOG(LOG_INFO, "Finished running bg job term\n");
         } else {
            //parent
            struct job* job = job_create_bg(cur_env->jobs, "TODO_cmd_from_term", forked_pid);
            cur_result = psh_calloc(sizeof(struct psh_result));
         }
      } else {
         cur_result = eval_and_or(cur_env, nodes_and_or->nodes[i]);
      }

      if (cur_result->special_code != CODE_NORMAL) {
         //TODO implement
         assert(0);
         return cur_result;
      }
   }

   return cur_result;
}

struct psh_result*
eval_if_clause(struct shell_env* cur_env, const struct psh_node* node_if_clause) {
   DEBUG_ASSERT(node_if_clause && node_if_clause->node_type == PSH_NODE_IF_CLAUSE);

   struct psh_result* cur_if_result = eval_compound_list(cur_env, node_if_clause->node_if_clause.node_compound_list_if);

   if (cur_if_result->exit_status == 0) {
      free(cur_if_result);
      return eval_compound_list(cur_env, node_if_clause->node_if_clause.node_compound_list_then);
   } else {
      /* free(cur_if_result); */
      struct psh_node* node_else_part = node_if_clause->node_if_clause.node_else_part;
      if (!node_else_part) {
         //TODO update ret_result?
         struct psh_result* ret_result = psh_calloc(sizeof(struct psh_result));
         return ret_result;
      }

      size_t nb_elif = node_else_part->node_else_part.nodes_compound_list_elif->size;
      size_t nb_body = node_else_part->node_else_part.nodes_compound_list_body->size;
      size_t i;
      for (i = 0; i < nb_elif; i++) {
         //process ELIFS
         cur_if_result = eval_compound_list(cur_env, (node_else_part->node_else_part.nodes_compound_list_elif->nodes)[i]);
         if (cur_if_result->exit_status == 0) {
            return eval_compound_list(cur_env, (node_else_part->node_else_part.nodes_compound_list_body->nodes)[i]);
         }
         //TODO free that?
      }
      if (nb_elif < nb_body) {
         //process ELSE
         return eval_compound_list(cur_env, (node_else_part->node_else_part.nodes_compound_list_body->nodes)[i]);
      }

      /* struct psh_result* ret_result = psh_calloc(sizeof(struct psh_result)); */
      return cur_if_result;
   }
}

struct psh_result*
eval_loop(struct shell_env* cur_env, const struct psh_node* node_loop) {
   DEBUG_ASSERT(node_loop && node_loop->node_type == PSH_NODE_LOOP);

   bool is_while_clause = node_loop->node_loop.is_while_clause; //else it's an until clause

   struct psh_node* node_compound_list = node_loop->node_loop.node_compound_list;
   struct psh_node* node_do_group = node_loop->node_loop.node_do_group;

   struct psh_result* cur_result_1 = NULL;
   struct psh_result* cur_result_2 = NULL;
   while (1) {
      free(cur_result_1);
      free(cur_result_2);
      cur_result_1 = eval_compound_list(cur_env, node_compound_list);
      cur_result_2 = NULL;

      if ((is_while_clause && cur_result_1->exit_status == 0) || (!is_while_clause && cur_result_1->exit_status != 0)) {
         cur_result_2 = eval_do_group(cur_env, node_do_group);
      } else {
         break;
      }

      switch (cur_result_2->special_code) {
         case CODE_BREAK:
            DEBUG_ASSERT(cur_result_2->special_val > 0);
            cur_result_2->special_val--;
            if (cur_result_2->special_val <= 0)
               cur_result_2->special_code = CODE_NORMAL;
            goto exit_loop;
         case CODE_CONTINUE:
            DEBUG_ASSERT(cur_result_2->special_val > 0);
            cur_result_2->special_val--;
            if (cur_result_2->special_val <= 0) {
               cur_result_2->special_code = CODE_NORMAL;
               break;
            } else goto exit_loop;
         case CODE_RETURN:
            fprintf(stderr, "didn't implement return yet\n");
            assert(0);
            goto exit_loop;
         default: //ignore
            DEBUG_ASSERT(cur_result_2->special_val == 0);
            break;
      }
   }
exit_loop:

   free(cur_result_1);

   //If nothing is executed
   if (cur_result_2 == NULL) {
      cur_result_2 = psh_calloc(sizeof(struct psh_result));
   }

   return cur_result_2;
}

struct psh_result*
eval_brace_group(struct shell_env* cur_env, const struct psh_node* node_brace_group) {
   assert(node_brace_group);
   assert(node_brace_group->node_type == PSH_NODE_BRACE_GROUP);
   return eval_compound_list(cur_env, node_brace_group->node_brace_group.node_compound_list);
}

struct psh_result*
eval_do_group(struct shell_env* cur_env, const struct psh_node* node_do_group) {
   DEBUG_ASSERT(node_do_group && node_do_group->node_type == PSH_NODE_DO_GROUP);
   struct psh_result* cur_result = eval_compound_list(cur_env, node_do_group->node_do_group.node_compound_list);
   return cur_result;
}


///Evaluates a command in a subshell and preserves its output in a psh_result*
struct psh_result*
eval_command_substitution(struct shell_env* cur_env, struct psh_string* cmd_str) {
   DEBUG_ASSERT(cur_env);
   DEBUG_ASSERT(cmd_str);
   struct psh_result* ret_result = psh_calloc(sizeof(struct psh_result));
   struct psh_string* out_str = psh_string_create("");

   int fds[2];
   int err_pipe = pipe(fds);
   if (err_pipe == -1)
      assert(0);
   //TODO what to do with the stderr inside of the command substitution?
   int pid = fork();
   if (pid < 0)
      assert(0);
   else if (pid == 0) {
      //Child
      close(fds[0]);
      if (dup2(fds[1], STDOUT_FILENO) == -1) {
         close(fds[1]);
         exit(1);
      }
      struct dyn_arr_tok* dyn_toks = psh_malloc(sizeof(struct dyn_arr_tok));
      dyn_arr_tok_init(dyn_toks);
      cur_env->tok_arr = dyn_toks;
      cur_env->psh_inputs = dyn_arr_psh_input_create_from(cmd_str);

      struct psh_node* node_program = parse_program(cur_env);
      if (IS_PARSE_ERR(node_program)) {
         //TODO handle errors
         fprintf(stderr, "psh: parse error\n");
         exit(1);
      }
      struct psh_result* ret_result = eval_program(cur_env, node_program);
      close(fds[1]);
      close(STDOUT_FILENO);
      if (ret_result)
         exit(ret_result->exit_status);
      else
         exit(1);
   } else {
      //Parent
      close(fds[1]);
      int status;
      waitpid(pid, &status, 0);
      FILE* pipe_file = fdopen(fds[0], "r");
      assert(pipe_file);
      char line[1024] = {0};
      while (fgets(line, sizeof(line), pipe_file) != NULL) {
         psh_string_append_str(out_str, line);
      }
      fclose(pipe_file);
   }

   //delete trailing newlines
   while (out_str->str_len > 0 && out_str->str[out_str->str_len-1] == '\n') {
      psh_string_delete_char(out_str, out_str->str_len-1);
   }
   ret_result->substitution_output = psh_string_to_cstr(out_str, true);
   
   return ret_result;
}

//Defined in psh.h
struct psh_input* read_file_to_psh_input(const char*);

_Nullable struct psh_result*
exec_dot_script(struct shell_env* cur_env, const char* path) {
   PSH_LOG(LOG_INFO, "path: '%s'", path);

   struct shell_env new_env;
   memcpy(&new_env, cur_env, sizeof(struct shell_env));
   new_env.tok_arr = dyn_arr_tok_create();
   struct psh_input* new_input = read_file_to_psh_input(path);
   if (!new_input) {
      perror("Failed to parse new_input for dot script");
      psh_input_free(new_input, true);
      return NULL;
   }
   new_env.psh_inputs = dyn_arr_psh_input_create_with(new_input);

   struct psh_node* node_program = parse_program(&new_env);
   struct psh_result* cur_result = NULL;
   if (!IS_PARSE_ERR(node_program)) {
      cur_result = eval_program(&new_env, node_program);
   }

   psh_node_free(node_program, true);
   dyn_arr_psh_input_free(new_env.psh_inputs, true);
   dyn_arr_tok_free(new_env.tok_arr);
   psh_string_free(new_input->psh_str);
   return cur_result;
}

_Nullable struct psh_string*
find_in_PATH(const struct shell_env* cur_env, _Nonnull const char* bin, _Nullable const char* PATH_override) {
   assert(bin);
   const char* PATH = (PATH_override == NULL) ? sym_get_value(cur_env->var_table, "PATH") : PATH_override;
   if (!PATH)
      return NULL;

   struct psh_string* cur_path = psh_string_create(NULL);
   int prev_semi_index = -1;
   const int PATH_len = strlen(PATH);

   for (int i = 0; i < PATH_len; i++) {
      if (PATH[i] == ':') {
         char* cur_path_str;
         if (i - 1 == prev_semi_index || i == PATH_len - 1) {
            cur_path_str = getcwd(NULL, 0);
            assert(cur_path_str);
         }
         else {
            cur_path_str = cur_path->str;
         }

         //Look through the delimited path

         DIR* cur_dir = opendir(cur_path_str);
         if (cur_dir) {
            struct dirent* cur_ent;
            while ((cur_ent = readdir(cur_dir)) != NULL) {
               //check the filename
               if (!strcmp(bin, cur_ent->d_name)) {
                  struct psh_string* ret_str = psh_string_create(cur_path_str);
                  pstr_append(ret_str, '/');
                  pstr_append(ret_str, cur_ent->d_name);

                  if (i - 1 == prev_semi_index || i == PATH_len - 1)
                     free(cur_path_str);
                  psh_string_free(cur_path);
                  closedir(cur_dir);
                  return ret_str;
               }
            }

            closedir(cur_dir);
         }

         if (i - 1 == prev_semi_index || i == PATH_len - 1)
            free(cur_path_str);
         psh_string_free(cur_path);
         cur_path = psh_string_create("");
         prev_semi_index = i;
      } else {
         psh_string_append_char(cur_path, PATH[i]);
      }
   }

   psh_string_free(cur_path);

   return NULL;
}


struct psh_result*
eval_simple_command(struct shell_env* cur_env, const struct psh_node* node_simple_command, int fd_in, int fd_out) {
   DEBUG_ASSERT(node_simple_command);
   DEBUG_ASSERT(node_simple_command->node_type == PSH_NODE_SIMPLE_COMMAND);
   struct psh_string* exec_path = NULL;
   struct psh_result* ret_result = psh_calloc(sizeof(struct psh_node));

   //The prefix is either assignments and/or io_redirections
   struct psh_node* node_cmd_prefix = node_simple_command->node_simple_command.node_cmd_prefix;
   struct psh_node* node_cmd_suffix = node_simple_command->node_simple_command.node_cmd_suffix;


   //get all redirections
   struct dyn_arr_node* nodes_redirections = dyn_arr_node_create();
   if (node_cmd_prefix) {
      //add all the redirections from the prefix
      struct dyn_arr_node* redirs = node_cmd_prefix->node_cmd_prefix.nodes_io_redirect;
      size_t nb_redirs = (redirs == NULL) ? 0 : redirs->size;
      for (size_t i = 0; i < nb_redirs; i++) {
         dyn_arr_node_push(nodes_redirections, redirs->nodes[i]);
      }
   }
   if (node_cmd_suffix) {
      //add all the redirections from the suffix
      struct dyn_arr_node* redirs = node_cmd_suffix->node_cmd_suffix.nodes_io_redirect;
      size_t nb_redirs = (redirs == NULL) ? 0 : redirs->size;
      for (size_t i = 0; i < nb_redirs; i++) {
         dyn_arr_node_push(nodes_redirections, redirs->nodes[i]);
      }
   }

   //This stores the all arguments for a program (except for argv[0]) and NULL
   struct dyn_arr_str args = {0};
   dyn_arr_str_init(&args);

   const char* cmd_str = NULL;
   if (node_simple_command->node_simple_command.node_cmd_word) {
      if (node_simple_command->node_simple_command.is_cmd_word) {
         cmd_str = node_simple_command->node_simple_command.node_cmd_word->node_cmd_word.word_str;
      } else {
         cmd_str = node_simple_command->node_simple_command.node_cmd_name->node_cmd_name.command_str;
      }
   }

   //Perform expansions here, if there are no resulting commands, do the assignments globally and return
   if (cmd_str != NULL) {
      struct wordexp_t* wordexp_cmd = psh_calloc(sizeof(struct wordexp_t));
      int ret = wordexp(cur_env, cmd_str, wordexp_cmd, 0);

      PSH_LOG(LOG_INFO, "node_cmd_suffix: '%p'\n", (void*)node_cmd_suffix);

      if (node_cmd_suffix) {
         const size_t words_len = node_cmd_suffix->node_cmd_suffix.words->size;
         for (size_t i = 0; i < words_len; i++) {
            ret = wordexp(cur_env, (node_cmd_suffix->node_cmd_suffix.words->strs)[i], wordexp_cmd, 0);
         }
      }

      for (size_t i = 0; i < wordexp_cmd->we_wordc; i++) {
         dyn_arr_str_push(&args, wordexp_cmd->we_wordv[i], true);
      }

      wordfree(wordexp_cmd);

      if (args.size == 0 && node_cmd_prefix == NULL) {
         goto free_and_ret;
      }

      cmd_str = args.strs[0];
   }

   // There are just prefixes, aka assignments or redirections
   // OR expansions led to command having 0 arguments
   // but there needs to be either assignments or redirections
   if (cmd_str == NULL || args.size == 0) {
      DEBUG_ASSERT(node_cmd_prefix);
      // If there are assignments, add them to the symbol table
      struct dyn_arr_str* assignment_words = node_cmd_prefix->node_cmd_prefix.assignment_words;
      size_t nb_assignments = (assignment_words == NULL) ? 0 : assignment_words->size;
      for (size_t i = 0; i < nb_assignments; i++) {
         const char* assignment_exp = expand(cur_env, assignment_words->strs[i], EXP_TILDE | EXP_PARAM | EXP_CMD_SUB | EXP_ARITH | EXP_QUOTE).charp;

         struct psh_string* name_str = psh_string_create(NULL);
         const char* cur_name_ptr = assignment_exp;
         while (*cur_name_ptr && *cur_name_ptr != '=') {
            psh_string_append_char(name_str, *cur_name_ptr++);
         }
         if (!*cur_name_ptr) {
            DEBUG_ASSERT(0);
         }

         struct psh_string* value_str = psh_string_create(cur_name_ptr + 1);

         struct sym_var_entry* cur_entry = sym_get_var_entry(cur_env->var_table, name_str->str);
         if (cur_entry) {
            if (cur_entry->is_readonly) {
               fprintf(stderr, "psh: '%s': cannot change: readonly variable\n", name_str->str);
               ret_result->exit_status = 1;
               goto free_and_ret;
            }
            free(cur_entry->value);
            cur_entry->value = psh_strdup(value_str->str);
         } else {
            sym_var_insert(cur_env->var_table, name_str->str, value_str->str, false);
         }
         psh_string_free(name_str);
         psh_string_free(value_str);
         free((char*)assignment_exp);
      }

      goto free_and_ret;

      //TODO implement the redirections later
   }


   cmd_str = args.strs[0];


   dyn_arr_str_push(&args, NULL, false); //make args.strs (argv) nullable


   //if there is a command name, perform command search and execution:
    //1. check if the command contains slashes:
   if (strchr(cmd_str, '/') != NULL) {
      exec_path = psh_string_create(cmd_str);
      assert(exec_path);
   }

   
   struct sym_func_entry* func_entry = sym_get_func_entry(cur_env->func_table, cmd_str);

   if (func_entry) {
//1. check if it's a function (add assignments at the beginning to the sym table)
      if (func_entry->func_type == FUNC_NORMAL) {
         //TODO do something with redirections here
         free(ret_result);
         //args have a NULL at the end, so we need to create a copy without
         struct dyn_arr_str* args_nonull = dyn_arr_str_create();
         for (uint i = 1; i < args.size - 1; i++) {
            dyn_arr_str_push(args_nonull, args.strs[i], true);
         }

         ret_result = eval_function(cur_env, func_entry->node_func, args_nonull, fd_in, fd_out);
         dyn_arr_str_free(args_nonull, true);      
         free(args_nonull);

         goto free_and_ret;
      }
//2. check if it's a special builtin (add assignments at the beginning to the sym table)
      else if (func_entry->func_type == FUNC_SPECIAL_BUILTIN) { //FUNC_SPECIAL_BUILTIN
         //TODO add the assignments before to the list of real assignments
         int* orig_fds = save_fds();
         enum redirect_output redir = perform_redirections(nodes_redirections, fd_in, fd_out);
         int ret_val = 0;
         if (!strcmp(cmd_str, "break") ||
             !strcmp(cmd_str, "continue") ||
             !strcmp(cmd_str, "return") ||
             !strcmp(cmd_str, ".")) {
            ret_val = func_entry->func(cur_env, &args, ret_result);
         } else if (!strcmp(cmd_str, "exec")) {
            ret_val = func_entry->func(cur_env, &args, nodes_redirections);
         } else {
            ret_val = func_entry->func(cur_env, &args);
         }
         ret_result->exit_status = ret_val;
         restore_fds(orig_fds);
         free(orig_fds);

         goto free_and_ret;
      }
//3. check if it's a normal builtin
      else if (func_entry->func_type == FUNC_BUILTIN) {
         //TODO pass on the temporary assignments
         /* fprintf(stderr, "Builtin: '%s'\n", cmd_str); */
         int* orig_fds = save_fds();
         enum redirect_output redir = perform_redirections(nodes_redirections, fd_in, fd_out);
         //TODO change function pointer signature to "int (*)(struct shell_env*, struct dyn_arr_str, ...)" since too many builtins need different information
         int ret_val = 0;
         if (!strcmp(cmd_str, "command")) {
            ret_val = func_entry->func(cur_env, &args, nodes_redirections, ret_result, fd_in, fd_out);
         } else {
            ret_val = func_entry->func(cur_env, &args);
         }

         restore_fds(orig_fds);
         free(orig_fds);

         ret_result->exit_status = ret_val;

         goto free_and_ret;
      }
      else assert(0);
   }


   //search PATH if doesn't have a '/'
   if (exec_path == NULL) {
      exec_path = find_in_PATH(cur_env, cmd_str, NULL);

      if (!exec_path) {
         /* fprintf(stderr, "piersh: command not found: '%s'\n", cmd_str); */
         ret_result->exit_status = 127;
         //TODO: find the line number
         command_not_found_error(cur_env, 1000, cmd_str);
         goto free_and_ret;
      }
   }


   //Ignore ret?
   int ret = execute_command_in_context(cur_env, exec_path, &args, nodes_redirections, ret_result, fd_in, fd_out);

free_and_ret:
   
   if (exec_path)
      psh_string_free(exec_path);
   dyn_arr_str_free(&args, true);
   dyn_arr_node_free(nodes_redirections, false);
   free(nodes_redirections);

   return ret_result;
}

//args:
// - ret_result: output_param
// - args: expected to have a NULL at the end
int execute_command_in_context(struct shell_env* cur_env, const struct psh_string* exec_path, struct dyn_arr_str* args, struct dyn_arr_node *nodes_redirections, struct psh_result* ret_result/*output*/, int fd_in, int fd_out) {
   int ret = 0;

   pid_t fork_pid = fork();
   assert(fork_pid != -1);
   if (fork_pid == 0) {
      //child
      //PERFORM REDIRECTION
      bool cmd_input_redir = false;
      bool cmd_output_redir = false;

      enum redirect_output out = perform_redirections(nodes_redirections, fd_in, fd_out);

      struct dyn_arr_str* dyn_env = sym_export_env(cur_env->var_table);
      dyn_arr_str_push(dyn_env, NULL, false);
      char** env_cstrs = dyn_arr_to_cstr_arr(dyn_env);

      errno = 0;
      int exec_ret = execve(exec_path->str, args->strs, env_cstrs);
      switch (errno) {
         case ENOEXEC:
            //TODO run as sh script if can't find an interpreter for the bin
            break;
         default:
            perror("psh");
            /* fprintf(stderr, "piersh: exec error: '%s'\n", exec_path->str); */
            break;
      }
      exit(1);
   } else {
      //parent
      int status;
      if (!cur_env->_is_inside_job) {
         struct job* job = job_create_fg(cur_env->jobs, "test cmd", fork_pid);

         status = job_wait_fg(cur_env, job);

         //TODO: deal with SIGSTP which stops the process and sends it to the background
         dyn_arr_job_clean_waited_for(cur_env->jobs);

         /* PSH_LOG(LOG_DEBUG, "Error status: %d\n", status); */
      } else {
         waitpid(fork_pid, &status, 0);
      }
      ret_result->exit_status = status;
   }

   return ret;
}

pid_t
fork_shell() {
   pid_t fork_pid = fork();
   assert(fork_pid >= 0);
   return fork_pid;
}
