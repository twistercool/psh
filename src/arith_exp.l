%{
#include "y.tab.h"
%}

%%
[0-9]+ { yylval = atoi(yytext); return NUMBER; }
[a-zA-Z][a-zA-Z_0-9]* { return VARIABLE; }
(\+\+) { return PLUS_PLUS; }
(--) { return MIN_MIN; }
(<<) { return LSHIFT_LSHIFT; }
(>>) { return RSHIFT_RSHIFT; }
(<=) { return LSHIFT_EQ; }
(>=) { return RSHIFT_EQ; }
(<<=) { return LSHIFT_LSHIFT_EQ; }
(>>=) { return RSHIFT_RSHIFT_EQ; }
(==) { return EQ_EQ; }
(!=) { return NOT_EQ; }
(&&) { return AND_AND; }
(\|\|) { return OR_OR; }
(\*=) { return TIMES_EQ; }
(\/=) { return DIV_EQ; }
(%=) { return MOD_EQ; }
(\+=) { return PLUS_EQ; }
(-=) { return MIN_EQ; }
(&=) { return AND_EQ; }
(^=) { return XOR_EQ; }
(\|=) { return OR_EQ; }
[ \t\v]+ ;
%%
