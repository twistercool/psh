#include "signals.h"

struct shell_env* glob_cur_env = NULL;

//TODO: save and restore errno!

static void sigchild_handle(siginfo_t *si, void* ucontext);
static void sigtstp_handle(siginfo_t *si, void* ucontext);

static void
add_trap_if_exists(int sig) {
   assert(sig < NB_SIGNALS);

   if (glob_cur_env->traps[sig]) {
      PSH_LOG(LOG_INFO, "There is a trap set for this signal: '%s'\n", glob_cur_env->traps[sig]);
      if (glob_cur_env->nb_awaiting_traps >= 32) {
         //TODO: find out what to do, lose the signal, maybe try to allocate memory with another memory allocator or something
         //currently just lose the signal
         PSH_LOG(LOG_ERROR, "Lost signal '%d'\n", sig);
         return;
      }
      glob_cur_env->awaiting_traps[sig] = (uint8_t) sig;
      glob_cur_env->nb_awaiting_traps++;
   } else {
      PSH_LOG(LOG_INFO, "No traps set for that signal\n");
   }
}

static void
debug_sighandler(int sig_nb, siginfo_t* si, void* ucontext) {
   assert(glob_cur_env);
   PSH_LOG(LOG_INFO, "Received signal '%d': %s\n", sig_nb, strsignal(sig_nb));

   add_trap_if_exists(sig_nb);

   switch (sig_nb) {
      case SIGCHLD: sigchild_handle(si, ucontext); break;
      case SIGTSTP: sigtstp_handle(si, ucontext); break;
   }
}

//Happens when children stop
//Mark a finished as finished, if it's a background job
static void
sigchild_handle(siginfo_t *si, void* ucontext) {
   assert(glob_cur_env);
   if (glob_cur_env->_is_inside_job) {
      PSH_LOG(LOG_INFO, "Recevied a sigchild inside job process\n");
      return;
   }

   PSH_LOG(LOG_DEBUG, "Recevied a SIGCHLD outside jobprocess\n");

   pid_t child_pid = si->si_pid;
   struct job* job = dyn_arr_job_get_pid(glob_cur_env->jobs, child_pid);
   if (!job) {
      //Can happen when another process sends sigchild "for fun"
      PSH_LOG(LOG_WARNING, "Didn't find job, pid: '%hu'\n", child_pid);
      return;
   }
   PSH_LOG(LOG_DEBUG, "Found job, pid: '%hu', jb_nb: '%lu'\n", child_pid, job->job_nb);
   if (job->is_foreground) return; //Waited for somewhere else

   switch (si->si_code) {
      case CLD_EXITED:
      case CLD_DUMPED:
      case CLD_KILLED: {
         //reap the child in the next "jobs" command (I think)
         job->state = JOB_STATE_DONE;
         job->extra_state = si->si_status;
         glob_cur_env->_cleanup_done_jobs = true;
      } break;
      case CLD_CONTINUED: {
         /* assert(job->state == JOB_STATE_STOPPED); */
         job->state = JOB_STATE_RUNNING;
         job->extra_state = 0;
      } break;
      case CLD_STOPPED: {
         /* assert(job->state == JOB_STATE_RUNNING); */
         job->state = JOB_STATE_STOPPED;
         job->extra_state = 0; //TODO not sure how to get the signal that stopped the child process?
      } break;
      default: break; //is CLD_TRAPPED, do nothing
   }
}

//Happens when terminal receives a ctrl+z (stop)
static void
sigtstp_handle(siginfo_t *si, void* ucontext) {
   struct job* fg_job = dyn_arr_job_get_foreground(glob_cur_env->jobs);
   if (!fg_job)
      return;

   fg_job->is_foreground = false;
   fg_job->state = JOB_STATE_STOPPED;
   fg_job->extra_state = 0;  //set in job_wait_pid
   kill(fg_job->pid, SIGSTOP);
}

enum psh_error
setup_sighandlers(struct shell_env* cur_env) {
   DEBUG_ASSERT(cur_env);

   glob_cur_env = cur_env;

   //assign all signals to first check if there's a trap

   struct sigaction* sa = psh_calloc(sizeof(struct sigaction));
   cur_env->sa = sa;
   /* sa->sa_flags = SA_RESTART | SA_SIGINFO; */
   sa->sa_flags = SA_SIGINFO;
   sa->sa_sigaction = debug_sighandler;
   sigfillset(&(sa->sa_mask));

   if (sigaction(SIGCHLD, sa, NULL) == -1) {
      perror("Failed to setup signal handler");
      fprintf(stderr, "DEBUG: Signal '%d': %s\n", SIGCHLD, strsignal(SIGCHLD));
      return PSH_ERROR;
   }
   if (sigaction(SIGTSTP, sa, NULL) == -1) {
      perror("Failed to setup signal handler");
      fprintf(stderr, "DEBUG: Signal '%d': %s\n", SIGTSTP, strsignal(SIGTSTP));
      return PSH_ERROR;
   }
   /* for (size_t i = 1; i < NB_SIGNALS; i++) { */
   /*    if (i == SIGKILL || i == SIGSTOP) */
   /*       continue; */
   /*    if (sigaction(i, sa, NULL) == -1) { */
   /*       perror("Failed to setup signal handler"); */
   /*       fprintf(stderr, "DEBUG: Signal '%lu': %s\n", i, strsignal(i)); */
   /*       return PSH_ERROR; */
   /*    } */
   /* } */

   return PSH_SUCCESS;
}

void
destroy_sighandlers(struct shell_env* cur_env) {
   struct sigaction* sa = cur_env->sa;
   if (!sa)
      return;

   sigemptyset(&(sa->sa_mask));

   free(cur_env->sa);
   cur_env->sa = NULL;
   glob_cur_env = NULL;
}
