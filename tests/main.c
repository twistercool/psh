#include "psh.h"
#include <criterion/criterion.h>

Test(psh_string_tests, test_append_char) {
   struct psh_string* str = psh_string_create(NULL);
   cr_assert(str->str_len == 0, "Non empty string created");
   char arr[] = "This is a test, let's hope that it indeed works fine! This is relatively long so that obvious allocation bugs will be found very easily";
   for (uint64_t i = 0; i < sizeof(arr) / sizeof(*arr) - 1; i++) {
      psh_string_append_char(str, arr[i]);
   }

   cr_assert(strcmp(str->str, arr) == 0, "Error in appending chars: incorrect ouput: %s", str->str);
   psh_string_free(str);
}

Test(psh_string_tests, test_append_str) {
   struct psh_string* str = psh_string_create(NULL);
   cr_assert(str->str_len == 0, "Non empty string created");
   char arr[] = "This is a test, let's hope that it indeed works fine! This is relatively long so that obvious allocation bugs will be found very easily";

   psh_string_append_str(str, arr);

   cr_assert(strcmp(str->str, arr) == 0, "Error in appending str: incorrect ouput: %s", str->str);
   psh_string_free(str);
}

Test(psh_string_tests, test_cstr) {
   char arr[] = "This is a test, let's hope that it indeed works fine! This is relatively long so that obvious allocation bugs will be found very easily";
   struct psh_string* str = psh_string_create(arr);
   cr_assert(strcmp(str->str, arr) == 0, "Error in initialising with arr[]: incorrect ouput: %s", str->str);
   char* cstr = psh_string_to_cstr(str, true);
   cr_assert(cstr != NULL, "Failed to convert to cstr");
   cr_assert(strcmp(cstr, arr) == 0, "Incorrect string generated");
}

Test(psh_string_tests, test_str_from_nb) {
   struct psh_string* str_0 = psh_string_from_nb(123442);
   cr_assert(strcmp(str_0->str, "123442") == 0, "Error converting to number incorrect ouput: %s", str_0->str);
   psh_string_free(str_0);

   struct psh_string* str_1 = psh_string_from_nb(0);
   cr_assert(strcmp(str_1->str, "0") == 0, "Error converting to number incorrect ouput: %s", str_1->str);
   psh_string_free(str_1);

   struct psh_string* str_2 = psh_string_from_nb(18446744073709551615LLU);
   cr_assert(strcmp(str_2->str, "18446744073709551615") == 0, "Error converting to number incorrect ouput: %s", str_2->str);
   psh_string_free(str_2);
}

Test(psh_string_tests, test_insert_char) {
   const char arr_1[] = "This is a test, let's hope that it indeed works fine! This is relatively long so that obvious allocation bugs will be found very easily";
   const char arr_2[] = "aThisXYZ is a test, let's hope that it indeed works fine! This is relatively long so that obvious allocation bugs will be found very easilyBy";
   struct psh_string* str = psh_string_create(arr_1);
   cr_assert(strcmp(str->str, arr_1) == 0, "Error in initialising with arr_1[]: incorrect ouput: %s", str->str);

   psh_string_insert_char(str, 'a', 0);
   psh_string_insert_char(str, 'B', strlen(str->str));
   psh_string_insert_char(str, 'y', strlen(str->str));
   psh_string_insert_char(str, 'Z', 5);
   psh_string_insert_char(str, 'Y', 5);
   psh_string_insert_char(str, 'X', 5);
   psh_string_insert_char(str, '0', 1000000);

   cr_assert(strcmp(str->str, arr_2) == 0, "Incorrect output");

   psh_string_free(str);
}

Test(psh_string_tests, test_delete_char) {
   char arr_1[] = "This is a test, let's hope that it indeed works fine! This is relatively long so that obvious allocation bugs will be found very easily";
   char arr_2[] = "hi is a test, let's hope that it indeed works fine! This is relatively long so that obvious allocation bugs will be found very easi";
   struct psh_string* str = psh_string_create(arr_1);
   cr_assert(strcmp(str->str, arr_1) == 0, "Error in initialising with arr_1[]: incorrect ouput: %s", str->str);

   psh_string_delete_char(str, 0);
   psh_string_delete_char(str, str->str_len - 1);
   psh_string_delete_char(str, str->str_len - 1);
   psh_string_delete_char(str, 2);
   psh_string_delete_char(str, str->str_len + 100000);

   cr_assert(strcmp(str->str, arr_2) == 0, "Incorrect output");

   psh_string_free(str);
}

Test(struct_c_tests, test_psh_strdup) {
   char* str0 = "";
   char* str_0 = psh_strdup(str0);
   cr_assert(str_0 != NULL, "Failed to psh_strdup: '%s'", str0);
   cr_assert(strcmp(str_0, str0) == 0, "Incorrectly psh_strdupped: '%s'", str0);
   free(str_0);
   const char* str1 = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
   char* str_1 = psh_strdup(str1);
   cr_assert(str_1 != NULL, "Failed to psh_strdup: '%s'", str1);
   cr_assert(strcmp(str_1, str1) == 0, "Incorrectly psh_strdupped: '%s'", str1);
   free(str_1);
   char str2[1000] = {0};
   for (uint64_t i = 0; i < sizeof(str2) / sizeof(*str2) - 1; i++)
      str2[i] = (351 * i ) % 255 + 1;
   char* str_2 = psh_strdup(str2);
   cr_assert(str_2 != NULL, "Failed to psh_strdup: '%s'", str2);
   cr_assert(strcmp(str_2, str2) == 0, "Incorrectly psh_strdupped: '%s'", str2);
   free(str_2);
}

/* Test(struct_c_tests, test_psh_getc) { */
/*    struct psh_input in = { */
/*       .cur_pos = 0, */
/*       .is_last_EOF = 0, */
/*       .psh_str = psh_string_create("") */
/*    }; */
/*    char c = psh_getc(&in); */
/*    cr_assert(in.cur_pos == 0, "Expected cur_pos to be '%d', was '%zu'", 0, in.cur_pos); */
/*    cr_assert(c == EOF, "Incorrect character received from psh_input: EOF expected, '%c received'", c); */
/*    cr_assert(in.is_last_EOF == true, "Incorrect is_last_EOF"); */
/*    psh_ungetc(&in); */
/*    cr_assert(in.cur_pos == 0, "Expected cur_pos to be '%d', was '%zu'", 0, in.cur_pos); */
/*    cr_assert(in.is_last_EOF == false, "Incorrect is_last_EOF"); */
/*    psh_string_append_str(in.psh_str, "Hello"); */
/*    cr_assert(in.cur_pos == 0, "Expected cur_pos to be '%d', was '%zu'", 0, in.cur_pos); */
/*    c = psh_getc(&in); */
/*    cr_assert(in.cur_pos == 1, "Expected cur_pos to be '%d', was '%zu'", 1, in.cur_pos); */
/*    cr_assert(c == 'H', "Incorrect character received from psh_input: 'H' expected, '%c received'", c); */
/*    c = psh_getc(&in); */
/*    cr_assert(in.cur_pos == 2, "Expected cur_pos to be '%d', was '%zu'", 2, in.cur_pos); */
/*    cr_assert(c == 'e', "Incorrect character received from psh_input: 'e' expected, '%c received'", c); */
/*    c = psh_getc(&in); */
/*    cr_assert(in.cur_pos == 3, "Expected cur_pos to be '%d', was '%zu'", 3, in.cur_pos); */
/*    cr_assert(c == 'l', "Incorrect character received from psh_input: 'l' expected, '%c received'", c); */
/*    c = psh_getc(&in); */
/*    cr_assert(in.cur_pos == 4, "Expected cur_pos to be '%d', was '%zu'", 4, in.cur_pos); */
/*    cr_assert(c == 'l', "Incorrect character received from psh_input: 'l' expected, '%c received'", c); */
/*    c = psh_getc(&in); */
/*    cr_assert(in.cur_pos == 5, "Expected cur_pos to be '%d', was '%zu'", 5, in.cur_pos); */
/*    cr_assert(c == 'o', "Incorrect character received from psh_input: 'o' expected, '%c received'", c); */
/*    c = psh_getc(&in); */
/*    cr_assert(in.cur_pos == 5, "Expected cur_pos to be '%d', was '%zu'", 5, in.cur_pos); */
/*    cr_assert(c == EOF, "Incorrect character received from psh_input: EOF expected, '%c received'", c); */
/* } */

/* Test(output_tests, test_simple_echo) { */
/*    struct shell_env cur_env = {0}; */
/*    init_shell_env(&cur_env, 0, NULL, NULL); */
/*    char* program = "echo test_output"; */
/*    psh_string_append_str(cur_env.psh_input->psh_str, program); */

/*    sym_var_insert(cur_env.var_table, "PATH", "/usr/local/sbin:/usr/local/bin:/usr/bin:/bin", true); */

/*    int fds[2]; */
/*    int pipe_err = pipe(fds); */
/*    assert(pipe_err == 0); */
/*    pid_t pid = fork(); */
/*    if (pid < 0) { */
/*       assert(0); */
/*    } */
/*    else if (pid == 0) { */
/*       //childish things */
/*       close(fds[0]); */
/*       dup2(fds[1], STDOUT_FILENO); */
/*       struct psh_node* node_program = parse_program(&cur_env); */
/*       assert(node_program != NULL); */
/*       struct psh_result* ret_result = eval_program(&cur_env, node_program); */
/*       assert(ret_result != NULL); */
/*       exit(ret_result->exit_status); */
/*    } else { */
/*       //parentish things */
/*       close(fds[1]); */
/*       int status; */
/*       waitpid(pid, &status, 0); */
/*       cr_assert(status == 0, "Actual status was: %d", status); */

/*       char* line = NULL; */
/*       size_t len = 0; */
/*       ssize_t nread; */

/*       FILE* pipe_read = fdopen(fds[0], "r"); */
/*       cr_assert(pipe_read, "Failed to open pipe for reading"); */
/*       nread = getline(&line, &len, pipe_read); */
/*       cr_assert(strcmp(line, "test_output\n") == 0, "Incorrect ouput: '%s'", line); */
/*       free(line); */
/*       fclose(pipe_read); */
/*    } */

/*    free_shell_env(&cur_env, false); */
/* } */

Test(output_tests, test_simple_parsing_for) {
   char* program = "for i in a b c d; do echo $i;done";

   struct dyn_arr_tok tok_arr = {0};
   dyn_arr_tok_init(&tok_arr);
   struct shell_env cur_env = {
      .psh_inputs = dyn_arr_psh_input_create_from(psh_string_create(program)),
      .tok_arr = &tok_arr,
      .jobs = dyn_arr_job_create(),
      .var_table = sym_table_create(4096 / sizeof(struct sym_table)),
   };

   sym_var_insert(cur_env.var_table, "PATH", "/usr/local/sbin:/usr/local/bin:/usr/bin:/bin", true);
   sym_var_insert(cur_env.var_table, "HOME", getenv("HOME"), true);

   cur_env.arg0 = psh_strdup("/bin/piersh");
   cur_env.args = dyn_arr_str_create();

   sym_init_undefined_variables(cur_env.var_table);

   cur_env.func_table = sym_table_create(4096 / sizeof(struct sym_table));
   sym_init_builtin_functions(cur_env.func_table);

   cur_env.alias_table = sym_table_create(4096 / sizeof(struct sym_table));

   int fds[2];
   int pipe_err = pipe(fds);
   assert(pipe_err == 0);
   pid_t pid = fork();
   if (pid < 0) {
      assert(0);
   } else if (pid == 0) {
      //childish things
      close(fds[0]);
      dup2(fds[1], STDOUT_FILENO);
      struct psh_node* node_program = parse_program(&cur_env);
      if (node_program == NULL)
         exit(66);
      struct psh_result* ret_result = eval_program(&cur_env, node_program);
      if (ret_result == NULL) {
         exit(67);
      }
      exit(ret_result->exit_status);
   } else {
      //parentish things
      close(fds[1]);
      int status;
      waitpid(pid, &status, 0);
      cr_assert(status == 0, "Actual status was: %d", status);

      char* line = NULL;
      size_t len = 0;
      ssize_t nread;

      FILE* pipe_read = fdopen(fds[0], "r");
      cr_assert(pipe_read, "Failed to open pipe for reading");
      nread = getdelim(&line, &len, '\0', pipe_read);
      cr_assert(strcmp(line, "a\nb\nc\nd\n") == 0, "Incorrect ouput: '%s'", line);
      free(line);
      fclose(pipe_read);
   }
}

Test(output_tests, test_simple_function) {
   char* program = "print_stuff() { i=$1; while test $i -le $1; do echo stuff; i=2; done; } ; print_stuff 0; print_stuff 1; print_stuff -1";

   struct dyn_arr_tok tok_arr = {0};
   dyn_arr_tok_init(&tok_arr);
   struct shell_env cur_env = {
      .psh_inputs = dyn_arr_psh_input_create_from(psh_string_create(program)),
      .tok_arr = &tok_arr,
      .jobs = dyn_arr_job_create(),
      .var_table = sym_table_create(4096 / sizeof(struct sym_table)),
   };

   sym_var_insert(cur_env.var_table, "PATH", "/usr/local/sbin:/usr/local/bin:/usr/bin:/bin", true);
   sym_var_insert(cur_env.var_table, "HOME", getenv("HOME"), true);

   cur_env.arg0 = psh_strdup("/bin/psh");
   cur_env.args = dyn_arr_str_create();

   sym_init_undefined_variables(cur_env.var_table);

   cur_env.func_table = sym_table_create(4096 / sizeof(struct sym_table));
   sym_init_builtin_functions(cur_env.func_table);

   cur_env.alias_table = sym_table_create(4096 / sizeof(struct sym_table));

   int fds[2];
   int pipe_err = pipe(fds);
   assert(pipe_err == 0);
   pid_t pid = fork();
   if (pid < 0) {
      assert(0);
   }
   else if (pid == 0) {
      //childish things
      close(fds[0]);
      dup2(fds[1], STDOUT_FILENO);
      struct psh_node* node_program = parse_program(&cur_env);
      if (node_program == NULL)
         exit(66);
      struct psh_result* ret_result = eval_program(&cur_env, node_program);
      if (ret_result == NULL) {
         exit(67);
      }
      exit(ret_result->exit_status);
   }
   else {
      //parentish things
      close(fds[1]);
      int status;
      waitpid(pid, &status, 0);
      cr_assert(status == 0, "Actual status was: %d", status);

      char* output = NULL;
      size_t len = 0;
      ssize_t nread;

      FILE* pipe_read = fdopen(fds[0], "r");
      cr_assert(pipe_read, "Failed to open pipe for reading");
      nread = getdelim(&output, &len, '\0', pipe_read);
      cr_assert(output != NULL, "Output is null");
      cr_assert(strcmp(output, "stuff\nstuff\nstuff\n") == 0, "Incorrect ouput: '%s'", output);
      free(output);
      fclose(pipe_read);
   }
}

Test(output_tests, test_simple_command_substituion) {
   char* program = "echo aaaa`echo \\`echo $(echo 'bbbb')\\``$(echo $(echo `echo 'cccc'`))";

   struct dyn_arr_tok tok_arr = {0};
   dyn_arr_tok_init(&tok_arr);
   struct shell_env cur_env = {
      .psh_inputs = dyn_arr_psh_input_create_from(psh_string_create(program)),
      .tok_arr = &tok_arr,
      .jobs = dyn_arr_job_create(),
      .var_table = sym_table_create(4096 / sizeof(struct sym_table)),
   };

   sym_var_insert(cur_env.var_table, "PATH", "/usr/local/sbin:/usr/local/bin:/usr/bin:/bin", true);
   sym_var_insert(cur_env.var_table, "HOME", getenv("HOME"), true);

   cur_env.arg0 = psh_strdup("/bin/piersh");
   cur_env.args = dyn_arr_str_create();

   sym_init_undefined_variables(cur_env.var_table);

   cur_env.func_table = sym_table_create(4096 / sizeof(struct sym_table));
   sym_init_builtin_functions(cur_env.func_table);

   cur_env.alias_table = sym_table_create(4096 / sizeof(struct sym_table));

   int fds[2];
   int pipe_err = pipe(fds);
   assert(pipe_err == 0);
   pid_t pid = fork();
   if (pid < 0) {
      assert(0);
   }
   else if (pid == 0) {
      //childish things
      close(fds[0]);
      dup2(fds[1], STDOUT_FILENO);
      struct psh_node* node_program = parse_program(&cur_env);
      if (node_program == NULL)
         exit(66);
      struct psh_result* ret_result = eval_program(&cur_env, node_program);
      if (ret_result == NULL) {
         exit(67);
      }
      exit(ret_result->exit_status);
   }
   else {
      //parentish things
      close(fds[1]);
      int status;
      waitpid(pid, &status, 0);
      cr_assert(status == 0, "Actual status was: %d", status);

      char* output = NULL;
      size_t len = 0;
      ssize_t nread;

      FILE* pipe_read = fdopen(fds[0], "r");
      cr_assert(pipe_read, "Failed to open pipe for reading");
      nread = getdelim(&output, &len, '\0', pipe_read);
      cr_assert(output != NULL, "Output is null");
      cr_assert(strcmp(output, "aaaabbbbcccc\n") == 0, "Incorrect ouput: '%s'", output);
      free(output);
      fclose(pipe_read);
   }
}

Test(pattern_match_tests, test_pattern) {
   cr_assert(is_pattern_match("", "") == true, "Pattern matching failure");
   cr_assert(is_pattern_match("", "e") == false, "Pattern matching failure");
   cr_assert(is_pattern_match("*", "") == true, "Pattern matching failure");
   cr_assert(is_pattern_match("*.c", "") == false, "Pattern matching failure");
   cr_assert(is_pattern_match("*.c", "main.c") == true, "Pattern matching failure");
   cr_assert(is_pattern_match("*lex*", "lelexicon") == true, "Pattern matching failure");
   cr_assert(is_pattern_match("*lex", "leX") == false, "Pattern matching failure");
   cr_assert(is_pattern_match("[He][Vl][sc]", "Hls") == true, "Pattern matching failure");
   cr_assert(is_pattern_match("[He][Vl][sc]", "HlC") == false, "Pattern matching failure");
   cr_assert(is_pattern_match("[He]b[Vl]d[sc]", "Hblds") == true, "Pattern matching failure");
   cr_assert(is_pattern_match("[!He]b[Vl]d[sc]", "vblds") == true, "Pattern matching failure");
   cr_assert(is_pattern_match("[He]*[Vl]*[sc]", "elephant") == false, "Pattern matching failure");
   cr_assert(is_pattern_match("[He]*[Vl]*[abcdefght]", "elephant") == true, "Pattern matching failure");
   cr_assert(is_pattern_match("???", "HlC") == true, "Pattern matching failure");
   cr_assert(is_pattern_match("[d]", "d") == true, "Pattern matching failure");
   cr_assert(is_pattern_match("*this* really*should match*", "this really should match") == true, "Pattern matching failure");
   cr_assert(is_pattern_match("*hello*", "hell") == false, "Pattern matching failure");
}

Test(pattern_match_tests, test_remove_min_suffix) {
   struct psh_string* pstr = NULL;
   cr_assert_not_null(pstr = remove_min_suffix("/home/pie/pie/hello", "*/pie"), "Remove min suffix failed");
   cr_assert_str_eq(pstr->str, "/home/pie/pie/hello", "Remove min suffix failed, ret: '%s'", pstr->str);
   psh_string_free(pstr);
   cr_assert_not_null(pstr = remove_min_suffix("/home/pie/pie/hello", "/pie/*"), "Remove min suffix failed");
   cr_assert_str_eq(pstr->str, "/home/pie", "Remove min suffix failed, ret: '%s'", pstr->str);
   psh_string_free(pstr);
}

Test(pattern_match_tests, test_remove_max_suffix) {
   struct psh_string* pstr = NULL;
   cr_assert_not_null(pstr = remove_max_suffix("/home/pie/pie/hello", "*/pie"), "Remove max suffix failed");
   cr_assert_str_eq(pstr->str, "/home/pie/pie/hello", "Remove max suffix failed, ret: '%s'", pstr->str);
   psh_string_free(pstr);
   cr_assert_not_null(pstr = remove_max_suffix("/home/pie/pie/hello", "/pie/*"), "Remove max suffix failed");
   cr_assert_str_eq(pstr->str, "/home", "Remove max suffix failed, ret: '%s'", pstr->str);
   psh_string_free(pstr);
}

Test(pattern_match_tests, test_remove_min_prefix) {
   struct psh_string* pstr = NULL;
   cr_assert_not_null(pstr = remove_min_prefix("/home/pie/pie/hello", "*/pie"), "Remove min prefix failed");
   cr_assert_str_eq(pstr->str, "/pie/hello", "Remove min prefix failed, ret: '%s'", pstr->str);
   psh_string_free(pstr);
   cr_assert_not_null(pstr = remove_min_prefix("/home/pie/pie/hello", "/pie/*"), "Remove min prefix failed");
   cr_assert_str_eq(pstr->str, "/home/pie/pie/hello", "Remove min prefix failed, ret: '%s'", pstr->str);
   psh_string_free(pstr);
}

Test(pattern_match_tests, test_remove_max_prefix) {
   struct psh_string* pstr = NULL;
   cr_assert_not_null(pstr = remove_max_prefix("/home/pie/pie/hello", "*/pie"), "Remove max prefix failed");
   cr_assert_str_eq(pstr->str, "/hello", "Remove max prefix failed, ret: '%s'", pstr->str);
   psh_string_free(pstr);
   cr_assert_not_null(pstr = remove_max_prefix("/home/pie/pie/hello", "/pie/*"), "Remove max prefix failed");
   cr_assert_str_eq(pstr->str, "/home/pie/pie/hello", "Remove max prefix failed, ret: '%s'", pstr->str);
   psh_string_free(pstr);
}
