#!/bin/bash
# shellcheck disable=SC2015,SC2016

#For aliases
shopt -s expand_aliases

warn() {
   echo "$*" 1>&2
}

die() {
   warn "$*"
   exit 1
}

test ! -z "$(echo "$PWD" | grep 'psh')" || die "Run script in the psh project as a subdir"

psh_bin=${PWD%/psh*}/psh/target/debug/psh

test -f "$psh_bin" || die "No debug build found: $psh_bin"

alias die_print_line='die "Error at line $LINENO"'
alias success='{ cur_line=$LINENO; succeed_test; }'
alias fail='{ cur_line=$LINENO; fail_test; }'

tests_succeeded=0
succeed_test() {
   tests_succeeded=$((tests_succeeded + 1))
}

tests_failed=0
fail_test() {
   warn "tests/main.sh|${cur_line}| Failed test of type: '${test_type}'"
   tests_failed=$((tests_failed + 1))
}



test_type='General'
test "$($psh_bin -c 'echo a')" = 'a' && success || fail
test "$($psh_bin -c 'echo a | wc -c')" = '2' && success || fail
test "$($psh_bin -c 'echo 2\>a')" = '2>a' && success || fail
test "$($psh_bin -c "echo 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa' | wc | wc")" = '      1       3      24' && success || fail
test "$($psh_bin -c 'for i in a b c d e; do echo $i; done')" = "$(echo -e "a\nb\nc\nd\ne\n")" && success || fail
test "$($psh_bin -c 'alias a="echo eh"; a a b')" = "eh a b" && success || fail
test "$($psh_bin -c 'alias a="echo eh"; a a b; unalias a; a >/dev/null || echo b')" = "$(echo -e "eh a b\nb")" && success || fail
# inline aliases (in the same compound_command) have weird behaviour (in the spec), not sure why
# test "$($psh_bin -c 'a () { if [ "$1" = "hi" ]; then echo b $*; else echo a $*; fi; }; alias a="a hi"; a a b; unalias a; a)" = "$(echo -e "a\nb")" && success || fail
test "$($psh_bin -c 'i=hello; case $i in *s*) echo nope ;; [Hh]ell) echo nope ;; [Hh][ge]ll*) echo Yup! ;; esac')" = 'Yup!' && success || fail

test_type='Return type'
test "$($psh_bin -c 'true; echo $?')" = '0' && success || fail
test "$($psh_bin -c 'false; echo $?')" != '0' && success || fail

test_type='Eval'
test "$($psh_bin -c "foo=10 x=foo; y='$'\$x; echo \$y; eval y='$'\$x; echo \$y")" = "$(echo -e '$foo\n10')" && success || fail

test_type='Heredoc'
test "$($psh_bin -c 'cat <<"EOF1"; echo this goes after the first heredoc ; cat <<EOF2; cat <<-"EOF3"
hello!
hello too!
EOF1
this is the second heredoc
EOF2
      heredoc_dash
EOF3
echo this goes at the end')" = "$(echo -e 'hello!\nhello too!\nthis goes after the first heredoc\nthis is the second heredoc\nheredoc_dash\nthis goes at the end\n')" && success || fail
test "$($psh_bin -c 'while read -r x; do printf "%s" "$x"; done <<"EOF1"
hello!
hello too!
EOF1')" = 'hello!hello too!' && success || fail

test_type='Arithmetic expansion'
test "$($psh_bin -c 'foo=10 x=10; x=$(( (foo+=40) + 5 )); echo $foo $x')" = "$(echo -e '50\n55\n')" && success || fail

#Parameter substitution tests
test_type='Parameter substitutions'
test "$($psh_bin -c 'a=1
set 2
echo ${a}b-$ab-${1}0-${10}-$10')" = '1b--20--20' && success || fail
test "$($psh_bin -c 'foo=asdf
echo ${foo-bar}xyz}
foo=
echo ${foo-bar}xyz}
unset foo
echo ${foo-bar}xyz}')" = "$(echo -e "asdfxyz}\nxyz}\nbarxyz}")" && success || fail
test "$($psh_bin -c 'echo $(echo hello)')" = 'hello' && success || fail
test "$($psh_bin -c 'echo "$(echo "hello")"')" = 'hello' && success || fail
test "$($psh_bin -c "echo \"$(echo "$(echo 'hello')")\"")" = 'hello' && success || fail
# test "$($psh_bin -c "echo $(echo 'hello')")" = 'hello' && success || fail
test "$($psh_bin -c 'unset x; echo ${x:-$(echo a)}')" = 'a' && success || fail
# test "$($psh_bin -c 'unset x; echo ${x:-'\''$(echo a)'\''}')" = '$(echo a)' && success || fail
test "$($psh_bin -c 'unset x; echo ${x:=abc}; echo $x')" = "$(echo -e 'abc\nabc')" && success || fail
test "$($psh_bin -c 'unset posix
echo ${posix:?}' 2>&1)" = 'psh: posix: parameter null or not set' && success || fail
test "$($psh_bin -c 'set a b c
echo ${3:+posix}')" = 'posix' && success || fail
test "$($psh_bin -c 'HOME=/usr/posix
echo ${#HOME}
')" = '10' && success || fail
test "$($psh_bin -c 'x=file.c
echo ${x%.c}.o')" = 'file.o' && success || fail
test "$($psh_bin -c 'x=/one/two/three
echo ${x##*/}')" = 'three' && success || fail
test "$($psh_bin -c 'x=$HOME/src/cmd; echo ${x#$HOME}')" = '/src/cmd' && success || fail
test "$($psh_bin -c 'x=posix/src/std; echo ${x%%/*}')" = 'posix' && success || fail
test "$($psh_bin -c 'a=; echo ${a:-word}')" = "$(echo -e 'word\n')" && success || fail
test "$($psh_bin -c 'a=; echo ${a-word}')" = "$(echo -e '\n')" && success || fail
test "$($psh_bin -c 'a=; echo ${a=word}; echo $a')" = "" && success || fail
test "$($psh_bin -c 'a=; echo ${a:=word}; echo $a')" = "$(echo -e 'word\nword\n')" && success || fail
test "$($psh_bin -c 'a=; echo ${a+word}')" = "word" && success || fail
test "$($psh_bin -c 'a=; echo ${a:+word}')" = "" && success || fail
test "$($psh_bin -c 'a=abcdefg; echo ${a}')" = "abcdefg" && success || fail
test "$($psh_bin -c 'a=abcdefg; echo $a;')" = "abcdefg" && success || fail
test "$($psh_bin -c 'a=abcdefg; echo ${#a}')" = "7" && success || fail
test "$($psh_bin -c ': ${X=abc}
if     false
then   :
else   echo $X
fi')" = "abc" && success || fail
test "$($psh_bin -c "c=\'; echo a b \"\$c' d\"")" = "a b '' d" && success || fail

test_type='Arguments and functions'
test "$($psh_bin -c 'set a b c d e; shift 2; echo $*')" = "c d e" && success || fail
test "$($psh_bin -c 'func () { shift 2; echo $* ; } ; func a b c d e')" = "c d e" && success || fail

test_type='Readonly'
test "$($psh_bin -c 'readonly a=bbb; echo $a; a=a; echo $a' 2>&1)" = "$(echo -e "bbb\npsh: 'a': cannot change: readonly variable\nbbb")" && success || fail



if [ "$tests_failed" = '0' ] && [ "$tests_succeeded" = '0' ]; then
   echo "No shell tests were run"
elif [ "$tests_failed" = '0' ] && [ "$tests_succeeded" != '0' ]; then
   echo "All ${tests_succeeded} shell tests passed successfully!"
else
   warn "Shell Tests failed: ${tests_failed}/$(( tests_succeeded + tests_failed ))"
fi
