#!/bin/bash
# shellcheck disable=SC2015,SC2016

#For aliases
shopt -s expand_aliases

warn() {
   echo "$*" 1>&2
}

die() {
   warn "$*"
   exit 1
}

expect -v >/dev/null 2>&1 || die "This test suite needs 'expect', please install it"
test ! -z "$(echo "$PWD" | grep 'psh')" || die "Run script in the psh project as a subdir"

psh_bin=${PWD%/psh*}/psh/target/debug/psh

test -f "$psh_bin" || die "No debug build found: $psh_bin"

alias die_print_line='die "Error at line $LINENO"'
alias success='{ cur_line=$LINENO; succeed_test; }'
alias fail='{ cur_line=$LINENO; fail_test; }'

tests_succeeded=0
succeed_test() {
   tests_succeeded=$((tests_succeeded + 1))
}

tests_failed=0
fail_test() {
   warn "tests/main.sh|${cur_line}| Failed test of type: '${test_type}'"
   tests_failed=$((tests_failed + 1))
}








test_type='test_0'
expect -c "
   spawn -noecho ${psh_bin}
   set timeout 2
   send \"exit\r\"
   expect {
      timeout { exit 1 }
      EOF exit
   }
" >/dev/null 2>&1 && success || fail
test_type='test_1'
# expect -c "
#    spawn -noecho ${psh_bin}
#    set timeout 2
#    send \"exit 1\r\"
#    expect {
#       timeout { exit 0 }
#       EOF { exit 1 }
#    }
# " >/dev/null 2>&1
# [ $? -eq 1 ] && success || fail
test_type='test_2'
expect -c "
   spawn -noecho ${psh_bin}
   set timeout 2
   send \"echo hello!\r\"
   expect {
      hello! {}
      default { exit 1 }
   }
   send \"exit\r\"
   expect {
      timeout { exit 1 }
      EOF exit
   }
" >/dev/null 2>&1 && success || fail

test_type='test_3'
expect -c "
   spawn -noecho ${psh_bin}
   set timeout 2
   send \"sleep 1 &\r\"
   send \"jobs\r\"
   expect -re {
      \"\[1\]  + running\" { exit }
      timeout { exit 1 }
      EOF exit
   }
" >/dev/null 2>&1 && success || fail





if [ "$tests_failed" = '0' ] && [ "$tests_succeeded" = '0' ]; then
   echo "No expect tests were run"
elif [ "$tests_failed" = '0' ] && [ "$tests_succeeded" != '0' ]; then
   echo "All ${tests_succeeded} expect tests passed successfully!"
else
   warn "Expect Tests failed: ${tests_failed}/$(( tests_succeeded + tests_failed ))"
fi
