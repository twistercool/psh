# PSH

This is the repository of the Pierre-Shell, a fun side project I'm making.

In order to build the project, run the command `make`.
You can then run the binary with `target/release/psh`.

You can get the debug binary with `make debug` and run it with `target/debug/psh`.

To build and run the tests, run `make test`.
   
It is meant to be a pretty-much fully compatible POSIX-shell, but it is not ready for day-to-day use yet.
