#ifndef __PSH_EVAL_H__
#define __PSH_EVAL_H__

#define __USE_XOPEN
#include <sys/stat.h>
#include <sys/wait.h>

#include <unistd.h>
#include <stdbool.h>
#include <dirent.h>
#include <pthread.h>
#include <fcntl.h>


#include "struct.h"
#include "parser.h"
#include "sym_table.h"
#include "redir.h"
#include "wordexp.h"

   

struct psh_result* eval_program(struct shell_env* cur_env, const struct psh_node* node_program);
struct psh_result* eval_complete_commands(struct shell_env* cur_env, const struct psh_node* cur_node_complete_commands);
struct psh_result* eval_complete_command(struct shell_env* cur_env, const struct psh_node* node_complete_command);
struct psh_result* eval_list(struct shell_env* cur_env, const struct psh_node* cur_node_list);
struct psh_result* eval_and_or(struct shell_env* cur_env, const struct psh_node* cur_node_and_or);
struct psh_result* eval_pipeline(struct shell_env* cur_env, const struct psh_node* node_pipeline);
struct psh_result* eval_pipe_sequence(struct shell_env* cur_env, const struct psh_node* node_pipe_sequence);
struct psh_result* eval_command(struct shell_env* cur_env, const struct psh_node* node_command, int fd_in, int fd_out);
struct psh_result* eval_compound_command(struct shell_env* cur_env, const struct psh_node* node_compound_command, int fd_in, int fd_out);
struct psh_result* eval_case_clause(struct shell_env* cur_env, const struct psh_node* node_case_clause);
struct psh_result* eval_function_definition(struct shell_env* cur_env, const struct psh_node* node_function_definition);
struct psh_result* eval_for_clause(struct shell_env* cur_env, const struct psh_node* node_for_clause);
struct psh_result* eval_subshell(struct shell_env* cur_env, const struct psh_node* node_subshell);
struct psh_result* eval_compound_list(struct shell_env* cur_env, const struct psh_node* node_compound_list);
struct psh_result* eval_term(struct shell_env* cur_env, const struct psh_node* node_term);
struct psh_result* eval_compound_list(struct shell_env* cur_env, const struct psh_node* node_compound_list);


struct psh_result* eval_if_clause(struct shell_env* cur_env, const struct psh_node* node_if_clause);
struct psh_result* eval_loop(struct shell_env* cur_env, const struct psh_node* node_loop);

struct psh_result* eval_brace_group(struct shell_env* cur_env, const struct psh_node* node_brace_group);
struct psh_result* eval_do_group(struct shell_env* cur_env, const struct psh_node* node_do_group);
struct psh_result* eval_simple_command(struct shell_env* cur_env, const struct psh_node* node_simple_command, int fd_in, int fd_out);


_Nullable struct psh_string* find_in_PATH(const struct shell_env* cur_env, _Nonnull const char* bin, _Nullable const char* PATH_override);
struct psh_result* exec_dot_script(struct shell_env* cur_env, const char* path);

int execute_command_in_context(struct shell_env* cur_env, const struct psh_string* exec_path, /*shallow copy*/ struct dyn_arr_str* args, struct dyn_arr_node *nodes_redirections, struct psh_result* ret_result/*output*/, int fd_in, int fd_out);
pid_t fork_shell();



#endif //__PSH_EVAL_H__
