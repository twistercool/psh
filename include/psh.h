#ifndef __PSH_H__
#define __PSH_H__

//For fdopen
#define _POSIX_C_SOURCE 200809L

#include <stdbool.h>
#include <stdint.h>

#include <termios.h>
#include <unistd.h>

#include "enum.h"
#include "struct.h"
#include "parser.h"
#include "eval.h"
#include "sym_table.h"
#include "signals.h"



void init_shell_env(struct shell_env* in_env, int argc, char** argv, char** env);
void free_shell_env(struct shell_env* in_env, bool free_input);

struct psh_string* _Nullable read_file_to_psh_string(const char* path);
struct psh_input* _Nullable read_file_to_psh_input(const char* path);

enum psh_error source_profile_files(struct shell_env* cur_env);

enum psh_error psh_start_interactive_shell(struct shell_env* cur_env);
enum psh_error psh_secondary_prompt(struct shell_env* cur_env);
/* struct psh_string* psh_read_stdin(struct shell_env* cur_env, char* prompt); */

enum gl_ret { PSH_GL_SUCCESS = 0, PSH_GL_EOF, PSH_GL_ERROR /*sets errno*/ }
psh_getline(FILE* _Nonnull file, struct psh_string* _Nonnull out);
void read_histfile(struct shell_env* _Nonnull cur_env);

void sleep_ms(const uint64_t ms);

enum psh_error sym_init_undefined_variables(struct sym_table* _Nonnull sym_table);
void sym_init_builtin_functions(struct sym_table* _Nonnull func_table);

struct psh_input* _Nonnull
cur_input(struct shell_env* cur_env);

#endif
