#ifndef __PSH_STRUCT_H__
#define __PSH_STRUCT_H__

#include <stdio.h>
/* #include <signal.h> */
#include <stdbool.h>
#include <assert.h>
#include <stdint.h>
//for strcasestr(const char*);
#define _GNU_SOURCE
#include <string.h>
#undef _GNU_SOURCE
#include <stdlib.h>
/* #include <setjmp.h> */

#include "enum.h"

#define CURSOR_LEFT ESC_S"[D"
#define CURSOR_RIGHT ESC_S"[C"

#ifndef _Nullable
#define _Nullable
#endif
#ifndef _Nonnull
#define _Nonnull
#endif

// removes assertion if not in debug build
#ifdef PSH_DEBUG
#define DEBUG_ASSERT(EXP) assert(EXP)
#define DEBUG_PRINTF(EXP) printf(EXP)
#else
#define DEBUG_ASSERT(EXP) do {} while (0)
#define DEBUG_PRINTF(EXP) do {} while (0)
#endif //PSH_DEBUG

#define ARR_SIZE(arr) (sizeof(arr) / sizeof(*arr))

//TODO very unsure about this one
#define NB_SIGNALS 32

#define TODO() do { fprintf(stderr, "Not yet implemented\n"); assert(0); } while(0)

struct shell_env {
   struct sym_table* var_table; //holds all variables
   struct sym_table* func_table; //holds functions and builtins
   struct sym_table* alias_table; //holds aliases

   struct dyn_arr_psh_input* psh_inputs;

   struct dyn_arr_tok* tok_arr; //holds all tokens parsed

//TODO: look at what to do with that
   /* struct tmp_allocator* tmp_alloc; */

   char* arg0; //$0
   struct dyn_arr_str* args; //$*
   uint16_t last_pipeline_status; //$?
   pid_t pid_last_backgroud_process; //$! //TODO actually implement job control
   pid_t pid_invoked_shell; //$$


//Error handling
//TODO: see what to do with that
   /* jmp_buf error_jmp; */


   pid_t pid_current_process;

//Flags (mostly from "set" or args on command line)
   bool is_interactive; //TODO maybe change to a flag?
   bool is_login; //TODO need to implement this
//TODO aren't actually implemented yet
   bool flag_allexport; //-a
   bool flag_errexit; //-e
   bool flag_ignoreeof;
   bool flag_monitor; //-m
   bool flag_noclobber; //-C
   bool flag_noglob; //-f
   bool flag_noexec; //-n
   bool flag_nolog;
   bool flag_notify; //-b
   bool flag_nounset; //-u
   bool flag_verbose; //-v
   bool flag_vi;
   bool flag_xtrace; //-x


//Job control/signal handling
   //NOTE: these will be accessed from signal handlers, always block signals if using them
   struct dyn_arr_job* jobs;
   char *traps[NB_SIGNALS]; //TODO implement traps
   uint8_t awaiting_traps[32];
   size_t nb_awaiting_traps;
   struct sigaction* sa;
   bool _is_inside_job; //911?
   bool _cleanup_done_jobs;

   bool _blocking_signals; //if set don't, don't UNBLOCK sigprocmask (unless you set it as well)

//State of env
   bool _is_parsing_cmd_name; //aliases are only expanded when this flag is set
   //TODO alias and unalias need to be run at parse time, but the messages are needed at runtime
   /* struct dyn_arr_str* _alias_unalias_messages; */
   /* struct dyn_arr_uint8* _alias_unalias_exit_status; */
};

struct psh_input* _Nonnull cur_input(struct shell_env* cur_env);

struct psh_string {
   size_t arr_size; /*size malloced*/
   size_t str_len; /*strlen of the actual string*/
   char* str;
};

struct psh_string* _Nonnull psh_string_create(const char* input);
struct psh_string* _Nonnull psh_string_clone(const struct psh_string* in_psh_str _Nonnull);
void psh_string_free(struct psh_string* input);
void psh_string_append_char(struct psh_string* in_psh_str, char input);
void psh_string_append_str(struct psh_string* in_psh_str, const char* input);
void psh_string_append_psh_str(struct psh_string* in_psh_str, const struct psh_string* const in_str);
void psh_string_insert_char(struct psh_string* in_psh_str, char c, uint64_t index);
void psh_string_insert_str(struct psh_string* in_psh_str, const char* str, uint64_t index);
void psh_string_delete_char(struct psh_string* in_psh_str, uint64_t index);
void psh_string_delete_chars(struct psh_string* in_psh_str, uint64_t index, size_t len);
void psh_string_reset(struct psh_string* _Nonnull in_psh_str);

char* psh_string_to_cstr(struct psh_string* in_psh_str, bool will_free);
struct psh_string* _Nullable psh_string_from_nb(uint64_t nb);

//Little fancy generic append
#define pstr_append(pstr, msg) _Generic(msg, \
   int: psh_string_append_char, \
   char: psh_string_append_char, \
   char*: psh_string_append_str, \
   struct psh_string*: psh_string_append_psh_str\
)(pstr, msg)



struct token {
   struct psh_string* psh_str;
   enum token_type type;
   uint64_t index_start;
   bool is_heredoc;
};


///Signals that special handling needs to occur within eval functions
enum special_code {
   CODE_NORMAL = 0,
   CODE_BREAK,
   CODE_CONTINUE,
   CODE_RETURN,
};

///Result of an eval function
struct psh_result {
   char* substitution_output;
   /* pid_t pid; */
   int64_t special_val; // special_val carries some information for special_code
   enum special_code special_code; // special_code is used for implementing some built-ins
   enum psh_error error; // did an error happen?
   uint16_t exit_status; // exit status of the commands
};


struct psh_input {
   struct psh_string *psh_str;
   size_t cur_pos;
   bool is_last_EOF;
};

struct psh_input* psh_input_create(void);
struct psh_input* psh_input_create_from(struct psh_string* in);
void psh_input_reset(struct psh_input* in);
void psh_input_free(struct psh_input* in, bool free_pstr);
int psh_peekc(struct shell_env*);
int psh_getc(struct shell_env*);
void psh_ungetc(struct shell_env*);
void psh_skip_whitespace(struct shell_env*);
void psh_reverse_skip_whitespace(struct psh_input* input);

char* _Nonnull psh_strdup(const char* in);

void* _Nonnull psh_malloc(size_t);
void* _Nonnull psh_calloc(size_t);
void* _Nonnull psh_realloc(void*, size_t);


#endif
