#ifndef __PSH_DYN_ARR_H__
#define __PSH_DYN_ARR_H__

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

#include "enum.h"
#include "struct.h"
#include "job_ctl.h"

struct heredoc {
   size_t start_index;
   size_t end_index;
};

struct dyn_arr_heredoc {
   struct heredoc* heredocs;
   size_t size;
   size_t capacity;
};
struct heredoc heredoc_from_begin_pos(struct dyn_arr_tok* in, size_t cur_pos);
struct heredoc heredoc_from_end_pos(struct dyn_arr_tok* in, size_t cur_pos);

void dyn_arr_heredoc_init(struct dyn_arr_heredoc *arr);
struct dyn_arr_heredoc* dyn_arr_heredoc_create(void);
void dyn_arr_heredoc_free(struct dyn_arr_heredoc *arr);
void dyn_arr_heredoc_push(struct dyn_arr_heredoc *arr, struct heredoc in);
struct heredoc dyn_arr_heredoc_pop(struct dyn_arr_heredoc *arr);

//

struct dyn_arr_tok {
   //TODO probably have to change it into a struct token **token
   struct token *tokens;
   struct dyn_arr_heredoc *heredocs;
   size_t size;
   size_t capacity;
   size_t pushback; //used to implement unget token
   size_t nb_EOF_toks;
};

void dyn_arr_tok_init(struct dyn_arr_tok *arr);
struct dyn_arr_tok* dyn_arr_tok_create(void);
void dyn_arr_tok_free(struct dyn_arr_tok *arr);
void dyn_arr_tok_push(struct dyn_arr_tok *arr, struct token in);
struct token* dyn_arr_tok_pop(struct dyn_arr_tok *arr);
struct token* dyn_arr_tok_point_last(struct dyn_arr_tok *arr);
struct token* dyn_arr_tok_point_last_no_pushback(struct dyn_arr_tok *arr);
void dyn_arr_tok_delete_last_no_pushback(struct dyn_arr_tok *arr);

enum psh_error debug_dyn_arr_tok_print(struct dyn_arr_tok *arr);

//

struct dyn_arr_str {
   char** strs;
   size_t size;
   size_t capacity;
};

void dyn_arr_str_init(struct dyn_arr_str *arr);
struct dyn_arr_str* dyn_arr_str_create(void);
void dyn_arr_str_free(struct dyn_arr_str *arr, bool free_all);
enum psh_error dyn_arr_str_push(struct dyn_arr_str *arr, char* in, bool copy_str);
void dyn_arr_str_insert_arr_at(struct dyn_arr_str *arr, const struct dyn_arr_str *cpy, size_t index);
void dyn_arr_str_delete(struct dyn_arr_str *arr, size_t index, size_t nb, bool free);
char* dyn_arr_str_pop(struct dyn_arr_str *arr);
char** dyn_arr_to_cstr_arr(struct dyn_arr_str* arr);

//

struct dyn_arr_ptr {
   void** ptrs;
   size_t size;
   size_t capacity;
};

void dyn_arr_ptr_init(struct dyn_arr_ptr *arr);
struct dyn_arr_ptr* dyn_arr_ptr_create(void);
void dyn_arr_ptr_free(struct dyn_arr_ptr *arr, bool free_self);
void dyn_arr_ptr_push(struct dyn_arr_ptr *arr, void* in);
void dyn_arr_ptr_free_elems(struct dyn_arr_ptr *arr);

//

struct psh_node;
struct dyn_arr_node {
   struct psh_node** nodes;
   size_t size;
   size_t capacity;
};

void dyn_arr_node_init(struct dyn_arr_node *arr);
struct dyn_arr_node* dyn_arr_node_create(void);
void dyn_arr_node_free(struct dyn_arr_node *arr, bool free_each_node);
void dyn_arr_node_push(struct dyn_arr_node *arr, struct psh_node* in);
struct psh_node* dyn_arr_node_pop(struct dyn_arr_node *arr);

//

struct dyn_arr_uint8 {
   uint8_t* uint8s;
   size_t size;
   size_t capacity;
};

void dyn_arr_uint8_init(struct dyn_arr_uint8 *arr);
struct dyn_arr_uint8* dyn_arr_uint8_create(void);
void dyn_arr_uint8_free(struct dyn_arr_uint8 *arr);
void dyn_arr_uint8_push(struct dyn_arr_uint8 *arr, uint8_t in);
enum psh_error dyn_arr_uint8_pop(struct dyn_arr_uint8 *arr, uint8_t* in);

//

struct dyn_arr_psh_input {
   struct psh_input** ins;
   size_t size;
   size_t capacity;
   uint16_t cur_input;
};

void dyn_arr_psh_input_init(struct dyn_arr_psh_input _Nonnull *arr);
struct dyn_arr_psh_input* _Nonnull dyn_arr_psh_input_create(void);
struct dyn_arr_psh_input* _Nonnull dyn_arr_psh_input_create_from(struct psh_string* _Nonnull in);
struct dyn_arr_psh_input* _Nonnull dyn_arr_psh_input_create_with(struct psh_input* _Nonnull in);
void dyn_arr_psh_input_free(struct dyn_arr_psh_input _Nullable *arr, bool free_psh_input);
void dyn_arr_psh_input_push(struct dyn_arr_psh_input _Nonnull *arr, struct psh_input _Nonnull *in);
/* enum psh_error dyn_arr_psh_input_pop(struct dyn_arr_psh_input *arr, struct psh_input* in); */
void dyn_arr_psh_input_delete_index(struct dyn_arr_psh_input *arr, size_t index, bool free);
struct psh_input* _Nullable dyn_arr_psh_input_pop(struct dyn_arr_psh_input *arr);

//

//Defined in "job.h"
struct dyn_arr_job;

void dyn_arr_job_init(struct dyn_arr_job *arr);
struct dyn_arr_job* dyn_arr_job_create(void);
void dyn_arr_job_free(struct dyn_arr_job *arr, bool free_jobs);
void dyn_arr_job_push(struct dyn_arr_job *arr, struct job* in);
void dyn_arr_job_delete_index(struct dyn_arr_job *arr, size_t index, bool free);
void dyn_arr_job_delete(struct dyn_arr_job *arr, struct job* job, bool free);
/* struct job* dyn_arr_job_pop(struct dyn_arr_job *arr); */
struct job* dyn_arr_job_get_nb(const struct dyn_arr_job *arr, size_t job_nb);
struct job* dyn_arr_job_get_pid(const struct dyn_arr_job *arr, pid_t pid);
struct job* dyn_arr_job_get_default(const struct dyn_arr_job *arr);
struct job* dyn_arr_job_get_foreground(const struct dyn_arr_job *arr);
struct job* dyn_arr_job_get_next(const struct dyn_arr_job *arr);
void dyn_arr_job_find_new_next(struct dyn_arr_job *jobs);
struct job* dyn_arr_job_get(const struct dyn_arr_job *jobs, const char* id);
void dyn_arr_job_clean_done(struct dyn_arr_job* arr);
void dyn_arr_job_clean_waited_for(struct dyn_arr_job* arr);

#endif
