#ifndef __PSH_SIGNALS_H__
#define __PSH_SIGNALS_H__

#include <stdbool.h>

#include "struct.h"
#include "dyn_arr.h"

static const char* const sig_str[] = {
   "EXIT", "HUP", "INT", "QUIT",
   "ILL", "TRAP", "ABRT", "BUS",
   "FPE", "KILL", "USR1", "SEGV",
   "USR2", "PIPE", "ALRM", "TERM",
   "STKFLT", "CHLD", "CONT", "STOP",
   "TSTP", "TTIN", "TTOU", "URG",
   "XCPU", "XFSZ", "VTALRM", "PROF",
   "WINCH", "IO", "PWR", "SYS",
}; //maybe add the RT[MIN][MAX]-x signals?
static uint8_t sig_str_len = sizeof(sig_str) / sizeof(*sig_str);

enum psh_error setup_sighandlers(struct shell_env* cur_env);
void destroy_sighandlers(struct shell_env* cur_env);

#endif // __PSH_SIGNALS_H__
