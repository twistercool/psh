#ifndef __PSH_PARSER_H__
#define __PSH_PARSER_H__

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

#include "enum.h"
#include "struct.h"
#include "dyn_arr.h"
#include "shell_error.h"
#include "sym_table.h"
#include "wordexp.h"

bool is_in_IFS(char c, const char* IFS);

#define ESC_S "\033" // equivalent to "\e"
#define ESC_C '\033' // equivalent to '\e' */

struct dyn_arr_tok;
struct dyn_arr_str;
struct dyn_arr_node;

enum node_type {
   PSH_NODE_UNKNOWN = 0U,
   PSH_NODE_PROGRAM,
   PSH_NODE_COMPLETE_COMMANDS,
   PSH_NODE_COMPLETE_COMMAND,
   PSH_NODE_LIST,
   PSH_NODE_AND_OR,
   PSH_NODE_PIPELINE,
   PSH_NODE_COMMAND,
   PSH_NODE_COMPOUND_COMMAND,
   PSH_NODE_SUBSHELL,
   PSH_NODE_COMPOUND_LIST,
   PSH_NODE_TERM,
   PSH_NODE_FOR_CLAUSE,
   PSH_NODE_CASE_CLAUSE,
   /* PSH_NODE_CASE_LIST_NS, */
   /* PSH_NODE_CASE_LIST, */
   /* PSH_NODE_CASE_ITEM_NS, */
   PSH_NODE_CASE_ITEM,
   PSH_NODE_PATTERN,
   PSH_NODE_IF_CLAUSE,
   PSH_NODE_ELSE_PART,
   PSH_NODE_LOOP,
   PSH_NODE_FUNCTION,
   PSH_NODE_BRACE_GROUP,
   PSH_NODE_DO_GROUP,
   PSH_NODE_SIMPLE_COMMAND,
   PSH_NODE_CMD_NAME,
   PSH_NODE_CMD_WORD,
   PSH_NODE_CMD_PREFIX,
   PSH_NODE_CMD_SUFFIX,
   PSH_NODE_IO_REDIRECT,
   PSH_NODE_IO_FILE,
   PSH_NODE_IO_HERE,
   PSH_NODE_SEPARATOR_OP,
   PSH_NODE_SEPARATOR,
   PSH_NODE_SEQUENTIAL_SEP,
};

struct psh_node_program {
   struct psh_node* node_complete_commands; //can be Null
};

struct psh_node_complete_commands {
   struct dyn_arr_node* nodes_complete_command;
};

struct psh_node_complete_command {
   struct psh_node* node_list;
   struct psh_node* node_separator_op; //can be NULL
};

struct psh_node_list {
   struct dyn_arr_node* nodes_and_or;
   struct dyn_arr_node* nodes_separator_op; //can be NULL
};

struct psh_node_and_or {
   struct dyn_arr_node* nodes_pipeline; //can't be NULL
   struct dyn_arr_uint8* dyn_is_AND_IF; //can be NULL
};

struct psh_node_pipeline {
   struct dyn_arr_node* nodes_command;
   bool is_banged;
};

struct psh_node_command {
   union {
      struct psh_node* node_simple_command;
      struct {
         struct psh_node* node_compound_command;
         struct dyn_arr_node* nodes_io_redirect; //can be NULL
      };
      struct psh_node* node_function;
   };
   enum node_type node_type; //can be PSH_NODE_SIMPLE_COMMAND, PSH_NODE_COMPOUND_COMMAND or PSH_NODE_FUNCTION
};

struct psh_node_compound_command {
   union {
      struct psh_node* node_brace_group;
      struct psh_node* node_subshell;
      struct psh_node* node_for_clause;
      struct psh_node* node_case_clause;
      struct psh_node* node_if_clause;
      struct psh_node* node_loop;
   };
   enum node_type node_type; //can be PSH_NODE_BRACE_GROUP, PSH_NODE_SUBSHELL, etc...
};

struct psh_node_subshell {
   struct psh_node* node_compound_list;
};

struct psh_node_compound_list {
   struct psh_node* node_term;
   struct psh_node* node_separator; //can be NULL
};

struct psh_node_term {
   struct dyn_arr_node* nodes_and_or;
   struct dyn_arr_node* nodes_separator; //can be NULL
};

struct psh_node_for_clause {
   char* name;
   struct dyn_arr_str* dyn_wordlist;
   struct psh_node* node_do_group;
};

struct psh_node_case_clause {
   char* word_str;
   struct dyn_arr_node* nodes_case_item; //can be NULL
};

struct psh_node_case_item {
   struct dyn_arr_str* dyn_patterns;
   struct psh_node* node_compound_list; //can be NULL
   bool is_ns;
};

struct psh_node_if_clause {
   struct psh_node* node_compound_list_if;
   struct psh_node* node_compound_list_then;
   struct psh_node* node_else_part; //can be NULL
};

struct psh_node_else_part {
   struct dyn_arr_node* nodes_compound_list_elif;
   struct dyn_arr_node* nodes_compound_list_body;
};

struct psh_node_loop {
   struct psh_node* node_compound_list;
   struct psh_node* node_do_group;
   bool is_while_clause; //either until_clause or while_clause
};

struct psh_node_function {
   char* function_name;
   struct psh_node* node_compound_command;
   struct dyn_arr_node* nodes_io_redirect;
};

struct psh_node_brace_group {
   struct psh_node* node_compound_list;
};

struct psh_node_do_group {
   struct psh_node* node_compound_list;
};

struct psh_node_simple_command {
   struct psh_node* node_cmd_prefix;
   union {
      struct psh_node* node_cmd_word;
      struct psh_node* node_cmd_name;
   };
   struct psh_node* node_cmd_suffix;

   bool is_cmd_word;
};

struct psh_node_cmd_name {
   union {
      char* command_str;
      char* assignment_str;
   };
   bool is_assignment;
};

struct psh_node_cmd_word {
   char* word_str;
};

struct psh_node_cmd_prefix {
   struct dyn_arr_str* assignment_words;
   struct dyn_arr_node* nodes_io_redirect;
};

struct psh_node_cmd_suffix {
   struct dyn_arr_str* words;
   struct dyn_arr_node* nodes_io_redirect;
};

struct psh_node_io_redirect {
   union {
      struct psh_node* node_io_file;
      struct psh_node* node_io_here;
   };
   int64_t io_number; //-1 indicates no io_number
   bool is_heredoc; //either io_file or io_here
};

struct psh_node_io_file {
   char* filename;
   enum token_type redirect_op;
};

struct psh_node_io_here {
   char* here_end; //string that signifies the end of the heredoc
   char* heredoc; //actual heredoc
   bool is_dlessdash;
};

/* struct psh_node_here_end { */
/* }; */

struct psh_node_separator_op {
   enum token_type type; //can be '&' or ';'
};

struct psh_node_separator {
   enum token_type type; //can be '&' or ';' or '\n'
};

struct psh_node_sequential_sep {
   uint64_t nb_newlines; //if above 0 is newline_list
};

//struct psh_error* can be a NODE_ERROR, in which case, we free stuff and bubble it up
struct psh_node {
   union {
      struct psh_node_program node_program;
      struct psh_node_complete_commands node_complete_commands;
      struct psh_node_complete_command node_complete_command;
      struct psh_node_list node_list;
      struct psh_node_and_or node_and_or;
      struct psh_node_pipeline node_pipeline;
      struct psh_node_command node_command;
      struct psh_node_compound_command node_compound_command;
      struct psh_node_subshell node_subshell;
      struct psh_node_compound_list node_compound_list;
      struct psh_node_term node_term;
      struct psh_node_for_clause node_for_clause;
      struct psh_node_case_clause node_case_clause;
      struct psh_node_case_item node_case_item;
      struct psh_node_if_clause node_if_clause;
      struct psh_node_else_part node_else_part;
      struct psh_node_loop node_loop;
      struct psh_node_function node_function;

      struct psh_node_brace_group node_brace_group;
      struct psh_node_do_group node_do_group;
      struct psh_node_simple_command node_simple_command;
      struct psh_node_cmd_name node_cmd_name;
      struct psh_node_cmd_word node_cmd_word;
      struct psh_node_cmd_prefix node_cmd_prefix;
      struct psh_node_cmd_suffix node_cmd_suffix;
      struct psh_node_io_redirect node_io_redirect;
      struct psh_node_io_file node_io_file;
      struct psh_node_io_here node_io_here;
      struct psh_node_separator_op node_separator_op;
      struct psh_node_separator node_separator;
      struct psh_node_sequential_sep node_sequential_sep;
   };
   enum node_type node_type;
   size_t begin_index; //used for ungetting nodes
};

struct psh_node_or_err { struct psh_node node; };


#define NODE_ERROR ((void*)0x1)
#define IS_PARSE_NOTHING(node) (node == NULL)
#define IS_PARSE_ERR(node) (node == NODE_ERROR)
#define IS_PARSE_NOTHING_OR_ERR(node) (node == NULL || node == NODE_ERROR)


// Parsing functions
struct psh_node* parse_program(struct shell_env* cur_env);
struct psh_node* parse_complete_commands(struct shell_env* cur_env);
struct psh_node* parse_complete_command(struct shell_env* cur_env);
struct psh_node* parse_list(struct shell_env* cur_env);
struct psh_node* parse_and_or(struct shell_env* cur_env);
struct psh_node* parse_pipeline(struct shell_env* cur_env);
struct psh_node* parse_command(struct shell_env* cur_env);
struct psh_node* parse_compound_command(struct shell_env* cur_env);
struct psh_node* parse_subshell(struct shell_env* cur_env);
struct psh_node* parse_compound_list(struct shell_env* cur_env);
struct psh_node* parse_term(struct shell_env* cur_env);
struct psh_node* parse_for_clause(struct shell_env* cur_env);
char* parse_name(struct shell_env* cur_env);
char* parse_wordlist_word(struct shell_env* cur_env);
struct dyn_arr_str* parse_wordlist(struct shell_env* cur_env);
struct psh_node* parse_case_clause(struct shell_env* cur_env);
/* struct dyn_arr_node* parse_case_list_ns(struct shell_env* cur_env); */
struct dyn_arr_node* parse_case_list(struct shell_env* cur_env);
/* struct psh_node* parse_case_item_ns(struct shell_env* cur_env); */
struct psh_node* parse_case_item(struct shell_env* cur_env);
struct dyn_arr_str* parse_pattern(struct shell_env* cur_env);
struct psh_node* parse_if_clause(struct shell_env* cur_env);
struct psh_node* parse_else_part(struct shell_env* cur_env);
struct psh_node* parse_loop(struct shell_env* cur_env);
struct psh_node* parse_function(struct shell_env* cur_env);
char* parse_fname(struct shell_env* cur_env);
struct psh_node* parse_brace_group(struct shell_env* cur_env);
struct psh_node* parse_do_group(struct shell_env* cur_env);
struct psh_node* parse_simple_command(struct shell_env* cur_env);
struct psh_node* parse_cmd_name(struct shell_env* cur_env);
struct psh_node* parse_cmd_word(struct shell_env* cur_env);
struct psh_node* parse_cmd_prefix(struct shell_env* cur_env);
struct psh_node* parse_cmd_suffix(struct shell_env* cur_env);
struct dyn_arr_node* parse_redirect_list(struct shell_env* cur_env);
struct psh_node* parse_io_redirect(struct shell_env* cur_env);
struct psh_node* parse_io_file(struct shell_env* cur_env);
char* parse_filename(struct shell_env* cur_env);
struct psh_node* parse_io_here(struct shell_env* cur_env);
char* parse_here_end(struct shell_env* cur_env);
uint64_t parse_newline_list(struct shell_env* cur_env);
uint64_t parse_linebreak(struct shell_env* cur_env);
struct psh_node* parse_separator_op(struct shell_env* cur_env);
struct psh_node* parse_separator(struct shell_env* cur_env);
struct psh_node* parse_sequential_sep(struct shell_env* cur_env);

struct psh_node* psh_node_copy(const struct psh_node* in_node);
void psh_node_free(struct psh_node* in_node, bool free_itself);

/* INTERNAL API*/
char* parse_word_7a(struct shell_env* cur_env);
char* parse_word_7b(struct shell_env* cur_env);

void debug_node_print_recursive(struct psh_node* input_node, int depth);

#endif // __PSH_PARSER_H__
