#ifndef __PSH_TOKEN_H__
#define __PSH_TOKEN_H__

#include <stdbool.h>

#include "struct.h"
#include "dyn_arr.h"

struct dyn_arr_tok;
enum token_type psh_get_token(struct shell_env* cur_env);
void psh_unget_token(struct shell_env* cur_env);
enum token_type psh_peek_token(struct shell_env* cur_env);

struct token* psh_tokenise_heredoc(struct shell_env* cur_env, const char* here_string, bool is_dlessdash);

enum psh_error psh_tokenise_expansion(struct token* cur_token, struct shell_env* cur_env, char first_c);
enum psh_error psh_tokenise_single_quote(struct token* cur_token, struct shell_env* cur_env);
enum psh_error psh_tokenise_double_quote(struct token* cur_token, struct shell_env* cur_env);

bool can_make_new_operator(const struct token* cur_token, const char in);
bool is_operator(const char *in);
enum token_type get_operator_token_type(const char *in);

enum token_type get_token_type(const struct token* in_token);
enum psh_error token_preparse(struct token* in_token, bool is_following_char_less_or_great);
void delete_tokens_until_token(struct shell_env* cur_env, struct token* in_token);
bool token_alias_expand(struct shell_env* cur_env, struct token* in_token);

#endif
