#ifndef __PSH_TMP_ALLOC_H__
#define __PSH_TMP_ALLOC_H__

#include <unistd.h>

#include "dyn_arr.h"

struct tmp_allocator {
   struct dyn_arr_ptr* dyn_mem;
};

struct tmp_allocator* tmp_alloc_create(void);
void tmp_alloc_free(struct tmp_allocator*);

void* tmp_alloc(struct tmp_allocator*, size_t); //zeroed out
void* tmp_realloc(struct tmp_allocator*, void*, size_t);
void tmp_reset(struct tmp_allocator*);


/* struct allocator { */
/*    void* (*alloc)(size_t); //zeroed memory */
/*    void* (*realloc)(void*, size_t); */
/*    union { */
/*       void (*free)(void*); */
/*       struct { */
/*          void (*free_all)(void*); */
/*          struct tmp_allocator* dyn_mem; */
/*       }; */
/*    }; */
/*    bool is_arena_alloc; */
/* }; */

/* static struct allocator global_allocator = (struct allocator) { */
/*    .alloc = psh_calloc, */
/*    .realloc = psh_realloc, */
/*    .free = free, */
/*    .is_arena_alloc = false */
/* }; */

/* static struct allocator tmp_allocator = (struct allocator) { */
/*    .alloc = tmp_alloc, */
/*    .realloc = psh_realloc, */
/*    .free_all = NULL */
/* }; */


#endif // __PSH_TMP_ALLOC_H__
