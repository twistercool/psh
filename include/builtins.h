#ifndef __PSH_BUILTINS_H__
#define __PSH_BUILTINS_H__

//For times() function for times builtin
#include <sys/times.h>
#include <sys/resource.h>
#include <sys/stat.h>

#include "eval.h"
#include "signals.h"

void sym_init_builtin_functions(_Nonnull struct sym_table* func_table);

bool uppercase_strequal(const char* restrict str0, const char* restrict str1);

#endif // __PSH_BUILTINS_H__
