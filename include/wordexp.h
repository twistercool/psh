#ifndef	__PSH_WORDEXP_H__
#define	__PSH_WORDEXP_H__

#include <sys/stat.h>

#include <features.h>
#define __need_size_t
#include <stddef.h>
#include <stdbool.h>
#include <pwd.h>
#include <dirent.h>

/* #include "struct.h" */
#include "dyn_arr.h"
#include "sym_table.h"
#include "pattern_match.h"
#include "shell_error.h"


enum word_expansion {
   EXP_TILDE = 1 << 0,
   EXP_PARAM = 1 << 1,
   EXP_CMD_SUB = 1 << 2,
   EXP_ARITH = 1 << 3,

   EXP_FIELD_SPLIT = 1 << 4,
   EXP_PATH = 1 << 5,
   EXP_QUOTE = 1 << 6,
};

union wordexp_ret {
   char* charp;
   struct dyn_arr_str* arrstr;
};

//Lets you specify exactly which expansions you on 'in', and you either get a 'char*' or a 'struct dyn_arr_str*'
//You're in charge of freeing it
//Returns 'arrstr' if you choose EXP_FIELD_SPLIT or EXP_PATH, else it returns 'charp'
union wordexp_ret expand(struct shell_env* cur_env, const char* restrict in, uint8_t flags);


/* __BEGIN_DECLS */

/* Bits set in the FLAGS argument to `wordexp'.  */
/* enum { */
    //WRDE_DOOFFS = (1 << 0),	/* Insert PWORDEXP->we_offs NULLs.  */
    //WRDE_APPEND = (1 << 1),	/* Append to results of a previous call.  */
    //WRDE_NOCMD = (1 << 2),	/* Don't do command substitution.  */
    //WRDE_REUSE = (1 << 3),	/* Reuse storage in PWORDEXP.  */
    //WRDE_SHOWERR = (1 << 4),	/* Don't redirect stderr to /dev/null.  */
    //WRDE_UNDEF = (1 << 5),	/* Error for expanding undefined variables.  */
    //__WRDE_FLAGS = (WRDE_DOOFFS | WRDE_APPEND | WRDE_NOCMD
	 //       | WRDE_REUSE | WRDE_SHOWERR | WRDE_UNDEF)
/* }; */

/* Structure describing a word-expansion run.  */
/*typedef */
struct wordexp_t {
   char **we_wordv;		/* List of expanded words.  */
   size_t we_wordc;		/* Count of words matched.  */
   size_t we_offs;		/* Slots to reserve in `we_wordv'.  */
};

/* Possible nonzero return values from `wordexp'.  */
/* enum { */
/* #ifdef __USE_XOPEN */
    /* //WRDE_NOSYS = -1,		/1* Never used since we support `wordexp'.  *1/ */
/* #endif */
    //WRDE_NOSPACE = 1,		/* Ran out of memory.  */
    //WRDE_BADCHAR,		/* A metachar appears in the wrong place.  */
    //WRDE_BADVAL,		/* Undefined var reference with WRDE_UNDEF.  */
    //WRDE_CMDSUB,		/* Command substitution with WRDE_NOCMD.  */
    //WRDE_SYNTAX			/* Shell syntax error.  */
/* }; */

/* Do word expansion of WORDS into PWORDEXP.  */
extern int wordexp (struct shell_env* cur_env, const char *__restrict __words,
         struct wordexp_t * restrict __pwordexp, int __flags);

/* Free the storage allocated by a `wordexp' call.  */
extern void wordfree (struct wordexp_t *__wordexp) /*__THROW*/;

/* __END_DECLS */

bool is_in_IFS(char c, const char* IFS);

#endif /* define __PSH_WORDEXP_H__  */
