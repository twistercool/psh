#ifndef __PSH_SYM_TABLE_H__
#define __PSH_SYM_TABLE_H__

#include <unistd.h>
#include <stdbool.h>
#include <sys/wait.h>
#include <dirent.h>
#include <pthread.h>
#include <fcntl.h>
#include <stdarg.h>

#include "token.h"
#include "parser.h"



// Value of existing variables that have no value (non-existing variables are NULL)
#define UNSET ((void*)0x1)



struct psh_result;

struct sym_var_entry {
   char* name;
   char* value;
   bool is_env;
   bool is_readonly;
   /*is_alias?, is_readonly, etc..*/
};

enum func_type {
   FUNC_NORMAL = 0,
   FUNC_BUILTIN,
   FUNC_SPECIAL_BUILTIN,
};

struct sym_func_entry {
   char* name;
   enum func_type func_type;
   union {
      //If it's a builtin/special builtin
      int (*func)(struct shell_env*, struct dyn_arr_str*, ...);
      //If it's a shell function
      struct psh_node* node_func;
   };
};

struct sym_alias_entry {
   char* name;
   char* cmd;
};

enum sym_node_type {
   SYM_NODE_VAR,
   SYM_NODE_FUNC,
   SYM_NODE_ALIAS,
};

struct sym_node {
   union {
      struct sym_var_entry* var_entry;
      struct sym_func_entry* func_entry;
      struct sym_alias_entry* alias_entry;
   };
   struct sym_node* next_sym_node;
   enum sym_node_type type;
};

struct sym_table {
   /* uint64_t (*hash_func)(char*, uint64_t); */
   size_t nb_sym_entries;
   size_t nb_sym_nodes;
   struct sym_node** sym_node_arr;
   /* pthread_mutex */ //needs to be thread-safe (currently single-threaded though)
};

struct sym_table* sym_table_create(size_t size);
enum psh_error sym_var_insert(struct sym_table* in_table, const char* name, const char* value, bool is_env);
struct sym_var_entry* sym_get_var_entry(const struct sym_table* in_table, const char* in_name);
const char* sym_get_value(const struct sym_table* in_table, const char* in_name);
enum psh_error sym_var_delete(struct sym_table* in_table, const char* in_name);

struct sym_func_entry* sym_get_func_entry(const struct sym_table* in_table, const char* in_name);
enum psh_error sym_func_insert(struct sym_table* in_table, const char* name, enum func_type func_type, ...);
void sym_func_delete(struct sym_table* in_table, const char* in_name);

struct sym_alias_entry* sym_get_alias_entry(const struct sym_table* in_table, const char* in_alias);
char* sym_get_alias_value(const struct sym_table* in_table, const char* in_name);
enum psh_error sym_alias_insert(struct sym_table* in_table, const char* name, const char* alias);
void sym_alias_delete(struct sym_table* in_table, const char* in_name);

struct dyn_arr_str* sym_export_all_variables(struct sym_table* in_table);
struct dyn_arr_str* sym_export_env(struct sym_table* in_table);
struct dyn_arr_str* sym_export_aliases(struct sym_table* in_table);

void sym_node_free(struct sym_node* in_node);
void sym_table_empty(struct sym_table* in_table);
void sym_table_free(struct sym_table* in_table);

#endif //__PSH_SYM_TABLE_H__
