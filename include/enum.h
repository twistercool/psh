#ifndef __PSH_ENUM_H__
#define __PSH_ENUM_H__

typedef unsigned int uint;

enum psh_error {
   PSH_SUCCESS = 0,
   PSH_ERROR,
   PSH_MEMORY_ERROR,
};

enum token_type {
   TOKTYPE_UNKNOWN = 0,
   TOKTYPE_EOF,
   TOKTYPE_TOKEN,

   TOKTYPE_WORD,
   TOKTYPE_ASSIGNMENT_WORD,
   TOKTYPE_NAME,
   TOKTYPE_IO_NUMBER,

   // Fake token that simply means the current token is to be parsed as an operator
   TOKTYPE_OPERATOR,

   // Don't change the order of oeprators without also changing it in 'static char* operator[]'!
   // Control operators
   TOKTYPE_AND, // &
   TOKTYPE_SEMICOLON, // ;
   TOKTYPE_PIPE, // |
   TOKTYPE_LPAREN, // (
   TOKTYPE_RPAREN, // )
   TOKTYPE_NEWLINE, // \n
   TOKTYPE_AND_IF, // &&
   TOKTYPE_OR_IF, // ||
   TOKTYPE_DSEMI, // ;;

   // Redirection operators
   TOKTYPE_LESSTHAN, // <
   TOKTYPE_GREATTHAN, // >
   TOKTYPE_DLESS, // <<
   TOKTYPE_DGREAT, // >>
   TOKTYPE_LESSAND, // <&
   TOKTYPE_GREATAND, // >&
   TOKTYPE_LESSGREAT, // <>
   TOKTYPE_DLESSDASH, // <<-
   TOKTYPE_CLOBBER, // >|

   // Reserved words
   TOKTYPE_If,
   TOKTYPE_Then,
   TOKTYPE_Else,
   TOKTYPE_Elif,
   TOKTYPE_Fi,
   TOKTYPE_Do,
   TOKTYPE_Done,
   TOKTYPE_Case,
   TOKTYPE_Esac,
   TOKTYPE_While,
   TOKTYPE_Until,
   TOKTYPE_For,
   TOKTYPE_Lbrace,
   TOKTYPE_Rbrace,
   TOKTYPE_Bang,
   TOKTYPE_In,
};

/*
   * Don't change the order of those without also changing it in 'enum token_type'!
*/
static const char* const operators[] = {
// Control operators
   "&",
   ";",
   "|",
   "(",
   ")",
   "\n",
   "&&",
   "||",
   ";;",

// Redirection operators
   "<",
   ">",
   "<<",
   ">>",
   "<&",
   ">&",
   "<>",
   "<<-",
   ">|",
};

#endif // __PSH_ENUM_H__
