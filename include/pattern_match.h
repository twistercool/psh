#ifndef __PSH_PATTERN_MATCH_H__
#define __PSH_PATTERN_MATCH_H__

#include <regex.h>

#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#include "struct.h"

bool is_pattern_match(const char* restrict pat, const char* restrict str);

struct psh_string* next_word_in_path(const char* in);

struct psh_string* remove_max_suffix(const char* str, const char* pat);
struct psh_string* remove_min_suffix(const char* str, const char* pat);
struct psh_string* remove_max_prefix(const char* str, const char* pat);
struct psh_string* remove_min_prefix(const char* str, const char* pat);
#endif // __PSH_PATTERN_MATCH_H__
