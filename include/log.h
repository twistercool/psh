#ifndef __PSH_LOG_H__
#define __PSH_LOG_H__

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

extern enum log_level {
   LOG_NONE = 0,
   LOG_INFO, //?
   LOG_TRACE,
   LOG_DEBUG,
   LOG_WARNING,
   LOG_ERROR,
   LOG_FATAL
} __log_level;
extern bool __log_is_init;

void psh_log_init(void);

#ifdef PSH_DEBUG
#define PSH_LOG(lvl, fmt, ...) do { \
   if (lvl >= __log_level) { \
      const char* log_str; \
      switch (lvl) { \
         case LOG_NONE:    log_str = "NONE";    break; \
         case LOG_INFO:    log_str = "INFO";    break; \
         case LOG_TRACE:   log_str = "TRACE";   break; \
         case LOG_DEBUG:   log_str = "DEBUG";   break; \
         case LOG_WARNING: log_str = "WARNING"; break; \
         case LOG_ERROR:   log_str = "ERROR";   break; \
         case LOG_FATAL:   log_str = "FATAL";   break; \
         default: assert(0); \
      } \
      fprintf(stderr, "[%s]: ", log_str); \
      fprintf(stderr, fmt __VA_OPT__(,) __VA_ARGS__); \
   } \
} while(0)
#else
#define PSH_LOG(lvl, str, ...) do {} while(0)
#endif // PSH_DEBUG

#endif // __PSH_LOG_H__
