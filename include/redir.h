#ifndef __PSH_REDIR_H__
#define __PSH_REDIR_H__

#include <unistd.h>
#include <stdbool.h>
#include <fcntl.h>

#include "parser.h"


#define MAX_FD 1024

//TODO temporary solution
enum redirect_output {
   REDIR_NONE = 0,
   REDIR_OUT,
   REDIR_IN,
   REDIR_BOTH,
};

enum redirect_output perform_redirections(struct dyn_arr_node* io_redirects, int fd_in, int fd_out);
int* save_fds(void);
void restore_fds(int* arr_fd);


#endif // __PSH_REDIR_H__
