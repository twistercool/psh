#ifndef __PSH_SHELL_ERROR_H__
#define __PSH_SHELL_ERROR_H__

#include <stdlib.h>

#include "struct.h"

enum jmp_err {
   JMP_ERR_SYNTAX = 1,
   //...
   JMP_ERR_EXPANSION,
   JMP_ERR_CMD_NOT_FOUND,
};

enum psh_err_type {
   PSH_ERR_SYNTAX = 1,
   //...
   PSH_ERR_EXPANSION,
   PSH_ERR_CMD_NOT_FOUND,
};

struct psh_error_struct {
   const char* msg;
   size_t line;
   enum psh_err_type err_type;
};


/* struct optional_node { */
   //node can have magic values with 
   /* struct psh_node* node; */
/* }; */

/* bool is_node_err(struct optional_node ) { */
   
/* } */

/* struct */ 


void shell_language_syntax_error(struct shell_env* cur_env);

void special_builtin_error();
void special_builtin_redirection_error();

void normal_builtin_error();
void normal_builtin_redirection_error();

void compound_command_redirection_error();

void function_redirection_error();

void variable_assignment_error();

void expansion_error(struct shell_env* cur_env, size_t line_no, const char* const msg);

void command_not_found_error(struct shell_env* cur_env, size_t line_no, const char* const cmd_name);


#endif //__PSH_SHELL_ERROR_H__
