#ifndef __PSH_JOB_CTL_H__
#define __PSH_JOB_CTL_H__

#include <sys/wait.h>

#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>

#include "struct.h"
#include "log.h"

struct dyn_arr_job {
   struct job** jobs;
   size_t size;
   size_t capacity;
};

enum job_state {
   JOB_STATE_RUNNING,
   JOB_STATE_DONE, /* (exit status) */
   JOB_STATE_STOPPED, /* Normal, (SIGTSTP), (SIGSTOP), (SIGTTIN), (SIGTTOU) */
};

enum job_current {
   JOB_CUR_DEFAULT,
   JOB_CUR_NEXT,
   JOB_CUR_REST,
};

struct job {
   char* in_command;
   size_t job_nb;
   pid_t pid; //pid of the leader? not sure
   pid_t gpid; //group pid
   enum job_state state;
   int extra_state; //is process exit status if JOB_DONE or the signal number for JOB_STOPPED
   enum job_current current;
   bool is_foreground;
   bool finished_wait;
};

struct job* job_create_fg(struct dyn_arr_job* arr, const char* cmd, pid_t pid);
struct job* job_create_bg(struct dyn_arr_job* arr, const char* cmd, pid_t pid);
void job_free(struct job* job);
void job_delete(struct dyn_arr_job* arr, struct job* job);
int job_wait_fg(struct shell_env* cur_env, struct job* job);

void psh_block_signals(struct shell_env* cur_env);
void psh_unblock_signals(struct shell_env* cur_env);

void check_and_exec_traps(struct shell_env* cur_env);

#endif // __PSH_JOB_CTL_H__
