VERSION = 0.2
INCS = -Iinclude
LIBS = -lm
CFLAGS += -std=gnu17 ${INCS} -march=native -O2 -Wall -Wpedantic -Wextra -Werror -Wno-unused-parameter -Wno-unused-variable -Wno-unused-but-set-variable -pipe -fasynchronous-unwind-tables
DEBUG_CFLAGS += -std=gnu17 ${INCS} -Wall -Wpedantic -Wextra -Wno-unused-parameter -Wno-unused-variable -Wno-unused-but-set-variable -DPSH_DEBUG -O0 -g -ggdb -pipe -fasynchronous-unwind-tables -fsanitize=undefined
LDFLAGS += ${LIBS} -flto
DEBUG_LDFLAGS += ${LIBS}
TEST_LDFLAGS += ${DEBUG_LDFLAGS} -lcriterion
CC ?= gcc
STRIP ?= strip

SRC = $(wildcard src/*.c)
HEADERS = $(wildcard include/*.h)
release_OBJ = $(patsubst src/%.c, target/release/%.o, ${SRC})
debug_OBJ = $(patsubst src/%.c, target/debug/%.o, ${SRC})
SRC_TEST = tests/main.c
OBJ_TEST = ${SRC_TEST:.c=.o}

target/release/%.o: src/%.c ${HEADERS}
	@mkdir -p target/release
	${CC} ${CFLAGS} ${LDFLAGS} -c $< -o $@

target/debug/%.o: src/%.c ${HEADERS}
	@mkdir -p target/debug
	${CC} ${DEBUG_CFLAGS} -c $< -o $@

target/release/psh: ${release_OBJ}
	${CC} $^ ${CFLAGS} ${LDFLAGS} -o $@
	${STRIP} $@

target/debug/psh: ${debug_OBJ}
	${CC} $^ ${DEBUG_CFLAGS} ${DEBUG_LDFLAGS} -o $@

tests/test: $(filter-out %/main.o, ${debug_OBJ}) ${SRC_TEST} target/debug/psh
	${CC} $(filter-out target/debug/psh, $^) ${DEBUG_CFLAGS} ${TEST_LDFLAGS} -o tests/test
	@echo -e '--RUNNING TESTS--'
	tests/main.sh 2>&1 | sed 's/psh: \(.*:.*\)/\1/g' || true
	@./tests/test -j0 2>&1 | sed 's/^\[----\] \(.*:.*:.*\)/\1/g' | sed 's/^boxfort-worker: //g' || true

clean:
	@echo -e '--CLEANING--'
	rm -f target/release/* target/debug/* ${OBJ_TEST}

all: target/release/psh

release: target/release/psh

debug: target/debug/psh

test: tests/test

.PHONY: all release debug tests/test test clean
